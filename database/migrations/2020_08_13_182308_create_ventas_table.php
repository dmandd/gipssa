<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVentasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ventas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('development_id');
            $table->unsignedBigInteger('client_id');
            $table->unsignedBigInteger('lote_id');
            $table->unsignedBigInteger('quotation_id');
            $table->string('total')->nullable();
            $table->string('status')->default(0);
            $table->timestamps();

            $table->foreign('development_id')->references('id')->on('developments');
            $table->foreign('client_id')->references('id')->on('clients');
            $table->foreign('lote_id')->references('id')->on('lotes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ventas');
    }
}
