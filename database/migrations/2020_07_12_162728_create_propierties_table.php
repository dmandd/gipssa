<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePropiertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('propierties', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('admin_id');
            $table->unsignedBigInteger('contact_id');
            $table->integer('outstanding')->default(0);
            $table->string('code')->nullable();
            $table->string('cover');
            $table->string('name');
            $table->string('type');
            $table->unsignedBigInteger('zone_id');
            $table->unsignedBigInteger('city_id');
            $table->string('operation');
            $table->string('location')->nullable();
            $table->string('colony')->nullable();
            $table->string('stret')->nullable();
            $table->string('terrain_m2')->nullable();
            $table->string('front_m2')->nullable();
            $table->string('depth_m2')->nullable();
            $table->string('build_m2')->nullable();
            $table->string('other_m2')->nullable();
            $table->string('level')->nullable();
            $table->string('bedrooms')->nullable();
            $table->string('bathrooms')->nullable();
            $table->string('first_price')->nullable();
            $table->string('last_price')->nullable();
            $table->string('other_price')->nullable();
            $table->string('status')->default('draft');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('propierties');
    }
}
