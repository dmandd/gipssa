<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdatePropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('propierties', function($table){
            $table->unsignedBigInteger('tag_id');
            $table->string('address')->nullable();
            $table->string('internal_number')->nullable();
            $table->string('external_number')->nullable();
            $table->string('accross')->nullable();
            $table->string('year')->nullable();
            $table->string('update')->nullable();
            $table->string('usage')->nullable();
            $table->string('paper')->nullable();
            $table->string('plano_catastral')->nullable();
            $table->string('cedula_catastral')->nullable();
            $table->string('libertad_gravamen')->nullable();
            $table->string('hipoteca')->nullable();
            $table->integer('contrato_renta')->nullable();
            $table->string('deuda')->nullable();
            $table->integer('habitable')->nullable();
            $table->string('habitable_porc')->nullable();
            $table->string('habitable_notes')->nullable();
            $table->string('delivery_time')->nullable();
            $table->string('accepted_credit')->nullable();
            $table->string('maintenance_quote')->nullable();
            $table->string('extra_quote')->nullable();
            $table->string('other_quote')->nullable();
            $table->string('comission_type')->nullable();
            $table->string('comission')->nullable();
            $table->string('comission_share')->nullable();
            $table->string('comission_note')->nullable();
            $table->string('enganche_type')->nullable();
            $table->string('enganche')->nullable();
            $table->string('enganche_note')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
