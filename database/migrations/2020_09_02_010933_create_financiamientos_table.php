<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFinanciamientosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('financiamientos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('development_id');
            $table->string('nombre');
            $table->string('tipo');
            $table->float('porcentaje_descuento')->default(0);
            $table->float('porcentaje_interes')->default(0);
            $table->string('tipo_enganche');
            $table->float('enganche');
            $table->float('enganche_diferido')->default(0);
            $table->integer('enganche_num_pagos')->default(0);
            $table->string('tipo_mensualidad')->nullable();
            $table->float('mensualidad')->default(0);
            $table->integer('mensualidad_num_pagos')->default(0);
            $table->string('tipo_saldo')->nullable();
            $table->float('saldo')->default(0);
            $table->string('status')->default('visible');
            $table->timestamps();

            $table->foreign('development_id')->references('id')->on('developments');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('financiamientos');
    }
}
