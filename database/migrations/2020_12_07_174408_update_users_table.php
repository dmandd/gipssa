<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('admins', function(Blueprint $table){
            $table->string('comision')->nullable();
            $table->date('birth_date')->nullable();
            $table->string('celular')->nullable();
            $table->string('direccion')->nullable();
            $table->string('lugar_nacimiento')->nullable();
            $table->string('identificacion')->nullable();
            $table->string('licencia_manejo')->nullable();
            $table->string('poliza_seguro')->nullable();
            $table->string('documento_permiso')->nullable();
            $table->string('curp')->nullable();
            $table->string('rfc')->nullable();
            $table->string('numero_seguridad')->nullable();
            $table->string('estado_salud')->nullable();
            $table->longText('enfermedad_cronica')->nullable();
            $table->string('estatura')->nullable();
            $table->string('peso')->nullable();
            $table->string('tipo_sangre')->nullable();
            $table->string('alergia')->nullable();
            $table->string('enfermedades')->nullable();
            $table->string('contacto')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
