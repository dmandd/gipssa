<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateProspectsTableAddAvance extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('prospects', function(Blueprint $table){
            $table->string('avance')->default('rojo');
            $table->string('desarrollo')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('prospects', function(Blueprint $table){
            $table->dropColumn('avance');
            $table->dropColumn('desarrollo');
        });
    }
}
