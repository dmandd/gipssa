<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignPropierties extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('propierties', function(Blueprint $table){
            $table->foreign('admin_id')->references('id')->on('admins');
            $table->foreign('contact_id')->references('id')->on('contacts');
            $table->foreign('zone_id')->references('id')->on('zones');
            $table->foreign('city_id')->references('id')->on('cities');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
