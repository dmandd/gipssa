<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDevelopmentClientDocumentationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('development_client_documentations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('development_id');
            $table->string('name');
            $table->string('slug');
            $table->string('status')->default('visible');
            $table->timestamps();

            $table->foreign('development_id')->references('id')->on('developments')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('development_client_documentations');
    }
}
