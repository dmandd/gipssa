<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdatePropiertiesAddMontoHipoteca extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('propierties', function(Blueprint $table){
            $table->string('monto_hipoteca')->nullable();
            $table->longText('habitable_notes')->change();
            $table->longText('comission_note')->change();
            $table->longText('enganche_note')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
