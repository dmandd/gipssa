import Swal from 'sweetalert2';
import { show, hide, proceed } from '../loader';

export default(function(){
    let _inputs = document.querySelectorAll('input[name="row[]"]');
    Array.from(_inputs).forEach(input => {
        input.addEventListener('click', e => {
            if (document.querySelectorAll('input[name="row[]"]:checked').length > 1) {
                $('#bin-delete').fadeIn();
                $('#bin-assign').fadeIn();
            }else{
                $('#bin-delete').fadeOut();
                $('#bin-assign').fadeOut();
            }
        });
    });
    
    if(document.getElementById('bin-delete')){
        let btn = document.getElementById('bin-delete');
        btn.addEventListener('click', e => {
            let to_delete = document.querySelectorAll('input[name="row[]"]:checked');
            Swal.fire({
                title: '¿Está seguro?',
                text: "Se eliminarán los registros seleccionados",
                icon: "warning",
                showCancelButton: true,
                confirmButtonText: "Sí, confirmar",
                cancelButtonText: "No, cancelar",
            }).then((result) => {
                if(result.value){
                    //Por cada input haremos una petición para eliminar
                    show();
                    Array.from(to_delete).forEach(item => {
                        if(document.getElementById('delete-'+item.value)){
                            axios.delete(document.getElementById('delete-'+item.value).dataset.route)
                            .then(response => {
                                if((response.status == 200) && (response.data.success)){
                                }else{
                                    Swal.fire('¡Ups!', 'Lo sentimos, algo salió mal', 'error');
                                }
                            })
                            .catch(err => {
                                Swal.fire('¡Ups!', 'Lo sentimos, algo salió mal', 'error');
                                throw new Error('Something went wrong', err);
                            });
                        }
                    });
                    // hide();
                    location.reload();
                }
            })
        });
    }

    if(document.getElementById('btn-asignar')){
        let btn_asignar = document.getElementById('btn-asignar');
        btn_asignar.addEventListener('click', e => {
            let to_assign = document.querySelectorAll('input[name="row[]"]:checked');
            Swal.fire({
                title: '¿Está seguro?',
                text: "Se asignarán los registros seleccionados",
                icon: "warning",
                showCancelButton: true,
                confirmButtonText: "Sí, confirmar",
                cancelButtonText: "No, cancelar",
            }).then((result) => {
                if(result.value){
                    //Por cada input haremos una petición para eliminar
                    show();
                    Array.from(to_assign).forEach(item => {
                        if(document.getElementById('assign-'+item.value)){
                            axios.put(document.getElementById('assign-'+item.value).value, {
                                admin_id: document.getElementById('admin_id').value
                            })
                            .then(response => {
                                if((response.status == 200) && (response.data.success)){
                                }else{
                                    Swal.fire('¡Ups!', 'Lo sentimos, algo salió mal', 'error');
                                }
                            })
                            .catch(err => {
                                Swal.fire('¡Ups!', 'Lo sentimos, algo salió mal', 'error');
                                throw new Error('Something went wrong', err);
                            });
                        }
                    });
                    location.reload();
                }
            })
        });
    };
})();