import LoaderRod from '../loader-rod';
import { sortedLastIndexOf } from 'lodash';
import tippy from 'tippy.js';
import 'tippy.js/dist/tippy.css'; // optional for styling
import 'tippy.js/animations/scale.css';

const numeral = require('numeral');

export default (function(){
    let current_lote;
    let current_plazo;
    let chepina = document.getElementById('chepina');
    let plazo = document.getElementById('plazo');
    let porc_enganche = document.getElementById('porc_enganche');
    let monto_enganche = document.getElementById('monto_enganche');
    let financiamiento = document.getElementById('financiamiento');
    let financiamiento_show = document.getElementById('financiamiento_show');
    let pago_mensual = document.getElementById('pago_mensual');
    let saldo = document.getElementById('saldo');
    let precio_final = document.getElementById('precio_final');
    let price = 0;

    LoaderRod.show();
    window.onload = (e) => {
        lotes.map(lote => {
			if(document.getElementById(lote.id)){
				let rect = document.getElementById(lote.id);
                rect.classList.add(lote.status);
                tippy('[data-tippy-content]');
                //Agregamos listener al rect
                rect.addEventListener('click', e => {
                    //Reset Form
                    console.log(lote);
                    if(lote.status == 'disponible'){
                        rect.dataset.toggle = 'modal';
                        rect.dataset.target = '#modal-form';
                        if(document.getElementById('lote_id'))
                            document.getElementById('lote_id').value = lote.id;
                        if(document.getElementById('lote-name'))
                            document.getElementById('lote-name').innerHTML = `<h3>${lote.name}</h3>`;
                        if(document.getElementById('chepina'))
                            document.getElementById('chepina').setAttribute('src', `https://crm.gipssa.com/${lote.chepina}`);
                        current_lote = lote;
                        if(plazo){
                            if(plazo.options[plazo.selectedIndex]){
                                current_plazo = plazo.options[plazo.selectedIndex].dataset.info;
                                updateQuotation();
                            }
                        }
                    }
                });
			}
        });
        //Listener cuando cambia el formulario
        if(plazo){
            plazo.addEventListener('change', e => {
                current_plazo = plazo.options[plazo.selectedIndex].dataset.info;
                updateQuotation();
            });
        }
        LoaderRod.destroy();
    }

    const updateQuotation = () => {
        if(!(plazo.options[plazo.selectedIndex].dataset.info)){
            return;
        }
        current_plazo = JSON.parse(plazo.options[plazo.selectedIndex].dataset.info);
        price = parseFloat(current_lote.price);
        //Calculamos el interés
        if(current_plazo.porcentaje_interes > 0)
            price += parseFloat(current_lote.price) * (parseFloat(current_plazo.porcentaje_interes) / 100);
        //Calculamos el descuento
        if(current_plazo.porcentaje_descuento){
            if(current_plazo.tipo_descuento == 'porcentaje'){
                price -= parseFloat(current_lote.price) * (parseFloat(current_plazo.porcentaje_descuento) / 100);
            }else{
                price = parseFloat(current_lote.price) - parseFloat(current_plazo.porcentaje_descuento);
            }
        }

        //Calculamos el monto del enganche
        monto_enganche.value = calculateEnganche(current_plazo);

        /**
         * TODO:
         * Actualizar si el plan es de contado 
         */
        if(current_plazo.tipo == 'contado'){
            document.getElementById('financiamiento-group').classList.add('d-none');
            document.getElementById('pago-mensual-group').classList.add('d-none');
        }else{
            document.getElementById('financiamiento-group').classList.remove('d-none');
            document.getElementById('pago-mensual-group').classList.remove('d-none');
        }

        //Calculamos el financiamiento
        financiamiento.value = calculateFinanciamiento(current_plazo);
        financiamiento_show.value = numeral(financiamiento.value).format('$0,0.00');
        //Actualizamos los meses de financiamiento
        mensualidad.value = current_plazo.mensualidad_num_pagos;
        pago_mensual.value = calculateMensualidad(current_plazo);

        //Calculamos el saldo
        saldo.value = calculateSaldo(current_plazo);
        //Actualizamos el precio
        precio_final.value = numeral(price).format('$0,0.00');
    }

    const calculateEnganche = (plazo) => {
        let enganche;
        if(plazo.tipo_enganche == 'porcentaje'){
            porc_enganche.value = plazo.enganche+'%';
            enganche = price * (plazo.enganche / 100);
        }else{
            document.getElementById('enganche-group').classList.toggle('d-none');
            enganche = plazo.enganche;
        }
        // TODO:
        // Mostrar los pagos diferidos para el enganche
        if(plazo.enganche_num_pagos != 0){
            let elms = document.querySelectorAll('.enganche-diferido');   
            Array.from(elms).forEach(el => {
                el.classList.remove('d-none');
            });
            document.getElementById('enganche_num_pagos').value = plazo.enganche_num_pagos;
            document.getElementById('pago_mensual_enganche').value = numeral((enganche / plazo.enganche_num_pagos)).format('$0,0.00');
        }else{
            let elms = document.querySelectorAll('.enganche-diferido');   
            Array.from(elms).forEach(el => {
                el.classList.add('d-none');
            });
        }
        return numeral(enganche).format('$0,0.00');
    }

    const calculateFinanciamiento = (plazo) => {
        if(plazo.tipo == 'financiamiento'){
            if(plazo.tipo_mensualidad == 'porcentaje'){
                return price * (plazo.mensualidad / 100);
            }else{
                return plazo.mensualidad;
            }
        }else{
            return '-';
        }
    }

    const calculateMensualidad = (plazo) => {
        if(plazo.tipo == 'financiamiento'){
            if(plazo.mensualidad_num_pagos > 0){
                if(plazo.tipo_mensualidad == 'porcentaje'){
                    return numeral(((price * (plazo.mensualidad / 100)) / plazo.mensualidad_num_pagos)).format('$0,0.00');
                }else{
                    return numeral((plazo.mensualidad / plazo.mensualidad_num_pagos)).format('$0,0.00');
                }
            }else{
                console.error('Invalid mensualidad_num_pagos division by zero');
            }
        }else{
            return '-';
        }
    }

    const calculateSaldo = (plazo) => {
        if(plazo.saldo > 0){
            if(plazo.tipo_saldo == 'porcentaje'){
                return numeral((price * (plazo.saldo / 100))).format('$0,0.00');
            }else{
                return numeral(plazo.saldo).format('$0,0.00');
            }
        }else{
            return numeral(0).format('$0,0.00');
        }
    }
})();