import ClipboardJS from 'clipboard';
import Swal from 'sweetalert2';

export default(() => {
    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        onOpen: (toast) => {
          toast.addEventListener('mouseenter', Swal.stopTimer)
          toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
    });
    
    var clipboard = new ClipboardJS('.btn-to-clipboard');
     
    clipboard.on('success', function(e) {
        Toast.fire({
            icon: 'success',
            title: 'Copiado al portapapeles'
        })
    });
})();

