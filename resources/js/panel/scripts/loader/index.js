import {Spinner} from 'spin.js';
import Swal from 'sweetalert2';

require('spin.js/spin.css');
var spinner;
const Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    onOpen: (toast) => {
      toast.addEventListener('mouseenter', Swal.stopTimer)
      toast.addEventListener('mouseleave', Swal.resumeTimer)
    }
});
const opts = {
    lines: 13, // The number of lines to draw
    length: 1, // The length of each line
    width: 17, // The line thickness
    radius: 45, // The radius of the inner circle
    scale: 0.4, // Scales overall size of the spinner
    corners: 1, // Corner roundness (0..1)
    color: '#000', // CSS color or array of colors
    fadeColor: 'transparent', // CSS color or array of colors
    speed: 1, // Rounds per second
    rotate: 0, // The rotation offset
    animation: 'spinner-line-fade-quick', // The CSS animation name for the lines
    direction: 1, // 1: clockwise, -1: counterclockwise
    zIndex: 2e9, // The z-index (defaults to 2000000000)
    className: 'spinner', // The CSS class to assign to the spinner
    top: '50%', // Top position relative to parent
    left: '50%', // Left position relative to parent
    shadow: '0 0 1px transparent', // Box-shadow for the lines
    position: 'absolute' // Element positioning
};
export function show(){
    spinner = new Spinner(opts).spin(document.body);
    document.body.style.pointerEvents = 'none';
}
export function hide(){
    spinner.stop();
    document.body.style.pointerEvents = 'initial';
}

export function proceed(btn){
    Toast.fire({
        icon: 'success',
        title: '¡Correcto! Operación exitosa'
    });

    if(btn.dataset.action){
        window.setTimeout(() => {
            eval(btn.dataset.action);                                            
        }, 1000);
    }else{
        window.setTimeout(() => {
            location.reload();
        }, 200);
    }
}


