import tippy from 'tippy.js';
import 'tippy.js/dist/tippy.css'; // optional for styling
import 'tippy.js/animations/scale.css';
import LoaderRod from '../loader-rod';
import Toast from '../Toast';

let elms = document.querySelectorAll('path, ellipse, rect');
let current_element;

Array.from(elms).forEach(el => {
    if(!el.dataset.tippyContent){
        el.dataset.tippyContentPrev = 'true';
        tippy('[data-tippy-content-prev]', {allowHTML: true, content:"Sin asignar"});
    }
    if(el.classList.contains('assigned')){
        return;
    }
    el.addEventListener('click', e => {
        current_element = el;
        $('#setLote').modal('show');
    });
});

//Eventos para escuchar el confirmar del boton del modal
if(document.getElementById('confirm-submit')){
    document.getElementById('confirm-submit').addEventListener('click', e => {
        if(document.getElementById('id_lote').value !== 'none'){
            let lote = JSON.parse(document.getElementById('id_lote').value);
            if(document.getElementById(lote.id)){
                Toast.fire({icon:'error', title:"Ups, esa unidad ya se encuentrá asignada"});
            }else{
                LoaderRod.show();
                current_element.setAttribute('id', lote.id);
                current_element.classList.add('assigned');
                current_element.dataset.tippyContent = lote.name;
                Toast.fire({icon:'success', title:"¡Correcto! Unidad asignada"});
            }
        }else{
            current_element.removeAttribute('id');
            current_element.dataset.tippyContent = "";
            current_element.removeAttribute('data-tippy-content');
            current_element.classList.remove('assigned');
            current_element.dataset.tippyContentPrev = 'true';
            tippy('[data-tippy-content-prev]', {allowHTML: true, content:"Sin asignar"});
        }
        let instance = current_element._tippy;
        if(instance)
            instance.destroy();
        tippy('[data-tippy-content]', {allowHTML: true,});
        LoaderRod.destroy();
        $('#setLote').modal('hide');
    });
}

//listener para el submit
if(document.getElementById('form-update')){
    document.getElementById('form-update').addEventListener('submit', e =>{
        e.preventDefault();
        document.getElementById('svg_edited').value = document.getElementById('drawing').innerHTML;
        document.getElementById('form-update').submit();
    });
}