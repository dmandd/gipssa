let rects = {};
function resizeSvg(draw){
    if(document.getElementById('width') && document.getElementById('height')){
        let dom_width = document.getElementById('width');
        let dom_height = document.getElementById('height');

        dom_width.addEventListener('change', e => {
            draw.size(dom_width.value, dom_height.value);
        });
        dom_height.addEventListener('change', e => {
            draw.size(dom_width.value, dom_height.value);
        });
    }
}
function drawRect(draw, SVG){
    let xi = 0; //Stands for initial x
    let yi = 0; //Stands for initial y

    let xf = 0; //Stands for final x
    let yf = 0; // Stands for final y
    //Listener al hacer click dentro del svg
    draw.on('mousedown', (e) => {
        if(document.getElementById('polygons')){
            if(document.getElementById('polygons').checked){
                //anotamos 
                xi = e.x;
                yi = e.y;
        
                console.log('xi: ', xi);
                console.log('yi: ', yi);
            }
        }
    });

    draw.on('mouseup', (e) => {
        if(document.getElementById('polygons')){
            if(document.getElementById('polygons').checked){
                xf = e.x;
                yf = e.y;
                let rect = draw.rect((Math.abs(xf - xi)), (Math.abs(yf - yi))).move(50, 50).attr({fill: '#ff00bb'});
                resizeRect(rect, SVG);
                document.getElementById('polygons').checked = false;
            }
        }
    });
}

function resizeRect(rect, SVG){
    rect.on('mousedown', () => {
        rect.selectize(true,{
            points: ['lt', 'rt', 'lb', 'rb'],
            pointSize: 9,
            strokeSize: 3,
            rotationPoint: false
        }).resize({saveAspectRatio:true});
        onSelectPanel(rect, SVG);
    });

    rect.on('mouseout', function() {
        rect.selectize(false);
    });
}

function onSelectPanel(el, SVG) {
    let parentEl = el;
    if(!SVG.get('#'+el.node.id+'-panel')) {
      let panel = draw.rect(120, 32)
        .attr({
          fill: "#ececec",
          stroke: '#333',
          strokeWidth: 1,
          id: el.node.id+'-panel'
        })
        .move(el.rbox(draw).x, el.rbox(draw).y+el.rbox(draw).h+8);

      panel.on('mouseup', function(e) {
        deselectPanel(parentEl);
      });
    }
} 
export { resizeSvg, drawRect };

