import tippy from 'tippy.js';
import 'tippy.js/dist/tippy.css'; // optional for styling
import 'tippy.js/animations/scale.css';

export default (function(){
    tippy('[data-tippy-content]', {allowHTML: true,});
})();