require('bootstrap-4-autocomplete');

// export default (() => {
    // console.log(autocomplete_options);
if(autocomplete_options){
    let elms = document.querySelectorAll('.autocomplete');
    Array.from(elms).forEach(el => {
        $(`[data-autocomplete-name='${el.dataset.autocompleteName}']`).autocomplete({
            source: JSON.parse(el.dataset.autocomplete),
            treshold: 1, 
            onSelectItem: (selected, textfield) => {
                // console.log(selected);
                // console.log(textfield);
                let obj = JSON.parse(el.dataset.autocomplete)[selected.label];
                document.getElementById('contact[name]').value = obj.name;
                document.getElementById('contact[type]').value = obj.type;
                document.getElementById('contact[phone]').value = obj.phone;
                document.getElementById('contact[email]').value = obj.email;
                document.getElementById('contact[address]').value = obj.address;
                document.getElementById('contact_id').value = obj.id;
            }
        });

        if(document.querySelector('[data-autocomplete-client]')){
            $(`[data-autocomplete-client='name']`).autocomplete({
                source: JSON.parse(el.dataset.autocomplete),
                treshold: 1, 
                onSelectItem: (selected, textfield) => {
                    let obj = JSON.parse(el.dataset.autocomplete)[selected.label];
                    document.getElementById('name').value = obj.name;
                    document.getElementById('surname').value = obj.surname;
                    document.getElementById('email').value = obj.email;
                    document.getElementById('phone').value = obj.phone;
                    document.getElementById('client_id').value = obj.id;
                    document.getElementById('lugar_nacimiento').value = obj.lugar_nacimiento;
                    document.getElementById('fecha_nacimiento').value = obj.fecha_nacimiento;
                    document.getElementById('ocupation').value = obj.ocupation;
                    document.getElementById('address').value = obj.address;
                    document.getElementById('municipio').value = obj.municipio;
                    document.getElementById('estado').value = obj.estado;
                    document.getElementById('cp').value = obj.cp;
                }
            });
        }

    });
    // let elms_dos = document.querySelectorAll('.autocomplete.email');
    // Array.from(elms_dos).forEach(el => {
    //     console.log(el.dataset.autocomplete);
    //     $(`[data-autocomplete-name='${el.dataset.autocompleteName}']`).autocomplete({
    //         source: JSON.parse(el.dataset.autocomplete),
    //         treshold: 1, 
    //         onSelectItem: (selected, textfield) => {
    //             // console.log(selected);
    //             // console.log(textfield);
    //             let obj = JSON.parse(el.dataset.autocomplete)[selected.label];
    //             document.getElementById('contact[name]').value = obj.name;
    //             document.getElementById('contact[type]').value = obj.type;
    //             document.getElementById('contact[phone]').value = obj.phone;
    //             document.getElementById('contact[email]').value = obj.email;
    //             document.getElementById('contact[address]').value = obj.address;
    //             document.getElementById('contact_id').value = obj.id;
    //         }
    //     });
    // });
    // let elms_tres = document.querySelectorAll('.autocomplete.telefono');
    // Array.from(elms_tres).forEach(el => {
    //     console.log(el.dataset.autocomplete);
    //     $(`[data-autocomplete-name='${el.dataset.autocompleteName}']`).autocomplete({
    //         source: JSON.parse(el.dataset.autocomplete),
    //         treshold: 1, 
    //         onSelectItem: (selected, textfield) => {
    //             // console.log(selected);
    //             // console.log(textfield);
    //             let obj = JSON.parse(el.dataset.autocomplete)[selected.label];
    //             document.getElementById('contact[name]').value = obj.name;
    //             document.getElementById('contact[type]').value = obj.type;
    //             document.getElementById('contact[phone]').value = obj.phone;
    //             document.getElementById('contact[email]').value = obj.email;
    //             document.getElementById('contact[address]').value = obj.address;
    //             document.getElementById('contact_id').value = obj.id;
    //         }
    //     });
    // });
}
// })();