import LoaderRod from '../../loader-rod';
LoaderRod.show();
$(document).ready(function(){
    var calendar = $('#calendar-'+calendar_id).fullCalendar('getCalendar');
    updateDateTitle(moment());
    LoaderRod.destroy();
    document.querySelector('.fullcalendar-btn-next').addEventListener('click', (e) => {
        LoaderRod.show();
        $('#calendar-'+calendar_id).fullCalendar('getCalendar').next();
        updateDateTitle($('#calendar-'+calendar_id).fullCalendar('getDate'));
        LoaderRod.destroy();
    });
    document.querySelector('.fullcalendar-btn-prev').addEventListener('click', (e) => {
        LoaderRod.show();
        $('#calendar-'+calendar_id).fullCalendar('getCalendar').prev();
        updateDateTitle($('#calendar-'+calendar_id).fullCalendar('getDate'));
        LoaderRod.destroy();
    });
    document.querySelector('.fullcalendar-btn-today').addEventListener('click', (e) => {
        LoaderRod.show();
        $('#calendar-'+calendar_id).fullCalendar('today');
        updateDateTitle($('#calendar-'+calendar_id).fullCalendar('getDate'));
        LoaderRod.destroy();
    });
    $('#calendar-'+calendar_id).fullCalendar('getCalendar').on('dayClick', function(date, jsEvent, view) {
        updateModalInfo();
        $('#new-event').modal('show');
        document.querySelector('.new-event--start').value = date.format('Y-M-DD');
    });
    $('#calendar-'+calendar_id).fullCalendar('getCalendar').on('eventClick', function(event, element) {
        //Actualizamos los datos del modal
        updateModalInfo(event);
        $('#new-event').modal('show');
    });
    // $('#calendar-'+calendar_id).fullCalendar('getCalendar').on('eventMouseover', function(event, jsEvent, view) {
    //     //Actualizamos los datos del modal
    //     console.log(event);
    //     console.log(jsEvent);
    //     console.log(view);
    // });
    // document.querySelector('.new-event--add').addEventListener('click', e => {
    //     document.getElementById('new-event-form').submit();
    // })
});

const updateDateTitle = (moment) => {
    document.querySelector('.fullcalendar-title').innerHTML = `${moment.format('MMMM')} ${moment.format('YYYY')}`;
}

const updateModalInfo = (json = {title: '',admin_id: '',contact_id: '',start: moment("09:00:00", "HH:mm:ss"),length: 30,room_id: '',description: '',color: '#f46340', method: 'POST', button: 'Agendar evento'}) => {
    if(json.action){
        document.getElementById('new-event-form').setAttribute('action', json.action);
    }else{
        document.getElementById('new-event-form').setAttribute('action', document.getElementById('new-event-form').dataset.formAction);
    }
    if(json.method == 'POST'){
        document.getElementById('_method').setAttribute('name', 'oo');
    }else{
        document.getElementById('_method').setAttribute('name', '_method');
    }
    document.getElementById('title').value = json.title;
    if(document.getElementById('admin_id'))
        document.getElementById('admin_id').value = json.admin_id;
    if(json.contact_id){
        console.log(json);
        updateSelect(json.contact_id);
    }else{
        updateSelect('');
    }
    document.getElementById('start_time').value = json.start.format('HH:mm:ss');
    if(json.end){
        document.getElementById('length').value = json.end.diff(json.start, 'minutes');
    }else{
        document.getElementById('length').value = json.length;
    }
    if(json.room_id)
        document.getElementById('room_id').value = json.room_id;
    document.getElementById('description').value = json.description;
    Array.from(document.querySelectorAll('.color-junta')).forEach(inp => {
        if(inp.value == json.color)
            inp.checked = true;
    });
    if(json.button){
        if(document.querySelector('.new-event--add'))
            document.querySelector('.new-event--add').textContent = json.button;
    }else{
        if(document.querySelector('.new-event--add'))
            document.querySelector('.new-event--add').textContent = 'Actualizar evento';
    }
    if(document.getElementById('delete-event')){
        if(json.delete){
            document.getElementById('delete-event').classList.remove('d-none');
            document.getElementById('delete-event').dataset.route = json.delete;
        }else{
            document.getElementById('delete-event').classList.add('d-none');
        }
    }

}