require('bootstrap-datepicker');
if(document.getElementById('agregar-fecha')){
    let btn = document.getElementById('agregar-fecha');
    btn.addEventListener('click', e => {
        let elms = document.querySelectorAll('.cont-fecha');
        document.getElementById('dias_pago_enganche').innerHTML += `
        <div class="col-6 mb-5 text-truncate">
            <label for="" class="form-control-label">
                <span class="text-muted">Definir Día Pago Enganche ${(elms.length + 1)}</span>
                <button type="button" aria-hidden="true" onclick="deleteFecha(this)" class="delete-fecha-enganche ml-3 close"><span aria-hidden="true">×</span></button>
            </label>
            <input type="text" name="dia_pago_enganche[]" class="form-control mydatepicker cont-fecha" required value="" autocomplete="off">
            <small><b>Formato fecha:</b> DD/MM/YYYY</small>
        </div>
        <div class="col-6 mb-5 text-truncate">
            <label for="" class="form-control-label">
                <span class="text-muted">Monto</span>
            </label>
            <input type="text" name="monto_enganche[]" class="form-control" required value="" autocomplete="off">
            <small>Ej. 5000, Ej. 5200.50</small>
        </div>
        `;

        $('.mydatepicker').datepicker({format: 'dd/mm/yyyy'});
    });
}
window.deleteFecha = function(el){
    el.parentElement.parentElement.remove();
    // $('.mydatepicker').datepicker({format: 'dd/mm/yyyy'});
}