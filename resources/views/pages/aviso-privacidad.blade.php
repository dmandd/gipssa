@extends('layouts.app')
@section('content')
<!--Page Wrapper starts-->
<div class="page-wrapper">
    <style>
        ul.list-style li{list-style: initial !important;}
    </style>
    @include('include.header')
    <div class="container mb-5 pb-5" style="padding-top:6rem;">
        <h3>Aviso de Privacidad</h3>
        <p>De acuerdo a lo establecido en la ley Federal Protección de Datos Personales", declara Valeria Solis Cervantes</b>, ser una empresa almente constituida de conformidad con las leyes mexicanas, con domicilio en CALLE 36 X 33 Y 35 Colonia San Ramon Norte C.P.97117, Mérida, Yucatán; y como responsable del tratamiento de sus datos personales, hace de su conocimiento que la información de nuestros proveedores, clientes y personal, es tratada de forma estrictamente confidencial por lo que al proporcionar sus datos personales, tales como:</p>
        <ul class="pl-5 list-style">
            <li>Nombre completo.</li>
            <li>Dirección.</li>
            <li>Registro federal de contribuyentes.</li>
            <li>Teléfonos de hogar, oficina y móviles.</li>
            <li>Correo electrónico.</li>
            <li>Curp.</li>
            <li>Acta de nacimiento.</li>
        </ul>
        <p>Estos serán utilizados única y exclusivamente para los siguientes fines:</p>
        <ul class="pl-5 list-style">
            <li>Procesos Administrativos.</li>
            <li>Estadísticas publicitarias.</li>
            <li>Campañas de fidelidad.</li>
            <li>Información y prestación de servicios.</li>
            <li>Actualización de la base de datos.</li>
            <li>Cualquier finalidad análoga o compatible con las anteriores.</li>
        </ul>
        <p>En el caso de datos sensibles, tales como:</p>
        <ul class="pl-5 list-style">
            <li>Datos financieros (Ingresos, estados de cuenta, y demás relacionados).</li>
            <li>Datos patrimoniales (Bienes materiales, inmuebles y demás relacionados).</li>
            <li>Datos personales (Cónyuge, estado civil, nacionalidad, educación, hijos y demás relacionados).</li>
            <li>Referencias familiares y no familiares (nombre, dirección, teléfono, relación, etc.)</li>
        </ul>
        <p>Estos serán utilizados única y exclusivamente para los siguientes fines:</p>
        <ul class="pl-5 list-style">
            <li>Estadísticas.</li>
            <li>Trámites ante autoridades administrativas federales, estatales, municipales, IMSS, SHCP, STPS, Instituciones financieras.</li>
            <li>Cualquier finalidad análoga o compatible con la anterior.</li>
        </ul>
        <p>Para prevenir el acceso no autorizado a sus datos personales y con el fin de asegurar que la información sea utilizada para los fines establecidos en este aviso de privacidad, hemos establecido diversos procedimientos con la finalidad de evitar el uso o divulgación no autorizados de sus datos, permitiéndonos tratarlos debidamente. Así mismo, le informamos que sus datos personales pueden ser transmitidos para ser tratados por personas distintas a esta empresa. Todos sus datos personales son tratados de acuerdo a la legislación aplicable y vigente en el país, por ello le informamos que usted tiene en todo momento los derechos (ARCO) de acceder, rectificar, cancelar u oponerse al tratamiento que le damos a sus datos personales; derecho que podrá hacer velar a través del area de privacidad encargada de la seguridad de datos personales en el Teléfono (999) 738 33 87 Valeria Solís Cervantes al Teléfono Directo (999) 261 83 05 o por medio de su correo electrónico: gipssa.desarrollos@gmail.com através de estos canales usted podrá actualizar sus datos y especificar el medio por el cual desea recibir información, ya que en caso de no contar con esta especificación de su parte, Valeria Solis Cervantes, establecerá libremente el canal que considere pertinente para enviarle información.</p>
        <p>Este aviso de privacidad podrá ser modificado por Valeria Solis Cervantes, dichas modificaciones serán oportunamente informadas a través de correo electrónico, teléfono, o cualquier otro medio de comunicación que Valeria Solis Cervantes, determine para el efecto.</p>
    </div>
</div>
<!--Page Wrapper ends-->
@include('include.footer')
@endsection