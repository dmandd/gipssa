<div class="property-item">
    <a class="property-img" target="_blank" href="{{ route('propiedades.detalle', ['id' => $property_new->id, 'slug' => $property_new->slug()]) }}"><img src="{{ $property_new->cover }}" alt="Imagen propiedad"> </a>
    <ul class="feature_text">
        @if (isset($property_new->tag()->name))
            <li class="feature_cb"><span>{{ $property_new->tag()->name }}</span></li>
        @endif
        <li class="feature_or"><span>{{ $property_new->operation() }}</span></li>
    </ul>
    <div class="property-author-wrap">
        <a target="_blank" href="{{ route('propiedades.detalle', ['id' => $property_new->id, 'slug' => $property_new->slug()]) }}" class="property-author">
            <h4 style="text-transform:uppercase;">{{ $property_new->name }}</h4>
        </a>
        <ul class="property-feature">
            @if ($property_new->bedrooms)
                <li>
                    <span>{{ $property_new->bedrooms }} Recámaras</span>
                </li>
            @endif
            @if ($property_new->bathrooms)
                <li>
                    <span>{{ $property_new->bathrooms }} Baños</span>
                </li>
            @endif
            @if ($property_new->terrain_m2)
                <li>
                    <span>{{ $property_new->terrain_m2 }} m&sup2;</span>
                </li>
            @endif

        </ul>
        <div class="featured-price">
            <p><span class="per_sale">Precio</span>${{ $property_new->first_price }}</p>
        </div>
    </div>
</div>