<div class="col-xl-4 col-md-6 col-sm-12">
    <div class="single-property-box">
        <div class="property-item">
            <a target="_blank" class="property-img" href="{{ route('propiedades.detalle', ['id' => $property_out->id, 'slug' => $property_out->slug()]) }}"><img src="{{ asset($property_out->cover) }}" alt="{{ asset('name') }}">
            </a>
            <ul class="feature_text">
                @if ($property_out->tag())
                    <li class="feature_cb"><span>{{ $property_out->tag()->name }}</span></li>
                @endif
                <li class="feature_or"><span>{{ $property_out->operation() }}</span></li>
            </ul>
            <div class="property-author-wrap">
                @if ($admin = $property_out->admin())
                    <a href="#" class="property-author">
                        <img src="{{ $admin->avatar }}" alt="Imagen asesor">
                        <span>{{ $admin->name }}</span>
                    </a>
                @endif
                {{-- <ul class="save-btn">
                    <li data-toggle="tooltip" data-placement="top" title="Photos"><a href=".photo-gallery" class="btn-gallery"><i class="lnr lnr-camera"></i></a></li>
                    <li data-toggle="tooltip" data-placement="top" title="" data-original-title="Bookmark"><a href="#"><i class="lnr lnr-heart"></i></a></li>
                    <li data-toggle="tooltip" data-placement="top" title="" data-original-title="Add to Compare"><a href="#"><i class="fas fa-arrows-alt-h"></i></a></li>
                </ul> --}}
                @if ($property_out->gallery()->count() > 0)
                    <div class="hidden photo-gallery">
                        @foreach ($property_out->gallery() as $item)
                            <a href="{{ $item->image }}"></a>
                        @endforeach
                    </div>
                @endif
            </div>
        </div>
        <div class="property-title-box">
            <h4><a target="_blank" href="{{ route('propiedades.detalle', ['id' => $property_out->id, 'slug' => $property_out->slug()]) }}">{{ $property_out->name }}</a></h4>
            {{-- <div class="property-location">
                <i class="fa fa-map-marker-alt"></i>
                <p>Ubicación</p>
            </div> --}}
            <ul class="property-feature">
                @if ($property_out->bedrooms)
                    <li> <i class="fas fa-bed"></i>
                        <span>{{ $property_out->bedrooms }} Recámaras</span>
                    </li>
                @endif
                @if ($property_out->bathrooms)
                    <li> <i class="fas fa-bath"></i>
                        <span>{{ $property_out->bathrooms }} Baños</span>
                    </li>
                @endif
                @if ($property_out->terrain_m2)
                    <li> <i class="fas fa-arrows-alt"></i>
                        <span>{{ $property_out->terrain_m2 }} m&sup2;</span>
                    </li>
                @endif
            </ul>
            <div class="trending-bottom">
                <a class="trend-right float-right">
                    <div class="trend-open">
                        <p><span class="per_sale">Precio</span>${{ $property_out->first_price }}</p>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>