@extends('layouts.app')
@section('content')
    <!--Page Wrapper starts-->
    <div class="page-wrapper">
        @include('include.header')
        <!--Breadcrumb section starts-->
        <div class="breadcrumb-section bg-h" style="background-image: url(https:/crm.gipssa.com/assets/images/FONDO6.jpg); ">
            <div class="row">
                <div class="col-lg-6 col-md-10 col-sm-10 text-center center-elements">
                    <img src="https://crm.gipssa.com/assets/images/imagen-inmobiliaria.png" style="margin-left:auto; margin-right:auto;" alt="logo" class="col-lg-11 col-md-10 col-sm-10 col-xs-8 img-fluid" >
                </div>
                <div class="col-lg-4 col-md-12 col-sm-12 g-inmobiliaria-form center-elements">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                       
                            <h2>Gipssa Inmobiliaria</h2>
                            <p>Te ayudamos a encontrar tu<br> propiedad ideal</p>
                        <div class="col-lg-12 row mt-5 mb-5 pb-4 pt-1 displayMobil">
                            <div class="blue-rombo"></div>
                            <div class="blue-rombo"></div>
                            <div class="blue-rombo"></div>
                        </div>
                        <div class="col-lg-10 col-md-12 col-sm-12 mt-5 px-0">
                            <form class="hero__form v1 filter listing-filter property-filter g-zoom-header ">
                                <div class="row">
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 ">
                                        <h3>ENCUENTRA TU HOGAR IDEAL</h3>
                                    </div>
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 py-2">
                                        <select name="zona" class="hero__form-input  form-control custom-select">
                                            <option value="">Seleccionar Zona</option>
                                            @if ($zones->count() > 0)
                                                @foreach ($zones as $zone)
                                                    <option value="{{ $zone->id }}" {{ (request()->zona == $zone->id) ? 'selected' : '' }}>{{ $zone->name }}</option>
                                                @endforeach
                                            @endif      
                                        </select>
                                    </div>
                                    <div class="col-xl-12 col-lg-12 col-sm-12 py-2 ">
                                        <select name="sort" class="listing-input hero__form-input  form-control custom-select">
                                            <option value="asc">Ordenar por más reciente</option>
                                            <option value="desc">Ordenar por más antiguo</option>
                                            <option value="destacado">Ordenar por destacado</option>
                                            <option value="mayor">Ordenar por precio(Menor a Mayor)</option>
                                            <option value="menor">Ordenar por precio(Mayor a Menor)</option>
                                        </select>
                                    </div>
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 py-2 ">
                                        <div class="submit_btn col-lg-12 px-0">
                                            <button class="btn v3 col-lg-12 " type="submit">Buscar</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 displayMobil">
                    <img src="https://crm.gipssa.com/assets/images/imagen-inmobiliaria-formas.png"  alt="logo" class="col-lg-9 img-fluid pull-r pr-0" >
                </div>
            </div>
        </div>
        <!--Breadcrumb section ends-->
        <div class="footer-wrapper v1 pt-20 pb-20 g-grey-color-area">
            <div class="row">
                <div class="col-md-12 g-section-grey-title text-center">
                    <h2 class="mb-0">PROPIEDADES</h2>
                </div>
            </div>
        </div>
        <!--Listing filter starts-->
        <div class="filter-wrapper style1 section-padding">
            <div class="container">
                <div class="row">
                    <!--Listing filter starts-->
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <form class="hero__form v1 filter listing-filter property-filter">
                                    <div class="row">
                                        <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12 col-12 py-3 pl-30 pr-0">
                                            <div class="input-search">
                                                <input type="text" name="search" id="place-event" placeholder="Buscar por nombre o código..." value="{{ request()->search }}">
                                            </div>
                                        </div>
                                        <div class="col-xl-2 col-lg-6 col-md-6 col-sm-12 col-12 py-3 pl-30 pr-0">
                                            <select name="zona" class="hero__form-input  form-control custom-select">
                                                <option value="">Seleccionar Zona</option>
                                                @if ($zones->count() > 0)
                                                    @foreach ($zones as $zone)
                                                        <option value="{{ $zone->id }}" {{ (request()->zona == $zone->id) ? 'selected' : '' }}>{{ $zone->name }}</option>
                                                    @endforeach
                                                @endif      
                                            </select>
                                        </div>
                                        <div class="col-xl-3 col-lg-6 col-sm-12 col-12 py-3 pl-3 pl-30 pr-0">
                                            <select name="sort" class="listing-input hero__form-input  form-control custom-select">
                                                <option value="asc">Ordenar por más reciente</option>
                                                <option value="desc">Ordenar por más antiguo</option>
                                                <option value="destacado">Ordenar por destacado</option>
                                                <option value="mayor">Ordenar por precio(Menor a Mayor)</option>
                                                <option value="menor">Ordenar por precio(Mayor a Menor)</option>
                                            </select>
                                        </div>
                                        {{-- <div class="col-xl-2 col-lg-6 col-md-6 col-sm-12 col-12 py-3 pl-30 pr-0">
                                            <select class="hero__form-input  form-control custom-select">
                                                <option>Seleccionar Ciudad</option>
                                                <option>Mérida</option>
                                                <option>Campeche</option>
                                                <option>Quintana Roo</option>
                                            </select>
                                        </div> --}}
                                        <div class="col-xl-3 col-lg-6 col-md-6 col-sm-12 col-12 py-3 pl-30 pr-0">
                                            <div class="submit_btn">
                                                <button class="btn v3" type="submit">Buscar</button>
                                            </div>
                                            <div class="dropdown-filter"><span>Más filtros</span></div>
                                        </div>
                                        <div class="explore__form-checkbox-list full-filter">
                                            <div class="row">
                                                <div class="col-lg-4 col-md-6 py-1 pr-30">
                                                    <select name="operacion" class="hero__form-input  form-control custom-select mb-20">
                                                        <option value="">Operación</option>
                                                        <option value="">Cualquiera</option>
                                                        @if ($operations->count() > 0)
                                                            @foreach ($operations as $op)
                                                                <option value="{{ $op->id }}" {{ (request()->operacion == $op->id) ? 'selected' : '' }}>{{ $op->name }}</option>
                                                            @endforeach
                                                        @endif
                                                    </select>
                                                </div>
                                                <div class="col-lg-4 col-md-6 py-1 pr-30 pl-0 ">
                                                    <select name="tipo" class="hero__form-input  form-control custom-select  mb-20">
                                                        <option value="">Tipo de propiedad</option>
                                                        @if ($types->count() > 0)
                                                            @foreach ($types as $type)
                                                                <option value="{{ $type->slug }}" {{ (request()->tipo == $type->slug) ? 'selected' : '' }}>{{ $type->name }}</option>
                                                            @endforeach
                                                        @endif
                                                    </select>
                                                </div>
                                                <div class="col-lg-4 col-md-6 py-1 pl-0">
                                                    <select name="nivel" class="hero__form-input  form-control custom-select  mb-20">
                                                        <option value="">Niveles</option>
                                                        <option>1</option>
                                                        <option>2</option>
                                                        <option>3</option>
                                                        <option>4</option>
                                                        <option>5+</option>
                                                    </select>
                                                </div>
                                                <div class="col-lg-4 col-md-6 py-1 pr-30">
                                                    <select name="recamara" class="hero__form-input  form-control custom-select  mb-20">
                                                        <option value="">Recámaras</option>
                                                        <option>1</option>
                                                        <option>2</option>
                                                        <option>3</option>
                                                        <option>4</option>
                                                        <option>5+</option>
                                                    </select>
                                                </div>
                                                <div class="col-lg-4 col-md-6 py-1 pr-30 pl-0">
                                                    <select name="baño" class="hero__form-input  form-control custom-select  mb-20">
                                                        <option value="">Baños</option>
                                                        <option>1</option>
                                                        <option>2</option>
                                                        <option>3</option>
                                                        <option>4</option>
                                                        <option>5+</option>
                                                    </select>
                                                </div>
                                                {{-- <div class="col-lg-4 col-md-6 py-1 pl-0">
                                                    <select class="hero__form-input  form-control custom-select  mb-20">
                                                        <option>Agentes</option>
                                                        <option>Bob Haris</option>
                                                        <option>Ross buttler</option>
                                                        <option>Andrew Saimons</option>
                                                        <option>Steve Austin</option>
                                                    </select>
                                                </div> --}}
                                                {{-- <div class="col-lg-4 col-md-6 py-1 pr-30">
                                                    <select class="hero__form-input  form-control custom-select  mb-20">
                                                        <option>Agencies</option>
                                                        <option>Onyx Equities</option>
                                                        <option>OVG Real Estate</option>
                                                        <option>Oxford Properties*</option>
                                                        <option>Cortland</option>
                                                    </select>
                                                </div> --}}
                                                {{-- <div class="col-lg-4 col-md-6 py-1 pr-30 pl-0">
                                                    <div class="filter-sub-area style1">
                                                        <div class="filter-title mb-10">
                                                            <p>Precio : <span><input type="text" id="amount_two"></span></p>
                                                        </div>
                                                        <div id="slider-range_two" class="price-range mb-30">
                                                        </div>
                                                    </div>
                                                </div> --}}
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!--Listing filter ends-->
                    <div class="col-md-12">
                        <div class="row pt-60   align-items-center">
                            <div class="col-lg-3 col-sm-5 col-5">
                                <div class="item-view-mode res-box">
                                    <!-- item-filter-list Menu starts -->
                                    <ul class="nav item-filter-list" role="tablist">
                                        <li><a class="active" data-toggle="tab" href="#grid-view"><i class="fas fa-th"></i></a></li>
                                        {{-- <li><a  data-toggle="tab" href="#list-view"><i class="fas fa-list"></i></a></li> --}}
                                    </ul>
                                    <!-- item-filter-list Menu ends -->
                                </div>
                            </div>
                            <div class="col-lg-5 col-sm-12">
                                <div class="item-element res-box  text-right sm-left">
                                    {{-- <p>Mostrando <span>{{ ($properties->perPage() * $properties->currentPage()) / $properties->perPage() }}-{{ $properties->perPage() * $properties->currentPage() }} of {{ $total }}</span> Listados</p> --}}
                                </div>
                            </div>
                        </div>
                        <div class="item-wrapper pt-40   ">
                            <div class="tab-content" id="myTabContent">
                                <div class="tab-pane  show active  fade property-grid" id="grid-view">
                                    <div class="row">
                                        @if ($properties->count() > 0)
                                            @foreach ($properties as $_property)
                                                @include('include.property-card')
                                            @endforeach
                                        @else
                                            <h2>No se encontraron resultados.</h2>
                                        @endif
                                        
                                    </div>
                                </div>
                                <!--pagination starts-->
                                <div class="post-nav nav-res pt-50  ">
                                    <div class="row">
                                        <div class="col-md-8 offset-md-2  col-xs-12 ">
                                            <div class="page-num text-center">
                                                {{ $properties->links() }}
                                                {{-- <ul>
                                                    <li class="active"><a href="#">1</a></li>
                                                    <li><a href="#">2</a></li>
                                                    <li><a href="#">3</a></li>
                                                    <li><a href="#">4</a></li>
                                                    <li><a href="#"><i class="lnr lnr-chevron-right"></i></a></li>
                                                </ul> --}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--pagination ends-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Listing filter ends-->
        <!-- Scroll to top starts-->
        <!-- Scroll to top ends-->
    </div>
    @include('include.footer')
    <!--Page Wrapper ends-->
@endsection