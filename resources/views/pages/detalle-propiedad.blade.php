@extends('layouts.app')
@section('content')
    @include('include.header')
            <!--Header ends-->
                <div class="property-details-wrap bg-cb screenContent">
                    <!--Listing Details Hero starts-->
                    <div class="single-property-header v1 bg-h mt-60" style="background-image: url({{ asset($property->cover) }});">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="list-details-title v1">
                                        <div class="row">
                                            <div class="col-lg-7 col-md-8 col-sm-12">
                                                <div class="single-listing-title float-left">
                                                    <h2>{{ $property->name }}<span class="btn v5">{{ $property->operation() }}</span></h2>
                                                    <p><i class="fa fa-map-marker-alt"></i> {{ $property->street }} #{{ $property->internal_number }} por {{ $property->accross }} {{ $property->colony }}</p>
                                                    <a href="#" class="property-author">
                                                        <img src="{{ asset($property->admin()->avatar) }}" alt="Imagen asesor">
                                                        <span>{{ $property->admin()->name }}</span>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="col-lg-5 col-md-4 col-sm-12">
                                                <div class="list-details-btn text-right sm-left">
                                                    <div class="trend-open">
                                                        <p><span class="per_sale">Precio inicial </span>${{ $property->first_price }}</p>
                                                    </div>
                                                    <br>
                                                    <ul class="list-btn">
                                                        {{-- <li class="share-btn">
                                                            <a href="#" class="btn v2" data-toggle="tooltip" title="Share"><i class="fas fa-share-alt"></i></a>
                                                            <ul class="social-share">
                                                                <li class="bg-fb"><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                                                <li class="bg-tt"><a href="#"><i class="fab fa-twitter"></i></a></li>
                                                                <li class="bg-ig"><a href="#"><i class="fab fa-instagram"></i></a></li>
                                                            </ul>
                                                        </li> --}}
                                                        {{-- <li class="save-btn">
                                                            <a href="#" class="btn v2" data-toggle="tooltip" title="Save"><i class="fa fa-heart"></i></a>
                                                        </li> --}}
                                                        <li class="print-btn">
                                                            {{-- <a href="#" class="btn v2" data-toggle="tooltip" title="Print"><i class="lnr lnr-printer"></i></a> --}}
                                                            <div class="addthis_inline_share_toolbox"></div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--Listing Details Hero ends-->
                    <!--Listing Details Info starts-->
                    <div class="single-property-details v1 mt-120">
                        <div class="container">
                            @if (session('success'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    <span class="alert-icon"><i class="ni ni-like-2"></i></span>
                                    <span class="alert-text"><strong>Correcto!</strong> {{ session('success') }}</span>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            @endif
                            <div class="row">
                                <div class="col-xl-8 col-lg-12">
                                    <div class="listing-desc-wrap mr-30">
                                        <div id="list-menu" class="list_menu">
                                            <ul class="list-details-tab fixed_nav">
                                                <li class="nav-item active"><a href="#description" class="active">Ubicación</a></li>
                                                @if (($property->gallery()) && ($property->gallery()->count() > 0))
                                                    <li class="nav-item"><a href="#gallery">Galería</a></li>
                                                @endif
                                                <li class="nav-item"><a href="#details">Detalles</a></li>
                                                @if (($property->areas()) && ($property->areas()->count() > 0))
                                                    <li class="nav-item"><a href="#floor_plan">Áreas</a></li>
                                                @endif
                                                <li class="nav-item"><a href="#book_tour">Book a Tour</a></li>
                                            </ul>
                                        </div>
                                        <!--Listing Details starts-->
                                        <div class="list-details-wrap">
                                            <div id="description" class="list-details-section">
                                                <div class="mt-40">
                                                    <h4 class="list-subtitle">Ubicación</h4>
                                                    @if ($property->location)
                                                        <a target="_blank" href="{{ $property->location }}" class="location-map">Ver Mapa<i class="fa fa-map-marker-alt"></i></a>
                                                    @endif
                                                    <ul class="listing-address">
                                                        <li>Ciudad : <span>Mérida</span></li>
                                                        <li>Calle : <span>{{ $property->street }}</span></li>
                                                        <li>Cruzamientos : <span>{{ $property->accross }}</span></li>
                                                        <li>Colonia : <span>{{ $property->colony }}</span></li>
                                                        <li>Zona : <span>{{ $property->zone()->name }}</span></li>
                                                        <li>Estado : <span>Yucatán</span></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            @if (($property->gallery()) && ($property->gallery()->count() > 0))
                                                <div id="gallery" class="list-details-section">
                                                    <h4>Galería</h4>
                                                    <!--Carousel Wrapper-->
                                                    <div id="carousel-thumb" class="carousel slide carousel-fade carousel-thumbnails list-gallery pt-2" data-ride="carousel">
                                                        <!--Slides-->
                                                        <div class="carousel-inner" role="listbox">
                                                            @foreach ($property->gallery() as $key => $gallery)
                                                                <div class="carousel-item {{ ($key == 0) ? 'active' : '' }}">
                                                                    <img class="d-block w-100" src="{{ asset($gallery->image) }}" alt="slide">
                                                                </div>
                                                            @endforeach
                                                        </div>
                                                        <!--Controls starts-->
                                                        <a class="carousel-control-prev" href="#carousel-thumb" role="button" data-slide="prev">
                                                            <span class="lnr lnr-arrow-left" aria-hidden="true"></span>
                                                            <span class="sr-only">Anterior</span>
                                                        </a>
                                                        <a class="carousel-control-next" href="#carousel-thumb" role="button" data-slide="next">
                                                            <span class="lnr lnr-arrow-right" aria-hidden="true"></span>
                                                            <span class="sr-only">Siguiente</span>
                                                        </a>
                                                        <!--Controls ends-->
                                                        <ol class="carousel-indicators  list-gallery-thumb">
                                                            @foreach ($property->gallery() as $key => $gallery)
                                                                <li data-target="#carousel-thumb" data-slide-to="{{ $key }}"><img class="img-fluid d-block w-100" src="images/single-listing/property_view_1.jpg" alt="..."></li>
                                                            @endforeach
                                                        </ol>
                                                    </div>
                                                    <!--/.Carousel Wrapper-->
                                                </div>
                                            @endif
                                            <div id="details" class="list-details-section">
                                                <div class="mb-40">
                                                    <h4>Detalles de propiedad</h4>
                                                    <ul class="property-info">
                                                        <li>Código : <span>{{ $property->code }}</span></li>
                                                        <li>Tipo : <span>{{ $property->type() }}</span></li>
                                                        <li>Operación : <span>{{ $property->operation() }}</span></li>
                                                        <li>Precio inicial : <span>${{ $property->first_price }}</span></li>
                                                        <li>Habitaciones : <span>{{ ($property->bedrooms) ? $property->bedrooms : 'Sin específicar' }}</span></li>
                                                        <li>Baños: <span>{{ ($property->bathrooms) ? $property->bathrooms : 'Sin específicar' }}</span></li>
                                                        <li>Año de construcción: <span>{{ ($property->year) ? $property->year : 'Sin específicar' }}</span></li>
                                                        <li>Niveles: <span>{{ ($property->level) ? $property->level : 'Sin específicar' }}</span></li>
                                                        <li>M&sup2; Terreno: <span>{{ ($property->terrain_m2) ? $property->terrain_m2 : 'Sin específicar' }}</span></li>
                                                        <li>M&sup2; Frente: <span>{{ ($property->front_m2) ? $property->front_m2 : 'Sin específicar' }}</span></li>
                                                        <li>M&sup2; Fondo: <span>{{ ($property->depth_m2) ? $property->depth_m2 : 'Sin específicar' }}</span></li>
                                                        <li>M&sup2; Construcción: <span>{{ ($property->build_m2) ? $property->build_m2 : 'Sin específicar' }}</span></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            @if (($property->areas()->where('status', 'visible')->get()) && ($property->areas()->where('status', 'visible')->get()->count() > 0))
                                                <div id="floor_plan" class="list-details-section">
                                                    <h4>Áreas</h4>
                                                    <div id="accordion10" role="tablist" class="pt-2">
                                                        @foreach ($property->areas()->where('status', 'visible')->get() as $key => $area)
                                                            <div class="card">
                                                                <div class="card-header" role="tab" id="headingOne">
                                                                    <a role="button" data-toggle="collapse" data-parent="#accordion10" href="#collapse_{{ $key }}" aria-expanded="{{ ($key == 0) ? 'true' : 'false'}}" aria-controls="collapse_{{ $key }}">
                                                                        <p>{{ $area->name }}</p>
                                                                        <i class="fas fa-angle-down"></i>
                                                                    </a>
                                                                </div>
                                                                <div id="collapse_{{ $key }}" class="panel-collapse collapse {{ ($key == 0) ? 'show' : ''}}" role="tabpanel" aria-labelledby="headingOne">
                                                                    <div class="card-body">
                                                                        <ul class="floor-list">
                                                                        @foreach ($area->equipment() as $equipment)
                                                                            <li><i class="fas fa-check"></i> {{ $equipment->name }}</li>
                                                                            @endforeach
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            @endif
                                            <div id="book_tour" class="list-details-section">
                                                <h4 class="list-details-title">Agendar una visita</h4>
                                                <form class="tour-form" method="POST" action="{{ route('propiedades.agendar', ['id_propiedad' => $property->id]) }}">
                                                    {{ csrf_field() }}
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div id="datepicker-from" class="input-group date" data-date-format="yyyy-mm-dd">
                                                                <input class="form-control" name="start_date" required type="text" placeholder="Fecha">
                                                                <span class="input-group-addon"><i class="lnr lnr-calendar-full"></i></span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <select required class="listing-input hero__form-input  form-control custom-select" name="start_time">
                                                                <option>Seleccione la hora</option>
                                                                <option value="08:00:00">8.00 am</option>
                                                                <option value="08:15:00">8.15 am</option>
                                                                <option value="08:30:00">8.30 am</option>
                                                                <option value="08:45:00">8.45 am</option>
                                                                <option value="09:00:00">9.00 am</option>
                                                                <option value="09:15:00">9.15 am</option>
                                                                <option value="09:30:00">9.30 am</option>
                                                                <option value="09:45:00">9.45 am</option>
                                                                <option value="10:00:00">10.00 am</option>
                                                                <option value="10:15:00">10.15 am</option>
                                                                <option value="10:30:00">10.30 am</option>
                                                                <option value="10:45:00">10.45 am</option>
                                                                <option value="11:00:00">11.00 am</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <input type="text" class="form-control filter-input" name="name" placeholder="Nombre">
                                                        </div>
                                                        <div class="col-md-4">
                                                            <input type="text" class="form-control filter-input" name="phone" placeholder="Teléfono">
                                                        </div>
                                                        <div class="col-md-4">
                                                            <input type="text" class="form-control filter-input" name="email" placeholder="Correo">
                                                        </div>
                                                        <div class="col-md-12">
                                                            <textarea class="contact-form__textarea mb-25" name="description" id="comment" placeholder="Mensaje"></textarea>
                                                            <input class="btn v3" type="submit" name="submit-contact" id="submit_contact" value="Enviar">
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-4 col-lg-12">
                                    <div id="list-sidebar" class="listing-sidebar">
                                        @if (isset($news) && ($news->count() > 0))
                                            <div class="widget">
                                                <h3 class="widget-title">Más recientes</h3>
                                                @foreach ($news as $new)
                                                    <li class="row recent-list">
                                                        <div class="col-lg-5 col-4">
                                                            <div class="entry-img">
                                                                <img src="{{ asset($new->cover) }}" alt="...">
                                                                <span>{{ $new->operation() }}</span>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-7 col-8 no-pad-left">
                                                            <div class="entry-text">
                                                                <h4 class="entry-title"><a href="{{ route('propiedades.detalle', ['id' => $new->id, 'slug' => $new->slug()]) }}">{{ $new->name }}</a></h4>
                                                                <div class="property-location">
                                                                    <i class="fa fa-map-marker-alt"></i>
                                                                    <p>{{ $new->street }}, {{ $new->colony }}, Mérida</p>
                                                                </div>
                                                                <div class="trend-open">
                                                                    <p>${{ $new->first_price }}</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                @endforeach
                                            </div>
                                        @endif
                                        @if (isset($outstanding) && ($outstanding->count() > 0))
                                            <div class="widget">
                                                <h3 class="widget-title">Propiedades destacadas</h3>
                                                <div class="swiper-container single-featured-list">
                                                    <div class="swiper-wrapper">
                                                        @foreach ($outstanding as $out)
                                                            <div class="swiper-slide single-property-box">
                                                                <div class="property-item">
                                                                    <a class="property-img" href="{{ route('propiedades.detalle', ['id' => $out->id, 'slug' => $out->slug()]) }}"><img src="{{ asset($out->cover) }}" alt="#">
                                                                    </a>
                                                                    <ul class="feature_text">
                                                                        <li class="feature_cb"><span> Destacado</span></li>
                                                                        @if (isset($out->tag()->name))
                                                                            <li class="feature_or"><span>{{ $out->tag()->name }}</span></li>
                                                                        @endif
                                                                    </ul>
                                                                    <div class="property-author-wrap">
                                                                        <h4><a href="{{ route('propiedades.detalle', ['id' => $new->id, 'slug' => $new->slug()]) }}" class="property-author">
                                                                                {{ $out->name }}
                                                                            </a>
                                                                        </h4>
                                                                        <div class="featured-price">
                                                                            <p><span class="per_sale">precio inicial</span>${{ $out->first_price }}</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @endforeach
                                                    </div>
                                                    <div class="slider-btn v3 single-featured-next"><i class="lnr lnr-arrow-right"></i></div>
                                                    <div class="slider-btn v3 single-featured-prev"><i class="lnr lnr-arrow-left"></i></div>
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--Listing Details Info ends-->
                    <!--Similar Listing starts-->
                    <div class="similar-listing-wrap pb-70 mt-70">
                        <div class="container">
                            <div class="col-md-12 px-0">
                                <div class="similar-listing">
                                    <div class="section-title v2">
                                        <h1>Más propiedades</h1>
                                    </div>
                                    <div class="swiper-container similar-list-wrap">
                                        <div class="swiper-wrapper">
                                            @foreach ($similar as $item)
                                                <div class="swiper-slide">
                                                    <div class="single-property-box">
                                                        <div class="property-item">
                                                            <a class="property-img" href="{{ route('propiedades.detalle', ['id' => $item->id, 'slug' => $item->slug()]) }}"><img src="{{ asset($item->cover) }}" alt="imagen_propiedad"> </a>
                                                            <ul class="feature_text">
                                                                @if (isset($item->tag()->name))
                                                                    <li class="feature_cb"><span>{{ $item->tag()->name }}</span></li>
                                                                @endif
                                                                <li class="feature_or"><span>{{ $item->operation() }}</span></li>
                                                            </ul>
                                                            <div class="property-author-wrap">
                                                                <a href="#" class="property-author">
                                                                    <img src="{{ asset($item->admin()->avatar) }}" alt="asesor">
                                                                    <span>{{ $item->admin()->name }}</span>
                                                                </a>
                                                                {{-- <ul class="save-btn">
                                                                    <li data-toggle="tooltip" data-placement="top" title="" data-original-title="Bookmark"><a href="#"><i class="lnr lnr-heart"></i></a></li>
                                                                    <li data-toggle="tooltip" data-placement="top" title="" data-original-title="Add to Compare"><a href="#"><i class="fas fa-arrows-alt-h"></i></a></li>
                                                                </ul> --}}
                                                            </div>
                                                        </div>
                                                        <div class="property-title-box">
                                                            <h4><a href="{{ route('propiedades.detalle', ['id' => $item->id, 'slug' => $item->slug()]) }}">{{ $item->name }}</a></h4>
                                                            {{-- <div class="property-location">
                                                                <i class="fa fa-map-marker-alt"></i>
                                                                <p>39 Casey Ave, Sunbury, VIC 3429</p>
                                                            </div> --}}
                                                            <ul class="property-feature">
                                                                <li> <i class="fas fa-bed"></i>
                                                                    <span>5 Bedrooms</span>
                                                                </li>
                                                                <li> <i class="fas fa-bath"></i>
                                                                    <span>4 Bath</span>
                                                                </li>
                                                                <li> <i class="fas fa-arrows-alt"></i>
                                                                    <span>2048 sq ft</span>
                                                                </li>
                                                                <li> <i class="fas fa-car"></i>
                                                                    <span>2 Garage</span>
                                                                </li>
                                                            </ul>
                                                            <div class="trending-bottom">
                                                                <div class="trend-left float-left">
                                                                    
                                                                </div>
                                                                <a class="trend-right float-right">
                                                                    <div class="trend-open">
                                                                        <p><span class="per_sale">precio inicial</span> ${{ $item->first_price }}</p>
                                                                    </div>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                    <div class="slider-btn v2 similar-next"><i class="lnr lnr-arrow-right"></i></div>
                                    <div class="slider-btn v2 similar-prev"><i class="lnr lnr-arrow-left"></i></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--Similar Listing ends-->
                </div>
                <!--Agent Chat box starts-->
                <div class="chatbox-wrapper screenContent">
                    <div class="chat-popup chat-bounce" data-toggle="tooltip" title="Tienes alguna duda? Pregunta !" data-placement="top">
                        <i class="fas fa-comment-alt"></i>
                    </div>
                    <div class="agent-chat-form v1">
                        <div class="container">
                            <div class="row">
                                <div class="col-4">
                                    <img class="agency-chat-img" src="{{ asset($property->admin()->avatar) }}" alt="...">
                                </div>
                                <div class="col-8 pl-0">
                                    <ul class="agent-chat-info">
                                        <li><i class="fas fa-user"></i>{{ $property->admin()->name }}</li>
                                        <li><i class="fas fa-envelope"></i>{{ $property->admin()->email }}</li>
                                        {{-- <li><a href="single-agent.html">View Listings</a></li> --}}
                                    </ul>
                                    <span class="chat-close"><i class="lnr lnr-cross"></i></span>
                                </div>
                            </div>
                            <div class="row mt-1">
                                <div class="col-md-12">
                                    <form action="#" method="POST">
                                        <div class="chat-group mt-1">
                                            <input class="chat-field" type="text" name="chat-name" id="chat-name" placeholder="Nombre">
                                        </div>
                                        <div class="chat-group mt-1">
                                            <input class="chat-field" type="text" name="chat-phone" id="chat-phone" placeholder="Teléfono">
                                        </div>
                                        <div class="chat-group mt-1">
                                            <input class="chat-field" type="text" name="chat-email" id="chat-email" placeholder="Email">
                                        </div>
                                        <div class="chat-group mt-1">
                                            <textarea class="form-control chat-msg" name="message" rows="4" placeholder="Description">Hola, estoy interesado en {{ $property->name }}-{{ $property->code }}</textarea>
                                        </div>
                                        <div class="chat-button mt-3">
                                            <a class="chat-btn" data-toggle="modal" data-target="#mortgage_result">Enviar Mensaje</a>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--Agent Chat box ends-->
                <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5fbbf0b73459d058"></script>
                    
                <div class="printContent">
                    <div class="content-print" style="page-break-after: always;">
                        <div class="pt-130">
                            <div class="g-top-card col-lg-12">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="row">
                                            <h2>{{ $property->name }}</h2>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="d-flex">
                                            <h4 class="mr-2">Medidas:</h4>
                                            <p>{{ ($property->front_m2) ? $property->front_m2 : 'Sin específicar' }} m&sup2; x {{ ($property->depth_m2) ? $property->depth_m2 : 'Sin específicar' }} m&sup2;</p>
                                            {{--<p>Terreno {{ ($property->terrain_m2) ? $property->terrain_m2 : 'Sin específicar' }} m&sup2;</p>
                                            <p>Construcción {{ ($property->build_m2) ? $property->build_m2 : 'Sin específicar' }} m&sup2;</p>--}}
                                        </div>  
                                        
                                    </div>
                                    <div class="col-sm-6">
                                        <h4>Precio: <span>${{ $property->first_price }}</span></h4>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12 py-60" style="padding-right:50px; padding-left:50px;">
                                <div class="row">
                                    @if (($property->gallery()) && ($property->gallery()->count() > 0))
                                        @php 
                                            foreach($property->gallery() as $key => $gallery){
                                                echo  '<div class="col-sm-6 text-center">
                                                        <img class="col-lg-12 my-2" src="https:/crm.gipssa.com/'.$gallery->image.'" alt="slide">
                                                    </div>';
                                                if($key == 5)
                                                break;
                                            }
                                            
                                        @endphp
                                    @endif
                                </div>
                            </div>   
                        </div>
                    </div>

                    <div class="content-print">
                        <div class="pt-130">
                            <div class="col-lg-12" style="padding-right:65px; padding-left:65px;">
                                <div class="row">
                                    <div class="col-lg-12 g-features-card px-0 ">
                                        <h4>Detalles de propiedad</h4>
                                        <ul>
                                            <li><span class="g-blue-dot mr-1"></span> Código : {{ $property->code }}</li>
                                            <li><span class="g-blue-dot mr-1"></span> Tipo : {{ $property->type() }}</li>
                                            <li><span class="g-blue-dot mr-1"></span> Operación : {{ $property->operation() }}</li>
                                            <li><span class="g-blue-dot mr-1"></span> Precio inicial : ${{ $property->first_price }}</li>
                                            <li><span class="g-blue-dot mr-1"></span> Habitaciones : {{ ($property->bedrooms) ? $property->bedrooms : 'Sin específicar' }}</li>
                                            <li><span class="g-blue-dot mr-1"></span> Baños: {{ ($property->bathrooms) ? $property->bathrooms : 'Sin específicar' }}</li>
                                            <li><span class="g-blue-dot mr-1"></span> Año de construcción: {{ ($property->year) ? $property->year : 'Sin específicar' }}</li>
                                            <li><span class="g-blue-dot mr-1"></span> Niveles: {{ ($property->level) ? $property->level : 'Sin específicar' }}</li>
                                            <li><span class="g-blue-dot mr-1"></span> M&sup2; Terreno: {{ ($property->terrain_m2) ? $property->terrain_m2 : 'Sin específicar' }}</li>
                                            <li><span class="g-blue-dot mr-1"></span> M&sup2; Frente: {{ ($property->front_m2) ? $property->front_m2 : 'Sin específicar' }}</li>
                                            <li><span class="g-blue-dot mr-1"></span> M&sup2; Fondo: {{ ($property->depth_m2) ? $property->depth_m2 : 'Sin específicar' }}</li>
                                            <li><span class="g-blue-dot mr-1"></span> M&sup2; Construcción: {{ ($property->build_m2) ? $property->build_m2 : 'Sin específicar' }}</li>
                                        </ul>
                                    </div>
                                    <div class="col-lg-12 g-features-card px-0 ">
                                        <h4>Ubicación</h4>
                                        <ul>
                                            <li><span class="g-blue-dot mr-1"></span> Ciudad : {{ $property->city }}</li>
                                            <li><span class="g-blue-dot mr-1"></span> Calle : {{ $property->street }}</li>
                                            <li><span class="g-blue-dot mr-1"></span> Cruzamientos : {{ $property->accross }}</li>
                                            <li><span class="g-blue-dot mr-1"></span> Colonia : {{ $property->colony }}</li>
                                            <li><span class="g-blue-dot mr-1"></span> Zona : {{ $property->zone()->name }}</li>
                                            <li><span class="g-blue-dot mr-1"></span> Estado : {{ $property->estate }}</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="content-print" style="padding-top:25%; ">
                        <div class="col-lg-12 col-md-12 col-sm-12 pb-5 text-center">
                            <img src="https://crm.gipssa.com/assets/images/inicio-logo.png" alt="logo" class="col-lg-3 col-md-3 col-sm-3 img-fluid" >
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 footer-wrapper v1 g-grey-color-area">
            
                            <div class="footer-top-area">
                                <div class="container">
                                <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12 center-block">
                                            <div class="text-center g-footer-title">
                                                <h2>CONTÁCTANOS</h2>
                                                <hr class="col-lg-4 col-md-4 col-sm-4 g-footer-hr"/>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12 pt-5 pb-5">
                                            <div class="row pb-5">
                                                <div class="col-lg-4 col-md-4 col-sm-4 text-center g-footer-subtitle pt-3">
                                                        <i class="fas fa-mobile-alt fa-3x mb-3"></i>
                                                        <h3>TELÉFONOS</h3>
                                                        <div class="center-block">
                                                            <div class="contact-info">
                                                                <div><i class="fas fa-phone-alt mr-2"></i><a href="tel:9994310411">9994 31 04 11</a></div>
                                                            </div>
                                                            <div class="contact-info">
                                                                <div><i class="fas fa-mobile-alt mr-2"></i><a href="tel:9995282820">9995 28 28 20</a></div>
                                                            </div>
                                                        </div>
                                                </div>
                                                <div class="col-lg-4 col-md-4 col-sm-4 d-flex">
                                                        <hr class="g-footer-vr"/>
                                                        <div class="col-lg-12 text-center g-footer-subtitle pt-3">
                                                            <i class="fas fa-map-marker-alt fa-3x mb-3"></i>
                                                            <h3>ENCUÉNTRANOS</h3>
                                                            <p>Calle 12 Núm. 112 por 5 y 9,<br>Montecristo, Mérida, Yucatán.<br>C.P. 97113</p>
                                                        </div>
                                                        
                                                        <hr class="g-footer-vr"/>
                                                    </div>
                                                    <div class="col-lg-4 col-lg-4 col-md-4 col-sm-4 text-center g-footer-subtitle pt-3">
                                                        <i class="fas fa-envelope fa-3x mb-3"></i>
                                                        <h3>ESCRÍBENOS</h3>
                                                        <div class="g-footer-mail">
                                                            <a href="mailto:contacto@gipssa.com"><p>contacto@gipssa.com</a>
                                                        </div>
                                                    </div>
                                            </div>
                                        </div>
                                </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 pt-5 g-print-social text-center">
                            <div class="row">
                                <div class="col-sm-4">
                                        <div><i class="fas fa-globe mr-2 fa-3x"></i><p>www.gipssa.com</p></div>
                                </div>
                                <div class="col-sm-4">
                                        <div><i class="fab fa-facebook-f mr-2 fa-3x"></i><p>Gipssa Inmobiliaria</p></div>
                                </div>
                                <div class="col-sm-4">
                                        <div><i class="fab fa-instagram mr-2 fa-3x"></i><p>gipssainmobiliaria</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            @include('include.footer')
@endsection