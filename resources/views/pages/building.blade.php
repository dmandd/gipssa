@extends('layouts.app')
@section('content')
<div class="page-wrapper">
    @include('include.header')
        <!--Breadcrumb section starts-->
        <div style="background-image: url(https:/crm.gipssa.com/assets/images/construccion-fondo1.jpg); width='100%'">
        <div class="container pt-120 pb-50" >
            <div class="row">
                <div class="col-md-12 text-center g-building pt-120 pb-120">
                   <h1>COTIZA NUESTROS SERVICIOS</h1>
                   <a href="https://api.whatsapp.com/send/?phone=529994707744" target="_blank" class="b-building-button">Edificio, construcción o remodelación habitacional, comercial e industrial</a>
                </div>
            </div>
        </div>
    </div>
    <!--Breadcrumb section ends-->
    <!--Contact info starts -->
    {{--<div class="contact-info section-padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-6">
                    <div class="contact-info-item">
                        <i class="fas fa-map-marker-alt"></i>
                        <h4>Nuestra oficina</h4>
                        <p>Calle 36 Núm. 287 x33 y 35, 
                            Col. San Ramón Norte, Mérida, Yucatán</p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="contact-info-item">
                        <i class="fas fa-phone-alt"></i>
                        <h4>Ponte en contacto</h4>
                        <p><strong>Directo : </strong>	+52 (999) 431 0411</p>
                        <p><strong>Email : </strong> contacto@gipssa.com</p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="contact-info-item">
                        <i class="fas fa-business-time"></i>
                        <h4>Nuestro Horario</h4>
                        <p><strong>Domingo : </strong> Cerrado</p>
                        <p><strong>Lunes-Viernes : </strong>09AM - 6PM</p>
                        <p><strong>Sábado : </strong>10AM - 2PM</p>
                    </div>
                </div>
            </div>
        </div>
    </div>--}}
    <!--Contact info ends -->

   
</div>
@include('include.footer')
@endsection