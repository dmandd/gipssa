@extends('layouts.app')
@section('content')
<div class="page-wrapper">
    @include('include.header')
        <!--Breadcrumb section starts-->
        <div class="container pt-130 pb-0" >
            <div class="col-lg-12 col-md-12 col-sm-12 g-section-title text-center">
                <div>
                    {{-- <p>Descubre propiedades populares</p> --}}
                    <h2 class="mb-0">DESARROLLOS</h2>
                </div>
                <div class="offset-lg-3 col-xl-6 col-lg-6 col-md-12 col-sm-12">
                    <p>
                        Nuestros desarrollos nacen con una visión eco turística para fomentar un crecimiento social, cultural 
                        y económico de la población local, logrando un retorno de inversión seguro con un desarrollo sustentable.
                    </p>
                </div>
                
            </div>
        </div>
        <div class="property-place pb-110" style="background-image: url(https://crm.gipssa.com/assets/images/desarrollo-fondo1.jpg); width='100%'">
            <div class="popular-place-wrap v2">
                <div class=" pt-130 pb-0">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12 mb-30" id="kukulkan">
                            <a href="http://grupocs.gipssa.com/punta-venado/" target="_blank">
                                <div class="single-place-wrap">
                                    <div class="single-place-image">
                                        <img src="https://crm.gipssa.com/assets/images/punta_venado.png" alt="place-image">
                                    </div>
                                    <div class="single-place-content">
                                        {{-- <h3>80 Propiedades Disponibles</h3> --}}
                                        <p>Ver más <i class="lnr lnr-arrow-right"></i></p>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-5 g-develop-kukulkan">
                            <div class="row m-1">
                                <div class="col-xl-12 col-lg-12 col-md-12 col-12 ">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <h2 class="kukulkan pull-r" style="color:#577863 !important;">PUNTA VENADO</h2>
                                        </div>
                                        <div class="col-lg-12">
                                            <h2 class="nerea pull-r"><b>QUINTAS, TERRENOS, INVERSIÓN</b></h2>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-12 col-lg-12 col-md-12 col-12 ">
                                    <p>
                                        Punta Venado es un desarrollo de Lotes Campestres ubicado
                                        cerca de la ciudad de Ticul, Yucatán, a 30min de la Zona
                                        Arqueológica de Uxmal y Grutas de Loltún, rodeado de
                                        Haciendas, Restaurantes típicos y maravillosas <br> .historias 
                                    </p>
                                    <br>
                                    <br>
                                    <p>
                                        Este desarrollo creado en el 2021, está conformado por 800
                                        lotes desde 300 m<sup>2</sup> divididos en varias etapas. Los lotes se
                                        entregan con accesos y calles <br> .blancas
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="property-place pb-110" >
            <div class="popular-place-wrap v2">
                <div class=" pt-50 pb-0">
                    <div class="row">
                        <div class="col-lg-5 offset-lg-1 col-md-12 col-sm-12 g-develop-nerea ">
                            <div>
                                <div class="col-lg-12">
                                    <h2 class="nerea-pink">NEREA</h2>
                                </div>
                                {{-- <p>Descubre propiedades populares</p> --}} 
                                <h2 class="col-lg-6 nerea"><b>THE BEST INVESTMENT</b></h2>
                            </div>
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                <p>
                                    Nerea es la oportunidad de invertir en lotes campestres a 5 minutos 
                                    de la orilla de la playa en Mahahual, Quintana Roo, en  la región 
                                    conocida como Costa Maya.
                                </p>
                                <br>
                                <br>
                                <p>
                                    Este desarrollo, que comenzó en agosto de 2019 contará con 5 etapas
                                    y un complejo eco-turístico enfocado en la bio construcción.
                                </p>
                                <br>
                                <br>
                                <p>
                                    Cuenta con acceso directo por la Carretera Mahahual - Xcalak lo 
                                    que da un extra a la plusvalía que tiene la zona por su alto crecimiento
                                    y proyección turística.
                                </p>
                            </div>
                            
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 mb-30" id="nerea">
                            <a href="http://grupocs.gipssa.com/nerea/" target="_blank">
                                <div class="single-place-wrap">
                                    <div class="single-place-image">
                                        <img src="https://crm.gipssa.com/assets/images/desarrollo-nerea.png" alt="place-image">
                                    </div>
                                    <div class="single-place-content">
                                        {{-- <h3>80 Propiedades Disponibles</h3> --}}
                                        <p>Ver más <i class="lnr lnr-arrow-right"></i></p>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="property-place pb-110" style="background-image: url(https://crm.gipssa.com/assets/images/desarrollo-fondo1.jpg); width='100%'">
            <div class="popular-place-wrap v2">
                <div class=" pt-50 pb-0">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12 mb-30" id="kukulkan">
                            <a href="https://landing.gipssa.com/kukulkan/adw" target="_blank">
                                <div class="single-place-wrap">
                                    <div class="single-place-image">
                                        <img src="https://crm.gipssa.com/assets/images/desarrollo-kukulkan.png" alt="place-image">
                                    </div>
                                    <div class="single-place-content">
                                        {{-- <h3>80 Propiedades Disponibles</h3> --}}
                                        <p>Ver más <i class="lnr lnr-arrow-right"></i></p>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-5 g-develop-kukulkan">
                            <div class="row m-1">
                                <div class="col-xl-12 col-lg-12 col-md-12 col-12 ">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <h2 class="kukulkan pull-r">KUKULKÁN</h2>
                                        </div>
                                        <div class="col-lg-12">
                                            <h2 class="nerea pull-r"><b>CIUDAD MAYA</b></h2>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-12 col-lg-12 col-md-12 col-12 ">
                                    <p>
                                        A sólo 5 minutos de la playa Santa Clara y 30 minutos de la ciudad de Mérida, en
                                        el pueblo de Dzidzantún, se encuentra Kukulkán, uno de nuestros desarrollos de lotes 
                                        de inversión. En un juego de arquitectura y pasado maya, Kukulkán muestra un aspecto de 
                                        pirámide, inspirado en una de las maravillas del mundo <br>.Chichén Itzá
                                    </p>
                                    <br>
                                    <br>
                                    <p>
                                        Kukulkán se ideo con un enfoque arquitectónico eco-sustentable, para utilizar con respeto la 
                                        naturaleza que lo rodea y seguir disfrutando de los paradisiacos lugares que se encuentran cerca, 
                                        como las playas de Costa Esmeralda, a 5 minutos del desarrollo. Todas sus amenidades como Casa Club 
                                        con mas de 6,400 m<sup>2</sup>, están alineadas con este <br> .principio
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    <!--Breadcrumb section ends-->
    <!--Contact info starts -->
    {{--<div class="contact-info section-padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-6">
                    <div class="contact-info-item">
                        <i class="fas fa-map-marker-alt"></i>
                        <h4>Nuestra oficina</h4>
                        <p>Calle 36 Núm. 287 x 33 y 35, 
                            Col. San Ramón Norte, Mérida, Yucatán</p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="contact-info-item">
                        <i class="fas fa-phone-alt"></i>
                        <h4>Ponte en contacto</h4>
                        <p><strong>Directo : </strong>	+52 (999) 431 0411</p>
                        <p><strong>Email : </strong> contacto@gipssa.com</p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="contact-info-item">
                        <i class="fas fa-business-time"></i>
                        <h4>Nuestro Horario</h4>
                        <p><strong>Domingo : </strong> Cerrado</p>
                        <p><strong>Lunes-Viernes : </strong>09AM - 6PM</p>
                        <p><strong>Sábado : </strong>10AM - 2PM</p>
                    </div>
                </div>
            </div>
        </div>
    </div>--}}
    <!--Contact info ends -->

    <!--Contact section starts-->
    
    <!--Contact section ends-->
    
</div>
@include('include.footer')
@endsection