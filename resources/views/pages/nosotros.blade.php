@extends('layouts.app')
@section('content')
<div class="page-wrapper">
    @include('include.header')
        <!--Breadcrumb section starts-->
        <div  style="background-image: url(https://crm.gipssa.com/assets/images/nosotros-fondo1.jpg); width='100%'">
            <div class="container pt-130 pb-0">
                <div class="row">
                    <div class="col-lg-6 col-md-12 col-sm-12 g-section-title">
                        <div>
                            {{-- <p>Descubre propiedades populares</p> --}}
                            <h2 class="mb-0">NOSOTROS</h2>
                        </div>
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                            <p>
                                Somos una empresa joven con una mirada fresca en el área de bienes raíces.
                                Comenzamos nuestro camino en 2005, aprendiendo de los mejores proyectos 
                                para sembrar nuestra primera semilla en 2011, cuando nos convertimos en
                                brokers inmobiliarios independientes.
                            </p>
                            <br>
                            <br>
                            <p>
                                En 2015, comenzamos a asociarnos con diferentes empresas del ámbito para 
                                participar en proyectos inmobiliarios. Lo que nos llevó en 2019, a lanzar 
                                nuestros primeros productos aunados a los seguimientos de ventas inmobiliarias 
                                y servicios de contrucción.
                            </p>
                            <br>
                            <br>
                            <p>
                                Desde ese momento y hasta ahora, hemos ido cimentando el camino para seguir
                                nuestro objetivo de crear desarrollos de alto nivel que sean competitivos
                                en el mercado y que den soluciones de calidad a la demanda inmobiliaria de 
                                nuestros clientes.
                            </p>
                        </div>
                        
                    </div>
                    <div class="col-lg-6 col-md-12 col-sm-12 text-center imgAgentes" style="margin-top:280px;">
                            <img src="https://crm.gipssa.com/assets/images/nosotros-agentes.png"  alt="nosotros" class="col-lg-12 col-md-8 col-sm-8 img-fluid" >
                        </div>
                </div>
            </div>
        </div>
     
        <div class="container pt-50 pb-50">
            <div class="row">
                <div class="col-lg-6 col-md-12 col-sm-12 text-center">
                    <img src="https://crm.gipssa.com/assets/images/nosotros-personas.png"  alt="nosotros" class="col-lg-12 col-md-8 col-sm-8 img-fluid" >
                </div>
                <div class="col-lg-6 col-md-12 col-sm-12 g-section-title">
                    <div>
                        {{-- <p>Descubre propiedades populares</p> --}}
                        <h2 class="mb-0"><span class="g-section-title-small">NUESTRA</span> MISIÓN Y VISIÓN</h2>
                    </div>
                    <div class="col-xl-12 col-lg-12 col-md-12 col-12">
                        <p>
                            <b><i>Misión:</i></b> Ofrecer opciones de alta calidad que puedan satisfacer
                            y dar solución a la demanda inmobiliaria en materia residencial, comercial e 
                            industrial, dentro de un marco profesional y ético.
                        </p>
                        <br>
                        <br>
                        <p>
                            <b><i>Visión:</i></b> Crear desarrollos de alto nivel competitivo a nivel nacional
                            e internacional, dando a nuestros clientes certeza jurídica y proyección económica
                            estable.
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div style="background: #EDFCFA;">
            <div class="container pt-50 pb-50">
                <div class="row">
                    <div class="col-lg-12 text-center g-about-us">
                        <h2>CONOCE LOS <b>VALORES</b>  DE NUESTRA EMPRESA</h2>
                        <p class="col-lg-8 offset-lg-2">Honestidad y ética <span class="g-blue-dot"></span> Eficiencia y Eficacia <span class="g-blue-dot"></span> 
                        Responsabilidad y Profesionalismo <span class="g-blue-dot"></span> Trabajo en equipo y productividad <span class="g-blue-dot"></span>
                        Integridad y respeto <span class="g-blue-dot"></span> Innovación y vanguardia
                        </p>
                    </div>
                </div>
            </div>
        </div>
    <!--Breadcrumb section ends-->
    <!--Contact info starts -->
    {{--<div class="contact-info section-padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-6">
                    <div class="contact-info-item">
                        <i class="fas fa-map-marker-alt"></i>
                        <h4>Nuestra oficina</h4>
                        <p>Calle 36 Núm. 287 por 33 y 35, 
                            Col. San Ramón Norte, Mérida, Yucatán</p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="contact-info-item">
                        <i class="fas fa-phone-alt"></i>
                        <h4>Ponte en contacto</h4>
                        <p><strong>Directo : </strong>	+52 (999) 431 0411</p>
                        <p><strong>Email : </strong> contacto@gipssa.com</p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="contact-info-item">
                        <i class="fas fa-business-time"></i>
                        <h4>Nuestro Horario</h4>
                        <p><strong>Domingo : </strong> Cerrado</p>
                        <p><strong>Lunes-Viernes : </strong>09AM - 6PM</p>
                        <p><strong>Sábado : </strong>10AM - 2PM</p>
                    </div>
                </div>
            </div>
        </div>
    </div>--}}
    <!--Contact info ends -->

    <div class="footer-wrapper v1 pt-20 pb-20 g-grey-color-area">
        <div class="row">
            <div class="col-md-12 g-section-grey-title text-center">
                <h2 class="mb-0">NUESTROS SERVICIOS</h2>
            </div>
        </div>
    </div>

    <div  style="background-image: url(https:/crm.gipssa.com/assets/images/nosotros-fondo2.jpg); width='100%'">
        <div class="container pt-100 " style="padding-bottom:250px;">
            <div class="row" >
                <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 ">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 card-service">
                        <div class="row">
                            <div class="col-lg-4 offset-lg-4 green-line"></div>
                            <div class="col-lg-12 text-center">
                                <img src="https://crm.gipssa.com/assets/images/icon-servicios.png" alt="logo" class=" img-fluid" >
                            </div>
                            <div class="col-lg-12">
                                <h3>Servicio Personalizado</h3>
                                <p>
                                    Servicio a tu medida basado en la responsabilidad y calidad humana.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 ">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 card-service">
                        <div class="row">
                            <div class="col-lg-4 offset-lg-4 green-line"></div>
                            <div class="col-lg-12 text-center">
                                <img src="https://crm.gipssa.com/assets/images/icon-inanciamiento.png" alt="logo" class=" img-fluid" >
                            </div>
                            <div class="col-lg-12">
                                <h3>Finaciamiento fácil y seguro</h3>
                                <p>
                                    Atractivos planes de finaciamiento con meses sin intereses.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 ">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 card-service">
                        <div class="row">
                            <div class="col-lg-4 offset-lg-4 green-line"></div>
                            <div class="col-lg-12 text-center">
                                <img src="https://crm.gipssa.com/assets/images/icon-asesoria.png" alt="logo" class=" img-fluid" >
                            </div>
                            <div class="col-lg-12">
                                <h3>Asesoría</h3>
                                <p>
                                    Asesoría jurídica y financiera para naconales y extranjeros.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 ">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 card-service">
                        <div class="row">
                            <div class="col-lg-4 offset-lg-4 green-line"></div>
                            <div class="col-lg-12 text-center">
                                <img src="https://crm.gipssa.com/assets/images/icon-exitosos.png" alt="logo" class=" img-fluid" >
                            </div>
                            <div class="col-lg-12">
                                <h3>10 desarollos exitosos con mas de 2,000 clientes satisfechos</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--Contact section starts-->
    
    <!--Contact section ends-->
    
</div>
@include('include.footer')
@endsection