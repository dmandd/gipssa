@extends('layouts.app')
@section('content')
<!--Page Wrapper starts-->
<div class="page-wrapper">
    @include('include.header')
    <!--Hero section starts-->
    <div class="hero-parallax bg-fixed" style="background-image: url(https://crm.gipssa.com/assets/images/FONDO1.jpg); width='100%'">
        <div class="overlay op-1"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="hero-slider-item">
                        <div class="row">
                            <div class="col-xl-12 col-lg-12 col-md-12 col-12">
                               <div class="col-xl-5 col-lg-5 col-md-12 col-12"
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-12" > 
                                        <div class="col-lg-12 text-center mb-5 pb-5">
                                            <img src="https://crm.gipssa.com/assets/images/inicio-logo.png" style="margin-left:auto; margin-right:auto;" alt="logo" class="col-lg-6 col-md-5 col-sm-8 col-xs-8 img-fluid" >
                                        </div>
                                        <div class="hero-slider-info g-shadow-box g-searcher-box g-zoom-header">
                                            <form id="form-1" action="{{ route('propiedades') }}" method="GET" class="hero__form v3 filter listing-filter needs-validation">
                                                <h4 class="mb-0">Encuentra con los expertos la propiedad que estás buscando.</h4>
                                                <div class="row">
                                                    <div class="col-lg-8">
                                                        <div class="row">
                                                            <div class="col-md-12 mb-3"></div>
                                                            <div class="col-lg-12 col-md-4 col-sm-6 mb-3">
                                                                <select required class="hero__form-input  form-control custom-select" name="operacion">
                                                                    <option>Operación</option>
                                                                    <option value="">Cualquiera</option>
                                                                    @if ((isset($operations)) && ($operations->count() > 0))
                                                                        @foreach ($operations as $op)
                                                                            <option value="{{ $op->id }}">{{ $op->name }}</option>
                                                                        @endforeach
                                                                    @endif
                                                                </select>
                                                            </div>
                                                            <div class="col-lg-12 col-md-4 col-sm-6 mb-3">
                                                                <select required class="hero__form-input  form-control custom-select" name="tipo">
                                                                    <option>Tipo de propiedad</option>
                                                                    @if (isset($types) && ($types->count() > 0))
                                                                        @foreach ($types as $t)
                                                                            <option value="{{ $t->slug }}">{{ $t->name }}</option>
                                                                        @endforeach
                                                                    @endif
                                                                </select>
                                                            </div>
                                                            <div class="col-lg-6 col-md-4 col-sm-6 mb-3">
                                                                <select required class="hero__form-input  form-control custom-select" name="recamara">
                                                                    <option>Recámaras</option>
                                                                    <option value="5">+5</option>
                                                                    <option value="4">4</option>
                                                                    <option value="3">3</option>
                                                                    <option value="2">2</option>
                                                                    <option value="1">1</option>
                                                                    <option value="0">0</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-lg-6 col-md-4 col-sm-6 mb-3">
                                                                <select required class="hero__form-input  form-control custom-select" name="baño">
                                                                    <option>Baños</option>
                                                                    <option value="5">+5</option>
                                                                    <option value="4">4</option>
                                                                    <option value="3">3</option>
                                                                    <option value="2">2</option>
                                                                    <option value="1">1</option>
                                                                    <option value="0">0</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <div class="row">
                                                            <div class="col-md-12 mb-3"></div>
                                                            <div class="col-lg-12 search_btn">
                                                                <a href="#" onclick="document.getElementById('form-1').submit()">Buscar</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                            </form>
                                        </div>
                                    </div>
                               </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Hero section ends-->
    {{-- Oculto de momento --}}
    {{-- <section class="lighter-gray borderless">
        <div class="text-center">
            <div class="row d-flex flex-wrap align-items-center justify-content-around">
                <div class="col-md-2 col-sm-12">
                    <img style="" src="https://a.slack-edge.com/80588/marketing/img/logos/company/_color/airbnb-logo.png" srcset="https://a.slack-edge.com/80588/marketing/img/logos/company/_color/airbnb-logo.png 1x, https://a.slack-edge.com/80588/marketing/img/logos/company/_color/airbnb-logo@2x.png 2x" alt="Airbnb" width="106">
                </div>
                <div class="col-md-2 col-sm-12">
                    <img style="" src="https://a.slack-edge.com/80588/marketing/img/logos/company/electronic-arts-logo.png" srcset="https://a.slack-edge.com/80588/marketing/img/logos/company/electronic-arts-logo.png 1x, https://a.slack-edge.com/80588/marketing/img/logos/company/electronic-arts-logo@2x.png 2x" alt="EA" width="40">
                </div>
                <div class="col-md-2 col-sm-12">
                    <img style="" src="https://a.slack-edge.com/80588/marketing/img/logos/company/_color/ameritrade-logo.png" srcset="https://a.slack-edge.com/80588/marketing/img/logos/company/_color/ameritrade-logo.png 1x, https://a.slack-edge.com/80588/marketing/img/logos/company/_color/ameritrade-logo@2x.png 2x" alt="Ameritrade" width="135">
                </div>
                <div class="col-md-2 col-sm-12">
                    <img style="" src="https://a.slack-edge.com/80588/marketing/img/logos/company/_color/oracle-logo.png" srcset="https://a.slack-edge.com/80588/marketing/img/logos/company/_color/oracle-logo.png 1x, https://a.slack-edge.com/80588/marketing/img/logos/company/_color/oracle-logo@2x.png 2x" alt="Oracle" width="140">
                </div>
                <div class="col-md-2 col-sm-12">
                    <img style="max-width:50px" src="https://a.slack-edge.com/80588/marketing/img/logos/company/_color/target-logo.png" srcset="https://a.slack-edge.com/80588/marketing/img/logos/company/_color/target-logo.png 1x, https://a.slack-edge.com/80588/marketing/img/logos/company/_color/target-logo@2x.png 2x" alt="Target" width="50">
                </div>
                <div class="col-md-2 col-sm-12">
                    <img style="" src="https://a.slack-edge.com/80588/marketing/img/logos/company/_color/autodesk-logo.png" srcset="https://a.slack-edge.com/80588/marketing/img/logos/company/_color/autodesk-logo.png 1x, https://a.slack-edge.com/80588/marketing/img/logos/company/_color/autodesk-logo@2x.png 2x" alt="Autodesk" width="106">
                </div>
            </div>
        </div>
    </section> --}}
    <!--Nuestras marcas-->
    <!--Nuestros Desarrollos-->
    <div style="background-image: url(https://crm.gipssa.com/assets/images/FONDO2.jpg); width='100%'" id="nosotros">
        <div class=" " >
            <div class="row">
                <div class="offset-lg-1 offset-md-1 offset-sm-1 col-lg-5 col-md-8 col-sm-8 col-xs-8 g-section-title" style="display: flex; align-items: center;">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-12">
                        <h2 class="mb-0">NOSOTROS</h2>
                        <p>
                            <b>Gipssa</b> es atención, seguimiento y servicio de primer nivel en <i>Inmobiliaria, Desarrollos y Construcción.</i><br>
                            Desde el 2011, cuando comenzamos  como brokers inmobiliarios, nos comprometimos a trabajar con honestidad y ética para
                            ser uno de los referentes en Yucatán y Quintana Roo. Nuestros desarrollos Kukulkán y Nerea son la muestra de nuestro 
                            potencial <b>Gipssa Inmobiliaria</b> y <b>Gipssa Constructora</b>, así como nuestro parámetro de calidad Premium.
                        </p>
                    </div>
                </div>
                <div class="col-lg-6 pl-0">
        <img src="https://crm.gipssa.com/assets/images/inicio-merida.png"  alt="nosotros" class="col-lg-12 col-md-8 col-sm-8 img-fluid pr-0 displayMobil" ></div>
            </div>
        </div>
        
    </div>
    <!--Proyectos-->
    <div class="footer-wrapper v1 pt-20 pb-20 g-grey-color-area">
        <div class="row">
            <div class="col-md-12 g-section-grey-title text-center">
                <h2 class="mb-0">CONOCE NUESTROS PROYECTOS</h2>
            </div>
        </div>
    </div>
    {{--<div class="property-place pb-110">
        <div class="popular-place-wrap v2">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12 mb-30" id="kukulkan">
                        <a href="https://landing.gipssa.com/kukulkan/adw" target="_blank">
                            <div class="single-place-wrap">
                                <div class="single-place-image">
                                    <img src="{{ asset('assets/images/kukulkan_cover.jpg') }}" alt="place-image">
                                </div>
                                <img class="img-center" src="{{ asset('assets/images/logo_kukulkan.png') }}" alt="">
                                <div class="single-place-content">
                                    {{-- <h3>80 Propiedades Disponibles</h3> --}}
                                    {{--<p>Ver más <i class="lnr lnr-arrow-right"></i></p>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 mb-30">
                        <a href="https://landing.gipssa.com/fb?fbclid=IwAR2Cms3bQDF8gLOLQSnVauq5n72RhvQVaiWt4xyJDf-llgxwVBrir9t5I0c" target="_blank">
                            <div class="single-place-wrap h-100">
                                <div class="single-place-image h-100">
                                    <img class="h-100" src="{{ asset('assets/images/nerea_cover_2.jpeg') }}" alt="place-image">
                                </div>
                                {{-- <span class="single-place-title">Nerea</span> --}}
                                {{-- <img class="img-center" src="{{ asset('assets/images/logo_nerea.png') }}" alt="Nerea" style=""> --}}
                                {{--<div class="single-place-content">
                                    <p>Ver más <i class="lnr lnr-arrow-right"></i></p>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>--}}
    <!--Trending events start-->
    {{--@if ($outstanding->count() > 0)
        <div class="trending-places pt-130 pb-130">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="section-title v1">
                            {{-- <p>Lorem ipsum dolor sit amet</p> --}}
                            {{--<h2>Descubre Propiedades Populares</h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <ul class="nav nav-tabs list-details-tab">
                            <li class="nav-item active">
                                <a data-toggle="tab" href="#all_property">Todas</a>
                            </li>
                            @if (($types) && ($types->count() > 0))
                                @foreach ($types as $type)
                                    <li class="nav-item ">
                                        @if ((isset($outstanding_type[$type->slug])) && ($outstanding_type[$type->slug]->count() > 0))
                                            <a data-toggle="tab" href="#{{ $type->slug }}">{{ $type->name }}</a>
                                        @endif
                                    </li>
                                @endforeach
                            @endif
                            {{-- <li class="nav-item">
                                <a data-toggle="tab" href="#for_rent">Renta</a>
                            </li> --}}
                       {{--</ul>
                    </div>
                    <div class="col-md-12">
                        <div class="tab-content mt-30">
                            <div class="tab-pane fade show active" id="all_property">
                                <div class="row">
                                    @foreach ($outstanding as $property_out)
                                        @include('pages.components.housecard')
                                    @endforeach
                                </div>
                            </div>
                            @if (($types) && ($types->count() > 0))
                                @foreach ($types as $type)
                                    <div class="tab-pane fade" id="{{ $type->slug }}">
                                        <div class="row">
                                            @if ((isset($outstanding_type[$type->slug])) && ($outstanding_type[$type->slug]->count() > 0))
                                                @foreach ($outstanding_type[$type->slug] as $property_out)
                                                    @include('pages.components.housecard')
                                                @endforeach
                                            @else
                                                <h5>Sin resultados...</h5>
                                            @endif
                                        </div>
                                    </div>
                                @endforeach
                            @endif
                        </div>
                    </div>
                    <div class="col-md-12 text-center mt-1">
                        <a href="{{ route('propiedades') }}" target="_blank" class="btn v9">Buscar Más</a>
                    </div>
                </div>
            </div>
        </div>
    @endif--}}
    <!--Trending events ends-->
    <!--Featured Property starts-->
    @if ($news->count() > 0)
    
        <div class="featured-property-section v2 " style="background-image: url(https://crm.gipssa.com/assets/images/FONDO3.jpg); width='100%'" id="nosotros">
            <div class="container pt-50 pb-50 ">
                <div class="row text-center" >
                    <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 ">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 g-project-card pr-0 pl-0 ">
                            <img src="https://crm.gipssa.com/assets/images/proyect-desarrollo.png" class="mb-3 img-fluid">
                            <p class="mb-3 pr-3 pl-3">Nuestros proyecos Kukulkán y Nerea son garantía de que tu inversión patrimonial no se devaluará, ya que se encuentran en una zona turística
                                y con un enfoque ecológico.
                            </p>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 ">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 g-project-card  pr-0 pl-0">
                            <img src="https://crm.gipssa.com/assets/images/proyect-inmobiliaria.png" class="mb-3 img-fluid">
                            <p class="mb-3 pr-3 pl-3">Con Gipssa inmobiliaria te ayudamos a comprar, equipar, vender o rentar la propiedad que estás buscando. Contamos con propiedades tanto 
                                en la peninsúla de Yucatán como en el resto de la República: encuentra desde una casa, departamento o town house, hasta un terreno o 
                                una casa en la playa.
                            </p>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 ">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 g-project-card  pr-0 pl-0">
                            <img src="https://crm.gipssa.com/assets/images/proyect-constructora.png" class="mb-3 img-fluid">
                            <p class="mb-3 pr-3 pl-3">Desarrollamos proyectos de alto nivel  competitivo en zonas privilegiadas, ofrecemos servicios de construcción o remodelación habitacional, 
                                comercial e industrial.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="row ">
                    <div class="col-xl-12 col-lg-12 ">
                        <div class="g-new-properties">
                            <h2>NUEVAS PROPIEDADES</h2>
                        </div>
                    </div>
                    <div class="col-xl-12 col-lg-12">
                        <div class="featured-property-wrap v2 swiper-container">
                            <div class="swiper-wrapper">
                                @foreach ($news as $property_new)
                                    <div class="swiper-slide single-property-box">
                                        @include('pages.components.houselarge')
                                    </div>
                                @endforeach
                            </div>
                            <!--Slider Arrows-->
                            <div class="slider-btn v2 featured_prev"><i class="lnr lnr-arrow-left"></i></div>
                            <div class="slider-btn v2 featured_next"><i class="lnr lnr-arrow-right"></i></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
    <!--Featured Property ends-->
    <!--Promo Section starts-->
    {{--<div id="about-us" class="promo-section v1 py-50">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-title v1">
                        {{-- <p>Lorem ipsum dolor sit.</p> --}}
                       {{-- <h2 class="barra-inicial">Nuestros servicios</h2>
                    </div>
                </div>
                <div class="col-xl-6">
                    <div class="promo-img ">
                        <img src="{{ asset('assets/images/Casa-GIPSSA.jpg') }}" alt="...">
                    </div>
                </div>
                <div class="col-xl-6 col-lg-12">
                    <div class="promo-content-wrap">
                        <div class="row promo-content">
                            <div class="col-md-2">
                                <img src="http://demo.lion-coders.com/html/sarchholm-real-estate-template/images/category/property_listing.png" alt="...">
                            </div>
                            <div class="col-md-10">
                                <div class="promo-text">
                                    <h4>Servicio Personalizado.</h4>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quibusdam, aspernatur.</p>
                                </div>
                            </div>
                        </div>
                        <div class="row promo-content">
                            <div class="col-md-2">
                                <img src="http://demo.lion-coders.com/html/sarchholm-real-estate-template/images/category/rent.png" alt="...">
                            </div>
                            <div class="col-md-10">
                                <div class="promo-text">
                                    <h4>Financiado Fácil y Seguro.</h4>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quibusdam, aspernatur.</p>
                                </div>
                            </div>
                        </div>
                        <div class="row promo-content">
                            <div class="col-md-2">
                                <img src="http://demo.lion-coders.com/html/sarchholm-real-estate-template/images/category/customer_support.png" alt="...">
                            </div>
                            <div class="col-md-10">
                                <div class="promo-text">
                                    <h4>Soporte 24/7.</h4>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quibusdam, aspernatur.</p>
                                </div>
                            </div>
                        </div>
                        <div class="row promo-content">
                            <div class="col-md-2">
                                <img src="http://demo.lion-coders.com/html/sarchholm-real-estate-template/images/category/deal_1.png" alt="...">
                            </div>
                            <div class="col-md-10">
                                <div class="promo-text">
                                    <h4>Más de 4.3k de Clientes satisfechos.</h4>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quibusdam, aspernatur.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>--}}
    <!--Promo Section ends-->
    <!-- Scroll to top starts-->
    {{--<span class="scrolltotop"><i class="lnr lnr-arrow-up"></i></span>--}}
    <!-- Scroll to top ends-->
</div>
<!--Page Wrapper ends-->
@include('include.footer')
@endsection