@extends('layouts.app')
@section('content')
<div class="page-wrapper">
    @include('include.header')
        <!--Breadcrumb section starts-->
    <div style="background-color:#EEEEEE;">
        <div class="container pt-120 pb-40" >
            <div class="row">
                <div class="col-lg-12">
                    <div class="row ">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-12 hero-slider-info g-shadow-box-2 g-contact-box" > 
                            <div class="row">
                                <div class="col-lg-6">
                                    <h2>¡AGENDA TU CITA!</h2>
                                    <p>Llena este formulario y un miembro de nuestro equipo te contactará a la brevedad.</p>
                                    <form action="{{ route('sendcustomermail') }}" method="post" id="contact_form">
                                        <div class="form-control-wrap">
                                            <div id="message" class="alert alert-danger alert-dismissible fade"></div>
                                            <div class="form-group">
                                                <input type="text" class="form-control" id="fname" placeholder="Nombre" name="fname">
                                            </div>
                                            <div class="form-group">
                                                <input type="number" class="form-control" id="fphone" placeholder="Teléfono" name="fphone">
                                            </div>
                                            <div class="form-group">
                                                <input type="email" class="form-control" id="email_address" placeholder="Correo electrónico" name="email">
                                            </div>
                                            <div class="form-group">
                                                <textarea class="form-control" rows="3" name="comment" id="comment" placeholder="Mensaje"></textarea>
                                            </div>
                                            <div class="form-group">
                                                <button type="submit" class="col-lg-4 offset-lg-4 btn v7 " >Enviar</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="col-lg-6" style="display: flex; align-items: center;">
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12 pb-5 text-center">
                                            <img src="https://crm.gipssa.com/assets/images/inicio-logo.png" alt="logo" class="col-lg-6 col-md-6 col-sm-6 img-fluid" >
                                        </div>
                                        <div class="row">
                                                <div class="col-lg-12 ">
                                                    <div class="text-center g-contacto-title">
                                                        <h2>CONTÁCTANOS</h2>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 pt-5 pb-5 g-zoom-contact">
                                                    <div class="row pb-5">
                                                        <div class="col-lg-4 text-center g-contacto-subtitle pt-3">
                                                                <i class="fas fa-mobile-alt fa-2x mb-3"></i>
                                                                <h3>TELÉFONOS</h3>
                                                                <div class="center-block">
                                                                    <div class="contact-info">
                                                                        <div><i class="fas fa-phone-alt mr-2"></i><a href="tel:9994310411">9994 31 04 11</a></div>
                                                                    </div>
                                                                    <div class="contact-info">
                                                                        <div><i class="fas fa-mobile-alt mr-2"></i><a href="tel:9994707744">99 94 70 77 44</a></div>
                                                                    </div>
                                                                </div>
                                                        </div>
                                                        <div class="col-lg-4 d-flex">
                                                                <hr class="g-contacto-vr displayMobil"/>
                                                                <div class="col-lg-12 text-center g-contacto-subtitle pt-3">
                                                                    <i class="fas fa-map-marker-alt fa-2x mb-3"></i>
                                                                    <h3>ENCUÉNTRANOS</h3>
                                                                    <p>Calle 36 Núm. 287 x 33 y 35,</p>
                                                                    <p>Col. San Ramón Norte, Mérida, Yucatán</p>                                                                </div>
                                                                
                                                                <hr class="g-contacto-vr displayMobil"/>
                                                            </div>
                                                            <div class="col-lg-4 text-center g-contacto-subtitle pt-3">
                                                                <i class="fas fa-envelope fa-2x mb-3"></i>
                                                                <h3>ESCRÍBENOS</h3>
                                                                <div class="g-contacto-mail">
                                                                    <a href="mailto:contacto@gipssa.com"><p>contacto@gipssa.com</a>
                                                                </div>
                                                            </div>
                                                    </div>
                                                </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Breadcrumb section ends-->
    <!--Contact info starts -->
    {{--<div class="contact-info section-padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-6">
                    <div class="contact-info-item">
                        <i class="fas fa-map-marker-alt"></i>
                        <h4>Nuestra oficina</h4>
                        <p>CALLE 36 Núm. 287 x 33 y 35, 
                        <Col. SanRamón Norte, Mérida, Yucatán</p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="contact-info-item">
                        <i class="fas fa-phone-alt"></i>
                        <h4>Ponte en contacto</h4>
                        <p><strong>Directo : </strong>	+52 (999) 431 0411</p>
                        <p><strong>Email : </strong> contacto@gipssa.com</p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="contact-info-item">
                        <i class="fas fa-business-time"></i>
                        <h4>Nuestro Horario</h4>
                        <p><strong>Domingo : </strong> Cerrado</p>
                        <p><strong>Lunes-Viernes : </strong>09AM - 6PM</p>
                        <p><strong>Sábado : </strong>10AM - 2PM</p>
                    </div>
                </div>
            </div>
        </div>
    </div>--}}
    <!--Contact info ends -->

    <div class="footer-wrapper v1 pt-20 pb-20 g-grey-color-area">
        <div class="row">
            <div class="col-md-12 g-section-grey-title text-center">
                <h2 class="mb-0">UBÍCANOS</h2>
            </div>
        </div>
    </div>

    <!--Contact section starts-->
    <div class="contact-section v1 mt-0 mb-0">
            <div class="google-maps">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d14896.219360853518!2d-89.62533126044926!3d21.030491599999994!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8f5677b5c9c2138b%3A0xe733e71ae4887dd1!2sGIPSSA!5e0!3m2!1ses-419!2smx!4v1651514028593!5m2!1ses-419!2smx" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
            </div>
    </div>
    <!--Contact section ends-->
    <div class="footer-bottom-area g-grey-color-area">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-8 offset-md-2 g-footer-privacy">
                    <p>
                        © gipssa.com 2022. Todos los derechos reservados. 
                        <a href="{{ route('aviso') }}">Aviso de privacidad</a>
                    </p>

                </div>
                <div class="col-lg-2 col-md-12 col-sm-12 text-center">
                    <a class="g-footer-social mr-1" href="{{ (request()->is('propiedades')) ? 'https://www.facebook.com/GipssaInmobiliaria' : 'https://www.facebook.com/gipssadesarrollos'}}" target="_blank" ><i class="g-footer-social-fb fab fa-facebook-f fa-2x"></i></a>
                    <a class="g-footer-social" href="{{ (request()->is('propiedades')) ? 'https://www.instagram.com/gipssainmobiliaria/':'https://www.instagram.com/gipssadesarrollos/'}}" target="_blank" ><i class="g-footer-social-ig fab fa-instagram fa-2x"></i></a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection