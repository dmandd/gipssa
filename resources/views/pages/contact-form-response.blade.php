@extends('layouts.app')
@section('content')
<!--Page Wrapper starts-->
<div class="page-wrapper">
    @include('include.header')
    <!--Hero section starts-->
    <div class="hero-parallax bg-fixed" style="background-image: url(https:/crm.gipssa.com/assets/images/IMAGEN_RESPUESTA_CONTACTO.jpg); width='100%'">
        <div class="overlay op-1"></div>
        <div class="container">
            
        </div>
    </div>
    <!--Hero section ends-->
  
<!--Page Wrapper ends-->
@include('include.footer')
@endsection