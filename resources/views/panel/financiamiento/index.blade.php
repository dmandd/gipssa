@extends('layouts.panel.app')
@section('content')
    <!-- Header -->
    @include('include.panel.header')
    <!-- Page content -->
    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col">
				<div class="card">
				<!-- Card header -->
                    <div class="card-header border-0">
                        <div class="row">
                            <div class="col-8">
                                <h6 class="heading-small text-muted mb-4">Información</h6>
                            </div>
                            <div class="col-4 text-right">
                               
                            </div>
                        </div>
                    </div>
                    <!-- Light table -->
					<div class="table-responsive pb-3">
						<table class="table align-items-center table-flush" id="dataTable">
							<thead class="thead-light">
								<tr>
									<th width="20px">
                                    </th>
									<th scope="col" class="sort" data-sort="status">Nombre</th>
									<th scope="col" class="sort" data-sort="status">Tipo</th>
									<th scope="col" class="sort" data-sort="status">Descuento</th>
									<th scope="col" class="sort" data-sort="status">Interés</th>
									<th scope="col" class="sort" data-sort="status">Pago/Enganche</th>
									<th scope="col" class="sort" data-sort="status">Financiamiento</th>
									<th scope="col" class="sort" data-sort="status">Saldo</th>
									<th scope="col">Visualización</th>
									<th scope="col">Acciones</th>
								</tr>
							</thead>
							<tbody class="list">
								@if ((isset($data)) && (count($data) > 0))
                                    @foreach ($data as $row)
                                        <tr>
                                            <td>
                                                <input type="checkbox" class="" id=""><span style="opacity:0;">{{ $row->id}}</span>
                                            </td>
                                            <td>{{ $row->nombre }}</td>
                                            <td>{{ $row->tipo }}</td>
                                            <td>{{ $row->porcentaje_descuento }}</td>
                                            <td>{{ $row->porcentaje_interes }}</td>
                                            <td>{{ $row->enganche }}</td>
                                            <td>{{ $row->mensualidad_num_pagos }}</td>
                                            <td>{{ $row->saldo }}</td>
                                            <td>{{ $row->status }}</td>
                                            <td class="text-right">
                                                <div class="dropdown">
                                                    <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <i class="fas fa-ellipsis-v"></i>
                                                    </a>
                                                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                                        <a class="dropdown-item" href="{{ route('panel.financiamiento.edit', ['desarrollo' => $row->development_id, 'financiamiento' => $row->id]) }}">Editar</a>
                                                        <a class="dropdown-item btn-delete" data-axios-method="delete" data-route="{{ route('panel.financiamiento.destroy', ['desarrollo' => $row->development_id, 'financiamiento' => $row->id]) }}" data-action="location.reload()" href="javascript:;">Eliminar</a>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
							</tbody>
						</table>
					</div>
				</div>
			</div>
        </div>
    </div>
@endsection