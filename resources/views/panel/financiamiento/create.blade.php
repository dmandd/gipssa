@extends('layouts.panel.app')
@section('content')
    <!-- Header -->
    @include('include.panel.header')
    <!-- Page content -->
    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col">
				<div class="card">
				<!-- Card header -->
					<div class="card-header border-0">
                        {{-- <h3 class="mb-0">Nuevo usuario</h3> --}}
                        <h6 class="heading-small text-muted mb-4">Información</h6>
					</div>
                    <!-- Light table -->
                    <div class="card-body">
                        <form action="{{ route('panel.financiamiento.store', ['desarrollo' => request()->desarrollo]) }}" method="POST" class="needs-validation" novalidate>
                            {{ csrf_field() }}
                            <div class="pl-lg-4">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="form-control-label" for="nombre">* Nombre</label>
                                            <input type="text" name="nombre" id="nombre" class="form-control" required autocomplete="off" value="">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="form-control-label" for="tipo">* Tipo</label>
                                            <select class="form-control" name="tipo" id="tipo">
                                                <option value="contado">Contado</option>
                                                <option value="contado_diferido">Contado diferido</option>
                                                <option value="financiamiento">Financiamiento</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="form-control-label" for="tipo_descuento">* Tipo de descuento</label>
                                            <select class="form-control" name="tipo_descuento" id="tipo_descuento">
                                                <option value="porcentaje">Porcentaje</option>
                                                <option value="cantidad">Cantidad</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="form-control-label" for="porcentaje_descuento">* Monto descuento</label>
                                            <input type="text" name="porcentaje_descuento" id="porcentaje_descuento" class="form-control" autocomplete="off" value="{{ (old('porcentaje_descuento')) ? old('porcentaje_descuento') : 0 }}">
                                        </div>
                                    </div>
                                </div>
                               
                                <div class="row d-none" id="contado-group">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="form-control-label" for="tipo_enganche">* Tipo de pago</label>
                                            <select class="form-control" name="contado[tipo_enganche]" id="contado[tipo_enganche]">
                                                <option value="porcentaje">Porcentaje</option>
                                                <option value="cantidad">Cantidad</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="form-control-label" for="enganche" data-tippy-content="El porcentaje se calcula en base al costo total">* Cantidad a pagar</label>
                                            <input type="text" name="contado[enganche]" id="contado[enganche]" class="form-control" autocomplete="off" value="">
                                        </div>
                                    </div>
                                   
                                    <input type="hidden" name="contado[enganche_num_pagos]" id="contado[enganche_num_pagos]" class="form-control" autocomplete="off" value="1">
                                       
                                </div>
                                <div class="row d-none" id="contado-diferido-group">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="form-control-label" for="tipo_enganche">* Tipo de pago</label>
                                            <select class="form-control" name="contado_diferido[tipo_enganche]" id="contado_diferido[tipo_enganche]">
                                                <option value="porcentaje">Porcentaje</option>
                                                <option value="cantidad">Cantidad</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="form-control-label" for="enganche" data-tippy-content="El porcentaje se calcula en base al costo total">* Monto a diferir</label>
                                            <input type="text" name="contado_diferido[enganche]" id="contado_diferido[enganche]" class="form-control" autocomplete="off" value="{{ old('mensualidad') }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="form-control-label" for="enganche_num_pagos">* Número de pagos</label>
                                            <select class="form-control" name="contado_diferido[enganche_num_pagos]" id="contado_diferido[enganche_num_pagos]">
                                                <option value="2" selected>2</option>
                                                <option value="3">3</option>
                                            </select>
                                        </div>
                                    </div>
                                </div> 
                                <div class="row d-none" id="enganche-group">
                                    {{-- <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="form-control-label" for="porcentaje_interes">* Porcentaje interés</label>
                                        </div>
                                    </div> --}}
                                    <input type="hidden" name="financiamiento[porcentaje_interes]" id="financiamiento[porcentaje_interes]" class="form-control" autocomplete="off" value="0">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="form-control-label" for="tipo_enganche">* Tipo de enganche</label>
                                            <select class="form-control" name="financiamiento[tipo_enganche]" id="financiamiento[tipo_enganche]">
                                                <option value="porcentaje">Porcentaje</option>
                                                <option value="cantidad">Cantidad</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="form-control-label" for="enganche">* Enganche</label>
                                            <input type="text" name="financiamiento[enganche]" id="financiamiento[enganche]" class="form-control" autocomplete="off" value="">
                                        </div>
                                    </div>
                                    {{-- <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="enganche_diferido" data-tippy-content="El porcentaje se calcula en base al costo total">* Porcentaje diferido (Enganche)</label>
                                        </div>
                                    </div> --}}
                                    <input type="hidden" name="financiamiento[enganche_diferido]" id="financiamiento[enganche_diferido]" class="form-control" autocomplete="off" value="0">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="form-control-label" for="enganche_num_pagos">* Número de pagos (Enganche)</label>
                                            <input type="text" name="financiamiento[enganche_num_pagos]" id="financiamiento[enganche_num_pagos]" class="form-control" autocomplete="off" value="{{ (old('enganche_num_pagos')) ? old('enganche_num_pagos') : 0 }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="form-control-label" for="tipo_mensualidad">* Tipo de mensualidad</label>
                                            <select class="form-control" name="financiamiento[tipo_mensualidad]" id="financiamiento[tipo_mensualidad]">
                                                <option value="porcentaje">Porcentaje</option>
                                                <option value="cantidad">Cantidad</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="form-control-label" for="mensualidad" data-tippy-content="El porcentaje se calcula en base al costo total">* Saldo a financiar</label>
                                            <input type="text" name="financiamiento[mensualidad]" id="financiamiento[mensualidad]" class="form-control" autocomplete="off" value="{{ old('mensualidad') }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="form-control-label" for="mensualidad_num_pagos">* Número de pagos</label>
                                            <input type="number" step="1" min="1" max="{{$desarrollo->max_delivery_time}}" name="financiamiento[mensualidad_num_pagos]" id="financiamiento[mensualidad_num_pagos]" class="form-control" autocomplete="off" value="{{ old('mensualidad_num_pagos') }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="form-control-label" for="tipo_saldo">* Tipo de saldo</label>
                                            <select class="form-control" name="tipo_saldo" id="tipo_saldo">
                                                <option value="porcentaje">Porcentaje</option>
                                                <option value="cantidad">Cantidad</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="form-control-label" for="saldo" data-tippy-content="El porcentaje se calcula en base al costo total">* Saldo</label>
                                            <input type="text" name="saldo" id="saldo" class="form-control" autocomplete="off" value="{{ old('saldo') }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="form-control-label" for="name">* Publicación</label>
                                            <select name="status" id="status" class="form-control">
                                                <option value="visible">Visible</option>
                                                <option value="hidden">Oculto</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="row">
                                    <div class="col-lg-12 text-center">
                                        <button class="btn btn-default">Confirmar</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
				</div>
			</div>
        </div>
    </div>
@endsection
@push('js')
    <script>
        $(document).ready(function(){
            
            let tipo = document.getElementById('tipo');
            changeDom(tipo.value);

            tipo.addEventListener('change', e => {
                changeDom(tipo.value);
            });

            function changeDom(tipo){
                if(tipo == 'contado'){
                    document.getElementById('contado-group').classList.remove('d-none');
                    document.getElementById('contado[enganche_num_pagos]').setAttribute('required',true);
                    document.getElementById('contado[enganche]').setAttribute('required', true);
                    document.getElementById('contado[tipo_enganche]').setAttribute('required', true);
                }else{
                    document.getElementById('contado[enganche_num_pagos]').removeAttribute('required');
                    document.getElementById('contado[enganche]').removeAttribute('required');
                    document.getElementById('contado[tipo_enganche]').removeAttribute('required');
                    document.getElementById('contado-group').classList.add('d-none');
                }

                if(tipo == 'contado_diferido'){
                    document.getElementById('contado-diferido-group').classList.remove('d-none');
                    document.getElementById('contado_diferido[enganche_num_pagos]').setAttribute('required',true);
                    document.getElementById('contado_diferido[enganche]').setAttribute('required', true);
                    document.getElementById('contado_diferido[tipo_enganche]').setAttribute('required', true);
                }else{
                    document.getElementById('contado_diferido[enganche_num_pagos]').removeAttribute('required');
                    document.getElementById('contado_diferido[enganche]').removeAttribute('required');
                    document.getElementById('contado_diferido[tipo_enganche]').removeAttribute('required');
                    document.getElementById('contado-diferido-group').classList.add('d-none');
                }

                if(tipo == 'financiamiento'){
                    document.getElementById('enganche-group').classList.remove('d-none');
                   
                }else{
                    document.getElementById('enganche-group').classList.add('d-none');
                }
            }
        });

        
    </script>
@endpush