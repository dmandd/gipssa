<div class="modal fade" id="new-event" tabindex="-1" role="dialog" aria-labelledby="new-event-label" style="display: none;" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-secondary" role="document">
		<div class="modal-content">
			<!-- Modal body -->
			<form id="new-event-form" class="needs-validation" novalidate method="POST" action="{{ route('panel.events.store') }}" data-form-action="{{ route('panel.events.store') }}">
				{{ csrf_field() }}
				<input type="hidden" id="_method" name="" value="PUT">
				<div class="modal-body">
					<div class="row">
						<div class="col-12 text-center">
							@can(PermissionKey::Event['permissions']['destroy']['name'])
								<a id="delete-event" class="btn btn-lg btn-icon rounded-circle btn-danger btn-action text-white" 
										type="button"
										data-toggle="tooltip"
										data-placement="top"
										data-title="¿Está seguro de eliminar?"
										data-text="Este registro se eliminará. La operación no puede revertirse"
										data-icon="question"
										data-axios-method="delete"
										data-route=""
										data-action="location.reload()"
										title="Eliminar">
									<span class="btn-inner--icon"><i class="fas fa-trash"></i></span>
								</a>
							@endcan
						</div>
					</div>
					<div class="form-group">
						<label class="form-control-label">*Título evento</label>
						<input type="text" id="title" name="title" required class="form-control new-event--title" placeholder="Junta" autocomplete="off">
					</div>
					@can(PermissionKey::Event['permissions']['assign']['name'])
						<div class="form-group">
							<label class="form-control-label">*Asignar</label>
							<select name="admin_id" id="admin_id" required class="form-control">
								<option value="">Selecciona una opción</option>
								@if (Admin::all()->count() > 0)
									@foreach (Admin::all() as $admin)
										<option value="{{ $admin->id }}">{{ $admin->name }}</option>
									@endforeach
								@endif
							</select>
						</div>
					@endcan
					<div class="form-group">
						<label class="form-control-label">Asignar contacto</label>
						<select name="contact_id" id="contact_id" class="selectpicker form-control" data-live-search="true">
							<option value="">Ninguno</option>
							@if (auth()->user()->contacts()->count() > 0)
								<optgroup label="Contactos Propiedades">
									@foreach (auth()->user()->contacts() as $contact)
										<option value="{{ $contact->id }}-{{ $contact->model() }}">{{ $contact->name }}</option>
									@endforeach
								</optgroup>
							@endif
							@if (auth()->user()->client()->count() > 0)
								<optgroup label="Clientes desarrollos">
									@foreach (auth()->user()->client() as $client)
										<option value="{{ $client->id }}-{{ $client->model() }}">{{ $client->name }} {{ $client->surname }}</option>
									@endforeach
								</optgroup>							
							@endif
							@if (auth()->user()->prospects()->count() > 0)
								<optgroup label="Prospectos">
									@foreach (auth()->user()->prospects() as $prospect)
										<option value="{{ $prospect->id }}-{{ $prospect->model() }}">{{ $prospect->name }}</option>
									@endforeach
								</optgroup>							
							@endif
						</select>
					</div>
					<div class="form-group">
						<label class="form-control-label">*Hora evento</label>
						<select name="start_time" id="start_time" required class="form-control">
							<option value="07:00:00">07:00 am</option>
							<option value="08:00:00">08:00 am</option>
							<option value="09:00:00">09:00 am</option>
							<option value="10:00:00">10:00 am</option>
							<option value="11:00:00">11:00 am</option>
							<option value="12:00:00">12:00 pm</option>
							<option value="13:00:00">01:00 pm</option>
							<option value="14:00:00">02:00 pm</option>
							<option value="15:00:00">03:00 pm</option>
							<option value="16:00:00">04:00 pm</option>
							<option value="17:00:00">05:00 pm</option>
							<option value="18:00:00">06:00 pm</option>
							<option value="19:00:00">07:00 pm</option>
							<option value="20:00:00">08:00 pm</option>
							<option value="21:00:00">09:00 pm</option>
						</select>
					</div>
					<div class="form-group">
						<label class="form-control-label">*Duración evento</label>
						<select name="length" id="length" class="form-control" required>
							<option value="30">30 mins</option>
							<option value="45">45 mins</option>
							<option value="60">1hr</option>
							<option value="90">1hr 30mins</option>
							<option value="120">2hr</option>
						</select>
					</div>
					<div class="form-group">
						<label class="form-control-label">Seleccionar sala</label>
						<select name="room_id" id="room_id" class="form-control">
							<option value="">Ninguno</option>
						</select>
					</div>
					<div class="form-group">
						<label class="form-control-label">Descripción</label>
						<textarea name="description" id="description" class="form-control" cols="30" rows="5"></textarea>
					</div>
					<div class="form-group mb-0">
						<label class="form-control-label d-block mb-3">Escoge un color</label>
						<div class="btn-group btn-group-toggle btn-group-colors event-tag" data-toggle="buttons">
							<label class="btn bg-info active"><input type="radio" name="color" class="color-junta" value="#49cdef" autocomplete="off" checked=""></label>
							<label class="btn bg-warning"><input type="radio" name="color" class="color-junta" value="#f46340" autocomplete="off"></label>
							<label class="btn bg-danger"><input type="radio" name="color" class="color-junta" value="#f4355d" autocomplete="off"></label>
							<label class="btn bg-success"><input type="radio" name="color" class="color-junta" value="#45ce89" autocomplete="off"></label>
							<label class="btn bg-default"><input type="radio" name="color" class="color-junta" value="#172b4d" autocomplete="off"></label>
							<label class="btn bg-primary"><input type="radio" name="color" class="color-junta" value="#5e72e4" autocomplete="off"></label>
						</div>
					</div>
					<input type="hidden" class="new-event--start" name="start_date" value="">
				</div>
				<!-- Modal footer -->
				<div class="modal-footer">
					@can(PermissionKey::Event['permissions']['create']['name'])
						<button class="btn btn-primary new-event--add">Agregar evento</button>
					@endcan
					<button type="button" class="btn btn-link ml-auto" data-dismiss="modal">Cerrar</button>
				</div>
			</form>
		</div>
	</div>
</div>