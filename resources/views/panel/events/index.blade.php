@extends('layouts.panel.app')
@push('style')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.css"/>
    {{-- <link rel="stylesheet" href="https://demos.creative-tim.com/argon-dashboard-pro/assets/css/argon.min.css?v=1.2.0"/> --}}
    <style>
        .swal2-container{z-index:9999!important;}
        .btn-light, .btn-light:hover, .btn-light:focus, .btn-light:not(:disabled):not(.disabled):active, .btn-light:not(:disabled):not(.disabled).active, .show > .btn-light.dropdown-toggle{background-color:white;color:#8898aa!important;border-color:#dee2e6;font-weight: 400;box-shadow:none;}
        .btn-group-colors>.btn:before {
            font-family: NucleoIcons,sans-serif;
            font-size: 14px;
            line-height: 28px;
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            content: '';
            transition: transform .2s,opacity .2s;
            transform: scale(0);
            opacity: 0;
            color: #fff;
        }
        .btn-group-colors>.btn.active:before {
            transform: scale(1);
            opacity: 1;
        }
        .btn:not(:disabled):not(.disabled).active, .btn:not(:disabled):not(.disabled):active {
            box-shadow: none;
        }
        .btn-group-vertical>.btn.active, .btn-group-vertical>.btn:active, .btn-group-vertical>.btn:focus, .btn-group>.btn.active, .btn-group>.btn:active, .btn-group>.btn:focus {
            z-index: 1;
        }
        .btn-group-colors>.btn {
            position: relative;
            width: 30px;
            height: 30px;
            margin-right: .5rem;
            margin-bottom: .25rem;
            padding: 0;
            border-radius: 50%!important;
            box-shadow: none;
        }
        .btn-group>.btn-group:not(:first-child), .btn-group>.btn:not(:first-child) {
            margin-left: -1px;
        }
        .fc-event-container{cursor: pointer;}
    </style>
@endpush
@section('content')
    <!-- Header -->
    @include('include.panel.header')
    <!-- Page content -->
    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-6">
                                <h6 class="fullcalendar-title h2 d-inline-block mb-0"></h6>
                            </div>
                            <div class="col-6 mt-3 mt-lg-0 text-lg-right">
                                <a href="#" class="fullcalendar-btn-prev btn btn-sm btn-neutral">
                                  <i class="fas fa-angle-left"></i>
                                </a>
                                <a href="#" class="fullcalendar-btn-today btn btn-sm btn-neutral">
                                  Hoy
                                </a>
                                <a href="#" class="fullcalendar-btn-next btn btn-sm btn-neutral">
                                  <i class="fas fa-angle-right"></i>
                                </a>
                                {{-- <a href="#" class="btn btn-sm btn-neutral" data-calendar-view="month">Month</a>
                                <a href="#" class="btn btn-sm btn-neutral" data-calendar-view="basicWeek">Week</a>
                                <a href="#" class="btn btn-sm btn-neutral" data-calendar-view="basicDay">Day</a> --}}
                            </div>
                        </div>
                    </div>
                    <div class="card-body p-0">
                        <div class="calendar fc fc-unthemed fc-ltr" id="calendar">
                            {!! $calendar->calendar() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('panel.events.components.modal')
@endsection
@push('js')
    <script src="{{ asset('panel/js/moment.js') }}"></script>
    <script src="{{ asset('panel/js/fullcalendar-es.js') }}"></script>
    <script src="{{ asset('panel/js/fullcalendar.js') }}"></script>
    {!! $calendar->script() !!}
    <script>const calendar_id = {{ Calendar::getId() }};</script>
    <script src="{{ asset('panel/js/calendar.js') }}"></script>
@endpush