<div class="modal fade" id="setLote" tabindex="-1" role="dialog" aria-labelledby="modal-form" aria-hidden="true">
    <div class="modal-dialog modal- modal-dialog-centered modal-sm" role="document">
        <div class="modal-content">   
            <div class="modal-body p-0">                
                <div class="card bg-secondary border-0 mb-0">
                    <div class="card-body px-lg-5 py-lg-5">
                        <div class="text-center text-muted mb-4">
                            <small>Lotes</small>
                        </div>
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="form-control-label" for="id_lote">* Unidad</label>
                                    <select name="id_lote" id="id_lote" class="form-control">
                                        @if ($development->lotes()->count() > 0)
                                            <option value="">Selecciona unidad</option>
                                            <option value="none">Remover unidad</option>
                                            @foreach ($development->lotes() as $lote)
                                                <option value='@json($lote)'>{{ $lote->name }}</option>
                                            @endforeach
                                        @else
                                            <option value="">No se han cargado unidades para este desarrollo...</option>
                                        @endif
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="text-center">
                            <button class="btn btn-primary my-4" id="confirm-submit">Confirmar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>