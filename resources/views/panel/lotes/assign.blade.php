@extends('layouts.panel.app')
@section('content')
<style>
    #canvas_background{cursor:default!important;}
    path, ellipse, rect{cursor: pointer;fill:#fff!important;fill-opacity: 0.3;transition:0.5s all}
    path:hover, ellipse:hover, rect:hover{fill-opacity: 0.5;}
    .assigned{fill:transparent!important;}
</style>
    <!-- Header -->
    @include('include.panel.header')
    <!-- Page content -->
    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col">
                <form action="{{ route('panel.lotes.assign.update', ['desarrollo' => $development->id]) }}" id="form-update" method="POST" class="needs-validation" novalidate>
                    <div class="card">
                    <!-- Card header -->
                        <div class="card-header border-0">
                            {{-- <h3 class="mb-0">Nuevo usuario</h3> --}}
                            <h6 class="heading-small text-muted mb-4">Información</h6>
                        </div>
                        <!-- Light table -->
                        <div class="card-body">
                            {{ csrf_field() }}
                            <textarea name="svg" id="svg_edited" cols="30" rows="10" class="d-none"></textarea>
                            <input type="hidden" name="_method" value="PUT">
                            <div class="pl-lg-4">
                                <div class="row">
                                        
                                        <div id="drawing" class="text-center" style="width:100%;">
                                            @php
                                                $svg = new DOMDocument();
                                                $svg->load(base_path().'/public_html/'.$development->plano);
                                                // Add CSS class (you can omit this line)
                                                $svg->documentElement->setAttribute("class", "logo");
                                                // Echo XML without version element
                                                echo $svg->saveXML($svg->documentElement);        
                                                // include_once(base_path().'/storage/app/public/images/'.$lotificacion->slug.'/svg/'.$lotificacion->lotes->configuracion->asset[0]->svg);
                                            @endphp
                                        </div>
                                    
                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="row">
                                    <div class="col-lg-12 text-center">
                                        <button class="btn btn-default">Confirmar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
			</div>
        </div>
    </div>
    @include('panel.lotes.components.modal')
@endsection
@push('js')
    <script src="{{ asset('panel/js/svgdot.js') }}"></script>
@endpush