@extends('layouts.panel.app')
@section('content')
    <!-- Header -->
    @include('include.panel.header')
    <!-- Page content -->
    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col">
				<div class="card">
				<!-- Card header -->
					<div class="card-header border-0">
                        {{-- <h3 class="mb-0">Nuevo usuario</h3> --}}
                        <h6 class="heading-small text-muted mb-4">Información</h6>
					</div>
                    <!-- Light table -->
                    <div class="card-body">
                        <form action="{{ route('panel.lotes.update', ['desarrollo' => request()->desarrollo, 'lote' => $lote->id]) }}" method="POST" class="needs-validation" novalidate>
                            {{ csrf_field() }}
                            <input type="hidden" name="_method" value="PUT">
                            <div class="pl-lg-4">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="dropzone-chepina" class="form-control-label">Chepina</label>
                                            <input class="form-control unfocus" type="text" name="chepina" id="chepina" value="{{ (old('chepina')) ? old('chepina') : $lote->chepina }}" data-asset="{{ asset('') }}">
                                            <a href="#modal-media" data-reference="dropzone-chepina" data-dropzone="image" data-toggle="modal" data-target="#modal-media" class="btn btn-default float-right mt-1">Escoger del multimedia</a>
                                            <div class="clearfix"></div>
                                            <div id="dropzone-chepina" data-route="{{ route('images.store') }}" data-target="#chepina" class="dropzone" style="border:0px">
                                                <div class="dz-default dz-message">Seleccionar archivo</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="num_lote">* Número de lote</label>
                                            <input type="text" name="num_lote" id="num_lote" class="form-control" required autocomplete="off" value="{{ (old('num_lote')) ? old('num_lote') : $lote->num_lote }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="area">* Metros (m&sup2;)</label>
                                            <input type="text" name="area" id="area" class="form-control" required autocomplete="off" value="{{ (old('area')) ? old('area') : $lote->area }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="form-control-label" for="name">* Publicación</label>
                                            <select name="status" id="status" class="form-control">
                                                <option value="disponible" {{ ($lote->status == 'disponible') ? 'selected' : '' }}>Disponible</option>
                                                <option value="apartado" {{ ($lote->status == 'apartado') ? 'selected' : '' }}>Apartado</option>
                                                <option value="vendido" {{ ($lote->status == 'vendido') ? 'selected' : '' }}>Vendido</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="form-control-label" for="price">* Precio</label>
                                            <input type="text" name="price" id="price" class="form-control" required autocomplete="off" value="{{ (old('price')) ? old('price') : $lote->price }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="form-control-label" for="medida">* Medidas</label>
                                            <input type="text" name="medida" id="medida" class="form-control" required autocomplete="off" value="{{ (old('medida')) ? old('medida') : $lote->medida }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="form-control-label" for="name">* Nombre</label>
                                            <input type="text" name="name" id="name" class="form-control" required autocomplete="off" value="{{ (old('name')) ? old('name') : $lote->name }}">
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <h5>Medidas y lindancias.</h5>
                                <div class="row">
                                    <div class="col-lg-12">
                                            <h3>Norte</h3>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="norte">Metros lienales</label>
                                            <input type="text" name="norte" id="norte" class="form-control"  autocomplete="off" value="{{ (old('norte')) ? old('norte') : $lote->norte }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-9">
                                        <div class="form-group">
                                            <label class="form-control-label" for="lindancia_norte">Lindancias</label>
                                            <textarea name="lindancia_norte" id="" rows="1" class="form-control">{{ $lote->lindancia_norte }}</textarea>
                                        </div>
                                    </div>

                                    <div class="col-lg-12">
                                        <h3>Nororiente</h3>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="nororiente">Metros lienales</label>
                                            <input type="text" name="nororiente" id="nororiente" class="form-control"  autocomplete="off" value="{{ (old('nororiente')) ? old('nororiente') : $lote->nororiente }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-9">
                                        <div class="form-group">
                                            <label class="form-control-label" for="lindancia_nororiente">Lindancias</label>
                                            <textarea name="lindancia_nororiente" id="" rows="1" class="form-control">{{ $lote->lindancia_nororiente }}</textarea>
                                        </div>
                                    </div>

                                    <div class="col-lg-12">
                                        <h3>Oriente</h3>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="oriente">Metros lienales</label>
                                            <input type="text" name="oriente" id="oriente" class="form-control"  autocomplete="off" value="{{ (old('oriente')) ? old('oriente') : $lote->oriente }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-9">
                                        <div class="form-group">
                                            <label class="form-control-label" for="lindancia_oriente">Lindancias</label>
                                            <textarea name="lindancia_oriente" id="" rows="1" class="form-control">{{ $lote->lindancia_oriente }}</textarea>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <h3>Suroriente</h3>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="suroriente">Metros lienales</label>
                                            <input type="text" name="suroriente" id="suroriente" class="form-control"  autocomplete="off" value="{{ (old('suroriente')) ? old('suroriente') : $lote->suroriente }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-9">
                                        <div class="form-group">
                                            <label class="form-control-label" for="lindancia_suroriente">Lindancias</label>
                                            <textarea name="lindancia_suroriente" id="" rows="1" class="form-control">{{ $lote->lindancia_suroriente }}</textarea>
                                        </div>
                                    </div>

                                    <div class="col-lg-12">
                                        <h3>Sur</h3>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="sur">Metros lienales</label>
                                            <input type="text" name="sur" id="sur" class="form-control"  autocomplete="off" value="{{ (old('sur')) ? old('sur') : $lote->sur }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-9">
                                        <div class="form-group">
                                            <label class="form-control-label" for="lindancia_sur">Lindancias</label>
                                            <textarea name="lindancia_sur" id="" rows="1" class="form-control">{{ $lote->lindancia_sur }}</textarea>
                                        </div>
                                    </div>

                                    <div class="col-lg-12">
                                        <h3>Surponiente</h3>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="surponiente">Metros lienales</label>
                                            <input type="text" name="surponiente" id="surponiente" class="form-control"  autocomplete="off" value="{{ (old('surponiente')) ? old('surponiente') : $lote->surponiente }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-9">
                                        <div class="form-group">
                                            <label class="form-control-label" for="lindancia_surponiente">Lindancias</label>
                                            <textarea name="lindancia_surponiente" id="" rows="1" class="form-control">{{ $lote->lindancia_surponiente }}</textarea>
                                        </div>
                                    </div>

                                    <div class="col-lg-12">
                                        <h3>Poniente</h3>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="poniente">Metros lienales</label>
                                            <input type="text" name="poniente" id="poniente" class="form-control"  autocomplete="off" value="{{ (old('poniente')) ? old('poniente') : $lote->poniente }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-9">
                                        <div class="form-group">
                                            <label class="form-control-label" for="lindancia_poniente">Lindancias</label>
                                            <textarea name="lindancia_poniente" id="" rows="1" class="form-control">{{ $lote->lindancia_poniente }}</textarea>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <h3>Norponiente</h3>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="norponiente">Metros lienales</label>
                                            <input type="text" name="norponiente" id="norponiente" class="form-control"  autocomplete="off" value="{{ (old('norponiente')) ? old('norponiente') : $lote->norponiente }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-9">
                                        <div class="form-group">
                                            <label class="form-control-label" for="lindancia_norponiente">Lindancias</label>
                                            <textarea name="lindancia_norponiente" id="" rows="1" class="form-control">{{ $lote->lindancia_norponiente }}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="row">
                                    <div class="col-lg-12 text-center">
                                        <button class="btn btn-default">Confirmar</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
				</div>
			</div>
        </div>
    </div>
    @include('include.panel.media')
@endsection