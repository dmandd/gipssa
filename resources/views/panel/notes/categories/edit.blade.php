@extends('layouts.panel.app')
@section('content')
    <!-- Header -->
    @include('include.panel.header')
    <!-- Page content -->
    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col">
				<div class="card">
				<!-- Card header -->
					<div class="card-header border-0">
                        {{-- <h3 class="mb-0">Nuevo usuario</h3> --}}
                        <h6 class="heading-small text-muted mb-4">Información</h6>
					</div>
                    <!-- Light table -->
                    <div class="card-body">
                        <form action="{{ route('notes.categories.update', ['id' => $category->id]) }}" method="POST" class="needs-validation" novalidate>
                            {{ csrf_field() }}
                            <input type="hidden" name="_method" value="PUT">
                            <div class="pl-lg-4">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label class="form-control-label" for="name">* Título</label>
                                            <input type="text" name="title" id="title" class="form-control" required autocomplete="off" value="{{ (old('title')) ? old('title') : $category->translates()->where('locale', 'es')->first()->title }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label class="form-control-label" for="status">* Publicación</label>
                                            <select name="status" id="status" class="form-control">
                                                <option {{ ($category->status == 'visible') ? 'selected' : '' }} value="visible">Visible</option>
                                                <option {{ ($category->status == 'hidden') ? 'selected' : '' }} value="hidden">Oculto</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="row">
                                    <div class="col-lg-12 text-center">
                                        @if (Auth::guard('web')->user()->role == 'admin')
                                            <button class="btn btn-default">Confirmar</button>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
				</div>
			</div>
        </div>
    </div>
@endsection