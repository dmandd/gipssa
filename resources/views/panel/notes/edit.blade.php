@extends('layouts.panel.app')
@section('content')
    <!-- Header -->
    @include('include.panel.header')
    <!-- Page content -->
    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col">
				<div class="card">
				<!-- Card header -->
					<div class="card-header border-0">
                        {{-- <h3 class="mb-0">Nuevo usuario</h3> --}}
                        <h6 class="heading-small text-muted mb-4">Información</h6>
					</div>
                    <!-- Light table -->
                    <div class="card-body">
                        <form action="{{ route('notes.update', ['id' => $note->id]) }}" method="POST" class="needs-validation" enctype="multipart/form-data" novalidate>
                            {{ csrf_field() }}
                            <input type="hidden" name="_method" value="PUT">
                            <div class="pl-lg-4">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <input type="hidden" name="preset_file" id="fileWithPreview-preset_file" value="{{ asset($note->cover) }}">
                                        <div class="custom-file-container fileWithPreview-elms" id="fileWithPreview" data-upload-id="fileWithPreview">
                                            <label>*Portada <a href="javascript:void(0)" class="custom-file-container__image-clear" title="Clear Image">&times;</a></label>
                                            <label class="custom-file-container__custom-file" >
                                                <input type="file" name="cover" class="custom-file-container__custom-file__custom-file-input" accept="*" aria-label="Escoger archivo">
                                                <input type="hidden" name="MAX_FILE_SIZE" value="10485760" />
                                                <span class="custom-file-container__custom-file__custom-file-control"></span>
                                            </label>
                                            <small>Medida ótipma 940px*650px</small>
                                            <div class="custom-file-container__image-preview"></div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label class="form-control-label" for="title">* Título</label>
                                            <input type="text" name="title" id="title" class="form-control" required autocomplete="off" value="{{ (old('title')) ? old('title') : $note->translates()->where('locale', 'es')->first()->title }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label class="form-control-label" for="category_id">* Categoría</label>
                                            <select name="category_id" id="category_id" class="form-control" required>
                                                @if ((isset($categories)) && (count($categories) > 0))
                                                    <option value="">Selecciona una opción</option>
                                                    @foreach ($categories as $category)
                                                        <option {{ ($note->category_id == $category->id) ? 'selected' : '' }} value="{{ $category->id }}">{{ $category->translates()->where('locale', 'es')->first()->title }}</option>
                                                    @endforeach
                                                @else
                                                    <option value="">Sin resultados</option>
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label class="form-control-label" for="content">* Contenido</label>
                                            <textarea name="content" required id="content" class="trumbowyg-panel" cols="30" rows="10">{{ $note->translates()->where('locale', 'es')->first()->content }}</textarea>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label class="form-control-label" for="metas">Tags</label>
                                            <input type="text" name="metas" id="metas" class="form-control" autocomplete="off" data-role="tagsinput" value="{{ (old('metas')) ? old('metas') : $note->translates()->where('locale', 'es')->first()->metas }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="form-control-label" for="display">Público orientado</label>
                                            <select name="display" id="display" class="form-control" required>
                                                <option {{ ($note->display == 'web') ? 'selected' : '' }} value="web">Web</option>
                                                <option {{ ($note->display == 'seller') ? 'selected' : '' }} value="seller">Vendedores</option>
                                                <option {{ ($note->display == 'both') ? 'selected' : '' }} value="both">Ambos</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="form-control-label" for="status">Publicación</label>
                                            <select name="status" id="status" class="form-control" required>
                                                <option {{ ($note->status == 'visible') ? 'selected' : '' }} value="visible">Visible</option>
                                                <option {{ ($note->status == 'hidden') ? 'selected' : '' }} value="hidden">Oculto</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="row">
                                    <div class="col-lg-12 text-center">
                                        @if (Auth::guard('web')->user()->role == 'admin')
                                            <button class="btn btn-default">Confirmar</button>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
				</div>
			</div>
        </div>
    </div>
@endsection