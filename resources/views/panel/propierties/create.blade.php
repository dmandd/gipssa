@extends('layouts.panel.app')
@section('content')
    <!-- Header -->
    @include('include.panel.header')
    <script>
        const autocomplete_options = @json($contacts);
    </script>
    <!-- Page content -->
    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col">
				<div class="card">
				<!-- Card header -->
					<div class="card-header border-0">
                        {{-- <h3 class="mb-0">Nuevo usuario</h3> --}}
                        <h6 class="heading-small text-muted mb-4">Información General</h6>
					</div>
                    <!-- Light table -->
                    <div class="card-body">
                        <form action="{{ route('panel.propierties.store') }}" method="POST" class="needs-validation" enctype="multipart/form-data" novalidate>
                            {{ csrf_field() }}
                            <div class="pl-lg-4">
                                <h5 class="heading-small text-muted mb-4">Información contacto</h5>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <input type="hidden" name="contact[admin_id]" value="{{ request()->user()->id }}">
                                        <input type="hidden" id="contact_id" name="contact[id]" value="">
                                        <div class="form-group" style="display:block!important">
                                            <label class="form-control-label" for="contact[name]">* Nombre</label>
                                            <input type="text" name="contact[name]" id="contact[name]" data-autocomplete='@json($contacts)' data-autocomplete-name="name" class="form-control autocomplete nombre" required autocomplete="off">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="form-control-label" for="contact[type]">* Tipo</label>
                                            <select name="contact[type]" id="contact[type]" class="form-control" required>
                                                <option value="">Selecciona una opción</option>
                                                <option value="propietario">Propietario</option>
                                                <option value="abogado">Abogado</option>
                                                <option value="representante">Representante</option>
                                                <option value="otro">Otro</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group" style="display:block!important">
                                            <label class="form-control-label" for="contact[phone]">* Teléfono</label>
                                            <input type="text" name="contact[phone]" id="contact[phone]" class="form-control" required autocomplete="off" value="{{ old('phone') }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group" style="display:block!important">
                                            <label class="form-control-label" for="contact[email]">* Correo</label>
                                            <input type="email" name="contact[email]" id="contact[email]" class="form-control" required autocomplete="off" value="{{ old('email') }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="form-control-label" for="contact[address]">* Dirección</label>
                                            <input type="text" name="contact[address]" id="contact[address]" class="form-control" required autocomplete="off" value="{{ old('address') }}">
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <h5 class="heading-small text-muted mb-4">Producto / Operación</h5>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="dropzone-cover" class="form-control-label">* Portada</label>
                                            <input class="form-control unfocus" type="text" name="cover" id="cover" value="{{ old('cover') }}" data-asset="{{ asset('') }}" required>
                                            <a href="#modal-media" data-reference="dropzone-cover" data-dropzone="image" data-toggle="modal" data-target="#modal-media" class="btn btn-default float-right mt-1">Escoger del multimedia</a>
                                            <div class="clearfix"></div>
                                            <div id="dropzone-cover" data-route="{{ route('images.store') }}" data-target="#cover" class="dropzone" style="border:0px">
                                                <div class="dz-default dz-message">Seleccionar archivo</div>
                                            </div>
                                        </div>
                                    </div>
                                    {{-- <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="code">* Código</label>
                                            <input type="text" name="code" id="code" class="form-control" required autocomplete="off" value="{{ old('code') }}">
                                        </div>
                                    </div> --}}
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="name">* Nombre del desarrollo</label>
                                            <input type="text" name="name" id="name" class="form-control" required autocomplete="off" value="{{ old('name') }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="type">* Tipo</label>
                                            <select name="type" id="type" class="form-control" required>
                                                <option value="">Selecciona una opción</option>
                                                @if (($types) && ($types->count() > 0))
                                                    @foreach ($types as $type)
                                                        <option value="{{ $type->id }}">{{ $type->name }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="operation">* Operación</label>
                                            <select name="operation" id="operation" class="form-control" required>
                                                <option value="">Selecciona una opción</option>
                                                @if (($operations) && ($operations->count() > 0))
                                                    @foreach ($operations as $operation)
                                                        <option value="{{ $operation->id }}">{{ $operation->name }}</option>
                                                    @endforeach
                                                @endif
                                                {{-- <option value="venta">Venta</option>
                                                <option value="traspaso">Traspaso</option>
                                                <option value="renta_venta">Renta para venta</option>
                                                <option value="renta">Renta</option>
                                                <option value="otro">Otro</option> --}}
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="tag_id">Etiquetas</label>
                                            <select name="tag_id" id="tag_id" class="form-control">
                                                <option value="">Selecciona una opción</option>
                                                @if ($tags)
                                                    @foreach ($tags as $tag)
                                                        <option value="{{ $tag->id }}">{{ $tag->name }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                    {{-- <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="address">* Dirección</label>
                                            <input type="text" name="address" id="address" class="form-control" required autocomplete="off" value="{{ old('address') }}">
                                        </div>
                                    </div> --}}
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="zone_id">* Zona</label>
                                            <select name="zone_id" id="zone_id" class="form-control" required>
                                                <option value="">Selecciona una opción</option>
                                                @if ($zones)
                                                    @foreach ($zones as $zone)
                                                        <option value="{{ $zone->id }}">{{ $zone->name }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                            
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="colony">* Colonia</label>
                                            <input type="text" name="colony" id="colony" class="form-control" required autocomplete="off" value="{{ old('colony') }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="street">* Calle/Avenida</label>
                                            <input type="text" name="street" id="street" class="form-control" required autocomplete="off" value="{{ old('street') }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="internal_number">* Número interior</label>
                                            <input type="text" name="internal_number" id="internal_number" class="form-control" required autocomplete="off" value="{{ old('internal_number') }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="external_number">* Número exterior</label>
                                            <input type="text" name="external_number" id="external_number" class="form-control" required autocomplete="off" value="{{ old('external_number') }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="accross">* Cruzamiento</label>
                                            <input type="text" name="accross" id="accross" class="form-control" required autocomplete="off" value="{{ old('accross') }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="location">Ubicación</label>
                                            <input type="text" name="location" id="location" class="form-control" autocomplete="off" value="{{ old('location') }}">
                                            <small>Link google maps</small>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="bedrooms">Número de habitaciones</label>
                                            <input type="text" name="bedrooms" id="bedrooms" class="form-control" autocomplete="off" value="{{ old('bedrooms') }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="bathrooms">Número de baños</label>
                                            <input type="text" name="bathrooms" id="bathrooms" class="form-control" autocomplete="off" value="{{ old('bathrooms') }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="outstanding">* Destacado</label>
                                            <select name="outstanding" id="outstanding" class="form-control" required>
                                                <option value="0">No</option>
                                                <option value="1">Sí</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label class="form-control-label" for="references">Referencias</label>
                                            <textarea class="form-control trumbowyg-panel" name="references" id="references" cols="30" rows="10">{{ old('references') }}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <h5 class="heading-small text-muted mb-4">Legal</h5>
                                <div class="row">
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="terrain_m2">* M&sup2; Terreno</label>
                                            <input type="text" name="terrain_m2" id="terrain_m2" class="form-control" required autocomplete="off" value="{{ old('terrain_m2') }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="front_m2">* M&sup2; Frente</label>
                                            <input type="text" name="front_m2" id="front_m2" class="form-control" required autocomplete="off" value="{{ old('front_m2') }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="depth_m2">* M&sup2; Fondo</label>
                                            <input type="text" name="depth_m2" id="depth_m2" class="form-control" required autocomplete="off" value="{{ old('depth_m2') }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="build_m2">* M&sup2; Construcción</label>
                                            <input type="text" name="build_m2" id="build_m2" class="form-control" required autocomplete="off" value="{{ old('build_m2') }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="year">* Año de construcción</label>
                                            <input type="text" name="year" id="year" class="form-control" required autocomplete="off" value="{{ old('year') }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="update">Última remodelación</label>
                                            <input type="text" name="update" id="update" class="form-control" autocomplete="off" value="{{ old('update') }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="level">Niveles</label>
                                            <input type="text" name="level" id="level" class="form-control" autocomplete="off" value="{{ old('level') }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="usage">* Uso</label>
                                            <select name="usage" id="usage" class="form-control" required>
                                                <option value="">Selecciona una opción</option>
                                                <option value="residencial">Residencial</option>
                                                <option value="comercial">Comercial</option>
                                                <option value="agrario">Agrario</option>
                                                <option value="industrial">Industrial</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="paper">* Escrituras</label>
                                            <select name="paper" id="paper" class="form-control" required>
                                                <option value="">Selecciona una opción</option>
                                                <option value="0">No</option>
                                                <option value="1">Sí</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="plano_catastral">* Plano catastral</label>
                                            <select name="plano_catastral" id="plano_catastral" class="form-control" required>
                                                <option value="">Selecciona una opción</option>
                                                <option value="0">No</option>
                                                <option value="1">Sí</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="cedula_catastral">* Cedula catastral</label>
                                            <select name="cedula_catastral" id="cedula_catastral" class="form-control" required>
                                                <option value="">Selecciona una opción</option>
                                                <option value="0">No</option>
                                                <option value="1">Sí</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="libertad_gravamen">* Libertad de gravamen</label>
                                            <select name="libertad_gravamen" id="libertad_gravamen" class="form-control" required>
                                                <option value="">Selecciona una opción</option>
                                                <option value="0">No</option>
                                                <option value="1">Sí</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="hipoteca">* Hipóteca</label>
                                            <select name="hipoteca" id="hipoteca" class="form-control" required>
                                                <option value="">Selecciona una opción</option>
                                                <option value="0">No</option>
                                                <option value="1">Sí</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="monto_hipoteca">*Monto Hipóteca</label>
                                            <input type="text" name="monto_hipoteca" id="monto_hipoteca" class="form-control" autocomplete="off" value="{{ old('monto_hipoteca') }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="contrato_renta">* Contrato de renta</label>
                                            <select name="contrato_renta" id="contrato_renta" class="form-control" required>
                                                <option value="">Selecciona una opción</option>
                                                <option value="0">No</option>
                                                <option value="1">Sí</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="deuda">Deudas</label>
                                            <input type="text" name="deuda" id="deuda" class="form-control" autocomplete="off" value="{{ old('deuda') }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="habitable">* Habitable</label>
                                            <select name="habitable" id="habitable" class="form-control" required>
                                                <option value="">Selecciona una opción</option>
                                                <option value="0">No</option>
                                                <option value="1">Sí</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="habitable_porc">* Defina porcentaje</label>
                                            <input type="text" name="habitable_porc" id="habitable_porc" class="form-control" required autocomplete="off" value="{{ old('habitable_porc') }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="delivery_time">* Tiempo de entrega</label>
                                            <input type="text" name="delivery_time" id="delivery_time" class="form-control" required autocomplete="off" value="{{ old('delivery_time') }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="accepted_credit">* Créditos aceptados</label>
                                            <input type="text" name="accepted_credit" id="accepted_credit" class="form-control" required autocomplete="off" value="{{ old('accepted_credit') }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label class="form-control-label" for="habitable_notes">Notas</label>
                                            <textarea class="form-control trumbowyg-panel" name="habitable_notes" id="habitable_notes" cols="30" rows="10">{{ old('habitable_notes') }}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <h5 class="heading-small text-muted mb-4">Finanzas</h5>
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="form-control-label" for="first_price">* Precio inicial</label>
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">$</span>
                                                </div>
                                                <input type="text" name="first_price" id="first_price" class="form-control" required autocomplete="off" value="{{ old('first_price') }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="form-control-label" for="last_price">* Precio final</label>
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">$</span>
                                                </div>
                                                <input type="text" name="last_price" id="last_price" class="form-control" required autocomplete="off" value="{{ old('last_price') }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="form-control-label" for="other_price">Otro</label>
                                            <input type="text" name="other_price" id="other_price" class="form-control" autocomplete="off" value="{{ old('other_price') }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="form-control-label" for="maintenance_quote">Cuota de mantenimiento</label>
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">$</span>
                                                </div>
                                                <input type="text" name="maintenance_quote" id="maintenance_quote" class="form-control" autocomplete="off" value="{{ old('maintenance_quote') }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="form-control-label" for="extra_quote">Costos extras</label>
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">$</span>
                                                </div>
                                                <input type="text" name="extra_quote" id="extra_quote" class="form-control" autocomplete="off" value="{{ old('extra_quote') }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="form-control-label" for="other_quote">Otro</label>
                                            <input type="text" name="other_quote" id="other_quote" class="form-control" autocomplete="off" value="{{ old('other_quote') }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="form-control-label" for="comission_type">* Tipo de comisión</label>
                                            <select name="comission_type" id="comission_type" class="form-control" required>
                                                <option value="">Selecciona una opción</option>
                                                <option value="porcentaje">Porcentaje</option>
                                                <option value="cantidad">Cantidad</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="form-control-label" for="comission">* Cantidad Comisión</label>
                                            <input type="text" name="comission" id="comission" class="form-control" required autocomplete="off" value="{{ old('comission') }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="form-control-label" for="comission_share">Comisión compartida (%)</label>
                                            <input type="text" name="comission_share" id="comission_share" class="form-control" autocomplete="off" value="{{ old('comission_share') }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label class="form-control-label" for="comission_note">Notas comisión</label>
                                            <textarea name="comission_note" id="comission_note" cols="30" rows="10" class="form-control trumbowyg-panel">{{ old('comission_note') }}</textarea>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="form-control-label" for="enganche_type">*Tipo de enganche</label>
                                            <select name="enganche_type" id="enganche_type" class="form-control" required>
                                                <option value="">Selecciona una opción</option>
                                                <option value="porcentaje">Porcentaje</option>
                                                <option value="cantidad">Cantidad</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="form-control-label" for="enganche">*Enganche</label>
                                            <input type="text" name="enganche" id="enganche" class="form-control" autocomplete="off" value="{{ old('enganche') }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label class="form-control-label" for="enganche_note">Notas enganche</label>
                                            <textarea  name="enganche_note" id="enganche_note" cols="30" rows="10" class="form-control trumbowyg-panel">{{ old('enganche_note') }}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                {{-- <h5>Áreas</h5>
                                <div class="row">
                                    @if ((isset($areas)) && (count($areas) > 0))
                                        @foreach ($areas as $area)
                                            <div class="col-12 mt-3">
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" id="{{ $area->id }}" name="area[]" value="{{ $area->id }}">
                                                    <label class="custom-control-label" for="{{ $area->id }}"><h4>{{ $area->name }}</h4></label>
                                                </div>
                                                @if ($area->equipment())
                                                    <div class="row">
                                                        @foreach ($area->equipment() as $item)
                                                            <div class="col-3">
                                                                <div class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" id="{{ $item->id }}" name="equipment[{{ $area->id }}]" value="{{ $item->id }}">
                                                                    <label class="custom-control-label" for="{{ $item->id }}"><h4>{{ $item->name }}</h4></label>
                                                                </div>  
                                                            </div>
                                                        @endforeach
                                                    </div>
                                                @endif
                                            </div>
                                        @endforeach
                                    @endif
                                </div> --}}
                            </div>
                            <div class="card-footer">
                                <div class="row">
                                    <div class="col-lg-12 text-center">
                                        <button class="btn btn-default">Confirmar</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
				</div>
			</div>
        </div>
    </div>
    @include('include.panel.media')
@endsection
@push('js')
    <script src="{{ asset('panel/js/autocomplete.js') }}"></script>
@endpush