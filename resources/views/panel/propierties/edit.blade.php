@extends('layouts.panel.app')
@section('content')
    <!-- Header -->
    @include('include.panel.header')
    <style>
        #dropzone-gallery{overflow-x:scroll;}
    </style>
    <!-- Page content -->
    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col">
				<div class="card">
				<!-- Card header -->
					<div class="card-header border-0">
                        {{-- <h3 class="mb-0">Nuevo usuario</h3> --}}
                        <div class="row">
                            <div class="col-8">
                                <h6 class="heading-small text-muted mb-4">Información General</h6>
                            </div>
                            <div class="col-4 text-right">
                                <div class="dropdown">
                                    <a href="#!" class="btn btn-default" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Opciones</a>
                                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow" style="">
                                        <a class="dropdown-item" href="{{ route('panel.areas.index', ['propiedade' => $property->id]) }}">Áreas</a>
                                    </div>
                                </div>
                            </div>
                        </div>
					</div>
                    <!-- Light table -->
                    <div class="card-body">
                        <form action="{{ route('panel.propierties.update', ['propiedade' => $property->id]) }}" method="POST" class="needs-validation" enctype="multipart/form-data" novalidate>
                            {{ csrf_field() }}
                            <input type="hidden" name="_method" value="PUT">
                            <div class="pl-lg-4">
                                @can(PermissionKey::Property['permissions']['contact']['name'])
                                    <h5 class="heading-small text-muted mb-4">Información contacto</h5>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label class="form-control-label" for="contact[name]">* Nombre</label>
                                                <input type="text" name="contact[name]" id="contact[name]" class="form-control" required autocomplete="off" value="{{ $property->contact->name }}" disabled>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label class="form-control-label" for="contact[type]">* Tipo</label>
                                                <select name="contact[type]" id="contact[type]" class="form-control" disabled>
                                                    <option value="">Selecciona una opción</option>
                                                    <option value="propietario" {{ ($property->contact->type == 'propietario') ? 'selected' : '' }}>Propietario</option>
                                                    <option value="abogado" {{ ($property->contact->type == 'abogado') ? 'selected' : '' }}>Abogado</option>
                                                    <option value="representante" {{ ($property->contact->type == 'representante') ? 'selected' : '' }}>Representante</option>
                                                    <option value="otro" {{ ($property->contact->type == 'otro') ? 'selected' : '' }}>Otro</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label class="form-control-label" for="contact[phone]">* Teléfono</label>
                                                <input type="text" name="contact[phone]" id="contact[phone]" class="form-control" disabled autocomplete="off" value="{{ $property->contact->phone }}">
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label class="form-control-label" for="contact[email]">* Correo</label>
                                                <input type="email" name="contact[email]" id="contact[email]" class="form-control" disabled autocomplete="off" value="{{ $property->contact->email }}">
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label class="form-control-label" for="address">* Dirección</label>
                                                <input type="text" name="address" id="address" class="form-control" disabled autocomplete="off" value="{{ $property->contact->address }}">
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                @endcan
                                <h5 class="heading-small text-muted mb-4">Producto / Operación</h5>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="dropzone-cover" class="form-control-label">* Portada</label>
                                            <input class="form-control unfocus" type="text" name="cover" id="cover" value="{{ (old('cover')) ? old('cover') : $property->cover }}" data-asset="{{ asset('') }}" required>
                                            <a href="#modal-media" data-dropzone="image" data-toggle="modal" data-target="#modal-media" class="btn btn-default float-right mt-1">Escoger del multimedia</a>
                                            <div class="clearfix"></div>
                                            <div id="dropzone-cover" data-route="{{ route('images.store') }}" data-target="#cover" class="dropzone" style="border:0px">
                                                <div class="dz-default dz-message">Seleccionar archivo</div>
                                            </div>
                                        </div>
                                    </div>
                                    @if(request()->user()->can(PermissionKey::Property['permissions']['update_code']['name']))
                                        <div class="col-lg-3">
                                            <div class="form-group">
                                                <label class="form-control-label" for="code">* Código</label>
                                                <input type="text" name="code" id="code" class="form-control" required autocomplete="off" value="{{ (old('code')) ? old('code') : $property->code }}">
                                            </div>
                                        </div>    
                                    @else
                                        <div class="col-lg-3">
                                            <div class="form-group">
                                                <label class="form-control-label" for="code">* Código</label>
                                                <input type="text" name="code" disabled id="code" class="form-control" required autocomplete="off" value="{{ (old('code')) ? old('code') : $property->code }}">
                                            </div>
                                        </div>
                                    @endif
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="name">* Nombre del desarrollo</label>
                                            <input type="text" name="name" id="name" class="form-control" required autocomplete="off" value="{{ (old('name')) ? old('name') : $property->name }}">
                                        </div>
                                    </div>
                                    @can(PermissionKey::Property['permissions']['assign']['name'])
                                        <div class="col-lg-3">
                                            <div class="form-group">
                                                <label class="form-control-label" for="admin_id">* Responsable</label>
                                                <select name="admin_id" id="admin_id" class="form-control" required>
                                                    <option value="">Selecciona una opción</option>
                                                    @if (($admins) && (count($admins) > 0))
                                                        @foreach ($admins as $admin)
                                                            <option value="{{ $admin->id }}" {{ ($admin->id == $property->admin_id) ? 'selected' : '' }}>{{ $admin->name }}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                    @endcan
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="type">* Tipo</label>
                                            <select name="type" id="type" class="form-control" required>
                                                <option value="">Selecciona una opción</option>
                                                @if (($types) && ($types->count() > 0))
                                                    @foreach ($types as $type)
                                                        <option value="{{ $type->id }}" {{ ($property->type == $type->id) ? 'selected' : '' }}>{{ $type->name }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="operation">* Operación</label>
                                            <select name="operation" id="operation" class="form-control" required>
                                                <option value="">Selecciona una opción</option>
                                                @if (($operations) && ($operations->count() > 0))
                                                    @foreach ($operations as $operation)
                                                        <option value="{{ $operation->id }}" {{ ($property->operation == $operation->id) ? 'selected' : '' }}>{{ $operation->name }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="tag_id">Etiquetas</label>
                                            <select name="tag_id" id="tag_id" class="form-control">
                                                <option value="">Selecciona una opción</option>
                                                @if ($tags)
                                                    @foreach ($tags as $tag)
                                                        <option value="{{ $tag->id }}" {{ ($property->tag_id == $tag->id) ? 'selected' : '' }}>{{ $tag->name }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                    {{-- <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="address">* Dirección</label>
                                            <input type="text" name="address" id="address" class="form-control" autocomplete="off" value="{{ (old('address')) ? old('address') : $property->address }}">
                                        </div>
                                    </div> --}}
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="zone_id">* Zona</label>
                                            <select name="zone_id" id="zone_id" class="form-control" required>
                                                <option value="">Selecciona una opción</option>
                                                @if ($zones)
                                                    @foreach ($zones as $zone)
                                                        <option value="{{ $zone->id }}" {{ ($property->zone_id == $zone->id) ? 'selected' : '' }}>{{ $zone->name }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                            
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="colony">* Colonia</label>
                                            <input type="text" name="colony" id="colony" class="form-control" required autocomplete="off" value="{{ (old('colony')) ? old('colony') : $property->colony }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="street">* Calle/Avenida</label>
                                            <input type="text" name="street" id="street" class="form-control" required autocomplete="off" value="{{ (old('street')) ? old('street') : $property->street }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="internal_number">Número interior</label>
                                            <input type="text" name="internal_number" id="internal_number" class="form-control" autocomplete="off" value="{{ (old('internal_number')) ? old('internal_number') : $property->internal_number }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="external_number">* Número exterior</label>
                                            <input type="text" name="external_number" id="external_number" class="form-control" required autocomplete="off" value="{{ (old('external_number')) ? old('external_number') : $property->external_number }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="accross">* Cruzamiento</label>
                                            <input type="text" name="accross" id="accross" class="form-control" required autocomplete="off" value="{{ (old('accross')) ? old('accross') : $property->accross }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="location">Ubicación</label>
                                            <input type="text" name="location" id="location" class="form-control" autocomplete="off" value="{{ (old('location')) ? old('location') : $property->location }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="outstanding">* Destacado</label>
                                            <select name="outstanding" id="outstanding" class="form-control" required>
                                                <option value="0" {{ ($property->outstanding == 0) ? 'selected' : '' }}>No</option>
                                                <option value="1" {{ ($property->outstanding == 1) ? 'selected' : '' }}>Sí</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="bedrooms">Número de habitaciones</label>
                                            <input type="text" name="bedrooms" id="bedrooms" class="form-control" autocomplete="off" value="{{ (old('bedrooms')) ? old('bedrooms') : $property->bedrooms }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="bathrooms">Número de baños</label>
                                            <input type="text" name="bathrooms" id="bathrooms" class="form-control" autocomplete="off" value="{{ (old('bathrooms')) ? old('bathrooms') : $property->bathrooms }}">
                                        </div>
                                    </div>
                                    @can(PermissionKey::Property['permissions']['publish']['name'])
                                        <div class="col-lg-3">
                                            <div class="form-group">
                                                <label class="form-control-label" for="status">Publicación</label>
                                                <select name="status" id="status" class="form-control">
                                                    <option value="">Selecciona una opción</option>
                                                    <option value="draft" {{ ($property->status == 'draft') ? 'selected' : '' }}>Borrador</option>
                                                    <option value="visible" {{ ($property->status == 'visible') ? 'selected' : '' }}>Publicado</option>
                                                    <option value="hidden" {{ ($property->status == 'hidden') ? 'selected' : '' }}>Oculto</option>
                                                </select>
                                            </div>
                                        </div>
                                    @endcan
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label class="form-control-label" for="references">Referencias</label>
                                            <textarea class="form-control trumbowyg-panel" name="references" id="references" cols="30" rows="10">{{ (old('references')) ? old('references') : $property->references }}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <h5 class="heading-small text-muted mb-4">Legal</h5>
                                <div class="row">
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="terrain_m2">* M&sup2; Terreno</label>
                                            <input type="text" name="terrain_m2" id="terrain_m2" class="form-control" required autocomplete="off" value="{{ (old('terrain_m2')) ? old('terrain_m2') : $property->terrain_m2 }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="front_m2">* M&sup2; Frente</label>
                                            <input type="text" name="front_m2" id="front_m2" class="form-control" required autocomplete="off" value="{{ (old('front_m2')) ? old('front_m2') : $property->front_m2 }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="depth_m2">* M&sup2; Fondo</label>
                                            <input type="text" name="depth_m2" id="depth_m2" class="form-control" required autocomplete="off" value="{{ (old('depth_m2')) ? old('depth_m2') : $property->depth_m2 }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="build_m2">* M&sup2; Construcción</label>
                                            <input type="text" name="build_m2" id="build_m2" class="form-control" required autocomplete="off" value="{{ (old('build_m2')) ? old('build_m2') : $property->build_m2 }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="year">* Año de construcción</label>
                                            <input type="text" name="year" id="year" class="form-control" required autocomplete="off" value="{{ (old('year')) ? old('year') : $property->year }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="update">Última remodelación</label>
                                            <input type="text" name="update" id="update" class="form-control" autocomplete="off" value="{{ (old('update')) ? old('update') : $property->update }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="level">Niveles</label>
                                            <input type="text" name="level" id="level" class="form-control" autocomplete="off" value="{{ (old('level')) ? old('level') : $property->level }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="usage">* Uso</label>
                                            <select name="usage" id="usage" class="form-control" required>
                                                <option value="">Selecciona una opción</option>
                                                <option value="residencial" {{ ($property->usage == 'residencial') ? 'selected' : '' }}>Residencial</option>
                                                <option value="comercial" {{ ($property->usage == 'comercial') ? 'selected' : '' }}>Comercial</option>
                                                <option value="agrario" {{ ($property->usage == 'agrario') ? 'selected' : '' }}>Agrario</option>
                                                <option value="industrial" {{ ($property->usage == 'industrial') ? 'selected' : '' }}>Industrial</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="paper">* Escrituras</label>
                                            <select name="paper" id="paper" class="form-control" required>
                                                <option value="">Selecciona una opción</option>
                                                <option value="0" {{ ($property->paper == '0') ? 'selected' : '' }}>No</option>
                                                <option value="1" {{ ($property->paper == '1') ? 'selected' : '' }}>Sí</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="plano_catastral">* Plano catastral</label>
                                            <select name="plano_catastral" id="plano_catastral" class="form-control" required>
                                                <option value="">Selecciona una opción</option>
                                                <option value="0" {{ ($property->plano_catastral == '0') ? 'selected' : '' }}>No</option>
                                                <option value="1" {{ ($property->plano_catastral == '1') ? 'selected' : '' }}>Sí</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="cedula_catastral">* Cedula catastral</label>
                                            <select name="cedula_catastral" id="cedula_catastral" class="form-control" required>
                                                <option value="">Selecciona una opción</option>
                                                <option value="0" {{ ($property->cedula_catastral == '0') ? 'selected' : '' }}>No</option>
                                                <option value="1" {{ ($property->cedula_catastral == '1') ? 'selected' : '' }}>Sí</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="libertad_gravamen">* Libertad de gravamen</label>
                                            <select name="libertad_gravamen" id="libertad_gravamen" class="form-control" required>
                                                <option value="">Selecciona una opción</option>
                                                <option value="0" {{ ($property->libertad_gravamen == '0') ? 'selected' : '' }}>No</option>
                                                <option value="1" {{ ($property->libertad_gravamen == '1') ? 'selected' : '' }}>Sí</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="hipoteca">* Hipóteca</label>
                                            <select name="hipoteca" id="hipoteca" class="form-control" required>
                                                <option value="">Selecciona una opción</option>
                                                <option value="0" {{ ($property->hipoteca == '0') ? 'selected' : '' }}>No</option>
                                                <option value="1" {{ ($property->hipoteca == '1') ? 'selected' : '' }}>Sí</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="monto_hipoteca">Monto Hipóteca</label>
                                            <input type="text" name="monto_hipoteca" id="monto_hipoteca" class="form-control" autocomplete="off" value="{{ (old('monto_hipoteca')) ? old('monto_hipoteca') : $property->monto_hipoteca }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="contrato_renta">* Contrato de renta</label>
                                            <select name="contrato_renta" id="contrato_renta" class="form-control" required>
                                                <option value="">Selecciona una opción</option>
                                                <option value="0" {{ ($property->contrato_renta == '0') ? 'selected' : '' }}>No</option>
                                                <option value="1" {{ ($property->contrato_renta == '1') ? 'selected' : '' }}>Sí</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="deuda">Deudas</label>
                                            <input type="text" name="deuda" id="deuda" class="form-control" autocomplete="off" value="{{ (old('deuda')) ? old('deuda') : $property->deuda }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="habitable">* Habitable</label>
                                            <select name="habitable" id="habitable" class="form-control" required>
                                                <option value="">Selecciona una opción</option>
                                                <option value="0" {{ ($property->habitable == '0') ? 'selected' : '' }}>No</option>
                                                <option value="1" {{ ($property->habitable == '1') ? 'selected' : '' }}>Sí</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="habitable_porc">* Defina porcentaje habitable</label>
                                            <input type="text" name="habitable_porc" id="habitable_porc" class="form-control" required autocomplete="off" value="{{ (old('habitable_porc')) ? old('habitable_porc') : $property->habitable_porc }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="delivery_time">* Tiempo de entrega</label>
                                            <input type="text" name="delivery_time" id="delivery_time" class="form-control" required autocomplete="off" value="{{ (old('delivery_time')) ? old('delivery_time') : $property->delivery_time }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="accepted_credit">* Créditos aceptados</label>
                                            <input type="text" name="accepted_credit" id="accepted_credit" class="form-control" required autocomplete="off" value="{{ (old('accepted_credit')) ? old('accepted_credit') : $property->accepted_credit }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label class="form-control-label" for="habitable_notes">Notas</label>
                                            <textarea class="form-control trumbowyg-panel" name="habitable_notes" id="habitable_notes" cols="30" rows="10">{{ (old('habitable_notes')) ? old('habitable_notes') : $property->habitable_notes }}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <h5 class="heading-small text-muted mb-4">Finanzas</h5>
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="form-control-label" for="first_price">* Precio inicial</label>
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">$</span>
                                                </div>
                                                <input type="text" name="first_price" id="first_price" class="form-control" required autocomplete="off" value="{{ (old('first_price')) ? old('first_price') : $property->first_price }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="form-control-label" for="last_price">* Precio final</label>
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">$</span>
                                                </div>
                                                <input type="text" name="last_price" id="last_price" class="form-control" required autocomplete="off" value="{{ (old('last_price')) ? old('last_price') : $property->last_price }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="form-control-label" for="other_price">Otro</label>
                                            <input type="text" name="other_price" id="other_price" class="form-control" autocomplete="off" value="{{ (old('other_price')) ? old('other_price') : $property->other_price }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="form-control-label" for="maintenance_quote">Cuota de mantenimiento</label>
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">$</span>
                                                </div>
                                                <input type="text" name="maintenance_quote" id="maintenance_quote" class="form-control" autocomplete="off" value="{{ (old('maintenance_quote')) ? old('maintenance_quote') : $property->maintenance_quote }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="form-control-label" for="extra_quote">Costos extras</label>
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">$</span>
                                                </div>
                                                <input type="text" name="extra_quote" id="extra_quote" class="form-control" autocomplete="off" value="{{ (old('extra_quote')) ? old('extra_quote') : $property->extra_quote }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="form-control-label" for="other_quote">Otro</label>
                                            <input type="text" name="other_quote" id="other_quote" class="form-control" autocomplete="off" value="{{ (old('other_quote')) ? old('other_quote') : $property->other_quote }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="form-control-label" for="comission_type">* Tipo de comisión</label>
                                            <select name="comission_type" id="comission_type" class="form-control" required>
                                                <option value="">Selecciona una opción</option>
                                                <option value="porcentaje" {{ ($property->comission_type == 'porcentaje') ? 'selected' : '' }}>Porcentaje</option>
                                                <option value="cantidad" {{ ($property->comission_type == 'cantidad') ? 'selected' : '' }}>Cantidad</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="form-control-label" for="comission">* Cantidad Comisión</label>
                                            <input type="text" name="comission" id="comission" class="form-control" required autocomplete="off" value="{{ (old('comission')) ? old('comission') : $property->comission }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="form-control-label" for="comission_share">Comisión compartida (%)</label>
                                            <input type="text" name="comission_share" id="comission_share" class="form-control" autocomplete="off" value="{{ (old('comission_share')) ? old('comission_share') : $property->comission_share }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label class="form-control-label" for="comission_note">Notas Comisión</label>
                                            <textarea name="comission_note" id="comission_note" cols="30" rows="10" class="form-control trumbowyg-panel">{{ (old('comission_note')) ? old('comission_note') : $property->comission_note }}</textarea>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="form-control-label" for="enganche_type">*Tipo de enganche</label>
                                            <select name="enganche_type" id="enganche_type" class="form-control" required>
                                                <option value="">Selecciona una opción</option>
                                                <option value="porcentaje" {{ ($property->enganche_type == 'porcentaje') ? 'selected' : '' }}>Porcentaje</option>
                                                <option value="cantidad" {{ ($property->enganche_type == 'cantidad') ? 'selected' : '' }}>Cantidad</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="form-control-label" for="enganche">*Enganche</label>
                                            <input type="text" name="enganche" id="enganche" class="form-control" autocomplete="off" value="{{ (old('enganche')) ? old('enganche') : $property->enganche }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label class="form-control-label" for="enganche_note">Notas enganche</label>
                                            <textarea  name="enganche_note" id="enganche_note" cols="30" rows="10" class="form-control trumbowyg-panel">{{ (old('enganche_note')) ? old('enganche_note') : $property->enganche_note }}</textarea>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="dropzone-gallery" class="form-control-label">* Galería (Tamaño máximo por archivo 1MB)</label>
                                            <input class="form-control unfocus" type="hidden" name="gallery" id="gallery" data-current='@json($property->gallery()->toArray())' value="" data-asset="{{ asset('') }}">
                                            {{-- <a href="#modal-media" data-dropzone="image" data-toggle="modal" data-target="#modal-media" class="btn btn-default float-right mt-1">Escoger del multimedia</a> --}}
                                            <div class="clearfix"></div>
                                            <div id="dropzone-gallery" data-route="{{ route('images.store.multiple') }}" data-delete-route="{{ route('propierties.remove.image', ['id' => $property->id]) }}" data-target="#gallery" class="dropzone multiple" style="border:0px;flex-direction:row!important;">
                                                <div class="dz-default dz-message" style="width:100%;">Seleccionar archivo</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <h5 class="text-muted">Áreas</h5>
                                @if ($property->areas()->get()->count() > 0)
                                    <div class="accordion" id="accordionExample">
                                        @foreach ($property->areas()->get() as $item)
                                            <div class="card">
                                                <div class="card-header" id="heading-{{ $item->id }}" data-toggle="collapse" data-target="#collapse-{{ $item->id }}" aria-expanded="true" aria-controls="collapse-{{ $item->id }}">
                                                    <h4 class="mb-0">{{ $item->name }}</h4>
                                                </div>
                                                <div id="collapse-{{ $item->id }}" class="collapse show" aria-labelledby="{{ $item->id }}" data-parent="#heading-{{ $item->id }}" style="">
                                                    <div class="card-body">
                                                        <div class="row">
                                                            @if ($item->equipment()->count() > 0)
                                                                @foreach ($item->equipment() as $e)
                                                                    <div class="col-3">
                                                                        <div class="custom-control custom-radio">
                                                                            <input type="radio" class="custom-control-input" id="equipment-{{ $e->id }}" checked>
                                                                            <label class="custom-control-label" for="equipment-{{ $e->id }}">{{ $e->name }}</label>
                                                                        </div>
                                                                    </div>
                                                                @endforeach
                                                            @else
                                                                <div class="col-12">
                                                                    <blockquote>Sin equipamiento</blockquote>
                                                                </div>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                </div>
                                        @endforeach
                                    </div>
                                @else
                                    <div class="col-12 text-center">
                                        <h3>Sin información</h3>
                                        {{-- <a href="{{ route('panel.areas.create', ['propiedade' => $property->id]) }}" class="btn btn-primary mb-5">Agregar área</a> --}}
                                    </div>
                                @endif
                            </div>
                            <div class="card-footer">
                                <div class="row">
                                    @can(PermissionKey::Property['permissions']['update']['name'])
                                        <div class="col-lg-12 text-center">
                                            <button class="btn btn-default">Confirmar</button>
                                        </div>
                                    @endcan
                                </div>
                            </div>
                        </form>
                    </div>
				</div>
			</div>
        </div>
    </div>
    @include('include.panel.media')
@endsection