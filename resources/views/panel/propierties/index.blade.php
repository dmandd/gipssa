
<--GGN LIBRERIA PARA VISUALZIAR IMAGENES --!>


@extends('layouts.panel.app')
@section('content')
    <!-- Header -->
    @include('include.panel.header')
    <!-- Page content -->
    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col">
				<div class="card">
				<!-- Card header -->
					<div class="card-header border-0">
						{{-- <h3 class="mb-0">Light table</h3> --}}
					</div>
                    <!-- Light table -->
					<div class="table-responsive pb-3">
						<table class="table align-items-center table-flush" id="dataTable">
							<thead class="thead-light">
								<tr>
									<th width="20px">
                                        {{-- <input type="checkbox" class="" id="customCheck1"> --}}
                                        {{-- <div class="custom-control custom-checkbox">
                                            <label class="custom-control-label" for="customCheck1"></label>
                                        </div> --}}
                                    </th>
									<th scope="col" class="sort" data-sort="status">Código</th>
                                    
                                    <th>Imagen</th>

									<th scope="col" class="sort" data-sort="status">Nombre</th>
									<th scope="col" class="sort" data-sort="status">Tipo</th>
                                    <th scope="col" class="sort" data-sort="status">Operación</th>
                                    <th>Responsable</th>
                                    <th scope="col">Precio</th>
                                    <th>Fecha de creación</th>
									<th scope="col">Acciones</th>
								</tr>
							</thead>
							<tbody class="list">
                            
								@if ((isset($data)) && (count($data) > 0))
                                    @foreach ($data as $row)
                                        <tr>
                                            <td>
                                                <input type="checkbox" class="" id=""><span style="opacity:0;">{{ $row->id}}</span>
                                            </td>
                                            <td>{{ $row->code }}</td>
                                            
                                         
                                            
                                            <td> 
                                                
                                            <a href={{asset($row->cover)}} data-toggle="modal" data-target="#exampleModal{{ $loop->index }}"" >
                                            <img  height=60px src= {{ asset($row->cover)}}  > 
                                                <!-- Modal -->
                                                <div class="modal fade" id="exampleModal{{ $loop->index }}" tabindex="-1"
                                                            role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                        <div class="modal-dialog modal-dialog-scrollable" role="document">
                                                            <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLabel">{{$row->name}}</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <img width=450px src={{asset($row->cover)}} </img>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                                                
                                                            </div>
                                                            </div>
                                                        </div>
                                                        </div>

                                          <!-- Fin Modal -->

                                            
                                            </a>
                                        </td>
                                            

     

                                            

                                            <td>{{ $row->name }}</td>
                                            <td>{{ $row->type() }}</td>
                                            <td>{{ $row->operation() }}</td>
                                            <td>{{ $row->admin()->name }} {{ $row->admin()->last_name }}</td>
                                            <td>$ {{ (gettype($row->first_price) == 'string') ? $row->first_price : number_format($row->first_price, 2) }}</td>
                                            <td>{{ date('d/m/Y', strtotime($row->created_at)) }}</td>
                                            <td class="text-right">
                                                <div class="dropdown">
                                                    
                                                
                                                    <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <i class="fas fa-ellipsis-v"></i>

                                                     

                                                        
                                                    </a>

                                                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                                        @can(PermissionKey::Property['permissions']['edit']['name'])
                                                            <a class="dropdown-item" href="{{ route('panel.propierties.edit', ['propiedade' => $row->id]) }}">Ver Detalle</a>
                                                        @endcan
                                                        @can(PermissionKey::Property['permissions']['destroy']['name'])
                                                            <a class="dropdown-item btn-delete" data-axios-method="delete" data-route="{{ route('panel.propierties.destroy', ['propiedade' => $row->id]) }}" data-action="location.reload()" href="javascript:;">Eliminar</a>
                                                        @endcan
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>

                                       


                                    @endforeach
                                @endif
							</tbody>
						</table>
					</div>
				</div>
			</div>
        </div>
    </div>
@endsection


