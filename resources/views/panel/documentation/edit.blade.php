@extends('layouts.panel.app')
@section('content')
    <!-- Header -->
    @include('include.panel.header')
    <!-- Page content -->
    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col">
				<div class="card">
				<!-- Card header -->
					<div class="card-header border-0">
                        {{-- <h3 class="mb-0">Nuevo usuario</h3> --}}
                        <h6 class="heading-small text-muted mb-4">Información</h6>
					</div>
                    <!-- Light table -->
                    <div class="card-body">
                        <form action="{{ route('panel.documentation.update', ['desarrollo' => request()->desarrollo, 'documentacion' => $documentation->id]) }}" method="POST" class="needs-validation" novalidate>
                            {{ csrf_field() }}
                            <input type="hidden" name="_method" value="PUT">
                            <div class="pl-lg-4">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="form-control-label" for="name">* Nombre</label>
                                            <input type="text" name="name" id="name" class="form-control" required autocomplete="off" value="{{ (old('name')) ? old('name') : $documentation->name }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="form-control-label" for="name">* Publicación</label>
                                            <select name="status" id="status" class="form-control">
                                                <option value="visible" {{ ($documentation->status == 'visible') ? 'selected' : '' }}>Visible</option>
                                                <option value="hidden" {{ ($documentation->status == 'hidden') ? 'selected' : '' }}>Oculto</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" name="mostrar_condicion" {{ $documentation->mostrar_condicion?'checked':'' }} value="{{ $documentation->mostrar_condicion }}" id="mostrar_condicion">
                                            <label class="custom-control-label" for="mostrar_condicion">Mostrar en condición de compra</label>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" name="show_contract" value="{{$documentation->show_contract}}" {{ ($documentation->show_contract) ? 'checked' : '' }} id="show_contract">
                                                <label class="custom-control-label" for="show_contract">Mostrar en contrato</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" name="fijo" value="1" {{ ($documentation->fijo) ? 'checked' : '' }} id="fijo">
                                                <label class="custom-control-label" for="fijo">Fijar en apartado</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="row">
                                    <div class="col-lg-12 text-center">
                                        <button class="btn btn-default">Confirmar</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
				</div>
			</div>
        </div>
    </div>
@endsection