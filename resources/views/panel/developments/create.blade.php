@extends('layouts.panel.app')
@section('content')
    <!-- Header -->
    @include('include.panel.header')
    <!-- Page content -->
    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col">
				<div class="card">
				<!-- Card header -->
					<div class="card-header border-0">
                        {{-- <h3 class="mb-0">Nuevo usuario</h3> --}}
                        <h6 class="heading-small text-muted mb-4">Información</h6>
					</div>
                    <!-- Light table -->
                    <div class="card-body">
                        <form action="{{ route('panel.developments.store') }}" method="POST" class="needs-validation" novalidate>
                            {{ csrf_field() }}
                            <div class="pl-lg-4">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="dropzone-logo" class="form-control-label">* Logo</label>
                                            <input class="form-control unfocus" type="text" name="logo" id="logo" value="{{ old('logo') }}" data-asset="{{ asset('') }}" required>
                                            <a href="#modal-media" data-reference="dropzone-logo" data-dropzone="image" data-toggle="modal" data-target="#modal-media" class="btn btn-default float-right mt-1">Escoger del multimedia</a>
                                            <div class="clearfix"></div>
                                            <div id="dropzone-logo" data-route="{{ route('images.store') }}" data-target="#logo" class="dropzone" style="border:0px">
                                                <div class="dz-default dz-message">Seleccionar archivo</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group">
                                            <label class="form-control-label" for="name">* Nombre</label>
                                            <input type="text" name="name" id="name" class="form-control" required autocomplete="off" value="{{ old('name') }}">
                                        </div>
                                    </div>
                                    <div class="col-2">
                                        <div class="form-group">
                                            <label class="form-control-label" for="stage">Etapa</label>
                                            <input type="text" name="stage" id="stage" class="form-control" autocomplete="off" value="{{ old('stage') }}">
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label class="form-control-label" for="location">* Ubicación</label>
                                            <input type="text" name="location" id="location" class="form-control" required autocomplete="off" value="{{ old('location') }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="form-control-label" for="admin_id">Apoderado legal</label>
                                            <select name="admin_id" id="admin_id" class="form-control">
                                                <option value="">Selecciona una opción</option>
                                                @if ((isset($attorneys)) && (count($attorneys) > 0))
                                                    @foreach ($attorneys as $attorney)
                                                        <option value="{{ $attorney->id }}" >{{ $attorney->name}} {{ $attorney->last_name}} - {{ $attorney->rfc}}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="form-control-label" for="cadastral_planking_id">Tablaje catastral</label>
                                            <select name="cadastral_planking_id" id="cadastral_planking_id" class="form-control">
                                                <option value="">Selecciona una opción</option>
                                                @if ((isset($cadastralPlankings)) && (count($cadastralPlankings) > 0))
                                                    @foreach ($cadastralPlankings as $cadastralPlanking)
                                                        <option value="{{ $cadastralPlanking->id }}">{{ $cadastralPlanking->name }} No.{{ $cadastralPlanking->number }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="form-control-label" for="type">* Tipo</label>
                                            <select name="type" id="type" class="form-control" required>
                                                <option value="lote">Lote</option>
                                                <option value="residencial">Residencial</option>
                                                <option value="departamentos">Departamentos</option>
                                                <option value="oficinas">Oficinas</option>
                                            </select>
                                        </div>
                                    </div>
                                    
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="name">* Publicación</label>
                                            <select name="status" id="status" class="form-control" required>
                                                <option value="visible">Visible</option>
                                                <option value="hidden">Oculto</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="max_delivery_time">* Tiempo de entrega (Meses)</label>
                                            <input type="text" name="max_delivery_time" id="max_delivery_time" class="form-control" autocomplete="off" value="{{ old('max_delivery_time') }}" required>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="dropzone-plano" class="form-control-label">* Plano</label>
                                            <input class="form-control unfocus" type="text" name="plano" id="plano" value="{{ old('plano') }}" data-asset="{{ asset('') }}" required>
                                            <a href="#modal-media" data-reference="dropzone-plano" data-dropzone="image" data-toggle="modal" data-target="#modal-media" class="btn btn-default float-right mt-1">Escoger del multimedia</a>
                                            <div class="clearfix"></div>
                                            <div id="dropzone-plano" data-route="{{ route('images.store') }}" data-target="#plano" class="dropzone" style="border:0px">
                                                <div class="dz-default dz-message">Seleccionar archivo</div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="dropzone-masterplan" class="form-control-label">* Master Plan</label>
                                            <input class="form-control unfocus" type="text" name="masterplan" id="masterplan" value="{{ old('masterplan') }}"
                                             data-asset="{{ asset('') }}" required>
                                            <a href="#modal-media" data-reference="dropzone-masterplan" data-dropzone="image" 
                                            data-toggle="modal" data-target="#modal-media" class="btn btn-default float-right mt-1">Escoger del multimedia</a>
                                            <div class="clearfix"></div>
                                            <div id="dropzone-masterplan" data-route="{{ route('images.store') }}" data-target="#masterplan" class="dropzone" style="border:0px">
                                                <div class="dz-default dz-message">Seleccionar archivo</div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="row">
                                    <div class="col-lg-12 text-center">
                                        <button class="btn btn-default">Confirmar</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
				</div>
			</div>
        </div>
    </div>
    @include('include.panel.media')
@endsection