@extends('layouts.panel.app')
@section('content')
    <!-- Header -->
    @include('include.panel.header')
    <link rel="stylesheet" href="{{ asset('assets/css/kanban.css') }}">
    <!-- Page content -->
    <div class="container-fluid mt--6 bg-yellow">
        <div class="row">
            <div class="col">
				<div class="card">
				<!-- Card header -->
				
					
                    <!-- Light table -->
					<div class="card-body">
					<div class="container-kanban">
                  
						<div class="kanban-board container-fluid mt-lg-3" tabindex="0">
							
							{{-- lead--}}
							<div class="kanban-col" tabindex="0">
								<div class="card-list">
									<div class="card-list-header">
										<h6>Leads</h6>
									</div>
									<div class="card-list-body" tabindex="0">
										@if(App\Prospect::where('status', 1)->where('admin_id', $user)->get()->count() > 0)
											@foreach (App\Prospect::where('status', 1)->where('admin_id', $user)->orderBy('created_at', 'desc')->get() as $item)
												@include('panel.prospects.components.kanban_card')
											@endforeach         
										@endif
									</div>
									<div class="card-list-footer">
									</div>
								</div>
							</div>
                            {{-- seguimiento --}}
							<div class="kanban-col" tabindex="0">
								<div class="card-list">
									<div class="card-list-header">
										<h6>En Seguimiento</h6>
									</div>
									<div class="card-list-body" tabindex="0">
                                        @if(App\Prospect::where('status', 2)->where('admin_id', $user)->get()->count() > 0)
                                            @foreach (App\Prospect::where('status', 2)->where('admin_id', $user)->orderBy('created_at', 'desc')->get() as $item)
                                                @include('panel.prospects.components.kanban_card')
                                            @endforeach         
                                        @endif
									</div>
									<div class="card-list-footer">
									</div>
								</div>
							</div>
                            {{-- clientes --}}
							<div class="kanban-col" tabindex="0">
								<div class="card-list">
									<div class="card-list-header">
										<h6>Clientes</h6>
									</div>
									<div class="card-list-body" tabindex="0">
                                        @if(App\Client::where('admin_id', $user)->get()->count() > 0)
                                            @foreach (App\Client::where('admin_id', $user)->orderBy('created_at', 'desc')->get() as $item)
                                                @include('panel.prospects.components.kanban_card')
                                            @endforeach         
                                        @endif
									</div>
									<div class="card-list-footer">
									</div>
								</div>
							</div>
                            {{-- dead Lead --}}
							<div class="kanban-col" tabindex="0">
								<div class="card-list">
									<div class="card-list-header">
										<h6>Dead Leads</h6>
									</div>
									<div class="card-list-body" tabindex="0">
                                        @if(App\Prospect::where('status', 4)->where('admin_id', $user)->get()->count() > 0)
                                            @foreach (App\Prospect::where('status', 4)->where('admin_id', $user)->orderBy('created_at', 'desc')->get() as $item)
                                                @include('panel.prospects.components.kanban_card')
                                            @endforeach         
                                        @endif
									</div>
									<div class="card-list-footer">
									</div>
								</div>
							</div>
							</div>
						</div>
					</div>
				</div>
				</div>
			</div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal-reserve" role="dialog" aria-labelledby="modal-reserve" aria-hidden="true">
    <div class="modal-dialog modal- modal-dialog-centered modal-sm" role="document">
        <div class="modal-content">   
            <div class="modal-body p-0">                
                <div class="card bg-white border-0 mb-0">
                    <div class="card-body px-lg-5 py-lg-5">
                        <div class="text-center text-muted mb-4">
                            <small>Elige un desarrollo para continuar</small>
                        </div>
                        <form method="POST" action="{{ route('panel.prospects.reserve') }}" class="needs-validation" novalidate>
                            {{ csrf_field() }}
                            <div class="row">
                                <input type="hidden" name="prospect_id" id="prospect_id" value="">
                                <div class="col-12">
                                    <div class="form-group">
										<label for="">Desarrollo</label>
										<select id="desarollo" name="desarrollo" class="form-control">
											@foreach (Development::where('status','visible')->get() as $desarrollo)
												<option value="{{ $desarrollo->id }}">{{ $desarrollo->name }}</option>
											@endforeach
										</select>
                                    </div>
                                </div>
                            </div>
                            <div class="text-center">
                                <button class="btn btn-primary my-4">Confirmar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal-reserve-2" role="dialog" aria-labelledby="modal-reserve-2" aria-hidden="true">
    <div class="modal-dialog modal- modal-dialog-centered modal-sm" role="document">
        <div class="modal-content">   
            <div class="modal-body p-0">                
                <div class="card bg-white border-0 mb-0">
                    <div class="card-body px-lg-5 py-lg-5">
                        <div class="text-center text-muted mb-4">
                            <small>Elige un desarrollo para continuar</small>
                        </div>
                        <form method="POST" action="{{ route('panel.prospects.reserve.client') }}" class="needs-validation" novalidate>
                            {{ csrf_field() }}
                            <div class="row">
                                <input type="hidden" name="client_id" id="client_id" value="">
                                <div class="col-12">
                                    <div class="form-group">
										<label for="">Desarrollo</label>
										<select id="desarollo" name="desarrollo" class="form-control">
											@foreach (Development::where('status','visible')->get() as $desarrollo)
												<option value="{{ $desarrollo->id }}">{{ $desarrollo->name }}</option>
											@endforeach
										</select>
                                    </div>
                                </div>
                            </div>
                            <div class="text-center">
                                <button class="btn btn-primary my-4">Confirmar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('js')
    <script>
        let elms = document.querySelectorAll('.reserve-info');
        Array.from(elms).forEach(el => {
            el.addEventListener('click', e => {
                let json = JSON.parse(el.dataset.info);
				console.log(json)
                document.getElementById('prospect_id').value = json.id;
				document.getElementById('client_id').value = json.id;
            });
        });

        //filtrado
        let filter = document.getElementById('filter');
        filter.addEventListener('keyup', e => {
            filterKanban(filter.value);
        });
        filter.addEventListener('search', e => {
            filterKanban("");
        });

        function filterKanban(text){
            
            let kanban_cards = document.querySelectorAll('.kanban_card');
            
            
            Array.from(kanban_cards).forEach(kanban => {
                
                text = text.toLowerCase().trim();
                if(text == ""){
                    kanban.style.display = "block";
                }else{
                    let lead = JSON.parse(kanban.dataset.item);
                    let nameExists = lead.name.toLowerCase().search(text);
                    let emailExists = lead.email.toLowerCase().search(text);
                    let phoneExists = lead.phone.search(text);

                    if(nameExists >= 0 || phoneExists >= 0 || emailExists >= 0){
                        kanban.style.display = "block";
                    }else{
                        kanban.style.display = "none";
                    }
                }
            });
        }
        
    </script>
@endpush