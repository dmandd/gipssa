@extends('layouts.panel.app')
@section('content')
    <!-- Header -->
    @include('include.panel.header')
    <style>
        .badge-yellow{background-color:#ffffae;}
    </style>
    <!-- Page content -->
    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col">
				<div class="card">
				<!-- Card header -->
					<div class="card-header border-0">
						<div class="row">
                            <div class="col-8">
                                <h6 class="heading-small text-muted mb-4">Información</h6>
                            </div>
                            <div class="col-4 text-right">
                                @can(PermissionKey::Prospect['permissions']['import']['name'])
                                    <div class="dropdown">
                                        <a href="#!" class="btn btn-default" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Opciones</a>
                                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow" style="">
                                            <a class="dropdown-item" href="{{ route('panel.prospects.index', ['vista' => 'usuarios']) }}">Cambiar vista por usuarios</a>
                                            <button type="button" class="dropdown-item" data-toggle="modal" data-target="#modal-form">Importar</button>
                                        </div>
                                    </div>
                                @endcan
                            </div>
                        </div>
					</div>
                    <!-- Light table -->
					<div class="table-responsive pb-3">
						<table class="table align-items-center table-flush" id="dataTable">
							<thead class="thead-light">
								<tr>
									<th width="20px">
                                    </th>
                                    <th>Fecha Creación</th>
									<th scope="col" class="sort" data-sort="status">Nombre</th>
									<th scope="col" class="sort" data-sort="status">Correo electrónico</th>
                                    <th scope="col" class="sort" data-sort="status">Teléfono</th>
                                    <th scope="col" class="sort" data-sort="status">Desarrollo</th>
                                    <th>Responsable</th>
									<th scope="col">Acciones</th>
								</tr>
							</thead>
							<tbody class="list">
								@if ((isset($data)) && (count($data) > 0))
                                    @foreach ($data as $row)
                                        <tr>
                                            <td>
                                                <input type="checkbox" name="row[]" value="{{ $row->id }}" class="" id=""><span style="opacity:0;">{{ $row->id}}</span>
                                            </td>
                                            <td>
                                                @switch($row->avance)
                                                    @case('rojo')
                                                        <span class="badge badge-md badge-circle badge-floating badge-danger border-white"> </span>    
                                                    @break
                                                    @case('verde')
                                                        <span class="badge badge-md badge-circle badge-floating badge-success border-white"> </span>    
                                                    @break
                                                    @case('amarillo')
                                                        <span class="badge badge-md badge-circle badge-floating badge-yellow border-white"> </span>    
                                                    @break
                                                    @default
                                                @endswitch 
                                                {{ date('d/m/Y', strtotime($row->created_at)) }}
                                            </td>
                                            <td>
                                                {{ $row->name }} {{ $row->last_name }}
                                            </td>
                                            <td>{{ $row->email }}</td>
                                            <td>{{ $row->phone }}</td>
                                            <td>{{ $row->desarrollo() }}</td>
                                            <td>{{ (isset($row->admin()->name)) ? $row->admin()->name : 'Sin asignar' }}</td>
                                            <td class="text-right">
                                                <div class="dropdown">
                                                    <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <i class="fas fa-ellipsis-v"></i>
                                                    </a>
                                                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                                        <input type="hidden" id="assign-{{ $row->id }}" value="{{ route('panel.prospects.update', ['prospecto' => $row->id]) }}">
                                                        <a class="dropdown-item" href="{{ route('panel.prospects.edit', ['prospecto' => $row->id]) }}">Ver detalle</a>
                                                        @can(PermissionKey::Prospect['permissions']['destroy']['name'])
                                                            <a class="dropdown-item btn-delete" id="delete-{{ $row->id }}" data-axios-method="delete" data-route="{{ route('panel.prospects.destroy', ['prospecto' => $row->id]) }}" data-action="location.reload()" href="javascript:;">Eliminar</a>
                                                        @endcan
                                                        @can(PermissionKey::Prospect['permissions']['create_client']['name'])
                                                            <a class="dropdown-item confirm-info" href="#confirmClient" data-target="#confirmClient" data-toggle="modal" data-info='@json($row)'>Crear Cliente</a>
                                                        @endcan
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
							</tbody>
						</table>
					</div>
				</div>
			</div>
        </div>
    </div>
    <div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledby="modal-form" aria-hidden="true">
        <div class="modal-dialog modal- modal-dialog-centered modal-sm" role="document">
            <div class="modal-content">   
                <div class="modal-body p-0">                
                    <div class="card bg-secondary border-0 mb-0">
                        <div class="card-body px-lg-5 py-lg-5">
                            <div class="text-center text-muted mb-4">
                                <small>Selecciona el archivo</small>
                            </div>
                            <form method="POST" action="{{ route('panel.prospects.import') }}" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="form-group mb-3">
                                    <div class="input-group input-group-merge input-group-alternative">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="ni ni-folder-17"></i></span>
                                        </div>
                                        <input class="form-control" placeholder="Ingrese el archivo" type="file" name="import" required>
                                    </div>
                                </div>
                                <div class="text-center">
                                    <button class="btn btn-primary my-4">Confirmar</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal-assign" tabindex="-1" role="dialog" aria-labelledby="modal-form" aria-hidden="true">
        <div class="modal-dialog modal- modal-dialog-centered modal-sm" role="document">
            <div class="modal-content">   
                <div class="modal-body p-0">                
                    <div class="card bg-secondary border-0 mb-0">
                        <div class="card-body px-lg-5 py-lg-5">
                            <div class="text-center text-muted mb-4">
                                <small>Asignar usuario</small>
                            </div>
                            <form id="form-assign" method="POST" action="">
                                {{ csrf_field() }}
                                <div class="form-group mb-3">
                                    <label for="">Usuario</label>
                                    <select name="" id="admin_id" class="form-control">
                                        <option value="">Selecciona una opción</option>
                                        @foreach (Admin::all() as $admin)
                                            <option value="{{ $admin->id }}">{{ $admin->name }} {{ $admin->last_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="text-center">
                                    <button id="btn-asignar" class="btn btn-primary my-4" type="button">Confirmar</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('panel.prospects.components.confirmModalClient')
    <div id="bin-assign" class="position-fixed" style="right:125px;bottom:20px;display:none;">
        <button class="btn btn-lg btn-icon btn-primary" data-toggle="modal" data-target="#modal-assign" type="button" style="border-radius:50px;font-size: 35px;padding: 9px 17px;" title="Asignar">
            <span class="btn-inner--icon">
                <i class="fas fa-tasks"></i>
            </span>
        </button>
    </div>
@endsection
@push('js')
    <script>
        let elms = document.querySelectorAll('.confirm-info');
        Array.from(elms).forEach(el => {
            el.addEventListener('click', e => {
                let json = JSON.parse(el.dataset.info);
                document.getElementById('name').value = json.name;
                document.getElementById('email').value = json.email;
                document.getElementById('phone').value = json.phone;
            });
        });
    </script>
@endpush