@extends('layouts.panel.app')
@section('content')
    <!-- Header -->
    @include('include.panel.header')
    <!-- Page content -->
    <div class="container mt--6">
        <div class="row">
            <div class="col">
				<div class="card">
				<!-- Card header -->
					<div class="card-header border-0">
                        <h6 class="heading-small text-muted mb-4">Información</h6>
                        <div class="nav-wrapper">
                            <ul class="nav nav-pills nav-fill flex-column flex-md-row" id="tabs-icons-text" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link mb-sm-3 mb-md-0 active" id="info-tab" data-toggle="tab" href="#info" role="tab" aria-controls="info" aria-selected="true">Información</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link mb-sm-3 mb-md-0" id="task-tab" data-toggle="tab" href="#task" role="tab" aria-controls="task" aria-selected="true" {{ ($data->admin_id) ? '' : 'disabled' }}>Tareas</a>
                                </li>
                                {{-- <li class="nav-item">
                                    <a class="nav-link mb-sm-3 mb-md-0" id="task-tab" data-toggle="tab" href="#task" role="tab" aria-controls="task" aria-selected="true">Archivos</a>
                                </li> --}}
                            </ul>
                        </div>
					</div>
                    <!-- Light table -->
                    <div class="card-body">
                        <div class="tab-content pl-lg-4">
                            <div class="tab-pane fade show active" id="info" role="tabpanel" aria-labelledby="info-tab">
                                <form action="{{ route('panel.prospects.update', ['prospecto' => $data->id]) }}" method="POST" class="needs-validation" novalidate>
                                    {{ csrf_field() }}
                                    <input type="hidden" name="_method" value="PUT">
                                    <div class="pl-lg-4">
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <label class="form-control-label" for="name">* Nombre</label>
                                                        @can(PermissionKey::Prospect['permissions']['update']['name'])
                                                            <input type="text" name="name" id="name" class="form-control" required autocomplete="off" value="{{ (old('name')) ? old('name') : $data->name }}">
                                                        @else
                                                            <input type="text" name="name" id="name" readonly class="form-control" required autocomplete="off" value="{{ (old('name')) ? old('name') : $data->name }}">
                                                        @endcan
                                                    </div>
                                                </div>
                                            @can(PermissionKey::Prospect['permissions']['assign']['name'])
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <label class="form-control-label" for="admin_id">* Asignado</label>
                                                        <select name="admin_id" id="admin_id" class="form-control" required>
                                                            <option value="">Selecciona una opción</option>
                                                            @if (($admins) && (count($admins) > 0))
                                                                @foreach ($admins as $admin)
                                                                    <option value="{{ $admin->id }}" {{ ($admin->id == $data->admin_id) ? 'selected' : '' }}>{{ $admin->name }}</option>
                                                                @endforeach
                                                            @endif
                                                        </select>
                                                    </div>
                                                </div>
                                            @endcan
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <label class="form-control-label" for="phone">* Teléfono</label>
                                                    @can(PermissionKey::Prospect['permissions']['update']['name'])
                                                        <input type="text" name="phone" id="phone" class="form-control" required autocomplete="off" value="{{ (old('phone')) ? old('phone') : $data->phone }}">
                                                    @else
                                                        <input type="text" name="phone" id="phone" readonly class="form-control" required autocomplete="off" value="{{ (old('phone')) ? old('phone') : $data->phone }}">
                                                    @endcan
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <label class="form-control-label" for="email">* Correo</label>
                                                    @can(PermissionKey::Prospect['permissions']['update']['name'])
                                                        <input type="email" name="email" id="email" class="form-control" required autocomplete="off" value="{{ (old('email')) ? old('email') : $data->email }}">
                                                    @else
                                                        <input type="email" name="email" id="email" readonly class="form-control" required autocomplete="off" value="{{ (old('email')) ? old('email') : $data->email }}">
                                                    @endcan
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <label class="form-control-label" for="priority">Prioridad</label>
                                                    <select class="form-control" name="priority" id="priority">
                                                        <option value="">Sin definir</option>
                                                        <option value="high" {{ ($data->confidence == 'high') ? 'selected' : '' }}>Alta</option>
                                                        <option value="medium" {{ ($data->confidence == 'medium') ? 'selected' : '' }}>Media</option>
                                                        <option value="low" {{ ($data->confidence == 'low') ? 'selected' : '' }}>Baja</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <label class="form-control-label" for="avance">Avance</label>
                                                    <select class="form-control" name="avance" id="avance">
                                                        <option value="verde" {{ ($data->avance == 'verde') ? 'selected' : '' }}>Potencial Cliente</option>
                                                        <option value="amarillo" {{ ($data->avance == 'amarillo') ? 'selected' : '' }}>En seguimiento</option>
                                                        <option value="rojo" {{ ($data->avance == 'rojo') ? 'selected' : '' }}>Sin interés</option>
                                                        <option value="gris" {{ ($data->avance == 'gris') ? 'selected' : '' }}>Nuevo</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <label class="form-control-label" for="desarrollo">Desarrollo</label>
                                                    <select class="form-control" name="desarrollo" id="desarrollo">
                                                        <option value="">Selecciona una opción</option>
                                                        @foreach (Development::all() as $dev)
                                                            <option value="{{ $dev->slug() }}" {{ ($data->desarrollo == $dev->slug()) ? 'selected' : '' }}>{{ $dev->name }}</option>
                                                        @endforeach
                                                        <option value="inmobiliaria">Inmobiliaria</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <label class="form-control-label" for="procedence">Procedencia</label>
                                                    <select class="form-control" name="procedence" id="procedence">
                                                        <option value="">Selecciona una opción</option>
                                                        <option value="FB" {{ ($data->procedence == 'FB') ? 'selected' : '' }}>Facebook</option>
                                                        <option value="WA" {{ ($data->procedence == 'WA') ? 'selected' : '' }}>WhatsApp</option>
                                                        <option value="OR" {{ ($data->procedence == 'OR') ? 'selected' : '' }}>Orgánico</option>
                                                        <option value="RF" {{ ($data->procedence == 'RF') ? 'selected' : '' }}>Referido</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label class="form-control-label" for="notes">Comentarios</label>
                                                    <textarea name="notes" class="form-control" id="" cols="30" rows="10">{{ $data->notes }}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-footer">
                                        <div class="row">
                                            <div class="col-lg-12 text-center">
                                                <button class="btn btn-default">Confirmar</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="tab-pane fade" id="task" role="tabpanel" aria-labelledby="task-tab">
                                <div class="content-list" data-filter-list="checklist">
                                    <div class="row content-list-head">
                                        <div class="col">
                                            <h1>Checklist</h1>
                                        </div>
                                        <div class="col">
                                            @can(PermissionKey::Task['permissions']['assign']['name'])
                                                <button class="btn btn-icon btn-primary ml-2" data-toggle="modal" data-target="#addActivity" title="Agregar Actividad" type="button"><span class="btn-inner--icon"><i class="ni ni-fat-add"></i></span></button>
                                            @endcan
                                        </div>
                                    </div>
                                    <!--end of content list head-->
                                    <form action="{{ route('panel.tasks.create.from.prospect.update') }}" method="POST" class="" tabindex="0">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="prospect_id" value="{{$data->id}}">
                                        <div class="content-list-body">
                                            <div class="row" tabindex="0">
                                                @if (count($tasks) > 0)
                                                    @foreach ($tasks as $task)
                                                        <div class="col-12 custom-control custom-checkbox mb-3 mt-3">
                                                            <div class="row">
                                                                <div class="col-6">
                                                                    <input type="checkbox" name="tasks[]" value="{{ $task['id'] }}" class="custom-control-input" id="customCheck-{{ $task['id'] }}" {{ ( (isset($task['complete'])) && ($task['complete'] == 1) ) ? 'checked' : '' }}>
                                                                    <label class="custom-control-label" for="customCheck-{{ $task['id'] }}">{{ $task['description'] }}</label>
                                                                </div>
                                                                <div class="col-6">
                                                                    @can(PermissionKey::Task['permissions']['destroy']['name'])
                                                                        @if ($task['default'] == 0)
                                                                            <a class="btn btn-lg btn-icon rounded-circle btn-danger btn-action text-white" 
                                                                                    type="button"
                                                                                    data-toggle="tooltip"
                                                                                    data-placement="top"
                                                                                    data-title="¿Está seguro de eliminar?"
                                                                                    data-text="Este registro se eliminará. La operación no puede revertirse"
                                                                                    data-icon="question"
                                                                                    data-axios-method="delete"
                                                                                    data-route="{{ route('panel.tasks.create.from.prospect.destroy', ['id' => $task['id']]) }}"
                                                                                    data-action="location.reload()"
                                                                                    title="Eliminar">
                                                                                <span class="btn-inner--icon"><i class="ni ni-fat-remove"></i></span>
                                                                            </a>
                                                                        @endif
                                                                    @endcan
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                @endif
                                            </div>
                                        </div>
                                        <br><br>
                                        <div class="row">
                                            <div class="col-12 text-center">
                                                <button class="btn btn-primary">Confirmar</button>
                                            </div>
                                        </div>
                                    </form>
                                    <!--end of content list body-->
                                    <br><br><br>
                                    <div class="row content-list-head mb-4">
                                        <div class="col">
                                            <h1>Notas</h1>
                                        </div>
                                        <div class="col">
                                            <button class="btn btn-icon btn-primary ml-2" data-toggle="modal" data-target="#addNote" title="Agregar Nota" type="button"><span class="btn-inner--icon"><i class="ni ni-fat-add"></i></span></button>
                                        </div>
                                    </div>
                                    <div class="content-list-body">
                                        @if ($data->notes()->count() > 0)
                                            @foreach ($data->notes() as $note)
                                                <div class="card card-note">
                                                    <div class="card-header">
                                                        <div class="row">
                                                            <div class="col-6">
                                                                <div class="media align-items-center">
                                                                    <img alt="user image" src="{{ asset($note->user()->avatar) }}" class="avatar filter-by-alt" data-toggle="tooltip" data-title="{{ $note->user()->name.' '.$note->user()->last_name }}" data-filter-by="alt">
                                                                    <div class="media-body ml-3">
                                                                        <h5 class="mb-0 H6-filter-by-text" data-filter-by="text">{{ $note->title }}</h5>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-6">
                                                                <span data-filter-by="text" class="SPAN-filter-by-text float-right">{{ time_passed($note->created_at) }}</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="card-body DIV-filter-by-text" data-filter-by="text">
                                                        {!! $note->description !!}
                                                    </div>
                                                </div>
                                            @endforeach
                                        @else
                                            <small>Sin notas</small>
                                        @endif
                                    </div>
                                  </div>
                            </div>
                        </div>
                    </div>
				</div>
			</div>
        </div>
    </div>
    @include('panel.tasks.components.modal')
    @include('panel.tasks.components.modalNotes')
@endsection