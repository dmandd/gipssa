@extends('layouts.panel.app')
@section('content')
    <!-- Header -->
    @include('include.panel.header')
    <style>
        .badge-yellow{background-color:#ffe440;}
        .badge-grey{background-color:#B6B6B6;}
    </style>
    <!-- Page content -->
    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col">
				<div class="card">
                    <div class="card-header border-0 d-flex justify-content-between ">
						<h3 class="mb-0">{{Admin::find($user)->name." ".Admin::find($user)->last_name}}</h3>
                        <div>
                            <a class="btn btn-default" href="{{ route('panel.prospects.index',['usuario'=>$user]) }}">Cambiar vista por kanban</a>
                            @can(PermissionKey::Prospect['permissions']['import']['name'])
                                <div class="dropdown">
                                    <a href="#!" class="btn btn-default" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Opciones</a>
                                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                        <button type="button" class="dropdown-item" data-toggle="modal" data-target="#modal-form">Importar</button>
                                        <a class="dropdown-item" href="{{ route('panel.prospects.export', ['user' => null]) }}">Exportar</a>
                                    </div>
                                </div>
                            @endcan
                        </div>
					</div>
                    <!-- Light table -->
					<div class="table-responsive pb-3">
						<table class="table align-items-center table-flush" id="dataTable">
							<thead class="thead-light">
								<tr>
									<th width="20px">
                                    </th>
                                    <th>Fecha Creación</th>
									<th scope="col" class="sort" data-sort="status">Nombre</th>
									<th scope="col" class="sort" data-sort="status">Correo electrónico</th>
                                    <th scope="col" class="sort" data-sort="status">Teléfono</th>
                                    <th scope="col" class="sort" data-sort="status">Desarrollo</th>
                                    <th>Responsable</th>
									<th scope="col">Acciones</th>
								</tr>
							</thead>
							<tbody class="list">
								@if (isset($user))
                                    @foreach (App\Prospect::where('status','!=', '4')->where('admin_id', $user)->orderBy('created_at', 'desc')->get() as $row)
                                        <tr>
                                            <td>
                                                <input type="checkbox" name="row[]" value="{{ $row->id }}" ><span style="opacity:0;">{{ $row->id}}</span>
                                            </td>
                                            <td>
                                                @switch($row->avance)
                                                    @case('rojo')
                                                        <span class="badge badge-md badge-circle badge-floating badge-danger border-white"> </span>    
                                                    @break
                                                    @case('verde')
                                                        <span class="badge badge-md badge-circle badge-floating badge-success border-white"> </span>    
                                                    @break
                                                    @case('amarillo')
                                                        <span class="badge badge-md badge-circle badge-floating badge-yellow border-white"> </span>    
                                                    @break
                                                    @case('gris')
                                                        <span class="badge badge-md badge-circle badge-floating badge-grey border-white"> </span>    
                                                    @break
                                                    @default
                                                @endswitch 
                                                {{ date('d/m/Y', strtotime($row->created_at)) }}
                                            </td>
                                            <td>
                                                {{ $row->name }} {{ $row->last_name }}
                                            </td>
                                            <td>
                                                <a href='mailto: {{ $row->email}}' target="_blank" >
                                                <img 
                                                     src="{{ asset('panel/img/brand/icon_email.png') }}"
                                                     height="25px">

                                                </a>
                                                {{ $row->email }}
                                                
                                            </td>
                                            <td>                                            
                                                <a href="https://api.whatsapp.com/send?phone={{ $row->phone }}"
                                                    target="_blank"
                                                    data-toggle="tooltip" title="https://api.whatsapp.com/send?phone="{{ $row->phone }}>                                                   
                                                    <img 
                                                     src="{{ asset('panel/img/brand/icon_whatsapp.png') }}"
                                                     height="25px"                                                                                                          
                                                     alt={{ $row->phone }}  >                                                  
                                                                                                   
                                                </a>

                                                <a href="tel:{{ $row->phone }}">{{$row->phone}} </a>

                                            </td>
                                            <td>{{ $row->desarrollo() }}</td>
                                            <td>{{ (isset($row->admin()->name)) ? $row->admin()->name : 'Sin asignar' }}
                                            </td>
                                            <!-- FIN PRUEBAS -->
                                            <td class="text-right">
                                                <div class="dropdown">
                                                    <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <i class="fas fa-ellipsis-v"></i>
                                                    </a>
                                                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                                        <input type="hidden" id="assign-{{ $row->id }}" value="{{ route('panel.prospects.update', ['prospecto' => $row->id]) }}">
                                                        <a class="dropdown-item" href="{{ route('panel.prospects.edit', ['prospecto' => $row->id]) }}">Ver detalle</a>
                                                        @can(PermissionKey::Prospect['permissions']['create_client']['name'])
                                                            <a class="dropdown-item btn-action" 
                                                                    data-title="¿Está seguro?"
                                                                    data-text="Este registro pasara a ser un cliente"
                                                                    data-icon="question"
                                                                    data-axios-method="post"
                                                                    data-route="{{ route('panel.prospects.client', ['prospecto' => $row->id]) }}"
                                                                    data-action="location.reload()"
                                                                    href="javascript:;">
                                                                Crear cliente
                                                            </a>
                                                        @endcan
                                                            <a href="#" data-toggle="modal" data-target="#modal-reserve" class="dropdown-item reserve-info" data-info='@json($row)'>Apartar</a>
                                                        @can(PermissionKey::Prospect['permissions']['destroy']['name'])
                                                            <a class="dropdown-item btn-delete" id="delete-{{ $row->id }}" data-axios-method="delete" data-route="{{ route('panel.prospects.destroy', ['prospecto' => $row->id]) }}" data-action="location.reload()" href="javascript:;">Eliminar</a>
                                                        @endcan
                                                        
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
							</tbody>
						</table>
					</div>
				</div>
			</div>
        </div>
    </div>
    <div class="modal fade" id="modal-reserve" role="dialog" aria-labelledby="modal-reserve" aria-hidden="true">
    <div class="modal-dialog modal- modal-dialog-centered modal-sm" role="document">
        <div class="modal-content">   
            <div class="modal-body p-0">                
                <div class="card bg-white border-0 mb-0">
                    <div class="card-body px-lg-5 py-lg-5">
                        <div class="text-center text-muted mb-4">
                            <small>Elige un desarrollo para continuar</small>
                        </div>
                        <form method="POST" id="form-reserve" action="{{ route('panel.prospects.reserve') }}" class="needs-validation" novalidate>
                            {{ csrf_field() }}
                            <div class="row">
                                <input type="hidden" name="prospect_id" id="prospect_id" value="">
                                <div class="col-12">
                                    <div class="form-group">
										<label for="">Desarrollo</label>
										<select id="desarollo" name="desarrollo" class="form-control">
											@foreach (Development::where('status','visible')->get() as $desarrollo)
												<option value="{{ $desarrollo->id }}">{{ $desarrollo->name }}</option>
											@endforeach
										</select>
                                    </div>
                                </div>
                            </div>
                            <div class="text-center">
                                <button class="btn btn-primary my-4">Confirmar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    <div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledby="modal-form" aria-hidden="true">
        <div class="modal-dialog modal- modal-dialog-centered modal-sm" role="document">
            <div class="modal-content">   
                <div class="modal-body p-0">                
                    <div class="card bg-secondary border-0 mb-0">
                        <div class="card-body px-lg-5 py-lg-5">
                            <div class="text-center text-muted mb-4">
                                <small>Selecciona el archivo</small>
                            </div>
                            <form method="POST" action="{{ route('panel.prospects.import') }}" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="form-group mb-3">
                                    <div class="input-group input-group-merge input-group-alternative">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="ni ni-folder-17"></i></span>
                                        </div>
                                        <input class="form-control" placeholder="Ingrese el archivo" type="file" name="import" required>
                                    </div>
                                </div>
                                <div class="text-center">
                                    <button class="btn btn-primaryanel.prospects.reserve.client my-4">Confirmar</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal-assign" tabindex="-1" role="dialog" aria-labelledby="modal-form" aria-hidden="true">
        <div class="modal-dialog modal- modal-dialog-centered modal-sm" role="document">
            <div class="modal-content">   
                <div class="modal-body p-0">                
                    <div class="card bg-secondary border-0 mb-0">
                        <div class="card-body px-lg-5 py-lg-5">
                            <div class="text-center text-muted mb-4">
                                <small>Asignar usuario</small>
                            </div>
                            <form id="form-assign" method="POST" action="{{ route('panel.prospects.reassign') }}">
                                {{ csrf_field() }}
                                <div class="form-group mb-3">
                                    <label for="">Usuario</label>
                                    <input type="hidden" id="selected_ids" name="selected_ids" value="" >
                                    <select name="admin_id" id="admin_id" class="form-control">
                                        <option value="">Selecciona una opción</option>
                                        @foreach (Admin::all() as $admin)
                                            <option value="{{ $admin->id }}">{{ $admin->name }} {{ $admin->last_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="text-center">
                                    <button id="btn-asig" class="btn btn-primary my-4">Confirmar</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('panel.prospects.components.confirmModalClient')
    <div id="bin-assign" class="position-fixed" style="right:125px;bottom:20px;display:none;">
        <button class="btn btn-lg btn-icon btn-primary" data-toggle="modal" data-target="#modal-assign" type="button" style="border-radius:50px;font-size: 35px;padding: 9px 17px;" title="Asignar">
            <span class="btn-inner--icon">
                <i class="fas fa-tasks"></i>
            </span>
        </button>
    </div>
@endsection
@push('js')
    <script>
        let elms = document.querySelectorAll('.reserve-info');
        Array.from(elms).forEach(el => {
            el.addEventListener('click', e => {
                let json = JSON.parse(el.dataset.info);
                console.log(json);
                document.getElementById('prospect_id').value = json.id;
            });
        });

        $( document ).ready(function() {
            $("[name='row[]']").on("click",function(){
                let ids = [];
                $("[name='row[]']").each(function(){
                    if(this.checked){
                        ids.push(this.value);
                    }
                });
                let json = JSON.stringify(ids);
                console.log(json);
                $("#selected_ids").val(json);
            });
        });
    </script>
@endpush