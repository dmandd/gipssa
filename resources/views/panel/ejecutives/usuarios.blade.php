@extends('layouts.panel.app')
@section('content')
    <!-- Header -->
    @include('include.panel.header') 
    <!-- Page content -->
    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col">
				<div class="card">
				<!-- Card header -->
					<div class="card-header border-0">
						
                    </div>
                    <div class="col-12">
                        <div class="row">
                            @foreach ($users as $user)
                             
                                <div class="col-lg-3 col-12">
                                    <a href="{{ route('panel.prospects.index', ['usuario' => $user->id]) }}">
                                    <!-- <a href="#"> -->
                                        <div class="card card-stats">
                                            <!-- Card body -->
                                            <div class="card-body">
                                                <div class="row">
                                                    <div class="col">
                                                        <h5 class="card-title text-uppercase text-muted mb-0">{{ $user->name }} {{ $user->last_name }}</h5>
                                                        <span class="h2 font-weight-bold mb-0">{{ $user->prospects()->count() }} leads asignados</span>
                                                    </div>
                                                    <div class="col-auto">
                                                        <div class="icon icon-shape bg-secondary text-white rounded-circle shadow">
                                                            <img src="{{ asset($user->avatar) }}" alt="avatar" 
                                                            
                                                            style="width:100%;">
                                                        </div>
                                                    </div>
                                                </div>
                                                    <p class="mt-3 mb-0 text-sm">
                                                        <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> {{ $user->rendimiento() }} %</span>
                                                        <span class="text-nowrap">Desde el último mes</span>
                                                    </p>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                
                            @endforeach
                        </div>
                    </div>
				</div>
			</div>
        </div>
    </div>
    <div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledby="modal-form" aria-hidden="true">
        <div class="modal-dialog modal- modal-dialog-centered modal-sm" role="document">
            <div class="modal-content">   
                <div class="modal-body p-0">                
                    <div class="card bg-secondary border-0 mb-0">
                        <div class="card-body px-lg-5 py-lg-5">
                            <div class="text-center text-muted mb-4">
                                <small>Selecciona el archivo</small>
                            </div>
                            <form method="POST" action="{{ route('panel.prospects.import') }}" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="form-group mb-3">
                                    <div class="input-group input-group-merge input-group-alternative">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="ni ni-folder-17"></i></span>
                                        </div>
                                        <input class="form-control" placeholder="Ingrese el archivo" type="file" name="import" required>
                                    </div>
                                </div>
                                <div class="text-center">
                                    <button class="btn btn-primary my-4">Confirmar</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection