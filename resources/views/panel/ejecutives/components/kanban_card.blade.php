<div class="card kanban_card" tabindex="0" data-item="{{$item}}">
    <div class="card-body">
        {{-- <div class="dropdown card-options">
            <a class="btn" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-ellipsis-v"></i>
            </a>
            <div class="dropdown-menu dropdown-menu-right">
                <a class="dropdown-item" href="#">Edit</a>
                <a class="dropdown-item text-danger" href="#">Archive List</a>
            </div>
        </div> --}}
        <div class="row">
            <div class="col-lg-10">
            @if(!is_null($item->status))
                <a href="{{ route('panel.prospects.edit', ['prospecto' => $item->id]) }}">
            @else
                <a href="{{ route('panel.clients.edit', ['cliente' => $item->id]) }}">
            @endif
                    <div class="card-title d-flex space-between">
                        <div >
                            <h6><i class="fa fa-user"></i>{{$item->name}} </h6> <br>
                            <h6 class="text-small">
                            <a href='mailto: {{$item->email}}' target="_blank" >
                                                <img 
                                                     src="{{ asset('panel/img/brand/icon_email.png') }}"
                                                     height="25px">

                                                </a>
                                                {{$item->email}}
                        
                        
                        </h6> <br>
                            <h6 class="text-small">


                            <a href="https://api.whatsapp.com/send?phone={{ $item->phone }}"
                                 target="_blank"
                                 title="https://api.whatsapp.com/send?phone="{{ $item->phone }}>
                                 <img src="{{ asset('panel/img/brand/icon_whatsapp.png') }}" height="25px" 
                                   alt={{ $item->phone }}  >                                            
                            </a>

                            <a href="tel:{{ $item->phone }}">{{$item->phone}} </a>
                            
                        
                        
                        
                            </h6>

                        </div>
                    </div> 
                </a>
            </div>
            <div class="dropdown col-lg-2">
                <a class="btn btn-sm btn-icon-only text-center" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fas fa-ellipsis-v"></i>
                </a>
                <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                    <input type="hidden" id="assign-{{ $item->id }}" value="{{ route('panel.prospects.update', ['prospecto' => $item->id]) }}">
                    @if(!is_null($item->status) && $item->status != 4)
                        @can(PermissionKey::Prospect['permissions']['create_client']['name'])
                            <a class="dropdown-item btn-action" 
                                    data-title="¿Está seguro?"
                                    data-text="Este registro pasara a ser un cliente"
                                    data-icon="question"
                                    data-axios-method="post"
                                    data-route="{{ route('panel.prospects.client', ['prospecto' => $item->id]) }}"
                                    data-action="location.reload()"
                                    href="javascript:;">
                                Crear cliente
                            </a>
                        @endcan 
                    @endif
                    @if($item->status != 4)
                        @if(!is_null($item->status))
                            <a href="#" data-toggle="modal" data-target="#modal-reserve" class="dropdown-item reserve-info" data-info='@json($item)'>Apartar</a>
                        @else
                            <a href="#" data-toggle="modal" data-target="#modal-reserve-2" class="dropdown-item reserve-info" data-info='@json($item)'>Apartar</a>
                        @endif
                    @endif
                    @if($item->status == 4)
                        <a class="dropdown-item btn-action" 
                                data-title="¿Está seguro?"
                                data-text="Este registro se recuperara y se ira a Leads nuevamente"
                                data-icon="question"
                                data-axios-method="post"
                                data-route="{{ route('panel.prospects.recover', ['prospecto' => $item->id]) }}"
                                data-action="location.reload()"
                                href="javascript:;">
                            Recuperar
                        </a>
                    @endif
                    @if(!is_null($item->status) && $item->status != 4)
                        @can(PermissionKey::Prospect['permissions']['destroy']['name'])
                            <a class="dropdown-item btn-delete" 
                                    id="delete-{{ $item->id }}" 
                                    data-axios-method="delete" 
                                    data-route="{{ route('panel.prospects.destroy', ['prospecto' => $item->id]) }}" 
                                    data-action="location.reload()" 
                                    href="javascript:;">
                                Eliminar
                            </a>
                        @endcan
                    @endif
                </div>
            </div>
        </div>
        <ul class="avatars">
            <li>
                @switch($item->avance)
                    @case('rojo')
                        <span class="badge badge-md badge-circle badge-floating badge-danger border-white"> </span>    
                    @break
                    @case('verde')
                        <span class="badge badge-md badge-circle badge-floating badge-success border-white"> </span>    
                    @break
                    @case('amarillo')
                        <span class="badge badge-md badge-circle badge-floating badge-yellow border-white"> </span>    
                    @break
                    @case('gris')
                        <span class="badge badge-md badge-circle badge-floating badge-grey border-white"> </span>    
                    @break
                    @default
                @endswitch 
            </li>
            {{-- <li>
                <a href="#" data-toggle="tooltip" title="Ravi">
                <img alt="Ravi Singh" class="avatar" src="https://pipeline.mediumra.re/assets/img/avatar-male-3.jpg">
                </a>
            </li> --}}
        </ul>
        <div class="card-meta d-flex justify-content-between">
            <span class="text-small">{{ time_passed($item->created_at) }}</span>
        </div>
    </div>
</div>

<style>
    .badge-yellow{background-color:#ffe440;}
    .badge-grey{background-color:#B6B6B6;}
</style>

