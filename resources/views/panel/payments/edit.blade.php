@extends('layouts.panel.app')
@section('content')
    <!-- Header -->
    @include('include.panel.header')
    <!-- Page content -->
    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col">
				<div class="card">
				<!-- Card header -->
					<div class="card-header border-0">
                        {{-- <h3 class="mb-0">Nuevo usuario</h3> --}}
                        <h6 class="heading-small text-muted mb-4">Información</h6>
					</div>
                    <!-- Light table -->
                    <div class="card-body">
                        <form action="{{ route('panel.payments.update', ['pago' => $payment]) }}" method="POST" class="needs-validation" novalidate>
                            {{ csrf_field() }}
                            <input type="hidden" name="_method" value="PUT">
                            <div class="pl-lg-4">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="form-control-label" for="fecha">* Fecha</label>
                                            <input type="text" name="fecha" id="fecha" class="form-control mydatepicker" required autocomplete="off" value="{{ (old('fecha')) ? old('fecha') : date('d/m/Y', strtotime($payment->fecha)) }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="form-control-label" for="recibo">* Nota/Factura</label>
                                            <input type="text" name="recibo" id="recibo" class="form-control" required autocomplete="off" value="{{ (old('recibo')) ? old('recibo') : $payment->recibo }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="form-control-label" for="beneficiario">* Beneficiario/Proveedor/Empresa</label>
                                            <input type="text" name="beneficiario" id="beneficiario" class="form-control" required autocomplete="off" value="{{ (old('beneficiario')) ? old('beneficiario') : $payment->beneficiario }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="form-control-label" for="concepto">* Concepto</label>
                                            <input type="text" name="concepto" id="concepto" class="form-control" required autocomplete="off" value="{{ (old('concepto')) ? old('concepto') : $payment->concepto }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="form-control-label" for="monto">* Monto</label>
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">$</span>
                                                </div>
                                                <input type="text" name="monto" id="monto" class="form-control" required autocomplete="off" value="{{ (old('monto')) ? old('monto') : $payment->monto }}">
                                            </div>
                                            <small>El monto no debe incluir comas. Ej. 5000.00</small>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="form-control-label" for="movimiento">* Movimiento</label>
                                            <select name="movimiento" id="movimiento" class="form-control">
                                                <option value="Egreso" {{ ($payment->movimiento == 'Egreso') ? 'selected' : '' }}>Egreso</option>
                                                <option value="Ingreso" {{ ($payment->movimiento == 'Ingreso') ? 'selected' : '' }}>Ingreso</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="form-control-label" for="tipo">* Tipo</label>
                                            <select name="tipo" id="tipo" required class="form-control">
                                                <option value="">Selecciona una opción</option>
                                                <option value="Vale" {{ ($payment->tipo == 'Vale') ? 'selected' : '' }}>Vale</option>
                                                <option value="Efectivo" {{ ($payment->tipo == 'Efectivo') ? 'selected' : '' }}>Efectivo</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="row">
                                    <div class="col-lg-12 text-center">
                                        <button class="btn btn-default">Confirmar</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
				</div>
			</div>
        </div>
    </div>
@endsection