@extends('layouts.panel.app')
@section('content')
    <!-- Header -->
    @include('include.panel.header')
    <!-- Page content -->
    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col">
				<div class="card">
				<!-- Card header -->
					<div class="card-header border-0">
                        {{-- <h3 class="mb-0">Light table</h3> --}}
                        <div class="nav-wrapper">
                            <ul class="nav nav-pills nav-fill flex-column flex-md-row" id="tabs-icons-text" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link mb-sm-3 mb-md-0 {{ (!isset(request()->filter)) ? 'active' : '' }}" href="{{ route('panel.payments.index') }}">Registro General</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link mb-sm-3 mb-md-0 {{ (request()->filter == 'Efectivo;Vale') ? 'active' : '' }}" href="?filter=Efectivo;Vale">Caja Chica</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link mb-sm-3 mb-md-0 {{ (request()->filter == 'Cuenta') ? 'active' : '' }}" href="?filter=Cuenta">Cuentas por pagar</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link mb-sm-3 mb-md-0 {{ (request()->filter == 'Gasto') ? 'active' : '' }}" href="?filter=Gasto">Registro de Gastos</a>
                                </li>
                            </ul>
                        </div>
					</div>
                    <!-- Light table -->
					<div class="table-responsive pb-3">
						<table class="table align-items-center table-flush" id="dataTable">
							<thead class="thead-light">
								<tr>
									<th width="20px">
                                    </th>
									<th scope="col" class="sort" data-sort="status">Fecha de pago</th>
									<th scope="col">#Nota/Factura </th>
									<th scope="col">Beneficiario/Proveedor/Empresa</th>
									<th scope="col">Concepto</th>
									<th scope="col">Tipo</th>
									<th scope="col">Monto</th>
									<th scope="col">Movimiento</th>
									<th scope="col">Saldo</th>
									<th scope="col">Acciones</th>
								</tr>
							</thead>
							<tbody class="list">
                                @php
                                    $saldo = 0;
                                @endphp
                                @if ((isset($data)) && (count($data) > 0))
                                    @foreach ($data as $row)
                                        @php
                                            if(strtotime($row->created_at) <= strtotime(date('Y-m-d H:i:s'))){
                                                if($row->movimiento == 'Ingreso'){
                                                    $saldo += $row->monto;
                                                }else{
                                                    $saldo -= $row->monto;
                                                }
                                            }
                                        @endphp
                                        <tr>
                                            <td>
                                                <input type="checkbox" class="" id=""><span style="opacity:0;">{{ $row->id}}</span>
                                            </td>
                                            <td>{{ $row->fecha }}</td>
                                            <td>{{ $row->recibo }}</td>
                                            <td>{{ $row->beneficiario }}</td>
                                            <td>{{ $row->concepto }}</td>
                                            <td>{{ $row->tipo }}</td>
                                            <td>${{ number_format($row->monto, 2) }}</td>
                                            <td>
                                                @if ($row->movimiento == 'Egreso')
                                                    <i class="fas fa-arrow-down text-danger"></i>
                                                @else
                                                    <i class="fas fa-arrow-up text-success"></i>
                                                @endif
                                                {{ $row->movimiento }}
                                            </td>
                                            <td>${{ number_format($saldo, 2) }}</td>
                                            <td class="text-right">
                                                <div class="dropdown">
                                                    <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <i class="fas fa-ellipsis-v"></i>
                                                    </a>
                                                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                                        <a class="dropdown-item" href="{{ route('panel.payments.edit', ['pago' => $row->id]) }}">Editar</a>
                                                        <a class="dropdown-item btn-delete" data-axios-method="delete" data-route="{{ route('panel.payments.destroy', ['pago' => $row->id]) }}" data-action="location.reload()" href="javascript:;">Eliminar</a>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td>Total: </td>
                                    <td><b>${{ number_format($total, 2) }}</b></td>
                                    <td></td>
                                </tr>
                            </tfoot>
						</table>
					</div>
				</div>
			</div>
        </div>
    </div>
@endsection