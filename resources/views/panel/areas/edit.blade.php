@extends('layouts.panel.app')
@section('content')
    <!-- Header -->
    @include('include.panel.header')
    <!-- Page content -->
    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col">
				<div class="card">
				<!-- Card header -->
					<div class="card-header border-0">
                        {{-- <h3 class="mb-0">Nuevo usuario</h3> --}}
                        <h6 class="heading-small text-muted mb-4">Información</h6>
					</div>
                    <!-- Light table -->
                    <div class="card-body">
                        <form action="{{ route('panel.areas.update', ['propiedade' => request()->propiedade, 'area' => $area->id]) }}" method="POST" class="needs-validation" novalidate>
                            {{ csrf_field() }}
                            <input type="hidden" name="_method" value="PUT">
                            <div class="pl-lg-4">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="form-control-label" for="name">* Nombre</label>
                                            <input type="text" name="name" id="name" class="form-control" required autocomplete="off" value="{{ (old('name')) ? old('name') : $area->name }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="form-control-label" for="name">* Publicación</label>
                                            <select name="status" id="status" class="form-control">
                                                <option value="visible" {{ ($area->status == 'visible') ? 'selected' : '' }}>Visible</option>
                                                <option value="hidden" {{ ($area->status == 'hidden') ? 'selected' : '' }}>Oculto</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <h5 class="heading-small text-muted mb-1">Equipamiento</h5>
                                <div class="row mb-3">
                                    @if (($equipment) && ($equipment->count() > 0))
                                        @foreach ($equipment as $equip)
                                            <div class="col-3 mt-3">
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" id="{{ $equip->id }}" name="equipment[]" value="{{ $equip->id }}" {{ ($area->hasEquipment($equip->id)) ? 'checked' : '' }}>
                                                    <label class="custom-control-label" for="{{ $equip->id }}">{{ $equip->name }}</label>
                                                </div>
                                            </div>
                                        @endforeach
                                    @else
                                        <div class="col-12 text-center">
                                            <a href="{{ route('panel.equipment.create') }}" class="btn btn-primary">Agregar equipamiento</a>
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="row">
                                    <div class="col-lg-12 text-center">
                                        <button class="btn btn-default">Confirmar</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
				</div>
			</div>
        </div>
    </div>
@endsection