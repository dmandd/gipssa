@extends('layouts.panel.app')
@section('content')
    <!-- Header -->
    @include('include.panel.header')
    <!-- Page content -->
    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col">
				<div class="card">
				<!-- Card header -->
					<div class="card-header border-0">
                        <div class="row mb-3">
                            <div class="col-6">
                                <h6 class="heading-small text-muted mb-4">Información</h6>
                            </div>
                            <div class="col-6 text-right">
                                @can(PermissionKey::Venta['permissions']['destroy']['name'])
                                    {{-- @if ($venta->status == 0) --}}
                                        <button class="btn btn-lg btn-icon rounded-circle btn-danger btn-action"
                                            type="button" 
                                            data-toggle="tooltip" 
                                            data-placement="top" 
                                            data-title="¿Está seguro de eliminar esta venta?" 
                                            data-text="La venta se eliminará" 
                                            data-icon="question" 
                                            data-axios-method="delete" 
                                            data-route="{{ route('panel.ventas.destroy', ['venta' => $venta->id]) }}" 
                                            data-action="location.href = '{{ route('panel.ventas.index', ['desarrollo' => $venta->development_id]) }}'"
                                            title="Eliminar">
                                            <span class="btn-inner--icon"><i class="ni ni-fat-remove"></i></span>
                                        </button>
                                    {{-- @endif --}}
                                @endcan
                                {{-- <div class="dropdown">
                                    <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fas fa-ellipsis-v"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                        <a class="dropdown-item" href="">Editar</a>
                                        <a class="dropdown-item btn-delete">Eliminar</a>
                                    </div>
                                </div> --}}
                            </div>
                        </div>
                        <div class="accordion" id="accordionExample">
                            <div class="card">
                                <div class="card-header" id="heading-1" data-toggle="collapse" data-target="#collapse-1" aria-expanded="false" aria-controls="collapse-1">
                                    <h4>Log</h4>
                                </div>
                                <div id="collapse-1" class="collapse" aria-labelledby="1" data-parent="#heading-1" style="">
                                    @foreach ($venta->asesor()->notifications as $notification)
                                        @if (($notification->data['venta']['id'] == $venta->id) && ($notification->type == 'App\Notifications\RevokeQuotation'))
                                            <div class="alert alert-danger" role="alert">
                                                <h3 class="text-white">Cotización Rechazada {{ date('d/m/Y', strtotime($notification->created_at)) }}</h3> 
                                                {{$notification->data['motivo']}}
                                            </div>
                                        @else
                                            @if ($notification->type == 'App\Notifications\ReviewAccepted' && ($notification->data['venta']['id'] == $venta->id))
                                                <div class="alert alert-success" role="alert">
                                                    @switch($notification->data['venta']['status'])
                                                        @case(1)
                                                            <h3 class="text-white">Aparatdo el {{ date('d/m/Y', strtotime($notification->created_at)) }}</h3>     
                                                        @break
                                                        @case(2)
                                                            <h3 class="text-white">Pago Contrato aprobado el {{ date('d/m/Y', strtotime($notification->created_at)) }}</h3> 
                                                        @break
                                                        @case(3)
                                                            <h3 class="text-white">Documentación aprobada el {{ date('d/m/Y', strtotime($notification->created_at)) }}</h3> 
                                                        @break
                                                        @case(4)
                                                            <h3 class="text-white">Condición de compra aprobada {{ date('d/m/Y', strtotime($notification->created_at)) }}</h3> 
                                                        @break
                                                        @default
                                                            
                                                    @endswitch
                                                </div>
                                            @endif
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <div class="nav-wrapper">
                            <ul class="nav nav-pills nav-fill flex-column flex-md-row" id="tabs-icons-text" role="tablist">
                                {{--<li class="nav-item">
                                    <a class="nav-link mb-sm-3 mb-md-0 {{ ($venta->status == 0) ? 'active': '' }}" id="cotizacion-tab" data-toggle="tab" href="#cotizacion" role="tab" aria-controls="cotizacion" aria-selected="true">Cotización</a>
                                </li>--}}
                                <li class="nav-item">
                                    {!! navLinkTab(($venta->status > 0), ['name' => 'Apartado y Pago del contrato', 'slug' => 'bloqueo'], ($venta->status == 1)) !!}
                                </li>
                                <li class="nav-item">
                                    {!! navLinkTab(($venta->status > 1), ['name' => 'Documentación y Contrato', 'slug' => 'documentacion'], ($venta->status == 2)) !!}
                                </li>
                                {{-- <li class="nav-item">
                                    {!! navLinkTab(($venta->status > 2), ['name' => 'Compra', 'slug' => 'condicion-compra'], ($venta->status == 3)) !!}
                                </li> --}}
                                @if (request()->user()->can(PermissionKey::Venta['permissions']['comisiones']['name']))
                                <li class="nav-item">
                                    {!! navLinkTab((($venta->status > 2) && (request()->user()->can(PermissionKey::Venta['permissions']['comisiones']['name']))), ['name' => 'Comisiones', 'slug' => 'comisiones'], ($venta->status == 10)) !!}
                                </li>
                                @endif
                                <li class="nav-item">
                                    {!! navLinkTab((($venta->status > 2) && (request()->user()->can(PermissionKey::Venta['permissions']['create_abono_enganche']['name']))), ['name' => 'Pago(s) de Contado y Enganche', 'slug' => 'enganche'], ($venta->status == 3)) !!}
                                </li>
                                {{-- <li class="nav-item">
                                    {!! navLinkTab(($venta->status > 4), ['name' => 'Pago Contrato', 'slug' => 'pago-contrato'], ($venta->status == 5)) !!}
                                </li> --}}
                                <li class="nav-item">
                                    {!! navLinkTab((($venta->status > 2) && (request()->user()->can(PermissionKey::Venta['permissions']['update_contrato']['name']))), ['name' => 'Subir contrato', 'slug' => 'contrato'], ($venta->status == 4))    !!}
                                </li>
                                
                                    <li class="nav-item">
                                        {!! navLinkTab((($venta->status > 3) && (request()->user()->can(PermissionKey::Venta['permissions']['seguimiento_pagos']['name']))), ['name' => 'Pago(s) de Saldo y Financiamiento', 'slug' => 'pagos'], ($venta->status == 5)) !!}
                                    </li>
                            </ul>
                        </div>
                    </div>
                    <!-- Light table -->
                    {{-- <form action="" method="POST" class="needs-validation" novalidate> --}}
                        {{-- {{ csrf_field() }} --}}
                        
                        <div class="card-body">
                            <div class="tab-content pl-lg-4" id="myTabContent">
                                {{--<div class="tab-pane fade {{ ($venta->status == 0) ? 'show active': '' }}" id="cotizacion" role="tabpanel" aria-labelledby="cotizacion-tab">
                                    @include('panel.ventas.components.cotizacion')
                                </div>--}}
                                <div class="tab-pane fade {{ ($venta->status == 1) ? 'show active': '' }}" id="bloqueo" role="tabpanel" aria-labelledby="bloqueo-tab">
                                    @include('panel.ventas.components.bloqueo')
                                </div>
                                <div class="tab-pane fade {{ ($venta->status == 2) ? 'show active': '' }}" id="documentacion" role="tabpanel" aria-labelledby="documentacion-tab">
                                    @include('panel.ventas.components.documentacion')
                                </div>
                                {{-- <div class="tab-pane fade {{ ($venta->status == 3) ? 'show active': '' }}" id="condicion-compra" role="tabpanel" aria-labelledby="condicion-compra-tab">
                                    @include('panel.ventas.components.condicion-compra')
                                </div> --}}
                                <div class="tab-pane fade {{ ($venta->status == 10) ? 'show active': '' }}" id="comisiones" role="tabpanel" aria-labelledby="comisiones-tab">
                                    @can(PermissionKey::Venta['permissions']['comisiones']['name'])
                                        @can(PermissionKey::Comission['permissions']['edit']['name'])
                                            @include('panel.ventas.components.comisiones')
                                        @endcan
                                        @can(PermissionKey::ComissionSupervision['permissions']['edit']['name'])
                                            @include('panel.ventas.components.comisionesSupervision')
                                        @endcan
                                    @endcan
                                </div>
                                <div class="tab-pane fade {{ ($venta->status == 3) ? 'show active': '' }}" id="enganche" role="tabpanel" aria-labelledby="enganche-tab">
                                    @can(PermissionKey::Venta['permissions']['create_abono_enganche']['name'])
                                        @include('panel.ventas.components.enganche')
                                    @endcan
                                </div>
                                {{-- <div class="tab-pane fade {{ ($venta->status == 5) ? 'show active': '' }}" id="pago-contrato" role="tabpanel" aria-labelledby="pago-contrato-tab">
                                    @include('panel.ventas.components.pago_contrato')
                                </div> --}}
                                <div class="tab-pane fade {{ ($venta->status == 4) ? 'show active': '' }}" id="contrato" role="tabpanel" aria-labelledby="contrato-tab">
                                    @can(PermissionKey::Venta['permissions']['update_contrato']['name'])
                                        @include('panel.ventas.components.contrato')
                                    @endcan
                                </div>
                                @if (($venta->status == 5))
                                    <div class="tab-pane fade {{ ($venta->status == 5) ? 'show active': '' }}" id="pagos" role="tabpanel" aria-labelledby="pagos-tab">
                                        @can(PermissionKey::Venta['permissions']['seguimiento_pagos']['name'])
                                            @include('panel.ventas.components.abonos')
                                        @endcan
                                    </div>
                                @endif
                            </div>
                        </div>
                    {{-- </form> --}}
				</div>
			</div>
        </div>
    </div>
    @include('panel.ventas.components.addAbono')
    @include('panel.ventas.components.addComplemento')
@endsection