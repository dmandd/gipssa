@extends('layouts.panel.app')
@section('content')
    <!-- Header -->
    @include('include.panel.header')
    <!-- Page content -->
    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col">
				<div class="card">
				<!-- Card header -->
					<div class="card-header border-0">
						<h3 class="mb-0">Cuentas por cobrar</h3>
                        <h2>Total sin cobrar: {{$total}}</h2>
					</div>
                    <!-- Light table -->
					<div class="table-responsive pb-3">
						<table class="table align-items-center table-flush" id="dataTable">
							<thead class="thead-light">
								<tr>
									<th width="20px">
                                    </th>
                                    <th>Cliente</th>
                                    <th>Lote</th>
                                    <th>Tipo</th>
                                    <th>Fecha</th>
                                    <th>Monto</th>
								</tr>
							</thead>
							<tbody class="list">
								@if ((isset($data)) && (count($data) > 0))
                                    @foreach ($data as $row)
                                        <tr>
                                            <td>
                                                <input type="checkbox" class="" id=""><span style="opacity:0;">{{ $row['id']}}</span>
                                            </td>
                                            <td>{{ $row['cliente'] }}</td>
                                            <td>{{ $row['lote'] }}</td>
                                            <td><a href="{{ route('panel.ventas.edit', ['venta' => $row['id']]) }}">{{ $row['tipo'] }}</a></td>
                                            <td>{{ $row['fecha'] }}</td>
                                            <td>{{ $row['monto'] }}</td>
                                            {{-- <td class="text-right">
                                                <div class="dropdown">
                                                    <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <i class="fas fa-ellipsis-v"></i>
                                                    </a>
                                                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                                        
                                                    </div>
                                                </div>
                                            </td> --}}
                                        </tr>
                                    @endforeach
                                @endif
							</tbody>
						</table>
					</div>
				</div>
			</div>
        </div>
    </div>
@endsection