

@extends('layouts.panel.app')
@section('content')
<style>
	.disponible {fill:green;fill-opacity:0.3;cursor: pointer;}
	.apartado {fill:red;fill-opacity:0.3;cursor: pointer;}
	.vendido {fill:red;fill-opacity:0.3;}
	.disponible:hover {fill-opacity:0.6;}
	.apartado:hover {fill-opacity:0.6;}
	.vendido:hover {fill-opacity:0.6;}
	.form-group.dropdown{width:100%;}
</style>
<script>
	const autocomplete_options = @json($clients);
	
</script>
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />

<!-- Header -->
@include('include.panel.header')
<!-- Page content -->

<div class="container-fluid mt--6">
	<div class="row">
		<div class="col">
			<div class="card">
			<!-- Card header -->
				<div class="card-header border-0">
					{{-- <h3 class="mb-0">Nuevo usuario</h3> --}}
					<h6 class="heading-small text-muted mb-4">Mapas</h6>
					<button id="xx" class="btn btn-lg btn-icon rounded-circle btn-default btn-to-clipboard" data-toggle="tooltip" data-placement="top" title="Compartir"
					 data-clipboard-text="{{ route('desarrollo.disponibilidad', ['slug' => Str::slug($desarrollo->name).'-etapa-'.$desarrollo->stage]) }}">
						<span class="btn-inner--icon"><i class="ni ni-curved-next"></i></span>
					</button>
				</div>
				<!-- Light table -->
				<div class="card-body text-center" style="overflow: scroll;">
						{{ showSVG($desarrollo->plano) }}
				</div>
			</div>
		</div>
	</div>
</div>

<form id="form" role="form" action="{{ route('panel.ventas.store') }}" method="POST" class="needs-validation" novalidate enctype="multipart/form-data">

		{{ csrf_field() }}
	<div class="modal fade" id="modal-form" role="dialog" aria-labelledby="modal-form" aria-hidden="true" style="display: none;">
		<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
			<div class="modal-content">
			
			

				<div class="modal-body p-0">
				<div class="card bg-secondary border-0 mb-0">
					<div class="card-body px-lg-5 py-lg-5 row">
						<div class="col-lg-4">
							<div class="text-muted text-center mt-2 mb-3"><small>Chepina</small></div>
							<img id="chepina" src="" style="width:100%;" alt="Chepina">
							<div class="text-center">
								<button id="btn-quotation" class="btn btn-primary my-4">Cotizar</button>
								<button id="btn-apart" class="btn btn-primary my-4">Apartar</button>
							</div>
						</div>
						<div class="col-lg-8">
							<div id="lote-name" class="text-muted mt-2 mb-3"><small></small></div>
							<div class="form-group mb-3">
								<label class="form-control-label" for="cliente">
									<span class="text-muted">Cliente</span>
								</label>
								<div class="row">
									<div class="col-md-9 "> 
									    <select id="cliente" name="cliente" class="js-example-basic-single" required>																		
											@if (count($clients) > 0)
												<option value="">Seleccione un cliente</option>
												@foreach ($clients as $item)
													<option value="{{ $item->id }}" 
													    {{ (isset($client)) ? 
														($client->id == $item->id ? 'selected':'') : '' }} 
														data-info='@json($item)'>{{ $item->name }} {{ $item->surname }} - {{ $item->email }}
													</option>
												@endforeach
											@endif
										</select>
									</div>
									<div class="col-md-3 ">
										<button class="btn btn-icon btn-primary btn-circle col-lg-12" data-toggle="modal" data-target="#modal-form-client" title="Agregar cliente" type="button"><span class="btn-inner--icon"><i class="ni ni-fat-add"></i></span></button>
									</div>
								</div>
							</div>
							<div class="form-group mb-3">
								<label class="form-control-label" for="plazo">
									<span class="text-muted">Plazo</span>
								</label>
								<select name="plazo" id="plazo" class="form-control" required>
									@if ($desarrollo->financiamientos()->count() > 0)
										<option value="">Selecciona una opción</option>
										@foreach ($desarrollo->financiamientos() as $item)
											<option value="{{ $item->id }}" data-info='@json($item)'>{{ $item->nombre }}</option>
										@endforeach
									@endif
								</select>
							</div>
							<div class="row ">

							<div class="form-group mb-3 col-lg-8"> 
								<label class="form-control-label" for="observaciones">
									<span class="text-muted">Observaciones</span>
								</label>
								<input id="observaciones" class="form-control" placeholder="" type="text" name="detail[observaciones]" value="">
							</div>

							<div class="form-group mb-3 col-lg-8">
								<label class="form-control-label" for="precio_real">
									<span class="text-muted">Precio</span>
								</label>
								<input id="precio_real" class="form-control unfocus" placeholder="" type="text" name="precio_real" value="">
							</div>
							<div class="form-group mb-3 col-lg-4">
								<label class="form-control-label" for="descuento">
									<span class="text-muted">Descuento</span>
								</label>
								<input id="descuento" class="form-control unfocus" placeholder="" type="text" name="descuento" value="">
							</div>
						</div>
							<div class="form-group mb-3">
								<label class="form-control-label" for="precio_final">
									<span class="text-muted">Precio Final</span>
								</label>
								<input id="precio_final" class="form-control unfocus" placeholder="" type="text" name="detail[precio_final]">
							</div>
							<input type="hidden" name="lote_id" id="lote_id" value="">
							<div class="row">
								<div class="form-group mb-3 col-lg-5">
									<label class="form-control-label" for="porc_enganche">
										<span class="text-muted" id="lblProporcionE" >Enganche</span>
									</label>
									<input id="porc_enganche" class="form-control" placeholder="" type="text" name="detail[porc_enganche]">
								</div>
								<div class="form-group mb-3 col-lg-7">
									<label class="form-control-label" for="enganche">
										<span class="text-muted" id="lblMontoE">Monto enganche</span>
									</label>
									<input id="monto_enganche" class="form-control" placeholder="" type="text" name="detail[monto_enganche]">
								</div>
							</div>
							<div class="row">
								<div class="form-group mb-3 d-none enganche-diferido col-lg-5">
									<label class="form-control-label" for="enganche_num_pagos">
										<span class="text-muted" id="lblNoPagos">No. Pagos (Enganche)</span>
									</label>
									<input id="enganche_num_pagos" class="form-control unfocus" placeholder="" type="text" name="detail[enganche_num_pagos]">
								</div>
								<div class="form-group mb-3 d-none enganche-diferido col-lg-7">
									<label class="form-control-label" for="pago_mensual_enganche">
										<span class="text-muted" id="lblPagoMensual">Pago Mensual Enganche</span>
									</label>
									<input id="pago_mensual_enganche" class="form-control unfocus" placeholder="" type="text" name="detail[pago_mensual_enganche]">
								</div>
							</div>
							
							<div class="form-group mb-3 financiamiento-group">
								<label class="form-control-label" for="financiamiento">
									<span class="text-muted">Monto a financiar</span>
								</label>
								<input id="financiamiento_show" class="form-control unfocus" placeholder="" type="text">
								<input id="financiamiento" class="form-control unfocus" placeholder="" type="hidden" name="detail[financiamiento]">
							</div>
							<div class="row">
								<div class="form-group mb-3 d-none financiamiento-group col-lg-4">
									<label class="form-control-label" for="mensualidad">
										<span class="text-muted">Mensualidades</span>
									</label>
									<input id="mensualidad" class="form-control unfocus" placeholder="" type="text" name="detail[mensualidad]">
								</div>
								<div class="form-group mb-3 d-none financiamiento-group col-lg-4">
									<label class="form-control-label" for="pago_mensual">
										<span class="text-muted">Pago mensual de</span>
									</label>
									<input id="pago_mensual" class="form-control unfocus" placeholder="" type="text" name="detail[pago_mensual]">
								</div>
								<div class="form-group mb-3 d-none financiamiento-group col-lg-4">
									<label class="form-control-label" for="ultima_mensualidad">
										<span class="text-muted" id="lblUltimaMensualidad">Última men.</span>
									</label>
									<input id="ultima_mensualidad" class="form-control unfocus" placeholder="" type="text" name="detail[ultima_mensualidad]">
								</div>
							</div>
							<div class="form-group mb-3" id="display-saldo">
								<label class="form-control-label" for="saldo">
									<span class="text-muted" id="lblSaldo">Saldo</span>
								</label>
								<input id="saldo" class="form-control unfocus" placeholder="" type="text" name="detail[saldo]">
							</div>
						</div>
					</div>
				</div>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="modal-form-apartar" role="dialog" aria-labelledby="modal-form-apartar" aria-hidden="true" style="display: none;">
		<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-body p-0">
					<div class="card bg-secondary border-0 mb-0">
						<div class="card-header bg-transparent pb-5">
							<div class="text-muted text-center mt-3 mb-1">Apartar</div>
							<div class="card-body px-lg-12 py-lg-12">
								<div class="text-center">
									<input type="hidden" id="venta_id" name="venta_id" value=""/>
									@if (($desarrollo->clientDocuments(1)) && ($desarrollo->clientDocuments(1)->count() > 0))
										@foreach ($desarrollo->clientDocuments(1) as $document)
											<div class="col-lg-12 mb-12 text-truncate">
												<label class="form-control-label" for="{{ $document->slug }}">
													<span class="text-muted">*{{ $document->name }} - </b></span><br>
												
													<small>Seleccionar un archivo no mayor a 500kb, Formato .jpeg, .jpg</small>
												</label>
												<div class="input-group input-group-merge input-group-alternative">
													<input type="file"  name="document[{{ $document->id }}]" id="{{ $document->slug }}" class="form-control form-control-alternative" accept="image/jpg,image/jpeg">
												</div>
											</div>
										@endforeach
									@endif
								</div>
								<div class="col-lg-12 text-center">
									<button class="btn btn-default">Confirmar</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>
<form role="form" action="{{ route('panel.clients.store') }}" method="POST" class="needs-validation" novalidate>
	{{ csrf_field() }}
	<div class="modal fade" id="modal-form-client" role="dialog" aria-labelledby="modal-form-client" aria-hidden="true" style="display: none;">
		<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-body p-0">
					<div class="card bg-secondary border-0 mb-0">
						<div class="card-header bg-transparent pb-5">
							<div class="text-muted text-center mt-3 mb-1">Agregar cliente nuevo</div>
						</div>
						<input type="hidden" name="desarrollo" value="{{$desarrollo->id}}">
						<div class="card-body px-lg-5 py-lg-5">
							<div class="form-group">
								<label class="form-control-label" for="name">
									<span class="text-muted">Nombre</span>
								</label>
								<input id="name" class="form-control"  type="text" name="name" autocomplete="off" value="" required>
							</div>
							<div class="form-group w-100">
								<label class="form-control-label" for="Apellido">
									<span class="text-muted">Apellido</span>
								</label>
								<input id="surname" class="form-control" value=""  type="text" name="surname" required autocomplete="off">
							</div>
							<div class="form-group w-100">
								<label class="form-control-label" for="email">
									<span  class="text-muted">Email</span>
								</label>
								<input id="email" class="form-control" value=""  type="email" name="email" required autocomplete="off">
							</div>
							<div class="form-group w-100">
								<label class="form-control-label" for="phone">
									<span  class="text-muted">Teléfono</span>
								</label>
								<input id="phone" class="form-control" value="" type="text" name="phone" required autocomplete="off">
							</div>
							<div class="form-group w-100">
								<label class="form-control-label" for="nacionalidad"><span required class="text-muted">Nacionalidad</span></label>
								<input type="text" name="nacionalidad" id="nacionalidad" class="form-control" autocomplete="off" value="Mexicana" required>
							</div>
							<div class="text-center">
								<button class="btn btn-primary my-4">Crear</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>
<div class="modal fade" id="modal-form-cotizar" role="dialog" aria-labelledby="modal-form-cotizar" aria-hidden="true" style="display: none;">
     

	<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
	<div class="modal-content">
	
	
	   <div class="modal-body p-0">
		<div class="card bg-secondary border-0 mb-0">
			<div class="card-header bg-transparent pb-5">
			<div class="text-muted text-center mt-3 mb-1">Cotizar</div>
			</div>
			<div class="card-body px-lg-5 py-lg-5">
			<div class="text-center">
				<a href="#" class="btn btn-lg btn-icon btn-default text-white" type="button" data-toggle="tooltip" data-placement="top" title="Descargar" id="btn-descargar">
					<span class="btn-inner--icon"><i class="ni ni-cloud-download-95"></i> Descargar</span>
				</a>
				<a href="#" data-toggle="modal" data-target="#sendMail" class="btn btn-lg btn-icon btn-default" data-toggle="tooltip" data-placement="top" title="Enviar por correo">
					<span class="btn-inner--icon"><i class="ni ni-curved-next"></i> Enviar por correo</span>
				</a>
			</div>
		</div>
		</div>
		</div>
	</div>
	</div>
</div>
<div class="modal fade" id="sendMail" role="dialog" aria-labelledby="sendMail" aria-hidden="true">
    <div class="modal-dialog modal- modal-dialog-centered modal-sm" role="document">
        <div class="modal-content">   
            <div class="modal-body p-0">                
                <div class="card bg-secondary border-0 mb-0">
                    <div class="card-body px-lg-5 py-lg-5">
                        <div class="text-center text-muted mb-4">
                            <small>Enviar Por Correo</small>
                        </div>
                        <form method="POST" action="{{ route('mail.send.quotation', ['id' => 1]) }}" class="needs-validation" novalidate>
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label class="form-control-label" for="to">* Destino</label>
                                        <input type="email" name="to" id="to" class="form-control" required autocomplete="off" value="">
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">
                                        <label class="form-control-label" for="subject">* Asunto</label>
                                        <input type="text" name="subject" id="subject" class="form-control" required autocomplete="off" value="">
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label class="form-control-label">Descripción</label>
                                        <textarea name="description" class="form-control" id="" cols="30" rows="10"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="text-center">
                                <button class="btn btn-primary my-4">Confirmar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('js')
														
	<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>	
	<script src="{{ asset('panel/js/cotizacion.js') }}?v=1.3"></script>
	<script src="{{ asset('panel/js/autocomplete.js') }}"></script>													
	<script>
		let lotes = @json($desarrollo->lotes());
		let venta_id = null;
		$(document).ready(function() {
   		 	$('.js-example-basic-single').select2();
			
			$(document).on('hidden.bs.modal', '.modal', function () {
				$('.modal:visible').length && $(document.body).addClass('modal-open');
			});
		
			$(document).on('click', '#btn-quotation', function (event) {
				
				let form = document.getElementById("form");
				if(form.checkValidity()){
					event.preventDefault();

					let token = $("#token").val();
					let admin_id = 1;
					let lote_id = $("#lote_id").val();
					let plazo = $("#plazo").val();
					let detalle = $("#form").serialize();
					console.log(detalle);


					$.ajax({
						url:'/cotizar',
						type:'POST',
						data:detalle,
						success: function(result){
							
						console.log(result);
						venta_id = result;
						$("#modal-form-cotizar").modal("show");
					}});
					}
				});

			$(document).on('click', '#btn-apart', function (event) {
				let form = document.getElementById("form");
				if(form.checkValidity()){
					event.preventDefault();

					let token = $("#token").val();
					let admin_id = 1;
					let lote_id = $("#lote_id").val();
					let plazo = $("#plazo").val();
					let detalle = $("#form").serialize();
					console.log(detalle);

					$.ajax({
						url:'/cotizar',
						type:'POST',
						data:detalle,
						success: function(result){
						console.log(result);
						venta_id = result;
						$("#venta_id").val(venta_id);
						$("#modal-form-apartar").modal("show");
					}});
				}
			});

			let delayTimer;
			$(document).on('keyup','#form :input', function() {
				clearTimeout(delayTimer);
				delayTimer = setTimeout(function(){recotizar()},800);
			});

			$(document).on('change','#form :input', function() {
				clearTimeout(delayTimer);
				delayTimer = setTimeout(function(){recotizar()},800);
			});

			$(document).on('click', '#btn-descargar', function () {
				window.open("/documentos/pdf/cotizacion/"+venta_id, '_blank');
			});
			$(document).on('click', '#btn-apartar', function () {
				window.open("/documentos/pdf/cotizacion/"+venta_id, '_blank');
			});

			$('#modal-form').on('hidden.bs.modal', function () {
				let form = document.getElementById("form").reset();
			});
		});

		const formatter = new Intl.NumberFormat('es-MX', {
			style: 'currency',
			currency: 'MXN',
		});
		
		const parseFloatValue = (value) => {
			return  parseFloat(value.replace(/[^0-9.-]+/g,""));
		};

		const checkNumeric = (numbers) => {
			let checkValue = true;
			for(let i=0;numbers.length>i;i++){
				if(!$.isNumeric(numbers[i])){
					checkValue = false;
				}
			}
			return checkValue;
		};

		const redondearPago = (plazo, cantidad) => {
			let mensualidad = cantidad/plazo;
			let redondeo_minimo = 50;
			let monto_mensual_redondeado = 0;

			if(mensualidad >= redondeo_minimo){
				let cociente = Math.floor(mensualidad / redondeo_minimo);

				monto_mensual_redondeado = cociente * redondeo_minimo;
			}else{
				monto_mensual_redondeado = 0;
			}

			return monto_mensual_redondeado;
		};

		const recotizar = () => {
			try{
				let plazo = document.getElementById("plazo");
				let porcentaje_enganche = $("#porc_enganche");
				let monto_enganche = $("#monto_enganche");
				let enganche_num_pagos = $("#enganche_num_pagos");
				let pago_mensual_enganche = $("#pago_mensual_enganche");
				let financiamiento = $("#financiamiento");
				let financiamiento_show = $("#financiamiento_show");
				let mensualidad = $("#mensualidad")
				let pago_mensual = $("#pago_mensual");
				let saldo = $("#saldo");
				let ultima_mensualidad = $("#ultima_mensualidad");
				let precio_final = $("#precio_final");
				let precio_real = $("#precio_real");
				let descuento = $("#descuento");

				plazo = JSON.parse(plazo.options[plazo.selectedIndex].dataset.info);
				precio_real = parseFloatValue(precio_real.val());
				descuento = parseFloatValue(descuento.val());

				if(checkNumeric([precio_real, descuento])){
					precio_final.val(formatter.format(precio_real - descuento));
				}else{
					precio_final.val("");
				}

				ultima_mensualidad.val("");

				if(plazo.tipo == "contado" || plazo.tipo == "contado_diferido" || plazo.tipo == "financiamiento"){
					let porcentaje_enganche_value = parseFloatValue(porcentaje_enganche.val());
					let precio_final_value = parseFloatValue(precio_final.val());

					if(checkNumeric([porcentaje_enganche_value, precio_final_value])){
						let nuevo_enganche = (porcentaje_enganche_value * precio_final_value)/100;
						let redondeo_enganche = redondearPago(1,nuevo_enganche);
						let enganche = redondeo_enganche;
						let nuevo_saldo = (precio_final_value - enganche);

						monto_enganche.val(formatter.format(enganche));
						saldo.val(formatter.format(nuevo_saldo));
					}else{
						monto_enganche.val("");
						saldo.val("");
					}
				}

				if(plazo.tipo == "contado_diferido" || plazo.tipo == "financiamiento"){
					let enganche_num_pagos_value = parseFloatValue(enganche_num_pagos.val());
					let monto_enganche_value = parseFloatValue(monto_enganche.val());
					let precio_final_value = parseFloatValue(precio_final.val());

					if(checkNumeric([enganche_num_pagos_value, monto_enganche_value, precio_final_value])){
						let nuevo_pago_mensual_enganche_redondeo = redondearPago(enganche_num_pagos_value,monto_enganche_value);
						let nuevo_saldo =  precio_final_value - (nuevo_pago_mensual_enganche_redondeo * enganche_num_pagos_value);

						pago_mensual_enganche.val(formatter.format(nuevo_pago_mensual_enganche_redondeo));
						monto_enganche.val(formatter.format(nuevo_pago_mensual_enganche_redondeo * enganche_num_pagos_value));
						saldo.val(formatter.format(nuevo_saldo));
					}else{
						pago_mensual_enganche.val("");
						saldo.val("");
					}
				} 
				
				if(plazo.tipo == "financiamiento"){
					financiamiento_show.val(saldo.val());
					financiamiento.val(parseFloatValue(saldo.val()));
					let mensualidad_value = parseFloatValue(mensualidad.val());
					let financiamiento_show_value = parseFloatValue(financiamiento_show.val());

					if(checkNumeric([mensualidad_value,financiamiento_show_value])){
						let nuevo_pago_mensual_redondeo = redondearPago((mensualidad_value),financiamiento_show_value);
						let ultimo_pago =  financiamiento_show_value - (nuevo_pago_mensual_redondeo * (mensualidad_value));
						
						if(ultimo_pago !== 0){
							nuevo_pago_mensual_redondeo = redondearPago((mensualidad_value-1),financiamiento_show_value);
							ultimo_pago =  financiamiento_show_value - (nuevo_pago_mensual_redondeo * (mensualidad_value-1));
						}

						pago_mensual.val(formatter.format(nuevo_pago_mensual_redondeo));
						ultima_mensualidad.val(formatter.format(ultimo_pago));
						saldo.val("");
					}else{
						pago_mensual.val("");
						ultima_mensualidad.val("");
						saldo.val("");
					}
				}
			}catch(e){
				console.log(e);
			}
		};

	</script>
@endpush

