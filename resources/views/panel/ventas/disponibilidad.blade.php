<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
    <!-- Icons -->
    <link rel="stylesheet" href="{{asset('panel/vendor/nucleo/css/nucleo.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('panel/vendor/@fortawesome/fontawesome-free/css/all.min.css')}}" type="text/css">
    <!-- Argon CSS -->
    <link rel="stylesheet" href="{{asset('panel/css/main.css?v=1.2.0')}}" type="text/css">
    <title>Gipssa plano</title>
    <style>
        .disponible {fill:green;fill-opacity:0.3;}
        .apartado {fill:red;fill-opacity:0.3;}
        .vendido {fill:red;fill-opacity:0.3;}
        .disponible:hover {fill-opacity:0.6;}
        .apartado:hover {fill-opacity:0.6;}
        .vendido:hover {fill-opacity:0.6;}
        .form-group.dropdown{width:100%;}
    </style>
</head>
<body>
    <!-- Page content -->
    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col">
                <div class="card">
                <!-- Card header -->
                    <div class="card-header border-0">
                        {{-- <h3 class="mb-0">Nuevo usuario</h3> --}}
                        <h6 class="heading-small text-muted mb-4">Mapa</h6>
                    </div>
                    <!-- Light table -->
                    <div class="card-body text-center" style="overflow: scroll;">
                            {{ showSVG($desarrollo->plano) }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
<script>
    let lotes = @json($desarrollo->lotes());
</script>
<script src="{{ asset('panel/js/cotizacion.js') }}?v=1.4"></script>
</html>



