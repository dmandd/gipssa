@extends('layouts.panel.app')
@section('content')
    <!-- Header -->
    @include('include.panel.header')
    <link rel="stylesheet" href="{{ asset('assets/css/kanban.css') }}">
    <!-- Page content -->
    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col">
				<div class="card">
				<!-- Card header -->
					<div class="card-header border-0 d-flex justify-content-between ">
						<h3 class="mb-0">Ventas - {{ mb_strtoupper($desarrollo->name)}} ETAPA {{mb_strtoupper($desarrollo->stage)}}</h3>
						<a href="{{ route('panel.ventas.create', ['desarrollo' => $desarrollo->id]) }}"  class="btn btn-sm btn-primary">Crear Apartado</a>
					</div>
                    <!-- Light table -->
					<div class="card-body">
					<div class="container-kanban">
						<div class="">
							
							
							
						</div>
						<div class="kanban-board container-fluid mt-lg-3" tabindex="0">{{-- COTIZACION --}}
							{{--<div class="kanban-col" tabindex="0">
								<div class="card-list">
									<div class="card-list-header">
										<h6>Cotización</h6>
										{{-- <div class="dropdown">
											<a class="btn" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
												<i class="fas fa-ellipsis-v"></i>
											</a>
											<div class="dropdown-menu dropdown-menu-right">
												<a class="dropdown-item" href="#">Edit</a>
												<a class="dropdown-item text-danger" href="#">Archive List</a>
											</div>
										</div>--}} 
									{{--</div>
									
									<div class="card-list-body" tabindex="0">
										@if (request()->user()->can(PermissionKey::Venta['permissions']['index']['name']))
											@if (Venta::where('status', 0)->where('development_id', request()->desarrollo)->get()->count() > 0)
												@foreach (Venta::where('status', 0)->where('development_id', request()->desarrollo)->orderBy('created_at', 'desc')->get() as $item)
													@include('panel.ventas.components.kanban_card')
												@endforeach
											@endif
										@else
											@if (Venta::with(['withQuotation'])->where('status', 0)->where('development_id', request()->desarrollo)->get()->count() > 0)
												@foreach (Venta::where('status', 0)->where('development_id', request()->desarrollo)->orderBy('created_at', 'desc')->get() as $item)
													@include('panel.ventas.components.kanban_card')
												@endforeach
											@endif
										@endif
									</div>
								</div>
							</div>--}}
							{{-- BLOQUEO --}}
							<div class="kanban-col" tabindex="0">
								<div class="card-list">
									<div class="card-list-header">
										<h6>Apartado y Pago del contrato</h6>
									</div>
									<div class="card-list-body" tabindex="0">
										@if (request()->user()->can(PermissionKey::Venta['permissions']['index']['name']))
											@if (Venta::where('status', 1)->where('development_id', request()->desarrollo)->get()->count() > 0)
												@foreach (Venta::where('status', 1)->where('development_id', request()->desarrollo)->orderBy('created_at', 'desc')->get() as $item)
													@include('panel.ventas.components.kanban_card')
												@endforeach
											@endif
										@else
											@if (Venta::where('admin_id', request()->user()->id)->where('status', 1)->where('development_id', request()->desarrollo)->join("quotations", "ventas.quotation_id", "=", "quotations.id")->get()->count() > 0)
												@foreach (Venta::select("ventas.*")->where('admin_id', request()->user()->id)->where('status', 1)->where('development_id', request()->desarrollo)->join("quotations", "ventas.quotation_id", "=", "quotations.id")->orderBy('ventas.created_at', 'desc')->get() as $item)
													@include('panel.ventas.components.kanban_card')
												@endforeach
											@endif
										@endif
									</div>
									<div class="card-list-footer">
									</div>
								</div>
							</div>
							{{-- DOCUMENTACION --}}
							<div class="kanban-col" tabindex="0">
								<div class="card-list">
									<div class="card-list-header">
									<h6>Documentación y Contrato</h6>
									
									</div>
									<div class="card-list-body" tabindex="0">
										@if (request()->user()->can(PermissionKey::Venta['permissions']['index']['name']))
											@if (Venta::where('status', 2)->where('development_id', request()->desarrollo)->get()->count() > 0)
												@foreach (Venta::where('status', 2)->where('development_id', request()->desarrollo)->orderBy('created_at', 'desc')->get() as $item)
													@include('panel.ventas.components.kanban_card')
												@endforeach
											@endif
										@else
											@if (Venta::where('admin_id', request()->user()->id)->where('status', 2)->where('development_id', request()->desarrollo)->join("quotations", "ventas.quotation_id", "=", "quotations.id")->get()->count() > 0)
												@foreach (Venta::select("ventas.*")->where('admin_id', request()->user()->id)->where('status', 2)->where('development_id', request()->desarrollo)->join("quotations", "ventas.quotation_id", "=", "quotations.id")->orderBy('ventas.created_at', 'desc')->get() as $item)
													@include('panel.ventas.components.kanban_card')
												@endforeach
											@endif
										@endif
									</div>
									<div class="card-list-footer">
									{{-- <button class="btn btn-link btn-sm text-small">Add task</button> --}}
									</div>
								</div>
							</div>
							{{-- CONDICION DE COMPRA --}}
							{{-- <div class="kanban-col" tabindex="0">
								<div class="card-list">
									<div class="card-list-header">
									<h6>Condición de compra</h6>
									
									</div>
									<div class="card-list-body" tabindex="0">
										@if (request()->user()->can(PermissionKey::Venta['permissions']['index']['name']))
											@if (Venta::where('status', 3)->where('development_id', request()->desarrollo)->get()->count() > 0)
												@foreach (Venta::where('status', 3)->where('development_id', request()->desarrollo)->get() as $item)
													@include('panel.ventas.components.kanban_card')
												@endforeach
											@endif
										@else
										@endif
									</div>
									<div class="card-list-footer">
									</div>
								</div>
							</div> --}}
				
							<div class="kanban-col" tabindex="0">
								<div class="card-list">
									<div class="card-list-header">
										<h6>Pago(s) de Contado y Enganche</h6>
									</div>
									<div class="card-list-body" tabindex="0">
										@if (request()->user()->can(PermissionKey::Venta['permissions']['index']['name']))
											@if (Venta::where('status', 3)->where('development_id', request()->desarrollo)->get()->count() > 0)
												@foreach (Venta::where('status', 3)->where('development_id', request()->desarrollo)->orderBy('created_at', 'desc')->get() as $item)
													@include('panel.ventas.components.kanban_card')
												@endforeach
											@endif
										@else
											@if (Venta::where('admin_id', request()->user()->id)->where('status', 3)->where('development_id', request()->desarrollo)->join("quotations", "ventas.quotation_id", "=", "quotations.id")->get()->count() > 0)
												@foreach (Venta::select("ventas.*")->where('admin_id', request()->user()->id)->where('status', 3)->where('development_id', request()->desarrollo)->join("quotations", "ventas.quotation_id", "=", "quotations.id")->orderBy('ventas.created_at', 'desc')->get() as $item)
													@include('panel.ventas.components.kanban_card')
												@endforeach
											@endif
										@endif
									</div>
								</div>
							</div>
							<div class="kanban-col" tabindex="0">
								<div class="card-list">
									<div class="card-list-header">
									<h6>Subir contrato</h6>
									</div>
									<div class="card-list-body" tabindex="0">
										@if (request()->user()->can(PermissionKey::Venta['permissions']['index']['name']))
											@if (Venta::where('status', 4)->where('development_id', request()->desarrollo)->get()->count() > 0)
												@foreach (Venta::where('status', 4)->where('development_id', request()->desarrollo)->orderBy('created_at', 'desc')->get() as $item)
													@include('panel.ventas.components.kanban_card')												
												@endforeach
											@endif
										@else
											@if (Venta::where('admin_id', request()->user()->id)->where('status', 4)->where('development_id', request()->desarrollo)->join("quotations", "ventas.quotation_id", "=", "quotations.id")->get()->count() > 0)
												@foreach (Venta::select("ventas.*")->where('admin_id', request()->user()->id)->where('status', 4)->where('development_id', request()->desarrollo)->join("quotations", "ventas.quotation_id", "=", "quotations.id")->orderBy('ventas.created_at', 'desc')->get() as $item)
													@include('panel.ventas.components.kanban_card')
												@endforeach
											@endif
										@endif
									</div>
								</div>
							</div>
							<div class="kanban-col" tabindex="0">
								<div class="card-list">
									<div class="card-list-header">
									<h6>Pago(s) de Saldo y Financiamiento</h6>
									</div>
									<div class="card-list-body" tabindex="0">
										@if (request()->user()->can(PermissionKey::Venta['permissions']['index']['name']))
											@if (Venta::where('status', 5)->where('development_id', request()->desarrollo)->get()->count() > 0)
												@foreach (Venta::where('status', 5)->where('development_id', request()->desarrollo)->orderBy('created_at', 'desc')->get() as $item)
													@include('panel.ventas.components.kanban_card')												
												@endforeach
											@endif
										@else
											@if (Venta::where('admin_id', request()->user()->id)->where('status', 5)->where('development_id', request()->desarrollo)->join("quotations", "ventas.quotation_id", "=", "quotations.id")->get()->count() > 0)
												@foreach (Venta::select("ventas.*")->where('admin_id', request()->user()->id)->where('status', 5)->where('development_id', request()->desarrollo)->join("quotations", "ventas.quotation_id", "=", "quotations.id")->orderBy('ventas.created_at', 'desc')->get() as $item)
													@include('panel.ventas.components.kanban_card')
												@endforeach
											@endif
										@endif
									</div>
								</div>
							</div>
							{{-- <div class="kanban-col" tabindex="0">
								<div class="card-list">
									<div class="card-list-header">
									<h6>Contrato</h6>
									</div>
									<div class="card-list-body" tabindex="0">
										@if (Venta::where('status', 6)->where('development_id', request()->desarrollo)->get()->count() > 0)
											@foreach (Venta::where('status', 6)->where('development_id', request()->desarrollo)->get() as $item)
												@include('panel.ventas.components.kanban_card')
											@endforeach
										@endif
									</div>
								</div>
							</div> --}}
				
							{{-- <div class="kanban-col">
							<div class="card-list">
								<button class="btn btn-link btn-sm text-small">Add list</button>
							</div> --}}
							</div>
						</div>
					</div>
				</div>
				</div>
			</div>
        </div>
    </div>
@endsection