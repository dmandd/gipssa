<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet">
    {{-- <link href="https://unpkg.com/tailwindcss@^1.0/dist/tailwind.min.css" rel="stylesheet"> --}}
    <title>{{$comission->tipo.' - Historial de pago de comisiones - Venta '.$venta->id}}</title>
    <style>
        @font-face {
            font-family: 'Avenir';
            src: url('../fonts/Avenir.eot?#iefix') format('embedded-opentype'),  url('../fonts/Avenir.woff') format('woff'), url('../fonts/Avenir.ttf')  format('truetype'), url('../fonts/Avenir.svg#Avenir') format('svg');
          }
        @page {
            margin:160px 0px 0px 0px !important;
            padding-top:20px!important;
            padding-bottom:30px!important;
            /* font-family:Arial, Helvetica, sans-serif; */
            font-family: 'Avenir', Helvetica, sans-serif;
        }
        body{margin-right: 50px;margin-left: 50px;}
        table{table-layout: fixed}
        .border-solid{border:1px solid #000;}
        .text-center{text-align: center;}
        table.informacion .label{height:12px;line-height: 12px;text-align:right;padding-right:10px;font-weight: bold;}
        table.informacion td{height: 30px;}
        table.corrida th{height: 30px;}
        table.corrida td{height: 20px;}
        p span.subtitle{background-color:#cccccc;padding:2px;}
        p span.subtitle{font-size:12px;}
        ul.clausulas{font-size:12px;}
        ul.clausulas li{margin-bottom:10px;}
        footer {
            position: fixed; 
            bottom: 60px; 
            left: 0px; 
            right: 0px;
            height: 50px; 
            text-align: center;
            line-height: 35px;
        }
    </style>
</head>
<body class="font-sans">
   
    <table style="width:100%;float:right;font-size:13px;margin-top:-120px;">
        <tbody>
            <td class="text-center" rowspan="5">
                <p style="width:70%;color:grey;font-size:24px;"><b>Historial de pagos</b></p>
            </td>
            <tr>
                <td class="">
                    <div style="font-weight:bold;width:100%;height:auto;text-align:right;">Fecha de Elaboración</div>
                </td>
                <td class="text-center border-solid">
                    {{ date('d/m/Y') }}
                </td>
            </tr>
            <tr>
                <td class="">
                    <div style="font-weight:bold;width:100%;height:auto;text-align:right;">Supervisor de Ventas</div>
                </td>
                <td class="text-center border-solid">
                    @if($supervisor->admin())
                        {{ $supervisor->admin()->name." ".$supervisor->admin()->last_name }}
                    @else
                        Sin supervisor asigando
                    @endif
                </td>
            </tr>
            <tr>
                <td class="">
                    <div style="font-weight:bold;width:100%;height:auto;text-align:right;">Ejecutivo de Ventas</div>
                </td>
                <td class="text-center border-solid">
                    {{ $venta->asesor()->name }} {{ $venta->asesor()->last_name }}
                </td>
            </tr>
            <tr>
                <td class="">
                    <div style="font-weight:bold;width:100%;height:auto;text-align:right;">Desarrollo</div>
                </td>
                <td class="text-center border-solid">
                    {{ $development->name }}
                </td>
            </tr>
            @if ($development->stage)
                <tr>
                    <td class="">
                        <div style="font-weight:bold;width:100%;height:auto;text-align:right;">Etapa</div>
                    </td>
                    <td class="text-center border-solid">
                        {{ $development->stage }}
                    </td>
                </tr>
            @endif
        </tbody>
    </table>
    <table style="margin-top:20%;"></table>
    <div class="section-informacion" style="margin-top:-120px;">
        <p ><span class="subtitle">DATOS GENERALES DEL CLIENTE</span></p>
        <table style="width:100%;border-collapse:collapse;font-size:12px;" class="informacion">
            <tbody>
                <tr>
                    <td class="" style="width:25%;">
                        <div class="label">Nombre completo</div>
                    </td>
                    <td class="border-solid" style="width:75%;padding-left:5px;" colspan="3">
                        {{ $client->name }} {{ $client->surname }}
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <table style="margin-top:15px;"></table>
    <div class="section-informacion">
        <p ><span class="subtitle">DESCRIPCIÓN DE LA OPERACIÓN</span></p>
        <table style="width:100%;border-collapse:collapse;font-size:12px;" class="informacion">
            <tbody>
                <tr>
                    <td class="" style="width:25%;">
                        <div class="label">Ubicación</div>
                    </td>
                    <td class="text-center border-solid" style="width:25%;">
                        {{ $development->location }}
                    </td>
                    <td class="" style="width:25%;">
                        <div class="label">Precio</div>
                    </td>
                    <td class="text-center border-solid" style="width:25%;">
                        $ {{ number_format($venta->lote()->price,2) }}
                    </td>
                    <td class="" style="width:25%;">
                        <div class="label">Tasa comisión</div>
                    </td>
                    <td class="text-center border-solid" style="width:25%;">
                        {{ $comission->percentage }} %
                    </td>
                </tr>
                <tr>
                    <td class="" style="width:25%;">
                        <div class="label">Desarrollo</div>
                    </td>
                    <td class="text-center border-solid" style="width:25%;padding-left:5px;background:#BFBFBF;">
                        {{ $development->name }}
                    </td>
                    <td class="" style="width:25%;">
                        <div class="label">Descuento</div>
                    </td>
                    <td class="text-center border-solid" style="width:25%;background:#BFBFBF;">
                        {{ $quotation->detail->plazo->tipo_descuento === "cantidad"?'$ '.number_format($quotation->detail->plazo->porcentaje_descuento,2):"" }}{{ $quotation->detail->plazo->tipo_descuento === "porcentaje"?$quotation->detail->plazo->porcentaje_descuento.' %':"" }}
                    </td>
                    <td class="" style="width:25%;">
                        <div class="label">Monto bruto</div>
                    </td>
                    <td class="text-center border-solid" style="width:25%;">
                        $ {{ number_format($comission->total_amount,2) }}
                    </td>
                </tr>
                <tr>
                    <td class="" style="width:25%;">
                        <div class="label">Etapa</div>
                    </td>
                    <td class="text-center border-solid" style="width:25%;">
                        {{ $development->stage }}
                    </td>
                    <td class="" style="width:25%;">
                        <div class="label">Precio Total</div>
                    </td>
                    <td class="text-center border-solid" style="width:25%;">
                        {{ $quotation->detail->precio_final }}
                    </td>
                    <td class="" style="width:25%;">
                        <div class="label">Parcialidades </div>
                    </td>
                    <td class="text-center border-solid" style="width:25%;">
                        {{ $comission->payments }}
                    </td>
                </tr>
                <tr>
                    <td class="" style="width:25%;">
                        <div class="label">Número de lote</div>
                    </td>
                    <td class="text-center border-solid" style="width:25%;background:#BFBFBF;">
                        L - {{ $quotation->lote()->num_lote }}
                    </td>
                    @if($quotation->detail->plazo->tipo == 'financiamiento')
                        <td class="" style="width:25%;">
                            <div class="label">% del Enganche</div>
                        </td>
                        <td class="text-center border-solid" style="width:25%;background:#BFBFBF;">
                            {{ $quotation->detail->porc_enganche }}
                        </td>
                    @else
                        <td class="" style="width:25%;">
                            <div class="label">% del Pago</div>
                        </td>
                        <td class="text-center border-solid" style="width:25%;background:#BFBFBF;">
                            {{ $quotation->detail->porc_enganche }}
                        </td>
                    @endif
                    <td class="" style="width:25%;">
                    </td>
                    <td class="text-center border-solid" style="width:25%;">
                    </td>
                </tr>
                <tr>
                    <td class="" style="width:25%;">
                        <div class="label">Medidas</div>
                    </td>
                    <td class="text-center border-solid" style="width:25%;">
                        {{ $venta->lote()->medida }}
                    </td>
                    @if($quotation->detail->plazo->tipo == 'financiamiento')
                        <td class="" style="width:25%;">
                            <div class="label">Monto Enganche</div>
                        </td>
                        <td class="text-center border-solid" style="width:25%;">
                            {{ $quotation->detail->monto_enganche }}
                        </td>
                    @else
                        <td class="" style="width:25%;">
                            <div class="label">Monto a Pagar</div>
                        </td>
                        <td class="text-center border-solid" style="width:25%;">
                            {{ $quotation->detail->monto_enganche }}
                        </td>
                    @endif
                    <td class="" style="width:25%;">
                    </td>
                    <td class="text-center border-solid" style="width:25%;">
                    </td>
                </tr>
                <tr>
                    <td class="" style="width:25%;">
                        <div class="label">M2</div>
                    </td>
                    <td class="text-center border-solid" style="width:25%;">
                        {{ $quotation->lote()->area }} M<sup>2</sup>
                    </td>
                    <td class="" style="width:25%;">
                        <div class="label">Saldo</div>
                    </td>
                    <td class="text-center border-solid" style="width:25%;">
                        {{ $quotation->detail->saldo }}
                    </td>
                    <td class="" style="width:25%;">
                    </td>
                    <td class="text-center border-solid" style="width:25%;">
                    </td>
                </tr>
                <tr>
                    <td class="" style="width:25%;">
                        <div class="label"></div>
                    </td>
                    <td class="text-center border-solid" style="width:25%;">
                    </td>
                    @if($quotation->detail->plazo->tipo == 'financiamiento')
                        <td class="" style="width:25%;">
                            <div class="label">Mensualidad</div>
                        </td>
                        <td class="text-center border-solid" style="width:25%;">
                            {{ $quotation->detail->pago_mensual }}
                        </td>
                        <td class="" style="width:25%;">
                        </td>
                        <td class="text-center border-solid" style="width:25%;">
                        </td>
                    @else
                        <td class="" style="width:25%;">
                        </td>
                        <td class="text-center border-solid" style="width:25%;">
                        </td>
                        <td class="" style="width:25%;">
                        </td>
                        <td class="text-center border-solid" style="width:25%;">
                        </td>
                    @endif
                </tr>
            </tbody>
        </table>
    </div>
    <br>
    <div class="section-informacion">
        <p ><span class="subtitle">DESCRIPCIÓN DE PAGOS</span></p>
        <table style="width:100%;border-collapse:collapse;font-size:12px;" class="informacion">
            <thead>
                <tr>
                    <th class="border-solid"># pago cliente</th>
                    <th class="border-solid">Fecha de pago</th>
                    <th class="border-solid">Monto</th>
                    <th class="border-solid">Ingreso Neto</th>
                    <th class="border-solid">Fecha pago comisión</th>
                    <th class="border-solid">Firma</th>
                </tr>
            </thead>
            <tbody>
                @if ($comission->payments > 0)
                    @foreach ($comission->payments() as $key => $payment)
                        @php
                            $fecha_pago = Carbon\Carbon::createFromFormat('Y-m-d', $payment->payment_date);
                        @endphp
                        <tr>
                            <td class="border-solid text-center">{{$key + 1}}</td>
                            <td class="border-solid text-center"></td>
                            <td class="border-solid text-center"></td>
                            <td class="border-solid text-center">${{ number_format($payment->payment_amount,2) }}</td>
                            <td class="border-solid text-center"></td>
                            <td class="border-solid text-center"></td>
                            

                        </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
    </div>
    <footer>
        <table style="width:100%;">
            <tbody>
                <tr>
                    <td class="text-center" style="width: 33%;">
                        <div style="border-top:1px solid gray;width:80%;margin:auto;">
                            <p style="width:100%;margin:2.5!important;">FIRMA</p>
                            <p style="width:100%;margin:2.5!important;">ADMINISTRACION</p>
                        </div>
                    </td>
                    <td class="text-center" style="width: 33%;">
                        <div style="border-top:1px solid gray;width:80%;margin:auto;">
                            <p style="width:100%;margin:2.5!important;">FIRMA</p>
                            <p style="width:100%;margin:2.5!important;">VENDEDOR</p>
                        </div>
                    </td>
                    <td class="text-center" style="width: 33%;">
                        <div style="border-top:1px solid gray;width:80%;margin:auto;">
                            <p style="width:100%;margin:2.5!important;">FIRMA</p>
                            <p style="width:100%;margin:2.5!important;">GERENTE</p>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </footer>
</body>
</html>