<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet">
    {{-- <link href="https://unpkg.com/tailwindcss@^1.0/dist/tailwind.min.css" rel="stylesheet"> --}}
    <title>Cotización</title>
    <style>
        @font-face {
            font-family: 'Avenir';
            src: url('../fonts/Avenir.eot?#iefix') format('embedded-opentype'),  url('../fonts/Avenir.woff') format('woff'), url('../fonts/Avenir.ttf')  format('truetype'), url('../fonts/Avenir.svg#Avenir') format('svg');
          }
        @page {
            margin:160px 0px 0px 0px !important;
            padding-top:20px!important;
            padding-bottom:30px!important;
            /* font-family:Arial, Helvetica, sans-serif; */
            font-family: 'Avenir', Helvetica, sans-serif;
        }
        body{margin-right: 50px;margin-left: 50px;}
        table{table-layout: fixed}
        .border-solid{border:1px solid #000;}
        .text-center{text-align: center;}
        table.informacion .label{height:12px;line-height: 12px;text-align:right;padding-right:10px;font-weight: bold;}
        table.informacion td{height: 30px;}
        table.corrida th{height: 30px;}
        table.corrida td{height: 20px;}
        p span.subtitle{background-color:#cccccc;padding:2px;}
        p span.subtitle{font-size:12px;}
        ul.clausulas{font-size:12px;}
        ul.clausulas li{margin-bottom:10px;}
        footer {
            position: fixed; 
            bottom: 60px; 
            left: 0px; 
            right: 0px;
            height: 50px; 
            text-align: center;
            line-height: 35px;
        }
    </style>
</head>
<body class="font-sans">
    {{--<div style="position:fixed;left:0;top:-140px;z-index:-1;">
        <img src="{{ asset('panel/img/bg-template.jpeg') }}" style="width:100%;height:1100px;" alt="" srcset="">
    </div>


    @if ($venta->lote()->chepina!="")
    <div style="position:fixed;left:70;top:100;z-index:-1;">
        <img src="{{ asset($venta->lote()->chepina) }}" style="height:100px;" alt="" srcset="">
    </div>        
    @endif
    --}}
    <table style="width:70%;float:right;font-size:13px;margin-top:-120px;">
        <tbody>
            <tr>
                <td class="text-center" colspan="2">
                    <h3 style="margin-left:250px;color:grey;">COTIZACIÓN</h3>
                </td>
            </tr>
            <tr>
                <td class="">
                    <div style="font-weight:bold;width:100%;height:auto;text-align:right;">Fecha de Elaboración</div>
                </td>
                <td class="text-center border-solid">
                    {{ date('d/m/Y') }}
                </td>
            </tr>
            @if (($quotation->detail->observaciones ?? '')==="")
                <tr>
                    <td class="">
                        <div style="font-weight:bold;width:100%;height:auto;text-align:right;">Ejecutivo de Ventas</div>
                    </td>
                    <td class="text-center border-solid">
                        {{ $venta->asesor()->name }} {{ $venta->asesor()->last_name }}
                    </td>
                </tr>  
            @endif
            <tr>
                <td class="">
                    <div style="font-weight:bold;width:100%;height:auto;text-align:right;">Desarrollo</div>
                </td>
                <td class="text-center border-solid">
                    {{ $development->name }}
                </td>
            </tr>
            @if ($development->stage)
                <tr>
                    <td class="">
                        <div style="font-weight:bold;width:100%;height:auto;text-align:right;">Etapa</div>
                    </td>
                    <td class="text-center border-solid">
                        {{ $development->stage }}
                    </td>
                </tr>
            @endif
        </tbody>
    </table>
    <table style="margin-top:30%;"></table>
    <div class="section-informacion" style="margin-top:-120px;">
        <p style="margin-left:15px;"><span class="subtitle">DATOS GENERALES DEL CLIENTE</span></p>
        <table style="width:100%;border-collapse:collapse;font-size:13px;" class="informacion">
            <tbody>
                @if (($quotation->detail->observaciones ?? '')!="")
                    <tr>
                        <td class="" style="width:25%;">
                            <div class="label">Observaciones</div>
                        </td>
                        <td class="text-center border-solid" style="width:75%;" colspan="3">
                        {{ $quotation->detail->observaciones ?? ''}} 
                        </td>
                    </tr>
                @endif

                @if (($quotation->detail->observaciones ?? '')==="")
                    <tr>
                        <td class="" style="width:25%;">
                            <div class="label">Nombre completo</div>
                        </td>
                        <td class="text-center border-solid" style="width:75%;" colspan="3">
                            {{ $client->name }} {{ $client->surname }}
                        </td>
                    </tr>               

                    <tr>
                        <td class="" style="width:25%;">
                            <div class="label">Email</div>
                        </td>
                        <td class="text-center border-solid" style="width:35%;">
                            {{ $client->email }}
                        </td>
                        <td class="" style="width:15%;">
                            <div class="label">Teléfono</div>
                        </td>
                        <td class="text-center border-solid" style="width:35%;">
                            {{ $client->phone }}
                        </td>
                    </tr>
                @endif
            </tbody>
        </table>
    </div>
    <table style="margin-top:15px;"></table>
    <div class="section-informacion">
        <p style="margin-left:15px;"><span class="subtitle">DESCRIPCIÓN DE LA OPERACIÓN</span></p>
        <table style="width:100%;border-collapse:collapse;font-size:13px;" class="informacion">
            <tbody>
                <tr>
                     <td class="" style="width:25%;">
                        <div class="label">Ubicación</div>
                    </td>
                    <td class=" border-solid" style="width:25%;padding-left:5px;">
                        {{ $development->location }}
                    </td>
                    
                   

                    <td class="" style="width:25%;">
                        <div class="label">Precio</div>
                    </td>
                    <td class="text-center border-solid" style="width:25%;">
                        $ {{ number_format($venta->lote()->price,2) }}
                    </td>
                    <td class="" style="width:25%;">
                        <div class="label">Tipo de compra</div>
                    </td>
                    <td class="text-center border-solid" style="width:25%;">
                        {{ $quotation->detail->plazo->nombre }}
                    </td>
                </tr>
                <tr>
                    <td class="" style="width:25%;">
                       <div class="label">Desarrollo</div>
                   </td>
                   <td class="text-center border-solid" style="width:25%;padding-left:5px;background:#BFBFBF;">
                       {{ $development->name }}
                   </td>
                   <td class="" style="width:25%;">
                       <div class="label">Descuento</div>
                   </td>
                   <td class="text-center border-solid" style="width:25%;background:#BFBFBF;">
                   {{ $quotation->detail->plazo->tipo_descuento === "cantidad"?'$ '.number_format($quotation->detail->plazo->porcentaje_descuento,2):"" }}{{ $quotation->detail->plazo->tipo_descuento === "porcentaje"?$quotation->detail->plazo->porcentaje_descuento.' %':"" }}
                   </td>
                   @if($quotation->detail->plazo->tipo == 'financiamiento' || $quotation->detail->plazo->tipo == 'contado_diferido')
                        <td class="" style="width:25%;">
                            <div class="label">{{$quotation->detail->plazo->tipo == 'financiamiento'? "Monto a financiar":"Pago a diferir"}}</div>
                        </td>
                        <td class="text-center border-solid" style="width:25%;">
                            {{ $quotation->detail->plazo->tipo == 'financiamiento' ? "$".number_format($quotation->detail->financiamiento,2) : $quotation->detail->monto_enganche }}
                        </td>
                    @else
                        <td class="" style="width:25%;">
                        </td>
                        <td class="text-center border-solid" style="width:25%;">
                        </td>
                    @endif
               </tr>
               <tr>
                    <td class="" style="width:25%;">
                        <div class="label">Etapa</div>
                    </td>
                    <td class="text-center border-solid" style="width:25%;">
                        {{ $development->stage }}
                    </td>
                    <td class="" style="width:25%;">
                        <div class="label">Precio Total</div>
                    </td>
                    <td class="text-center border-solid" style="width:25%;">
                        {{ $quotation->detail->precio_final }}
                    </td>
                    @if($quotation->detail->plazo->tipo == 'financiamiento' || $quotation->detail->plazo->tipo == 'contado_diferido')
                        <td class="" style="width:25%;">
                            <div class="label">{{ $quotation->detail->plazo->tipo == 'financiamiento' ? "No. meses":"No. Pagos"}}</div>
                        </td>
                        <td class="text-center border-solid" style="width:25%;background:#BFBFBF;">
                            {{ $quotation->detail->plazo->tipo == 'financiamiento' ? $quotation->detail->mensualidad : $quotation->detail->enganche_num_pagos }}
                        </td>
                    @else
                        <td class="" style="width:25%;">
                        </td>
                        <td class="text-center border-solid" style="width:25%;">
                        </td>
                    @endif
                </tr>
                <tr>
                    <td class="" style="width:25%;">
                        <div class="label">Número de lote</div>
                    </td>
                    <td class="text-center border-solid" style="width:25%;background:#BFBFBF;">
                       L - {{ $quotation->lote()->num_lote }}
                    </td>
                    @if($quotation->detail->plazo->tipo == 'financiamiento')
                        <td class="" style="width:25%;">
                            <div class="label">% del Enganche</div>
                        </td>
                        <td class="text-center border-solid" style="width:25%;background:#BFBFBF;">
                            {{ $quotation->detail->porc_enganche }}
                        </td>
                        <td class="" style="width:25%;">
                            <div class="label">Mensualidad</div>
                        </td>
                        <td class="text-center border-solid" style="width:25%;">
                            {{ $venta->quotation()->detail->pago_mensual }}
                        </td>
                    @else
                        <td class="" style="width:25%;">
                            <div class="label">% del Pago</div>
                        </td>
                        <td class="text-center border-solid" style="width:25%;background:#BFBFBF;">
                            {{ $quotation->detail->porc_enganche }}
                        </td>
                        <td class="" style="width:25%;">
                            <div class="label">{{ $quotation->detail->plazo->tipo == 'contado_diferido' ? "Pagos de":""}}</div>
                        </td>
                        @if($quotation->detail->plazo->tipo == 'contado_diferido')
                            <td class="text-center border-solid" style="width:25%;">
                                {{$quotation->detail->pago_mensual_enganche}}
                            </td>
                        @else
                            <td class="text-center border-solid" style="width:25%;">
                            </td>
                        @endif
                    @endif
                </tr>
                <tr>
                    <td class="" style="width:25%;">
                        <div class="label">Medidas</div>
                    </td>
                    <td class="text-center border-solid" style="width:25%;">
                        {{ $venta->lote()->medida }}
                    </td>
                    @if($quotation->detail->plazo->tipo == 'financiamiento')
                        <td class="" style="width:25%;">
                            <div class="label">Monto Enganche</div>
                        </td>
                        <td class="text-center border-solid" style="width:25%;">
                            {{ $quotation->detail->monto_enganche }}
                        </td>
                        <td class="" style="width:25%;">
                            <div class="label">Última mensualidad</div>
                        </td>
                        <td class="text-center border-solid" style="width:25%;">
                            {{ isset($quotation->detail->ultima_mensualidad)?$quotation->detail->ultima_mensualidad:"" }}
                        </td>
                    @else
                        <td class="" style="width:25%;">
                            <div class="label">Monto a Pagar</div>
                        </td>
                        <td class="text-center border-solid" style="width:25%;">
                            {{ $quotation->detail->monto_enganche }}
                        </td>
                        <td class="" style="width:25%;">
                        </td>
                        <td class="text-center border-solid" style="width:25%;">
                        </td>
                    @endif
                </tr>
                <tr>
                    <td class="" style="width:25%;">
                        <div class="label">M2</div>
                    </td>
                    <td class="text-center border-solid" style="width:25%;">
                        {{ $quotation->lote()->area }} M<sup>2</sup>
                    </td>
                    <td class="" style="width:25%;">
                        <div class="label">Saldo</div>
                    </td>
                    <td class="text-center border-solid" style="width:25%;">
                        {{ $quotation->detail->saldo }}
                    </td>
                    <td class="" style="width:25%;">
                    </td>
                    <td class="text-center border-solid" style="width:25%;">
                    </td>
                </tr>
            </tbody>
        </table>
    </div>

    <br><br>

    <table style="margin-top:16px;"></table>
    @if ($quotation->detail->plazo->tipo == 'financiamiento')
        <div class="section-firma">
            <table style="width:100%;border-collapse:collapse;font-size:12px;" class="corrida">
                <thead>
                    <tr>
                        <th class="text-center border-solid"># Pago</th>
                        <th class="text-center border-solid">Importe</th>
                        <th class="text-center border-solid">Saldo</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $corrida = $venta->corrida(date('Y-m-d'),true);
                    @endphp
                    @for ($i = 0; $i < $corrida->num_pagos; $i++)
                        @php
                            $corrida->saldo -= getAmmount((isset($quotation->detail->ultima_mensualidad) && $i==$corrida->num_pagos-1)?$quotation->detail->ultima_mensualidad:$quotation->detail->pago_mensual);
                        @endphp
                        <tr>
                            <td class="text-center border-solid">{{ ($i+1) }}</td>
                            <td class="text-center border-solid">{{  (isset($quotation->detail->ultima_mensualidad) && $i==$corrida->num_pagos-1)?$quotation->detail->ultima_mensualidad:$quotation->detail->pago_mensual }}</td>
                            <td class="text-center border-solid">${{ (($corrida->saldo < 0)) ? '0.00' : number_format($corrida->saldo, 2) }}</td>
                        </tr>
                    @endfor
                </tbody>
            </table>
            <table style="margin-top:5px;"></table>
            <table style="margin-top:5px;"></table>
            <center><p style="font-size:12px;">** El monto a pagar de la última mensualidad podria variar por el redondeo en relación a los pagos anteriores.</p></center>
        </div>
    @endif

    <br><br>
    <div class="section-restricciones">
        <ul class="clausulas">
            <li>Cotización sujeta a disponibilidad, con una vigencia no mayor de 30 dias.</li>
            <li>La presente no constituye ninguna obligación o compromiso de entrega ni ocupación.</li>
            <li>La certificaión del Contatro de Promesa de Compra Venta tendá un costo de $2,500.00 pesos pagadero</li>
            <li>Los datos personales proporcionados serán utilizados unica y exclusivamente  para estadisticas y procesos internos administrativos y venta del Desarrollo Inmobiliario {{ $development->name }}, de conformidad al aviso de privacidad que se encuentra en la pagina www.GIPSSA.com  mismo que fui enteredo y firmando de conformidad.</li>
            <li>Los montos pagados no serán reembolsables, en caso de desistir de la operación.</li>
        </ul>
    </div>
</body>
</html>