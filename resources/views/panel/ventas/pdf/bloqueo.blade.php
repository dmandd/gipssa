<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet">
    {{-- <link href="https://unpkg.com/tailwindcss@^1.0/dist/tailwind.min.css" rel="stylesheet"> --}}
    <title>Recibo</title>
    <style>
       @font-face {
            font-family: 'Avenir';
            src: url('../fonts/Avenir.eot?#iefix') format('embedded-opentype'),  url('../fonts/Avenir.woff') format('woff'), url('../fonts/Avenir.ttf')  format('truetype'), url('../fonts/Avenir.svg#Avenir') format('svg');
          }
        @page {
            margin:40px 80px 40px 80px !important;
            padding-top:20px!important;
            padding-bottom:30px!important;
            /* font-family:Arial, Helvetica, sans-serif; */
            font-family: 'Avenir', Helvetica, sans-serif;
        }
        table{table-layout: fixed}
        .border-solid{border:1px solid #000;}
        .text-center{text-align: center;}
        table.informacion .label{height:12px;line-height: 12px;text-align:right;padding-right:10px;font-weight: bold;}
        p span.subtitle{background-color:#cccccc;padding:2px;}
        p span.subtitle{font-size:12px;}
        ul.clausulas{font-size:12px;}
        ul.clausulas li{margin-bottom:10px;}
        footer {
            position: fixed; 
            bottom: 60px; 
            left: 0px; 
            right: 0px;
            height: 50px; 
            text-align: center;
            line-height: 35px;
        }
    </style>
</head>
<body class="font-sans">
    <div style="position:fixed;left:0;top:0;z-index:-1;">
        <img src="{{ asset('panel/img/bg-template.jpeg') }}" style="width:100%;height:1080px;" alt="" srcset="">
    </div>
    <table style="width:70%;float:right;font-size:13px;">
        <tbody>
            <tr>
                <td class="text-center" colspan="2">
                    <h3 style="margin-left:250px;color:grey;">RECIBO PAGO DE CONTRATO</h3>
                </td>
            </tr>
            <tr>
                <td class="">
                    <div style="font-weight:bold;width:100%;height:auto;text-align:right;">Fecha de Elaboración</div>
                </td>
                <td class="text-center border-solid">
                    {{ date('d/m/Y') }}
                </td>
            </tr>
            <tr>
                <td class="">
                    <div style="font-weight:bold;width:100%;height:auto;text-align:right;">Ejecutivo de Ventas</div>
                </td>
                <td class="text-center border-solid">
                    {{ $quotation->admin()->name }} {{ $quotation->admin()->last_name }}
                </td>
            </tr>
            <tr>
                <td class="">
                    <div style="font-weight:bold;width:100%;height:auto;text-align:right;">Desarrollo</div>
                </td>
                <td class="text-center border-solid">
                    {{ $development->name }}
                </td>
            </tr>
            @if ($development->stage)
                <tr>
                    <td class="">
                        <div style="font-weight:bold;width:100%;height:auto;text-align:right;">Etapa</div>
                    </td>
                    <td class="text-center border-solid">
                        {{ $development->stage }}
                    </td>
                </tr>
            @endif
            <tr>
                <td class="">
                    <div style="font-weight:bold;width:100%;height:auto;text-align:right;">Número de lote</div>
                </td>
                <td class="text-center border-solid">
                    {{ $venta->lote()->name }}
                </td>
            </tr>
        </tbody>
    </table>
    <table style="margin-top:25%;"></table>
    <div class="section-informacion">
        <p style="margin-left:15px;"><span class="subtitle">DATOS GENERALES DEL CLIENTE</span></p>
        <table style="width:100%;border-collapse:collapse;font-size:13px;" class="informacion">
            <tbody>
                <tr>
                    <td class="" style="width:25%;">
                        <div class="label">Nombre completo</div>
                    </td>
                    <td class="text-center border-solid" style="width:75%;" colspan="3">
                        {{ $client->name }} {{ $client->surname }}
                    </td>
                </tr>
                {{-- <tr>
                    <td class="" style="width:25%;">
                        <div class="label">Lugar de Nacimiento</div>
                    </td>
                    <td class="text-center border-solid" style="width:75%;" colspan="3">
                        {{ $client->lugar_nacimiento }}
                    </td>
                </tr> --}}
                {{-- <tr>
                    <td class="" style="width:30%;">
                        <div class="label">Fecha De Nacimiento</div>
                    </td>
                    <td class="text-center border-solid" style="width:30%;">
                        {{ $client->fecha_nacimiento }}
                    </td>
                    <td class="text-center border-solid" style="width:15%">
                        <div class="label">Ocupación</div>
                    </td>
                    <td class="text-center border-solid" style="width:30%">
                        {{ $client->ocupation }}
                    </td>
                </tr> --}}
                {{-- <tr>
                    <td class="" style="width:25%;">
                        <div class="label">Domicilio Actual</div>
                    </td>
                    <td class="text-center border-solid" style="width:75%;" colspan="3">
                        {{ $client->address }}
                    </td>
                </tr> --}}
                <tr>
                    <td class="" style="width:25%;">
                        <div class="label">Email</div>
                    </td>
                    <td class="text-center border-solid" style="width:30%;">
                        {{ $client->email }}
                    </td>
                    <td class="text-center border-solid" style="width:15%">
                        <div class="label">Teléfono</div>
                    </td>
                    <td class="text-center border-solid" style="width:30%">
                        {{ $client->phone }}
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <table style="margin-top:15px;"></table>
    <div class="section-informacion">
        <p style="margin-left:15px;"><span class="subtitle">DESCRIPCIÓN DEL PAGO</span></p>
        <table style="width:100%;border-collapse:collapse;font-size:13px;" class="informacion">
            <tbody>
                <tr>
                    <td class="" style="width:25%;">
                        <div class="label">Fecha</div>
                    </td>
                    <td class="text-center border-solid" style="width:25%;">
                        {{ $venta->fecha_bloqueo }}
                    </td>
                    <td class="" style="width:25%;">
                        <div class="label">Hora</div>
                    </td>
                    <td class="text-center border-solid" style="width:25%;">
                        {{-- //TODO: --}}
                        {{ date('H:i') }}
                    </td>
                    <td class="" style="width:25%;">
                        <div class="label">Método</div>
                    </td>
                    <td class="text-center border-solid" style="width:25%;">
                        Transferencia
                    </td>
                </tr>
                <tr>
                    <td class="" style="width:25%;">
                        <div class="label">Banco</div>
                    </td>
                    <td class="text-center border-solid" style="width:25%;">
                        {{ $venta->recibos('pago-bloqueo')->first()->banco()->name }}
                    </td>
                    <td class="" style="width:25%;">
                        <div class="label">N. de cuenta</div>
                    </td>
                    <td class="text-center border-solid" style="width:25%;">
                        {{ $venta->recibos('pago-bloqueo')->first()->banco()->n_cuenta }}
                    </td>
                    <td class="" style="width:25%;">
                        <div class="label">Ref. / Operación</div>
                    </td>
                    <td class="text-center border-solid" style="width:25%;">
                        {{ $venta->recibos('pago-bloqueo')->first()->message }}
                    </td>
                </tr>
                <tr>
                    <td class="" style="width:25%;">
                        <div class="label">Importe</div>
                    </td>
                    <td class="text-center border-solid" style="width:25%;">
                        $ {{ number_format($venta->monto_bloqueo, 2) }}
                    </td>
                    <td class="" style="width:25%;">
                        <div class="label">Importe en letra</div>
                    </td>
                    <td class="text-center border-solid" style="width:75%;" colspan="3">
                       (Son: {{ num2letras($venta->monto_bloqueo) }})
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <table style="margin-top:5px;"></table>
    <table style="margin-top:5px;"></table>
    <div class="section-restricciones">
        <p style="margin-left:15px;"><span class="">TOTAL A PAGAR: <b>$ {{ number_format($venta->monto_bloqueo, 2) }} (Son: {{ num2letras($venta->monto_bloqueo) }})</b></span></p>  
        <br>
        <ul class="clausulas">
            <li>La presente, no constituye ninguna obligación o compromiso de entrega ni ocupación.</li>
            <li>El monto pagado en concepto de REALIZACIÓN DE CONTRATO, no será reembolsable en caso de desistir de la operación.</li>
            <li>El monto calculado por mora, se calcula a partir del sexto día de su fecha de pago por letra por mes, no es relativo al saldo del predio y deberá pagarse al momento del abono a la letra del mes a pagar.</li>
            <li>El tiempo máximo establecido para la firma del contrato es de 20 días, en caso de rebasar este período, el lote se liberará y quedará disponible para ser elegido por otro comprador. El monto pagado se quedará en resguardo para cuando el cliente decida reanudar con el proceso de compra en cualquier otro lote.</li>
            <li>El tiempo establecido para el pago del enganche es el establecido en la cotización, condición de compra y corrida financiera, en caso de rebasar este período, el lote se liberará y quedará disponible para ser elegido por otro comprador. El monto pagado se quedará en resguardo para cuando el cliente decida reanudar con el proceso de compra en cualquier otro lote.</li>
            <li>Los datos personales proporcionados, serán utilizados única y exclusivamente para estadísticas y procesos internos administrativos y de ventas, de conformidad al aviso de privacidad que se encuentra en www.gipssa.com, mismo que fui enterado y firmando de conformidad.</li>
            <li>El cliente tiene conocimiento que la expedición de los comprobantes fiscales correspondientes, le serán enviados a su correo electrónico que previamente ha proporcionado, a mas tardar de 3 a 5 días hábiles siguientes a la fecha en que haya efectuado el pago.</li>
        </ul>
    </div>
    <footer>
        <table style="width:100%;">
            <tbody>
                <tr>
                    <td class="text-center" style="width: 50%;">
                        <div style="border-top:1px solid gray;width:80%;color:#000;margin:auto;">
                            <p style="width:100%;margin-bottom:0px!important;margin-top:-10px!important;">Nombre, Firma y Fecha</p>
                            <p style="width:100%;margin-top:-20px!important;margin-bottom:0px!important;">Ejecutivo de Ventas</p>
                        </div>
                    </td>
                    <td class="text-center" style="width: 50%;">
                        <div style="border-top:1px solid gray;width:80%;color:#000;margin:auto;">
                            <p style="width:100%;margin-bottom:0px!important;margin-top:-10px!important;">Nombre, Firma y Fecha</p>
                            <p style="width:100%;margin-top:-20px!important;margin-bottom:0px!important;">Cliente</p>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </footer>
</body>
</html>