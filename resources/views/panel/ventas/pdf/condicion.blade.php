<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet">
    {{-- <link href="https://unpkg.com/tailwindcss@^1.0/dist/tailwind.min.css" rel="stylesheet"> --}}
    <title>Condición compra</title>
    <style>
        @font-face {
            font-family: 'Avenir';
            src: url('../fonts/Avenir.eot?#iefix') format('embedded-opentype'),  url('../fonts/Avenir.woff') format('woff'), url('../fonts/Avenir.ttf')  format('truetype'), url('../fonts/Avenir.svg#Avenir') format('svg');
          }
        @page {
            margin:160px 0px 0px 0px !important;
            padding-top:20px!important;
            padding-bottom:30px!important;
            /* font-family:Arial, Helvetica, sans-serif; */
            font-family: 'Avenir', Helvetica, sans-serif;
        }
        body{margin-right: 60px;margin-left: 60px;}
        table{table-layout: fixed}
        .border-solid{border:1px solid #000;}
        .text-center{text-align: center;}
        table.informacion .label{height:12px;line-height: 12px;text-align:right;padding-right:10px;font-weight: bold;}
        p span.subtitle{background-color:#cccccc;padding:2px;}
        p span.subtitle{font-size:12px;}
        ul.clausulas{font-size:12px;}
        ul.clausulas li{margin-bottom:10px;}
        footer {
            position: fixed; 
            bottom: 60px; 
            left: 0px; 
            right: 0px;
            height: 50px; 
            text-align: center;
            line-height: 35px;
        }
    </style>
</head>
<body class="font-sans">
    <div style="position:fixed;left:0;top:-140px;z-index:-1;">
        <img src="{{ asset('panel/img/bg-template.jpeg') }}" style="width:100%;height:1100px;" alt="" srcset="">
    </div>
    <table style="width:70%;float:right;font-size:12px;margin-top:-120px;">
        <tbody>
            <tr>
                <td class="text-center" colspan="2">
                    <p style="margin-left:250px;color:grey;"><b>CONDICIÓN DE COMPRA</b></p>
                </td>
            </tr>
            <tr>
                <td class="">
                    <div style="font-weight:bold;width:100%;height:auto;text-align:right;">Fecha de Elaboración</div>
                </td>
                <td class="text-center border-solid">
                    {{ date('d/m/Y') }}
                </td>
            </tr>
            <tr>
                <td class="">
                    <div style="font-weight:bold;width:100%;height:auto;text-align:right;">Ejecutivo de Ventas</div>
                </td>
                <td class="text-center border-solid">
                    {{ $venta->asesor()->name }} {{ $venta->asesor()->last_name }}
                </td>
            </tr>
            <tr>
                <td class="">
                    <div style="font-weight:bold;width:100%;height:auto;text-align:right;">Desarrollo</div>
                </td>
                <td class="text-center border-solid">
                    {{ $development->name }}
                </td>
            </tr>
            @if ($development->stage)
                <tr>
                    <td class="">
                        <div style="font-weight:bold;width:100%;height:auto;text-align:right;">Etapa</div>
                    </td>
                    <td class="text-center border-solid">
                        {{ $development->stage }}
                    </td>
                </tr>
            @endif
        </tbody>
    </table>
    <table style="margin-top:19%;"></table>
    <div class="section-informacion" style="margin-top:-120px;">
        <p ><span class="subtitle">DATOS GENERALES DEL CLIENTE</span></p>
        <table style="width:100%;border-collapse:collapse;font-size:12px;" class="informacion">
            <tbody>
                <tr>
                    <td class="" style="width:25%;">
                        <div class="label">Nombre completo</div>
                    </td>
                    <td class="border-solid" style="width:75%;padding-left:5px;" colspan="3">
                        {{ $client->name }} {{ $client->surname }}
                    </td>
                </tr>
                <tr>
                    <td class="" style="width:25%;">
                        <div class="label">Identificación</div>
                    </td>
                    <td class=" border-solid" style="width:30%;padding-left:5px;">
                        @switch($client->identification_type)
                            @case(1)
                                INE
                                @break
                            @case(2)
                                IFE
                                @break
                            @case(3)
                                VISA
                                @break
                            @case(4)
                                FM2
                                @break
                            @case(5)
                                Cédula Profesional
                                @break
                            @case(6)
                                Pasaporte
                                @break
                            @default
                                ---
                        @endswitch {{ $client->identificacion }}
                    </td>
                    <td class="text-center border-solid" style="width:15%">
                        <div class="label">CURP</div>
                    </td>
                    <td class=" border-solid" style="width:30%;padding-left:5px;">
                        {{ $client->curp }}
                    </td>
                </tr>
                <tr>
                    <td class="" style="width:25%;">
                        <div class="label">Lugar de Nacimiento</div>
                    </td>
                    <td class="border-solid" style="width:30%;padding-left:5px;">
                        {{ $client->lugar_nacimiento }}
                    </td>
                    <td class="text-center border-solid" style="width:15%">
                        <div class="label">RFC</div>
                    </td>
                    <td class=" border-solid" style="width:30%;padding-left:5px;">
                        {{ $client->rfc }}
                    </td>
                </tr>
                <tr>
                    <td class="" style="width:30%;">
                        <div class="label">Fecha De Nacimiento</div>
                    </td>
                    <td class=" border-solid" style="width:30%;padding-left:5px;">
                        {{ date('d/m/Y', strtotime($client->fecha_nacimiento)) }}
                    </td>
                    <td class="text-center border-solid" style="width:15%">
                        <div class="label">Ocupación</div>
                    </td>
                    <td class=" border-solid" style="width:30%;padding-left:5px;">
                        {{ $client->ocupation }}
                    </td>
                </tr>
                <tr>
                    <td class="" style="width:25%;">
                        <div class="label">Domicilio Actual</div>
                    </td>
                    <td class=" border-solid" style="width:75%;padding-left:5px;" colspan="3">
                        {{ $client->address }}
                    </td>
                </tr>
                <tr>
                    <td class="" style="width:25%;">
                        <div class="label">Municipio, Edo, C.P.</div>
                    </td>
                    <td class=" border-solid" style="width:75%;padding-left:5px;" colspan="3">
                        {{ ($client->municipio) ? $client->municipio : '' }} {{ ($client->estado) ? ', '.$client->estado : '' }} {{ ($client->cp) ? ', '.$client->cp : '' }}
                    </td>
                </tr>
                <tr>
                    <td class="" style="width:30%;">
                        <div class="label">Email</div>
                    </td>
                    <td class=" border-solid" style="width:30%;padding-left:5px;">
                        {{ $client->email }}
                    </td>
                    <td class="text-center border-solid" style="width:15%">
                        <div class="label">Teléfono</div>
                    </td>
                    <td class="r border-solid" style="width:30%;padding-left:5px;">
                        {{ $client->phone }}
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="section-informacion">
        <p ><span class="subtitle">DESCRIPCIÓN DE LA OPERACIÓN</span></p>
        <table style="width:100%;border-collapse:collapse;font-size:12px;" class="informacion">
            <tbody>
                <tr>
                    <td class="" style="width:25%;">
                        <div class="label">Ubicación</div>
                    </td>
                    <td class="text-center border-solid" style="width:25%;">
                        {{ $development->location }}
                    </td>
                    <td class="" style="width:25%;">
                        <div class="label">Precio</div>
                    </td>
                    <td class="text-center border-solid" style="width:25%;">
                        $ {{ number_format($venta->lote()->price,2) }}
                    </td>
                    <td class="" style="width:25%;">
                        <div class="label">Tipo de compra</div>
                    </td>
                    <td class="text-center border-solid" style="width:25%;">
                        {{ $quotation->detail->plazo->nombre }}
                    </td>
                </tr>
                <tr>
                    <td class="" style="width:25%;">
                       <div class="label">Desarrollo</div>
                   </td>
                   <td class="text-center border-solid" style="width:25%;padding-left:5px;background:#BFBFBF;">
                       {{ $development->name }}
                   </td>
                   <td class="" style="width:25%;">
                       <div class="label">Descuento</div>
                   </td>
                   <td class="text-center border-solid" style="width:25%;background:#BFBFBF;">
                        {{ $quotation->detail->plazo->tipo_descuento === "cantidad"?'$ '.number_format($quotation->detail->plazo->porcentaje_descuento,2):"" }}{{ $quotation->detail->plazo->tipo_descuento === "porcentaje"?$quotation->detail->plazo->porcentaje_descuento.' %':"" }}
                   </td>
                   @if($quotation->detail->plazo->tipo == 'financiamiento' || $quotation->detail->plazo->tipo == 'contado_diferido')
                        <td class="" style="width:25%;">
                            <div class="label">{{$quotation->detail->plazo->tipo == 'financiamiento'? "Monto a financiar":"Pago a diferir"}}</div>
                        </td>
                        <td class="text-center border-solid" style="width:25%;">
                            {{ $quotation->detail->plazo->tipo == 'financiamiento' ? "$".number_format($quotation->detail->financiamiento,2) : $quotation->detail->monto_enganche }}
                        </td>
                    @else
                        <td class="" style="width:25%;">
                        </td>
                        <td class="text-center border-solid" style="width:25%;background:#fff;">
                        </td>
                    @endif
               </tr>
               <tr>
                    <td class="" style="width:25%;">
                        <div class="label">Etapa</div>
                    </td>
                    <td class="text-center border-solid" style="width:25%;">
                        {{ $development->stage }}
                    </td>
                    <td class="" style="width:25%;">
                        <div class="label">Precio Total</div>
                    </td>
                    <td class="text-center border-solid" style="width:25%;">
                        {{ $quotation->detail->precio_final }}
                    </td>
                    @if($quotation->detail->plazo->tipo == 'financiamiento' || $quotation->detail->plazo->tipo == 'contado_diferido')
                        <td class="" style="width:25%;">
                            <div class="label">{{ $quotation->detail->plazo->tipo == 'financiamiento' ? "No. meses":"No. Pagos"}}</div>
                        </td>
                        <td class="text-center border-solid" style="width:25%;background:#BFBFBF;">
                            {{ $quotation->detail->plazo->tipo == 'financiamiento' ? $quotation->detail->mensualidad : $quotation->detail->enganche_num_pagos }}
                        </td>
                    @else
                        <td class="" style="width:25%;">
                        </td>
                        <td class="text-center border-solid" style="width:25%;background:#fff;">
                        </td>
                    @endif
                </tr>
                <tr>
                    <td class="" style="width:25%;">
                        <div class="label">Número de lote</div>
                    </td>
                    <td class="text-center border-solid" style="width:25%;background:#BFBFBF;">
                       L - {{ $quotation->lote()->num_lote }}
                    </td>
                    @if($quotation->detail->plazo->tipo == 'financiamiento')
                        <td class="" style="width:25%;">
                            <div class="label">% del Enganche</div>
                        </td>
                        <td class="text-center border-solid" style="width:25%;background:#BFBFBF;">
                            {{ $quotation->detail->porc_enganche }}
                        </td>
                        <td class="" style="width:25%;">
                            <div class="label">Mensualidad</div>
                        </td>
                        <td class="text-center border-solid" style="width:25%;">
                            {{ $venta->quotation()->detail->pago_mensual }}
                        </td>
                    @else
                        <td class="" style="width:25%;">
                            <div class="label">% del Pago</div>
                        </td>
                        <td class="text-center border-solid" style="width:25%;background:#BFBFBF;">
                            {{ $quotation->detail->porc_enganche }}
                        </td>
                        <td class="" style="width:25%;">
                            <div class="label">{{ $quotation->detail->plazo->tipo == 'contado_diferido' ? "Pagos de":""}}</div>
                        </td>
                        @if($quotation->detail->plazo->tipo == 'contado_diferido')
                            <td class="text-center border-solid" style="width:25%;">
                                {{$quotation->detail->pago_mensual_enganche}}
                            </td>
                        @else
                            <td class="text-center border-solid" style="width:25%;background:#fff;">
                            </td>
                        @endif
                    @endif
                </tr>
                <tr>
                    <td class="" style="width:25%;">
                        <div class="label">Medidas</div>
                    </td>
                    <td class="text-center border-solid" style="width:25%;">
                        {{ $venta->lote()->medida }}
                    </td>
                    @if($quotation->detail->plazo->tipo == 'financiamiento')
                        <td class="" style="width:25%;">
                            <div class="label">Monto Enganche</div>
                        </td>
                        <td class="text-center border-solid" style="width:25%;">
                            {{ $quotation->detail->monto_enganche }}
                        </td>
                        <td class="" style="width:25%;">
                            <div class="label">Última mensualidad</div>
                        </td>
                        <td class="text-center border-solid" style="width:25%;">
                            {{ isset($quotation->detail->ultima_mensualidad)?$quotation->detail->ultima_mensualidad:"" }}
                        </td>
                    @else
                        <td class="" style="width:25%;">
                            <div class="label">Monto a Pagar</div>
                        </td>
                        <td class="text-center border-solid" style="width:25%;">
                            {{ $quotation->detail->monto_enganche }}
                        </td>
                        <td class="" style="width:25%;">
                        </td>
                        <td class="text-center border-solid" style="width:25%;background:#fff;">
                        </td>
                    @endif
                </tr>
                <tr>
                    <td class="" style="width:25%;">
                        <div class="label">M2</div>
                    </td>
                    <td class="text-center border-solid" style="width:25%;">
                        {{ $quotation->lote()->area }} M<sup>2</sup>
                    </td>
                    <td class="" style="width:25%;">
                        <div class="label">Saldo</div>
                    </td>
                    <td class="text-center border-solid" style="width:25%;">
                        {{ $quotation->detail->saldo }}
                    </td>
                    <td class="" style="width:25%;">
                    </td>
                    <td class="text-center border-solid" style="width:25%;background:#fff;">
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="section-informacion">
        <p ><span class="subtitle">DESCRIPCIÓN DE PAGOS</span></p>
        <table style="width:100%;border-collapse:collapse;font-size:12px;" class="informacion">
            <thead>
                <tr>
                    <th class="border-solid"></th>
                    <th class="border-solid">Monto</th>
                    <th class="border-solid">Fecha Programada</th>
                    <th class="border-solid">Método</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="border-solid text-center">Contrato</td>
                    <td class="border-solid text-center">${{ number_format($venta->monto_bloqueo, 2) }}</td>
                    <td class="border-solid text-center">{{ date('d/m/Y', strtotime($venta->fecha_bloqueo)) }}</td>
                    <td class="border-solid text-center">Transferencia</td>
                </tr>
                @if ($venta->fechas_enganche()->get()->count() > 0)
                    @foreach ($venta->fechas_enganche()->get() as $i => $fecha)
                        @if($quotation->detail->plazo->tipo == 'financiamiento' || $quotation->detail->plazo->tipo == 'contado_diferido')
                            <tr>
                                <td class="border-solid text-center">{{$quotation->detail->plazo->tipo == 'financiamiento' ? "Enganche":"Pago"}} {{ ($i + 1) }}</td>
                                <td class="border-solid text-center">${{ number_format(getAmmount($fecha->monto),2) }}</td>

                                
                                

                                

                                <td class="border-solid text-center">{{ date('d/m/Y', strtotime($fecha->fecha)) }}</td>
                                <td class="border-solid text-center">Transferencia</td>
                            </tr>
                        @else
                            <tr>
                                <td class="border-solid text-center">Pago </td>
                                <td class="border-solid text-center">{{ $fecha->monto }}</td>
                                <td class="border-solid text-center">{{ date('d/m/Y', strtotime($fecha->fecha)) }}</td>
                                <td class="border-solid text-center">Transferencia</td>
                            </tr>
                        @endif
                    @endforeach
                @else
                    @if ($venta->quotation()->detail->enganche_num_pagos > 0)
                        @for ($i = 0; $i < $venta->quotation()->detail->enganche_num_pagos; $i++)
                            <tr>
                                <td class="border-solid text-center">Enganche {{ ($i + 1) }}</td>
                                <td class="border-solid text-center">Por Definir</td>
                                <td class="border-solid text-center">Por Definir</td>
                                <td class="border-solid text-center">Por Definir</td>
                            </tr>
                        @endfor
                    @else
                        <tr>
                            <td class="border-solid text-center">Enganche</td>
                            <td class="border-solid text-center">Por Definir</td>
                            <td class="border-solid text-center">Por Definir</td>
                            <td class="border-solid text-center">Por Definir</td>
                        </tr>
                    @endif
                @endif
                @if ($venta->quotation()->detail->plazo->tipo == 'financiamiento')
                    <tr>
                        <td class="border-solid text-center">Mensualidades</td>
                        <td class="border-solid text-center">{{ $venta->quotation()->detail->pago_mensual }}</td>
                        <td class="border-solid text-center">{{ date('d/m/Y', strtotime($venta->dia_pago)) }}</td>
                        <td class="border-solid text-center">Transferencia</td>
                    </tr>
                @endif
                @if(!is_null($venta->fecha_pago_saldo))
                    <tr>
                        <td class="border-solid text-center">Saldo</td>
                        <td class="border-solid text-center">{{ $venta->quotation()->detail->saldo }}</td>
                        <td class="border-solid text-center">{{ date('d/m/Y', strtotime($venta->fecha_pago_saldo))}}</td>
                        <td class="border-solid text-center">Transferencia</td>
                    </tr>
                @endif
            </tbody>
        </table>
    </div>
    <div class="section-restricciones">
        <p ><span class="subtitle">FIRMA DEL CONTRATO</span></p>  
        <table style="width:100%;border-collapse:collapse;font-size:12px;" class="informacion">
            <thead>
                <tr>
                    <th class="border-solid">Fecha de firma</th>
                    <th class="border-solid">Ubicación</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="border-solid text-center">{{ date('d/m/Y', strtotime($venta->fecha_firma_contrato)) }}</td>
                    <td class="border-solid text-center">{{ $venta->ubicacion_firma_contrato }}</td>
                </tr>
            </tbody>
        </table>
        
    </div>
    <div>
        <div class="section-informacion">
            <p ><span class="subtitle">OBSERVACIONES:</span></p>
            <p>{{$venta->observaciones}}</p>
        </div>
        <ul class="clausulas" style="page-break-before: always;">
            <li>La presente condición de compra, forma parte del expediente administrativo donde el cliente puede revisar y dar aviso de alguna corrección y/o actualización de datos, otorgando en este acto su consentimiento, y autoriza esta operación con la firma presencial de esta condición de compra, y/o mediante la aceptación por algún medio digital convencional de comunicación, y/o en su caso con la firma digital en la plataforma digital CENTINELA.</li>
            <li>El cliente se compromete a entregar toda la documentación solicitada para la elaboración de su expediente personal, así como para la emisión de la factura correspondiente, para el caso de requerirla, en caso de no entregar su constancia de situación Fiscal al cierre del mes corriente, se facturará al Registro Federal de Contribuyentes de manera genérica, con el concepto de gastos en general.</li>
            <li>La presente, no constituye ninguna obligación o compromiso de entrega ni ocupación. Los montos pagados no serán reembolsables, en caso de desistir de la operación.</li>
            <li>El cliente en caso de ser de nacionalidad extranjera, se compromete desde la aceptación de la presente condición de compra a realizar por su cuenta los trámites legales correspondientes de conformidad con las leyes mexicanas para poder realizar la escrituración correspondiente.</li>
            <li>Para realizar la escrituración del predio deberá cubrir los honorarios y derechos que la notaría le fije para la realización de la escritura a su favor, una vez que se haya finiquitado la obligación de pago y se cumpla con el tiempo estipulado de entrega en el contrato de promesa de compra venta correspondiente.</li>
            <li>Los datos personales proporcionados, serán utilizados única y exclusivamente para estadísticas y procesos internos administrativos y de ventas, de conformidad al aviso de privacidad que se encuentra en <a href="www.gipssa.com" target="_blank">www.gipssa.com</a> mismo que fui enterado y firmando de conformidad.</li>
        </ul>
    </div>
    <table style="margin-top:50px;"></table>
    @if (($venta->development()->clientDocuments()) && ($venta->development()->clientDocuments()->count() > 0))
        @foreach ($venta->development()->clientDocuments() as $document)
            @if ((isset($venta->documentation($document->id)->file)) && ($document->mostrar_condicion))
                <img style="width:40%;margin-top:3%;margin-right:3%;" src="{{ asset($venta->documentation($document->id)->file) }}" alt="{{ $document->name }}">
            @endif
        @endforeach
    @endif
    <table style="margin-top:10px;"></table>
    <table style="width:100%;">
        <tbody>
            <tr>
                <td class="text-center" style="width: 50%;">
                    <div style="border-top:1px solid gray;width:80%;margin:auto;">
                        <p style="width:100%;margin:2.5!important;">Nombre, Firma y Fecha</p>
                        <p style="width:100%;margin:2.5!important;">Ejecutivo de Ventas</p>
                    </div>
                </td>
                <td class="text-center" style="width: 50%;">
                    <div style="border-top:1px solid gray;width:80%;margin:auto;">
                        <p style="width:100%;margin:2.5!important;">Nombre, Firma y Fecha</p>
                        <p style="width:100%;margin:2.5!important;">Cliente</p>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
   {{-- <footer>
        <table style="width:100%;">
            <tbody>
                <tr>
                    <td class="text-center" style="width: 50%;">
                        <div style="border-top:1px solid gray;width:80%;margin:auto;">
                            <p style="width:100%;margin-bottom:0px!important;margin-top:-10px!important;">Nombre, Firma y Fecha</p>
                            <p style="width:100%;margin-top:-20px!important;margin-bottom:0px!important;">Ejecutivo de Ventas</p>
                        </div>
                    </td>
                    <td class="text-center" style="width: 50%;">
                        <div style="border-top:1px solid gray;width:80%;margin:auto;">
                            <p style="width:100%;margin-bottom:0px!important;margin-top:-10px!important;">Nombre, Firma y Fecha</p>
                            <p style="width:100%;margin-top:-20px!important;margin-bottom:0px!important;">Cliente</p>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </footer>--}}
</body>
</html>