<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet">
    {{-- <link href="https://unpkg.com/tailwindcss@^1.0/dist/tailwind.min.css" rel="stylesheet"> --}}
    <title>Corrida Financiera</title>
    <style>
        @font-face {
            font-family: 'Avenir';
            src: url('../fonts/Avenir.eot?#iefix') format('embedded-opentype'),  url('../fonts/Avenir.woff') format('woff'), url('../fonts/Avenir.ttf')  format('truetype'), url('../fonts/Avenir.svg#Avenir') format('svg');
          }
        @page {
            margin:80px 80px 40px 80px !important;
            padding-top:20px!important;
            padding-bottom:30px!important;
            /* font-family:Arial, Helvetica, sans-serif; */
            font-family: 'Avenir', Helvetica, sans-serif;
        }
        table{table-layout: fixed}
        .border-solid{border:1px solid #000;}
        .text-center{text-align: center;}
        table.informacion .label{height:12px;line-height: 12px;text-align:right;padding-right:10px;font-weight: bold;}
        p span.subtitle{background-color:#cccccc;padding:2px;}
        p span.subtitle{font-size:12px;}
        ul.clausulas{font-size:12px;}
        ul.clausulas li{margin-bottom:10px;}
        footer {
            position: fixed; 
            bottom: 60px; 
            left: 0px; 
            right: 0px;
            height: 50px; 
            text-align: center;
            line-height: 35px;
        }
    </style>
</head>
<body class="font-sans">
    <div style="position:fixed;left:0;top:0;z-index:-1;">
        
    </div>
    <table style="width:70%;float:right;font-size:12px;margin-top:-50px;">
        <tbody>
            <tr>
                <td class="text-center" colspan="2">
                    <p style="margin-left:250px;color:grey;"><b>ANEXO A</b></p>
                    <p style="margin-left:250px;color:grey;"><b>CORRIDA FINANCIERA</b></h3>
                </td>
            </tr>
            <tr>
                <td class="">
                    <div style="font-weight:bold;width:100%;height:auto;text-align:right;">Fecha de Elaboración</div>
                </td>
                <td class="text-center border-solid">
                    {{ date('d/m/Y') }}
                </td>
            </tr>
            <tr>
                <td class="">
                    <div style="font-weight:bold;width:100%;height:auto;text-align:right;">Ejecutivo de Ventas</div>
                </td>
                <td class="text-center border-solid">
                    {{ $venta->asesor()->name }} {{ $venta->asesor()->last_name }}
                </td>
            </tr>
            <tr>
                <td class="">
                    <div style="font-weight:bold;width:100%;height:auto;text-align:right;">Desarrollo</div>
                </td>
                <td class="text-center border-solid">
                    {{ $development->name }}
                </td>
            </tr>
            @if ($development->stage)
                <tr>
                    <td class="">
                        <div style="font-weight:bold;width:100%;height:auto;text-align:right;">Etapa</div>
                    </td>
                    <td class="text-center border-solid">
                        {{ $development->stage }}
                    </td>
                </tr>
            @endif
        </tbody>
    </table>
    <table style="margin-top:16%;"></table>
    <div class="section-informacion">
        <p><span class="subtitle">DATOS GENERALES DEL CLIENTE</span></p>
        <table style="width:100%;border-collapse:collapse;font-size:12px;" class="informacion">
            <tbody>
                <tr>
                    <td class="" style="width:25%;">
                        <div class="label">Nombre completo</div>
                    </td>
                    <td class="text-center border-solid" style="width:75%;" colspan="3">
                        {{ $client->name }} {{ $client->surname }}
                    </td>
                </tr>
                <tr>
                    <td class="" style="width:25%;">
                        <div class="label">Email</div>
                    </td>
                    <td class="text-center border-solid" style="width:35%;">
                        {{ $client->email }}
                    </td>
                    <td class="" style="width:15%;">
                        <div class="label">Teléfono</div>
                    </td>
                    <td class="text-center border-solid" style="width:35%;">
                        {{ $client->phone }}
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="section-informacion">
        <p ><span class="subtitle">DESCRIPCIÓN DE LA OPERACIÓN</span></p>
        <table style="width:100%;border-collapse:collapse;font-size:12px;" class="informacion">
            <tbody>
                <tr>
                    <td class="" style="width:25%;">
                        <div class="label">Ubicación</div>
                    </td>
                    <td class="text-center border-solid" style="width:25%;">
                        {{ $development->location }}
                    </td>
                    <td class="" style="width:25%;">
                        <div class="label">Precio</div>
                    </td>
                    <td class="text-center border-solid" style="width:25%;">
                        $ {{ number_format($venta->lote()->price,2) }}
                    </td>
                    <td class="" style="width:25%;">
                        <div class="label">Tipo de compra</div>
                    </td>
                    <td class="text-center border-solid" style="width:25%;">
                        {{ $quotation->detail->plazo->nombre }}
                    </td>
                </tr>
                <tr>
                    <td class="" style="width:25%;">
                       <div class="label">Desarrollo</div>
                   </td>
                   <td class="text-center border-solid" style="width:25%;padding-left:5px;background:#BFBFBF;">
                       {{ $development->name }}
                   </td>
                   <td class="" style="width:25%;">
                       <div class="label">Descuento</div>
                   </td>
                   <td class="text-center border-solid" style="width:25%;background:#BFBFBF;">
                        {{ $quotation->detail->plazo->tipo_descuento === "cantidad"?'$ '.number_format($quotation->detail->plazo->porcentaje_descuento,2):"" }}{{ $quotation->detail->plazo->tipo_descuento === "porcentaje"?$quotation->detail->plazo->porcentaje_descuento.' %':"" }}
                   </td>
                   @if($quotation->detail->plazo->tipo == 'financiamiento' || $quotation->detail->plazo->tipo == 'contado_diferido')
                        <td class="" style="width:25%;">
                            <div class="label">{{$quotation->detail->plazo->tipo == 'financiamiento'? "Monto a financiar":"Pago a diferir"}}</div>
                        </td>
                        <td class="text-center border-solid" style="width:25%;">
                            {{ $quotation->detail->plazo->tipo == 'financiamiento' ? "$".number_format($quotation->detail->financiamiento,2) : $quotation->detail->monto_enganche }}
                        </td>
                    @else
                        <td class="" style="width:25%;">
                        </td>
                        <td class="text-center border-solid" style="width:25%;background:#fff;">
                        </td>
                    @endif
               </tr>
               <tr>
                    <td class="" style="width:25%;">
                        <div class="label">Etapa</div>
                    </td>
                    <td class="text-center border-solid" style="width:25%;">
                        {{ $development->stage }}
                    </td>
                    <td class="" style="width:25%;">
                        <div class="label">Precio Total</div>
                    </td>
                    <td class="text-center border-solid" style="width:25%;">
                        {{ $quotation->detail->precio_final }}
                    </td>
                    @if($quotation->detail->plazo->tipo == 'financiamiento' || $quotation->detail->plazo->tipo == 'contado_diferido')
                        <td class="" style="width:25%;">
                            <div class="label">{{ $quotation->detail->plazo->tipo == 'financiamiento' ? "No. meses":"No. Pagos"}}</div>
                        </td>
                        <td class="text-center border-solid" style="width:25%;background:#BFBFBF;">
                            {{ $quotation->detail->plazo->tipo == 'financiamiento' ? $quotation->detail->mensualidad : $quotation->detail->enganche_num_pagos }}
                        </td>
                    @else
                        <td class="" style="width:25%;">
                        </td>
                        <td class="text-center border-solid" style="width:25%;background:#fff;">
                        </td>
                    @endif
                </tr>
                <tr>
                    <td class="" style="width:25%;">
                        <div class="label">Número de lote</div>
                    </td>
                    <td class="text-center border-solid" style="width:25%;background:#BFBFBF;">
                       L - {{ $quotation->lote()->num_lote }}
                    </td>
                    @if($quotation->detail->plazo->tipo == 'financiamiento')
                        <td class="" style="width:25%;">
                            <div class="label">% del Enganche</div>
                        </td>
                        <td class="text-center border-solid" style="width:25%;background:#BFBFBF;">
                            {{ $quotation->detail->porc_enganche }}
                        </td>
                        <td class="" style="width:25%;">
                            <div class="label">Mensualidad</div>
                        </td>
                        <td class="text-center border-solid" style="width:25%;">
                            {{ $venta->quotation()->detail->pago_mensual }}
                        </td>
                    @else
                        <td class="" style="width:25%;">
                            <div class="label">% del Pago</div>
                        </td>
                        <td class="text-center border-solid" style="width:25%;background:#BFBFBF;">
                            {{ $quotation->detail->porc_enganche }}
                        </td>
                        <td class="" style="width:25%;">
                            <div class="label">{{ $quotation->detail->plazo->tipo == 'contado_diferido' ? "Pagos de":""}}</div>
                        </td>
                        @if($quotation->detail->plazo->tipo == 'contado_diferido')
                            <td class="text-center border-solid" style="width:25%;">
                                {{$quotation->detail->pago_mensual_enganche}}
                            </td>
                        @else
                            <td class="text-center border-solid" style="width:25%;background:#fff;">
                            </td>
                        @endif
                    @endif
                </tr>
                <tr>
                    <td class="" style="width:25%;">
                        <div class="label">Medidas</div>
                    </td>
                    <td class="text-center border-solid" style="width:25%;">
                        {{ $venta->lote()->medida }}
                    </td>
                    @if($quotation->detail->plazo->tipo == 'financiamiento')
                        <td class="" style="width:25%;">
                            <div class="label">Monto Enganche</div>
                        </td>
                        <td class="text-center border-solid" style="width:25%;">
                            {{ $quotation->detail->monto_enganche }}
                        </td>
                        <td class="" style="width:25%;">
                            <div class="label">Última mensualidad</div>
                        </td>
                        <td class="text-center border-solid" style="width:25%;">
                            {{ isset($quotation->detail->ultima_mensualidad)?$quotation->detail->ultima_mensualidad:"" }}
                        </td>
                    @else
                        <td class="" style="width:25%;">
                            <div class="label">Monto a Pagar</div>
                        </td>
                        <td class="text-center border-solid" style="width:25%;">
                            {{ $quotation->detail->monto_enganche }}
                        </td>
                        <td class="" style="width:25%;">
                        </td>
                        <td class="text-center border-solid" style="width:25%;background:#fff;">
                        </td>
                    @endif
                </tr>
                <tr>
                    <td class="" style="width:25%;">
                        <div class="label">M2</div>
                    </td>
                    <td class="text-center border-solid" style="width:25%;">
                        {{ $quotation->lote()->area }} M<sup>2</sup>
                    </td>
                    <td class="" style="width:25%;">
                        <div class="label">Saldo</div>
                    </td>
                    <td class="text-center border-solid" style="width:25%;">
                        {{ $quotation->detail->saldo }}
                    </td>
                    <td class="" style="width:25%;">
                    </td>
                    <td class="text-center border-solid" style="width:25%;background:#fff;">
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <table style="margin-top:16px;"></table>
    @if ($quotation->detail->plazo->tipo == 'financiamiento')
        <div class="section-firma">
            <table style="width:100%;border-collapse:collapse;font-size:12px;" class="corrida">
                <thead>
                    <tr>
                        <th class="text-center border-solid"># Pago</th>
                        <th class="text-center border-solid">Fecha Pago</th>
                        <th class="text-center border-solid">Importe</th>
                        <th class="text-center border-solid">Saldo</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $corrida = $venta->corrida();
                    @endphp
                    @for ($i = 0; $i < $corrida->num_pagos; $i++)
                        @php
                            $corrida->saldo -= getAmmount((isset($quotation->detail->ultima_mensualidad) && $i==$corrida->num_pagos-1)?$quotation->detail->ultima_mensualidad:$quotation->detail->pago_mensual);
                        @endphp
                        <tr>
                            <td class="text-center border-solid">{{ ($i+1) }}</td>
                            <td class="text-center border-solid">{{ date('d/m/Y', strtotime($corrida->fecha))}}</td>
                            <td class="text-center border-solid">{{  (isset($quotation->detail->ultima_mensualidad) && $i==$corrida->num_pagos-1)?$quotation->detail->ultima_mensualidad:$quotation->detail->pago_mensual }}</td>
                            <td class="text-center border-solid">${{ (($corrida->saldo < 0)) ? '0.00' : number_format($corrida->saldo, 2) }}</td>
                        </tr>
                        @php
                            $dia = $corrida->dia; 

                            if(date("m",strtotime($corrida->fecha)) != "01" && intval($dia) == 31 ){
                                if(checkdate(intval(date("m",strtotime($corrida->fecha))), $dia, intval(date("Y",strtotime($corrida->fecha)))))
                                    $dia = 30;
                            }elseif(date("m",strtotime($corrida->fecha)) == "01" && intval($dia) >= 29 ){
                                $dia = 29;
                                if(!checkdate(2, $dia, intval(date("Y",strtotime($corrida->fecha)))))
                                    $dia = 28;
                            }

                            $corrida->fecha = date("Y-m-".$dia, strtotime(date("Y-m",strtotime($corrida->fecha))."+1 month"));
                        @endphp
                    @endfor
                </tbody>
            </table>
            <table style="margin-top:5px;"></table>
            <table style="margin-top:5px;"></table>
            <center><p style="font-size:12px;">** El monto a pagar de la última mensualidad podria variar por el redondeo en relación a los pagos anteriores.</p></center>
        </div>
    @endif
    <table style="margin-top:120px;"></table>
    <table style="width:100%;">
        <tbody>
            <tr>
                <td class="text-center" style="width: 50%;">
                    <div style="border-top:1px solid gray;width:80%;margin:auto;">
                        <p style="width:100%;margin-bottom:0px!important;margin-top:10px!important;">PROMINENTE COMPRADOR</p>
                        {{-- <p style="width:100%;margin-top:-20px!important;margin-bottom:0px!important;">Ejecutivo de Ventas</p> --}}
                    </div>
                </td>
                <td class="text-center" style="width: 50%;">
                    <div style="border-top:1px solid gray;width:80%;margin:auto;">
                        <p style="width:100%;margin-bottom:0px!important;margin-top:10px!important;">PROMINENTE VENDEDOR</p>
                        {{-- <p style="width:100%;margin-top:-20px!important;margin-bottom:0px!important;">Cliente</p> --}}
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
   
</body>
</html>