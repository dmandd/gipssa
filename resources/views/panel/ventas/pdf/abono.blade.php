<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet">
    {{-- <link href="https://unpkg.com/tailwindcss@^1.0/dist/tailwind.min.css" rel="stylesheet"> --}}
    <title>Recibo</title>
    <style>
        @page {
            margin:20px!important;
            padding:0px!important;
            /* font-family:Arial, Helvetica, sans-serif; */
            font-family: 'Roboto', sans-serif;
        }
        table{table-layout: fixed}
        .border-solid{border:1px solid #000;}
        .text-center{text-align: center;}
        table.informacion .label{height:25px;line-height: 20px;text-align:right;padding-right:10px;font-weight: bold;}
        p span.subtitle{background-color:#cccccc;padding:2px;}
        ul.clausulas{font-size:11px;}
        ul.clausulas li{margin-bottom:10px;}
        footer {
            position: fixed; 
            bottom: 60px; 
            left: 0px; 
            right: 0px;
            height: 50px; 
            text-align: center;
            line-height: 35px;
        }
    </style>
</head>
<body class="font-sans">
    <div style="position:fixed;left:0;top:0;z-index:-1;">
        <img src="{{ asset('panel/img/bg-template.jpeg') }}" style="width:100%;height:1080px;" alt="" srcset="">
    </div>
    <table style="width:70%;float:right;font-size:13px;">
        <tbody>
            <tr>
                @php
                    $count = 0;
                    $contar = true;
                    foreach($venta->recibos($recibo->slug) as $rec){
                        if($contar)
                            $count++;

                        if($rec->id == $recibo->id){
                            $contar = false;
                        }
                    }
                @endphp
                <td class="text-center" colspan="2">
                    <h3 style="margin-left:250px;color:grey;text-transform:uppercase">RECIBO PAGO</h3>
                </td>
            </tr>
            <tr>
                <td class="">
                    <div style="font-weight:bold;width:100%;height:auto;text-align:right;">Concepto</div>
                </td>
                <td class="text-center border-solid">
                    @if ($recibo->slug == 'mensualidad')
                        ABONO A CAPITAL
                    @else
                        {{ $recibo->title }} #{{ $count }}
                    @endif
                </td>
            </tr>
            @if ($recibo->slug == 'mensualidad')
                <tr>
                    <td class="">
                        <div style="font-weight:bold;width:100%;height:auto;text-align:right;">Número de pago</div>
                    </td>
                    <td class="text-center border-solid">
                        {{ $count }}/{{ $venta->quotation()->detail->mensualidad }}
                    </td>
                </tr>
            @endif
            <tr>
                <td class="">
                    <div style="font-weight:bold;width:100%;height:auto;text-align:right;">Fecha de Elaboración</div>
                </td>
                <td class="text-center border-solid">
                    {{ date('d/m/Y') }}
                </td>
            </tr>
            @if ($recibo->slug !== 'mensualidad')
                <tr>
                    <td class="">
                        <div style="font-weight:bold;width:100%;height:auto;text-align:right;">Ejecutivo de Ventas</div>
                    </td>
                    <td class="text-center border-solid">
                        {{ $quotation->admin()->name }} {{ $quotation->admin()->last_name }}
                    </td>
                </tr>
            @endif
            <tr>
                <td class="">
                    <div style="font-weight:bold;width:100%;height:auto;text-align:right;">Desarrollo</div>
                </td>
                <td class="text-center border-solid">
                    {{ $development->name }}
                </td>
            </tr>
            @if ($development->stage)
                <tr>
                    <td class="">
                        <div style="font-weight:bold;width:100%;height:auto;text-align:right;">Etapa</div>
                    </td>
                    <td class="text-center border-solid">
                        {{ $development->stage }}
                    </td>
                </tr>
            @endif
            <tr>
                <td class="">
                    <div style="font-weight:bold;width:100%;height:auto;text-align:right;">Número de lote</div>
                </td>
                <td class="text-center border-solid">
                    {{ $venta->lote()->name }}
                </td>
            </tr>
            @if ($recibo->slug == 'mensualidad')
                <tr>
                    <td class="">
                        <div style="font-weight:bold;width:100%;height:auto;text-align:right;">Fecha programada de pago</div>
                    </td>
                    <td class="text-center border-solid">
                        Día {{ date('d', strtotime($venta->dia_pago)) }} de cada mes
                    </td>
                </tr>
            @endif
        </tbody>
    </table>
    <table style="margin-top:30%;"></table>
    <div class="section-informacion">
        <p style="margin-left:15px;"><span class="subtitle">DATOS GENERALES DEL CLIENTE</span></p>
        <table style="width:100%;border-collapse:collapse;font-size:13px;" class="informacion">
            <tbody>
                <tr>
                    <td class="" style="width:25%;">
                        <div class="label">Nombre completo</div>
                    </td>
                    <td class="text-center border-solid" style="width:75%;" colspan="3">
                        {{ $client->name }} {{ $client->surname }}
                    </td>
                </tr>
                {{-- <tr>
                    <td class="" style="width:25%;">
                        <div class="label">Lugar de Nacimiento</div>
                    </td>
                    <td class="text-center border-solid" style="width:75%;" colspan="3">
                        {{ $client->lugar_nacimiento }}
                    </td>
                </tr>
                <tr>
                    <td class="" style="width:30%;">
                        <div class="label">Fecha De Nacimiento</div>
                    </td>
                    <td class="text-center border-solid" style="width:30%;">
                        {{ $client->fecha_nacimiento }}
                    </td>
                    <td class="text-center border-solid" style="width:15%">
                        <div class="label">Ocupación</div>
                    </td>
                    <td class="text-center border-solid" style="width:30%">
                        {{ $client->ocupation }}
                    </td>
                </tr>
                <tr>
                    <td class="" style="width:25%;">
                        <div class="label">Domicilio Actual</div>
                    </td>
                    <td class="text-center border-solid" style="width:75%;" colspan="3">
                        {{ $client->address }}
                    </td>
                </tr> --}}
                <tr>
                    <td class="" style="width:25%;">
                        <div class="label">Email</div>
                    </td>
                    <td class="text-center border-solid" style="width:30%;">
                        {{ $client->email }}
                    </td>
                    <td class="text-center border-solid" style="width:15%">
                        <div class="label">Teléfono</div>
                    </td>
                    <td class="text-center border-solid" style="width:30%">
                        {{ $client->phone }}
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <table style="margin-top:15px;"></table>
    <div class="section-informacion">
        <p style="margin-left:15px;"><span class="subtitle">DESCRIPCIÓN DEL PAGO</span></p>
        <table style="width:100%;border-collapse:collapse;font-size:13px;" class="informacion">
            <tbody>
                <tr>
                    <td class="" style="width:25%;">
                        <div class="label">Fecha</div>
                    </td>
                    <td class="text-center border-solid" style="width:25%;">
                        {{ $recibo->fecha_pago }}
                    </td>
                    <td class="" style="width:25%;">
                        <div class="label">Hora</div>
                    </td>
                    <td class="text-center border-solid" style="width:25%;">
                        {{-- //TODO: --}}
                        {{ date('H:i') }}
                    </td>
                    <td class="" style="width:25%;">
                        <div class="label">Método</div>
                    </td>
                    <td class="text-center border-solid" style="width:25%;">
                        Transferencia
                    </td>
                </tr>
                <tr>
                    <td class="" style="width:25%;">
                        <div class="label">Banco</div>
                    </td>
                    <td class="text-center border-solid" style="width:25%;">
                        {{ $recibo->banco()->name }}
                    </td>
                    <td class="" style="width:25%;">
                        <div class="label">N. de cuenta</div>
                    </td>
                    <td class="text-center border-solid" style="width:25%;">
                        {{ $recibo->banco()->n_cuenta }}
                    </td>
                    <td class="" style="width:25%;">
                        <div class="label">Ref. / Operación</div>
                    </td>
                    <td class="text-center border-solid" style="width:25%;">
                        {{ $recibo->message }}
                    </td>
                </tr>
                <tr>
                    <td class="" style="width:25%;">
                        <div class="label">Importe</div>
                    </td>
                    <td class="text-center border-solid" style="width:25%;">
                        $ {{ number_format($recibo->monto, 2) }}
                    </td>
                    <td class="" style="width:25%;">
                        <div class="label">Importe en letra</div>
                    </td>
                    <td class="text-center border-solid" style="width:75%;" colspan="3">
                       (Son: {{ num2letras($recibo->monto) }})
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <table style="margin-top:5px;"></table>
    <table style="margin-top:5px;"></table>
    <div class="section-restricciones">
        <p style="margin-left:15px;"><span class="">TOTAL A PAGAR: <b>$ {{ number_format($recibo->monto, 2) }} (Son: {{ num2letras($recibo->monto) }})</b></span></p>  
        <br>
        @if ($recibo->slug == 'mensualidad')
            <ul class="clausulas">
                <li>La presente, no constituye ninguna obligación o compromiso de entrega ni ocupación.</li>
                <li>El monto pagado en concepto de PAGO DE ABONO A CAPITAL, no será reembolsable en caso de desistir de la operación.</li>
                <li>El monto calculado por mora, se calcula a partir del sexto día de su fecha de pago por letra por mes, no es relativo al saldo del predio y deberá pagarse al momento del abono a la letra del mes a pagar.</li>
            </ul>
        @else
            <ul class="clausulas">
                <li>La presente, no constituye ninguna obligación o compromiso de entrega ni ocupación.</li>
                <li>El monto pagado en concepto de PAGO DE ENGANCHE, no será reembolsable en caso de desistir de la operación.</li>
                <li>El monto calculado por mora, se calcula a partir del sexto día de su fecha de pago por letra por mes, no es relativo al saldo del predio y deberá pagarse al momento del abono a la letra del mes a pagar.</li>
                <li>El tiempo establecido para el pago del enganche es el establecido en la cotización, condición de compra y corrida financiera, en caso de rebasar este período, el lote se liberará y quedará disponible para ser elegido por otro comprador. El monto pagado se quedará en resguardo para cuando el cliente decida reanudar con el proceso de compra en cualquier otro lote.</li>
            </ul>
        @endif
    </div>
    <footer>
        <table style="width:100%;">
            <tbody>
                <tr>
                    <td class="text-center" style="width: 50%;">
                        <div style="border-top:1px solid gray;width:80%;color:#000;margin:auto;">
                            <p style="width:100%;margin-bottom:0px!important;margin-top:-10px!important;">Nombre, Firma y Fecha</p>
                            <p style="width:100%;margin-top:-20px!important;margin-bottom:0px!important;">Ejecutivo de Ventas</p>
                        </div>
                    </td>
                    <td class="text-center" style="width: 50%;">
                        <div style="border-top:1px solid gray;width:80%;color:#000;margin:auto;">
                            <p style="width:100%;margin-bottom:0px!important;margin-top:-10px!important;">Nombre, Firma y Fecha</p>
                            <p style="width:100%;margin-top:-20px!important;margin-bottom:0px!important;">Cliente</p>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </footer>
</body>
</html>