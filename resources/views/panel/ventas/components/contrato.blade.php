<form action="{{ route('panel.ventas.update', ['venta' => $venta->id]) }}" method="POST" enctype="multipart/form-data">
    {{ csrf_field() }}
    <input type="hidden" name="_method" value="PUT">
    <input type="hidden" name="venta_id" value="{{ $venta->id }}">
    <div class="pl-lg-4">
        <div class="row justify-content-center">
            <div class="col-6 mb-5">
                <label class="form-control-label" for="contrato">
                    <span class="text-muted">* Contrato FIRMADO</span><br>
                    @if ($venta->contrato)
                        <small><a href="{{ asset($venta->contrato) }}" target="_blank"><b>Ver</b></a> documento subido</small><br>
                    @endif
                    {{-- @if (isset($venta->recibos('contrato')[0]))
                        @if (isset($venta->recibos('contatro')[0]->file))
                        @endif
                    @endif --}}
                    <small>Seleccionar un archivo no mayor a 500kb</small>
                </label>
                <div class="input-group input-group-merge input-group-alternative">
                    <input type="file" name="contrato" id="contrato" class="form-control form-control-alternative">
                </div>
            </div>
            {{-- {{ dd($venta->getContrato()) }} --}}
            {{-- <div class="col-12">
                <div class="form-group">
                    <label class="form-control-label" for="contrato">Texto Contrato</label>
                    <textarea name="contrato_text" id="contrato" cols="30" rows="10" class="form-control trumbowyg-panel">{{ $venta->getContrato() }}</textarea>
                </div>
            </div> --}}
            <div class="col-lg-12 text-center">
                <button class="btn btn-default">Confirmar</button>
            </div>
        </div>
    </div>
</form>