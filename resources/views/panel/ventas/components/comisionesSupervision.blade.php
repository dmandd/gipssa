<div class="pl-lg-4">
    <hr>
    <div class="flex-row text-center mb-4" style="justify-content:space-evenly;">
        @if($venta->comissionSupervision()?$venta->comissionSupervision()->payments:false)
            <a href="{{ route('comission.pdf.history', ['comission' => $venta->comissionSupervision()->id]) }}" target="_blank" class="btn btn-lg btn-icon btn-default active" type="button" data-toggle="tooltip" data-placement="top" title="Descargar historial de pagos de comisiones">
                <span class="btn-inner--icon"><i class="ni ni-cloud-download-95"></i> Descargar pagos supervisor</span>
            </a>
            @can(PermissionKey::ComissionSupervision['permissions']['destroy']['name'])
                <a href="{{ route('panel.comissions.lock', ['comission' => $venta->comissionSupervision()->id]) }}" class="btn btn-lg btn-icon rounded-circle btn-default active" type="button" data-toggle="tooltip" data-placement="top" title="{{$venta->comissionSupervision()->status?'Bloquear':'Liberar'}} comision">
                    <span class="btn-inner--icon"><i class="{{$venta->comissionSupervision()->status?'fa fa-lock':'ni ni-lock-circle-open'}}"></i></span>
                </a>
            @endcan
        @endif
    </div>
    @php
        $update = false;
        if(request()->user()->can(PermissionKey::ComissionSupervision['permissions']['update']['name']))
            $update = true;
    @endphp
    <h4>Supervisor: {{isset($venta->comissionSupervision()->admin_id)?($supervisor = Admin::find($venta->comissionSupervision()->admin_id))?$supervisor->name." ".$supervisor->last_name:"":"Sin supervisor asigando"}} | Valor de la venta {{$venta->quotation()->detail->precio_final}}</h4>
    @if($venta->comissionSupervision())
        <form id="form-comisionS" action="{{ route('panel.comission.update') }}" method="GET" class="row">
    @else
        <form id="form-comisionS" action="{{ route('panel.comission.store') }}" method="POST" class="row">
    @endif
        {{ csrf_field() }}
        <div class="col-lg-3">
            <div class="form-group">
                <label class="form-control-label" for="admin_id">Supervisor</label>
                <select name="admin_id" id="admin_id" class="form-control">
                    <option value="">Selecciona un supervisor</option>
                    @if (($admins) && (count($admins) > 0))
                        @foreach ($admins as $admin)
                            <option value="{{ $admin->id }}" {{ ($venta->comissionSupervision() ? ($admin->id == $venta->comissionSupervision()->admin_id ? 'selected' : '') : '') }}>{{ $admin->name." ".$admin->last_name }}</option>
                        @endforeach
                    @endif
                </select>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="form-group">
                <label class="form-control-label" for="paymentsS">* No. pagos</label>
                <input type="number" min="0" max="10" name="payments" id="paymentsS" class="form-control precalculateS" autocomplete="off" value="{{$venta->comissionSupervision()?$venta->comissionSupervision()->payments:''}}" {{!$update?'readonly':''}}>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="form-group">
                <label class="form-control-label" for="percentageS">* % comisión</label>
                <input type="number" min="0" max="100" name="percentage" id="percentageS" class="form-control precalculateS" autocomplete="off" value="{{$venta->comissionSupervision()?$venta->comissionSupervision()->percentage:''}}" {{!$update?'readonly':''}}>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="form-group">
                <label class="form-control-label" for="taxesS">* % retención</label>
                <input type="number" min="0" max="100" name="taxes" id="taxesS" class="form-control precalculateS" autocomplete="off" value="{{$venta->comissionSupervision()?$venta->comissionSupervision()->taxes:''}}" {{!$update?'readonly':''}}>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="form-group">
                <label class="form-control-label" for="total_amountS">Pago bruto</label>
                <input type="text" name="total_amount" id="total_amountS" class="form-control" autocomplete="off" value="{{$venta->comissionSupervision()?'$'.number_format($venta->comissionSupervision()->total_amount,2):'$0.00'}}" readonly>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="form-group">
                <label class="form-control-label" for="retentionsS">Retención</label>
                <input type="text" name="retentions" id="retentionsS" class="form-control" autocomplete="off" value="{{$venta->comissionSupervision()?'$'.number_format($venta->comissionSupervision()->retentions,2):'$0.00'}}" readonly>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="form-group">
                <label class="form-control-label" for="pay_amountS">Total a pagar</label>
                <input type="text" name="pay_amount" id="pay_amountS" class="form-control" autocomplete="off" value="{{$venta->comissionSupervision()?'$'.number_format($venta->comissionSupervision()->pay_amount,2):'$0.00'}}" readonly>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="form-group">
                <label class="form-control-label" for="paid_amountS">Total pagado</label>
                <input type="text" name="paid_amount" id="paid_amountS" class="form-control" autocomplete="off" value="{{$venta->comissionSupervision()?'$'.number_format($venta->comissionSupervision()->paid_amount,2):'$0.00'}}" readonly>
            </div>
        </div>
        <input type="hidden" name="venta_id" id="venta_idS" class="form-control" autocomplete="off" value="{{$venta->id}}">
        <input type="hidden" name="type" id="typeS" class="form-control" autocomplete="off" value="SUPERVISOR">
        <div class="col-lg-12">
            <div class="form-group">
                <label class="form-control-label" for="notesS">Comentarios</label>
                <textarea name="notes" class="form-control" id="notesS" cols="30" rows="10" {{!$update?'readonly':''}}>{{ $venta->comissionSupervision()?$venta->comissionSupervision()->notes:"" }}</textarea>
            </div>
        </div>
    </form>
    <div class="col-12 text-center">
        @if($update)
            <button class="btn btn-default " id="guardarComisionS" title="Guardar">Guardar</button>
        @endif
    </div>
    <br>
    <div class="table-responsive pb-3">
        <table class="table align-items-center table-flush" id="dataTableSupervision">
            <thead class="thead-light">
                <tr>
                    <th scope="col" class="sort" data-sort="status">No. Pago</th>
                    <th>Fecha de pago</th>
                    <th>Importe pago</th>
                    <th scope="col" class="sort" data-sort="status">Pagado</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody class="list">
                @if ($venta->comissionSupervision())
                    @foreach ($venta->comissionSupervision()->payments() as $key => $item)
                    <tr>
                        <td>{{$key+1}}</td>
                        <td><input type='text' name='payment_date' id="payment_date{{$item->id}}" class='form-control mydatepicker' value="{{date('d/m/Y', strtotime($item->payment_date))}}" autocomplete='off' {{$item->paid?"readonly":(!$update?"readonly":"")}}></td>
                        <td><input type='number' name='payment_amount' id="payment_amount{{$item->id}}" class='form-control' value="{{$item->payment_amount}}" autocomplete='off' {{$item->paid?"readonly":(!$update?"readonly":"")}}></td>
                        <td>
                            @if(!$item->paid)
                                <span class="badge badge-warning">No Pagado</span>
                            @else
                                <span class="badge badge-success">Pagado</span>
                            @endif
                        </td>
                        <td>
                            @if($update)
                                <div class="dropdown">
                                    <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fas fa-ellipsis-v"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                        @if(!$item->paid)
                                            <a class="dropdown-item btn-action" 
                                                data-title="¿Está seguro?"
                                                data-text="Esta comisión se pagara y no podra modificarse"
                                                data-icon="question"
                                                data-axios-method="post"
                                                data-route="{{ route('panel.comission.payment.pay', ['payment_id' => $item->id]) }}"
                                                data-action="location.reload()"
                                                href="javascript:;">
                                                Pagar
                                            </a>
                                            <a class="dropdown-item" href="javascript:;" onclick="update('{{$item->id}}')">
                                                Guardar Cambios
                                            </a>
                                        @else
                                            <a class="dropdown-item" onclick="showModal('#modal-form-evidence','{{$item->id}}')" href="javascript:;">Subir evidencia de pago</a>
                                            @if(!is_null($item->file))
                                                <a class="dropdown-item" href="{{ asset($item->file) }}" target="_blank">Ver evidencia de pago</a>
                                            @endif
                                            <a class="dropdown-item btn-action" 
                                                data-title="¿Está seguro?"
                                                data-text="De revertir el registro de pago de esta comision"
                                                data-icon="question"
                                                data-axios-method="post"
                                                data-route="{{ route('panel.comission.payment.unpay', ['payment_id' => $item->id]) }}"
                                                data-action="location.reload()"
                                                href="javascript:;">
                                                Revertir Pago
                                            </a>
                                        @endif
                                    </div>
                                </div>
                            @endif
                        </td>
                    </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
    </div>
</div>
@push('js')
    <script>
        $(document).ready(function() {

            let dT = $('#dataTableSupervision').DataTable({
                "language": {
                    "paginate": {
                    "previous": "<",
                    "next": ">"
                    },
                    "emptyTable": "No hay contenido disponible",
                    "info": "Mostrando desde _START_ hasta _END_ de _TOTAL_ registros",
                    "infoEmpty": "Mostrando desde 0 hasta 0 de 0 registros",
                    "lengthMenu": "Mostrar _MENU_ registros",
                    "loadingRecords": "Cargando...",
                    "processing":     "Procesando...",
                    "search":         "Buscar:",
                    "zeroRecords":    "No se encontraron registros",
                },
                "columnDefs": [ {
                    "orderable": false,
                    "className": 'select-checkbox',
                    "targets":   0
                } ],
                "select": {
                    "style": 'os',
                    "selector": 'td:first-child'
                },
                "order": [
                    [0, 'asc']
                ]
            });

            $(".precalculateS").on("change",function(e){
                e.preventDefault();
                precalcularS();
            });

            $("#guardarComisionS").on("click",function(){
                $("#form-comisionS").submit();
            });


            function precalcularS(){
                let money_format = Intl.NumberFormat('en-US');
                let percentage  = $("#percentageS").val() == null?0:$("#percentageS").val();
                let taxes       = $("#taxesS").val() == null?0:$("#taxesS").val();
                let total       = "{{$venta->quotation()->detail->precio_final}}".replace(/\$|,/g, '');
                let total_bruto = 0
                let total_impuestos = 0;
                let total_a_pagar = 0;
                
                total_bruto = parseFloat((total*percentage)/100).toFixed(2);
                total_impuestos = parseFloat((total_bruto*taxes)/100).toFixed(2);

                total_a_pagar = total_bruto - total_impuestos;

                $("#total_amountS").val("$"+money_format.format(total_bruto));
                $("#retentionsS").val("$"+money_format.format(total_impuestos));
                $("#pay_amountS").val("$"+money_format.format(total_a_pagar));
            };
        });

    </script>
@endpush