<!-- Modal -->
<div class="modal fade" id="addComplemento" tabindex="-1" role="dialog" aria-labelledby="addComplementoLabel" aria-hidden="true">
    <form action="{{ route('panel.ventas.recibos.store', ['venta' => $venta->id]) }}" method="POST" class="needs-validation" novalidate enctype="multipart/form-data">
        {{ csrf_field() }}
        <input type="hidden" name="venta_id" value="{{ $venta->id }}">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="addComplementoLabel">Agregar complemento de pago</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    
                    <div class="col-12 mb-3 text-truncate">
                        <label for="" class="form-control-label">
                            <span class="text-muted">Monto a pagar</span>
                        </label>
                        <input type="text" name="monto" class="form-control" required value="">
                        <small>Monto en decimal****</small><br>
                        <small>Ejemplo.: $545.50</small>
                    </div>
                    <div class="col-6 mb-3 text-truncate">
                        <label for="" class="form-control-label">
                            <span class="text-muted">Fecha de pago</span>
                        </label>
                        <input type="text" name="fecha_pago" class="form-control mydatepicker" required value="" autocomplete="off">
                    </div>
                    <div class="col-6 mb-3 text-truncate">
                        <label for="" class="form-control-label">
                            <span class="text-muted">Concepto</span>
                        </label>
                        <select name="title" id="title" class="form-control" required>
                            <option value="">Selecciona una opción</option>
                            @if ($venta->status > 3)
                                <option selected value="Mensualidad">Mensualidad</option>
                            @else
                                <option selected value="Abono Enganche" autocomplete="off">Abono enganche</option>
                            @endif
                        </select>
                    </div>
                    <div class="col-6 mb-3 text-truncate">
                        <label for="" class="form-control-label">
                            <span class="text-muted">Forma de pago</span>
                        </label>
                        <select name="payment_way" id="payment_way" class="form-control" required>
                            <option value="">Selecciona una opción</option>
                            <option value="01">Efectivo</option>
                            <option value="02">Cheque nominativo</option>
                            <option value="03">Transferencia electrónica de fondos</option>
                            <option value="04">Tarjeta de crédito</option>
                            <option value="05">Monedero electrónico</option>
                            <option value="06">Dinero electrónico</option>
                            <option value="08">Vales de despensa</option>
                            <option value="12">Dación en pago</option>
                            <option value="13">Pago por subrogación</option>
                            <option value="14">Pago por consignación</option>
                            <option value="15">Condonación</option>
                            <option value="17">Compensación</option>
                            <option value="23">Novación</option>
                            <option value="24">Confusión</option>
                            <option value="25">Remisión de deuda</option>
                            <option value="26">Prescripción o caducidad</option>
                            <option value="27">A satisfacción del acreedor</option>
                            <option value="28">Tarjeta de débito</option>
                            <option value="29">Tarjeta de servicios</option>
                            <option value="30">Aplicación de anticipos</option>
                            <option value="99">Por definir</option>
                        </select>
                    </div>
                    <div class="col-6 mb-3 text-truncate">
                        <label for="" class="form-control-label">
                            <span class="text-muted">Método de pago</span>
                        </label>
                        <select name="payment_method" id="payment_method" class="form-control" required>
                            <option value="">Selecciona una opción</option>
                            <option value="PUE">PUE-Pago en una sola exhibición</option>
                            <option value="PPD">PPD-Pago en parcialidades o diferido</option>
                        </select>
                    </div>
                    <div class="col-12 mb-3 text-truncate">
                        <label for="" class="form-control-label">
                            <span class="text-muted">Referencia/Observaciones</span>
                        </label>
                        <textarea name="message" class="form-control"  rows="3" value=""></textarea>
                    </div>
                    <div class="col mb-3 text-truncate">
                        <label for="banco_id" class="form-control-label">
                            <span class="text-muted">Cuenta bancaria</span>
                        </label>
                        <select name="banco_id" id="banco_id" class="form-control" required>
                            <option value="">Selecciona una opción</option>
                            @if ($venta->development()->bancos()->count() > 0)
                                @foreach ($venta->development()->bancos() as $banco)
                                    <option value="{{ $banco->id }}">{{ $banco->name }} - {{ $banco->n_cuenta }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
              <button class="btn btn-primary">Guardar</button>
            </div>
          </div>
        </div>
    </form>
</div>