<script
  src="https://code.jquery.com/jquery-3.5.1.min.js"
  integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
  crossorigin="anonymous"></script>
<div class="d-flex flex-row align-items-center" style="justify-content:space-evenly;">
    {{-- @if ($venta->status == 1)
        <button class="btn btn-lg btn-icon rounded-circle btn-default btn-action" type="button" 
                data-toggle="tooltip" 
                data-placement="top" 
                title="Solicitar Autorización"
                data-title="¿Está seguro de enviar a revisión?" 
                data-text="La venta será revisada" 
                data-icon="question" 
                data-axios-method="post" 
                data-axios-body='{"status": 1}' 
                data-route="{{ route('panel.ventas.review', ['venta' => $venta->id]) }}" 
                data-action="location.reload()" >
            <span class="btn-inner--icon"><i class="ni ni-bulb-61"></i></span>
        </button>
    @endif --}}
    <a href="{{ route('ventas.pdf.condicion', ['venta' => $venta->id]) }}" target="_blank" class="btn btn-lg btn-icon btn-default active" type="button" data-toggle="tooltip" data-placement="top" title="Descargar condición de compra">
        <span class="btn-inner--icon"><i class="ni ni-cloud-download-95"></i> Condición de compra</span>
    </a>
    @if (isset($venta->recibos('pago-bloqueo')->first()->banco_id))
        <a href="{{ route('ventas.pdf.bloqueo', ['venta' => $venta->id]) }}" target="_blank" class="btn btn-lg btn-icon btn-default active" type="button" data-toggle="tooltip" data-placement="top" title="Descargar recibo de bloqueo">
            <span class="btn-inner--icon"><i class="ni ni-cloud-download-95"></i> Recibo de bloqueo</span>
        </a>
    @endif
</div>
@include('panel.ventas.components.cotizacion')
<hr>
<form action="{{ route('panel.ventas.update', ['venta' => $venta->id]) }}" method="POST" class="needs-validation" novalidate enctype="multipart/form-data">
    {{ csrf_field() }}
    <input type="hidden" name="_method" value="PUT">
    <div class="px-lg-4">
        <div class="d-flex align-items-center mb-4">
            <h5>{{$venta->quotation()->detail->plazo->tipo == 'financiamiento'? 'Fecha(s) Enganche':'Fecha(s) Pago' }}</h5> 
            {{--<button class="btn btn-icon btn-primary ml-2" id="agregar-fecha" title="Agregar Fecha Enganche" type="button"><span class="btn-inner--icon"><i class="ni ni-fat-add"></i></span></button>--}}
        </div>
        <div class="row" id="dias_pago_enganche">
            @if ($venta->fechas_enganche()->get()->count() > 0)
                @foreach ($venta->fechas_enganche()->get() as $i => $fecha)
                    <div class="col-6 mb-5 text-truncate">
                        <label for="" class="form-control-label">
                            <span class="text-muted">Definir Día Pago {{$venta->quotation()->detail->plazo->tipo == 'financiamiento'? 'Enganche':'' }} {{ ($venta->quotation()->detail->plazo->tipo == 'contado')?'':($i + 1) }}</span>
                        </label>
                        <input type="text" name="dia_pago_enganche[]" class="form-control mydatepicker cont-fecha" required value="{{ $fecha->fecha() }}" autocomplete="off">
                        <small><b>Formato fecha:</b> DD/MM/YYYY</small>
                    </div>
                    <div class="col-6 mb-5 text-truncate">
                        <label for="" class="form-control-label">
                            <span class="text-muted">Monto</span>
                        </label>
                        <input type="text" name="monto_enganche[]" class="form-control" required readonly value="{{ is_null($fecha->monto)? $quotation->detail->monto_enganche:$fecha->monto }}" autocomplete="off">
                        <small>Ej. 5000, Ej. 5200.50</small>
                    </div>
                @endforeach
            @else
                @if ($venta->quotation()->detail->enganche_num_pagos > 0)
                    @for ($i = 0; $i < $venta->quotation()->detail->enganche_num_pagos; $i++)
                        <div class="col-6 mb-5 text-truncate">
                            <label for="" class="form-control-label">
                                <span class="text-muted">Definir Día Pago {{$venta->quotation()->detail->plazo->tipo == 'financiamiento'? 'Enganche':'' }} {{ ($venta->quotation()->detail->plazo->tipo == 'contado')?'':($i + 1) }}</span>
                            </label>
                            <input type="text" name="dia_pago_enganche[]" class="form-control mydatepicker cont-fecha" required value="" autocomplete="off">
                            <small><b>Formato fecha:</b> DD/MM/YYYY</small>
                        </div>
                        <div class="col-6 mb-5 text-truncate">
                            <label for="" class="form-control-label">
                                <span class="text-muted">Monto</span>
                            </label>
                            <input type="text" name="monto_enganche[]" class="form-control" required readonly value="{{$venta->quotation()->detail->pago_mensual_enganche}}" autocomplete="off">
                            <small>Ej. 5000, Ej. 5200.50</small>
                        </div>
                    @endfor
                @else
                    <div class="col-6 mb-5 text-truncate">
                        <label for="" class="form-control-label">
                            <span class="text-muted">Definir Día Pago Enganche </span>
                        </label>
                        <input type="text" name="dia_pago_enganche[]" class="form-control mydatepicker cont-fecha" required value="" autocomplete="off">
                        <small><b>Formato fecha:</b> DD/MM/YYYY</small>
                    </div>
                    <div class="col-6 mb-5 text-truncate">
                        <label for="" class="form-control-label">
                            <span class="text-muted">Monto</span>
                        </label>
                        <input type="text" name="monto_enganche[]" class="form-control" required value="" autocomplete="off">
                        <small>Ej. 5000, Ej. 5200.50</small>
                    </div>
                @endif
            @endif
        </div>
        @if($venta->quotation()->detail->plazo->tipo == 'financiamiento')
        <hr>
        <h5>Fecha Pago Mensualidad</h5>
        <div class="row">
            <div class="col-6 mb-5 text-truncate">
                <label for="" class="form-control-label">
                    <span class="text-muted">Definir Día Pago Primera Mensualidad</span>
                </label>
                <input type="text" name="dia_pago" class="form-control mydatepicker" required value="{{ $venta->diaPago() }}" autocomplete="off">
                <small><b>Formato fecha:</b> DD/MM/YYYY</small>
            </div>
        </div>
        @endif
        @if($venta->quotation()->detail->saldo !== '$0.00' && !is_null($venta->quotation()->detail->saldo))
        <hr>
        <h5>Fecha pago de saldo</h5>
        <div class="row">
            <div class="col-6 mb-5 text-truncate">
                <label for="" class="form-control-label">
                    <span class="text-muted">Definir Día Pago</span>
                </label>
                <input type="text" name="fecha_pago_saldo" class="form-control mydatepicker cont-fecha" required value="{{ $venta->fechaPagoSaldo() }}" autocomplete="off">
                <small><b>Formato fecha:</b> DD/MM/YYYY</small>
            </div>
            <div class="col-6 mb-5 text-truncate">
                <label for="" class="form-control-label">
                    <span class="text-muted">Monto</span>
                </label>
                <input type="text" name="monto_saldo" class="form-control" required readonly value="{{$venta->quotation()->detail->saldo}}" autocomplete="off">
                <small>Ej. 5000, Ej. 5200.50</small>
            </div>
        </div>
        @endif
        <hr>
        <h4>Definir Parametros de Contrato</h4>
        <div class="row">
            <div class="col-6 mb-5 text-truncate">
                <label for="" class="form-control-label">
                    <span class="text-muted">Fecha de pago</span>
                </label>
                <input type="text" name="fecha_bloqueo" class="form-control mydatepicker" required value="{{ $venta->fechaBloqueo() }}" autocomplete="off">
                <small><b>Formato fecha:</b> DD/MM/YYYY</small>
            </div>
            <div class="col-6 mb-5 text-truncate">
                <label for="" class="form-control-label">
                    <span class="text-muted">Monto a pagar</span>
                </label>
                <input type="text" name="monto_bloqueo" class="form-control" required value="{{ $venta->monto_bloqueo }}" autocomplete="off">
                <small>Ej. 5000, Ej. 5200.50</small>
            </div>
        </div>
        <div class="row">
            <div class="col-6 mb-5 text-truncate">
                <label for="" class="form-control-label">
                    <span class="text-muted">Fecha de la firma</span>
                </label>
                <input type="text" name="fecha_firma_contrato" class="form-control mydatepicker" required value="{{ $venta->fechaFirmaContrato() }}" autocomplete="off">
                <small><b>Formato fecha:</b> DD/MM/YYYY</small>
            </div>
            <div class="col-6 mb-5 text-truncate">
                <label for="" class="form-control-label">
                    <span class="text-muted">Ubicación de la firma</span>
                </label>
                <select name="ubicacion_firma_contrato" id="ubicacion_firma_contrato" class="form-control" required>
                    <option value="">Selecciona una opción</option>
                    <option {{ $venta->ubicacion_firma_contrato == "domicilio" ? 'selected' : ''}} value="domicilio">Domicilio</option>
                    <option {{ $venta->ubicacion_firma_contrato == "gipssa" ? 'selected' : '' }} value="gipssa">Gipssa</option>
                    <option {{ $venta->ubicacion_firma_contrato == "notaria" ? 'selected' : '' }} value="notaria">Notaria</option>
                </select>
                <small>Una breve descripción</small>
            </div>
        </div>
        <hr>
        <h5>Observaciones de la venta</h5>
        <div class="row">
            <div class="col-12 mb-3 text-truncate">
                <label for="" class="form-control-label">
                    <span class="text-muted">Observaciones</span>
                </label>
                <textarea name="observaciones" class="form-control"  rows="6" value="">{{ $venta->observaciones }}</textarea>
            </div>
        </div>
        <hr>
        <h5>Información del pago</h5>
        <div class="row">
            <div class="col-4 mb-3 text-truncate">
                <label for="" class="form-control-label">
                    <span class="text-muted">Concepto</span>
                </label>
                <select name="title" id="title" class="form-control" required="">
                    <option value="">Selecciona una opción</option>
                    <option selected value="Pago Bloqueo" autocomplete="off">Pago Contrato</option>
                </select>
            </div>
            <div class="col-4 mb-3 text-truncate">
                <label for="" class="form-control-label">
                    <span class="text-muted">Forma de pago</span>
                </label>
                <select name="payment_way" id="payment_way" class="form-control" required>
                    <option value="">Selecciona una opción</option>
                    
                    <option {{ ((isset($venta->recibos('pago-bloqueo')->first()->payment_way)) &&($venta->recibos('pago-bloqueo')->first()->payment_way == "01")) ? 'selected' : ''}} value="01">Efectivo</option>
                    <option {{ ((isset($venta->recibos('pago-bloqueo')->first()->payment_way)) &&($venta->recibos('pago-bloqueo')->first()->payment_way == "02")) ? 'selected' : '' }} value="02">Cheque nominativo</option>
                    <option {{ ((isset($venta->recibos('pago-bloqueo')->first()->payment_way)) &&($venta->recibos('pago-bloqueo')->first()->payment_way == "03")) ? 'selected' : '' }} value="03">Transferencia electrónica de fondos</option>
                    <option {{ ((isset($venta->recibos('pago-bloqueo')->first()->payment_way)) &&($venta->recibos('pago-bloqueo')->first()->payment_way == "04")) ? 'selected' : '' }} value="04">Tarjeta de crédito</option>
                    <option {{ ((isset($venta->recibos('pago-bloqueo')->first()->payment_way)) &&($venta->recibos('pago-bloqueo')->first()->payment_way == "05")) ? 'selected' : '' }} value="05">Monedero electrónico</option>
                    <option {{ ((isset($venta->recibos('pago-bloqueo')->first()->payment_way)) &&($venta->recibos('pago-bloqueo')->first()->payment_way == "06")) ? 'selected' : '' }} value="06">Dinero electrónico</option>
                    <option {{ ((isset($venta->recibos('pago-bloqueo')->first()->payment_way)) &&($venta->recibos('pago-bloqueo')->first()->payment_way == "08")) ? 'selected' : '' }} value="08">Vales de despensa</option>
                    <option {{ ((isset($venta->recibos('pago-bloqueo')->first()->payment_way)) &&($venta->recibos('pago-bloqueo')->first()->payment_way == "12")) ? 'selected' : '' }} value="12">Dación en pago</option>
                    <option {{ ((isset($venta->recibos('pago-bloqueo')->first()->payment_way)) &&($venta->recibos('pago-bloqueo')->first()->payment_way == "13")) ? 'selected' : '' }} value="13">Pago por subrogación</option>
                    <option {{ ((isset($venta->recibos('pago-bloqueo')->first()->payment_way)) &&($venta->recibos('pago-bloqueo')->first()->payment_way == "14")) ? 'selected' : '' }} value="14">Pago por consignación</option>
                    <option {{ ((isset($venta->recibos('pago-bloqueo')->first()->payment_way)) &&($venta->recibos('pago-bloqueo')->first()->payment_way == "15")) ? 'selected' : '' }} value="15">Condonación</option>
                    <option {{ ((isset($venta->recibos('pago-bloqueo')->first()->payment_way)) &&($venta->recibos('pago-bloqueo')->first()->payment_way == "17")) ? 'selected' : '' }} value="17">Compensación</option>
                    <option {{ ((isset($venta->recibos('pago-bloqueo')->first()->payment_way)) &&($venta->recibos('pago-bloqueo')->first()->payment_way == "23")) ? 'selected' : '' }} value="23">Novación</option>
                    <option {{ ((isset($venta->recibos('pago-bloqueo')->first()->payment_way)) &&($venta->recibos('pago-bloqueo')->first()->payment_way == "24")) ? 'selected' : '' }} value="24">Confusión</option>
                    <option {{ ((isset($venta->recibos('pago-bloqueo')->first()->payment_way)) &&($venta->recibos('pago-bloqueo')->first()->payment_way == "25")) ? 'selected' : '' }} value="25">Remisión de deuda</option>
                    <option {{ ((isset($venta->recibos('pago-bloqueo')->first()->payment_way)) &&($venta->recibos('pago-bloqueo')->first()->payment_way == "26")) ? 'selected' : '' }} value="26">Prescripción o caducidad</option>
                    <option {{ ((isset($venta->recibos('pago-bloqueo')->first()->payment_way)) &&($venta->recibos('pago-bloqueo')->first()->payment_way == "27")) ? 'selected' : '' }} value="27">A satisfacción del acreedor</option>
                    <option {{ ((isset($venta->recibos('pago-bloqueo')->first()->payment_way)) &&($venta->recibos('pago-bloqueo')->first()->payment_way == "28")) ? 'selected' : '' }} value="28">Tarjeta de débito</option>
                    <option {{ ((isset($venta->recibos('pago-bloqueo')->first()->payment_way)) &&($venta->recibos('pago-bloqueo')->first()->payment_way == "29")) ? 'selected' : '' }} value="29">Tarjeta de servicios</option>
                    <option {{ ((isset($venta->recibos('pago-bloqueo')->first()->payment_way)) &&($venta->recibos('pago-bloqueo')->first()->payment_way == "10")) ? 'selected' : '' }} value="30">Aplicación de anticipos</option>
                    <option {{ ((isset($venta->recibos('pago-bloqueo')->first()->payment_way)) &&($venta->recibos('pago-bloqueo')->first()->payment_way == "99")) ? 'selected' : '' }} value="99">Por definir</option>
                </select>
            </div>
            <div class="col-4 mb-3 text-truncate">
                <label for="" class="form-control-label">
                    <span class="text-muted">Método de pago</span>
                </label>
                <select name="payment_method" id="payment_method" class="form-control" required>
                    <option value="">Selecciona una opción</option>
                    <option {{ ((isset($venta->recibos('pago-bloqueo')->first()->payment_method)) &&($venta->recibos('pago-bloqueo')->first()->payment_method == "PUE")) ? 'selected' : '' }} value="PUE">PUE-Pago en una sola exhibición</option>
                    <option {{ ((isset($venta->recibos('pago-bloqueo')->first()->payment_method)) &&($venta->recibos('pago-bloqueo')->first()->payment_method == "PPD")) ? 'selected' : '' }} value="PPD">PPD-Pago en parcialidades o diferido</option>
                </select>
            </div>
            <div class="col-12 mb-3 text-truncate">
                <label for="banco_id" class="form-control-label">
                    <span class="text-muted">Cuenta bancaria</span>
                </label>
                <select name="banco_id" id="banco_id" class="form-control" required>
                    <option value="">Selecciona una opción</option>
                    @if ($venta->development()->bancos()->count() > 0)
                        @foreach ($venta->development()->bancos() as $banco)
                            <option value="{{ $banco->id }}" {{ ( (isset($venta->recibos('pago-bloqueo')->first()->banco_id)) && ($venta->recibos('pago-bloqueo')->first()->banco_id == $banco->id) ) ? 'selected' : '' }}>{{ $banco->name }} - {{ $banco->n_cuenta }}</option>
                        @endforeach
                    @endif
                </select>
            </div>
            <div class="col-12 mb-3 text-truncate">
                <label for="" class="form-control-label">
                    <span class="text-muted">Referencia/Observaciones</span>
                </label>
                <textarea name="message" class="form-control"  rows="3" value="">{{ isset($venta->recibos('pago-bloqueo')->first()->message) ? $venta->recibos('pago-bloqueo')->first()->message : '' }}</textarea>
            </div>
            
            {{-- <div class="col mb-5 text-truncate">
                <label for="" class="form-control-label">
                    <span class="text-muted">Comprobante Pago Bloqueo</span>
                </label>
                <input type="file" name="comprobante_pago" {{ ($venta->comprobante_pago !== '') ? '' : 'required' }} class="form-control">
                @if ($venta->reciboBloqueo())
                    <br><small><a href="{{ asset($venta->reciboBloqueo()->file) }}" target="_blank">Ver documento</a></small>
                @endif
            </div> --}}
        </div>
        <div class="row">
            @if (($venta->fecha_bloqueo) && ($venta->monto_bloqueo) )
                <div class="col-12 text-center">
                    
                    
                    @if($venta->recibos('pago-bloqueo')->first()->factura_id == null)
                        <button class="btn btn-default">Guardar cambios</button>
                        @can(PermissionKey::Venta['permissions']['facturar_pagos']['name'])
                            <a class="btn btn-icon btn-default text-white" href=" {{ route('panel.facturacion.store', ['venta' => $venta->id, 'recibo_id' => $venta->recibos('pago-bloqueo')->first()->id]) }} ">
                                <span class="btn-inner--icon">Facturar</span>
                            </a>
                        @endcan
                    @else
                        @can(PermissionKey::Venta['permissions']['facturar_pagos']['name'])
                            <a class="btn btn-icon btn-default text-white" href="{{ route('panel.facturacion.delete', ['recibo_id' => $venta->recibos('pago-bloqueo')->first()->id]) }}" data-toggle="tooltip" data-placement="top" title="Cancelar factura">
                                <span class="btn-inner--icon"> Cancelar factura</span>
                            </a>
                            <button class="btn btn-default"
                                data-title="¿Está seguro de autorizar?"
                                data-text="Este registro se autorizará. La operación no puede revertirse"
                                data-icon="question"
                                data-axios-method="get"
                                data-route="{{ route('panel.ventas.recibos.store', ['venta' => $venta->id,'recibo_id' => $venta->recibos('pago-bloqueo')->first()->id]) }}"
                                data-action="location.reload()"
                                disabled
                                >
                                Enviar factura
                            </button>
                            <a class="btn btn-icon btn-default text-white" href="{{ route('panel.facturacion.download', ['id' => $venta->recibos('pago-bloqueo')->first()->factura_id]) }}" data-toggle="tooltip" data-placement="top" title="Descargar factura">
                                <span class="btn-inner--icon"><i class="ni ni-cloud-download-95"></i> Descargar factura</span>
                            </a>
                        @endcan    
                    @endif

                    @if ($venta->status == 1)
                        @can(PermissionKey::Venta['permissions']['parametros_bloqueo']['name'])
                            <button class="btn btn-default btn-action"
                                    data-toggle="tooltip" 
                                    data-placement="top" 
                                    data-title="¿Está seguro de confirmar esta etapa?" 
                                    data-text="La venta se moverá a Documentación y Contrato" 
                                    data-icon="question" 
                                    data-axios-method="put" 
                                    data-axios-body='{"status": 2}' 
                                    data-route="{{ route('panel.ventas.update', ['venta' => $venta->id]) }}" 
                                    data-action="location.reload()"         
                            title="Aprobar">Confirmar etapa</button>
                        @endcan
                    @endif
                </div>
            @else
                <div class="col-12 text-center">
                    <button class="btn btn-default">Guardar cambios</button>
                </div>
            @endif
        </div>
    </div>
</form>