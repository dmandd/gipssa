<div class="pl-lg-4">
    <div class="flex-row text-center mb-5" style="justify-content:space-evenly;">
        @if($venta->comission()?$venta->comission()->payments:false)
            <a href="{{ route('comission.pdf.history', ['comission' => $venta->comission()->id]) }}" target="_blank" class="btn btn-lg btn-icon btn-default active" type="button" data-toggle="tooltip" data-placement="top" title="Descargar historial de pagos de comisiones">
                <span class="btn-inner--icon"><i class="ni ni-cloud-download-95"></i> Descargar pagos asesor</span>
            </a>
            @can(PermissionKey::Comission['permissions']['destroy']['name'])
                <a href="{{ route('panel.comissions.lock', ['comission' => $venta->comission()->id]) }}" class="btn btn-lg btn-icon rounded-circle btn-default active" type="button" data-toggle="tooltip" data-placement="top" title="{{$venta->comission()->status?'Bloquear':'Liberar'}} comision">
                    <span class="btn-inner--icon"><i class="{{$venta->comission()->status?'fa fa-lock':'ni ni-lock-circle-open'}}"></i></span>
                </a>
            @endcan
        @endif
    </div>
    @php
        $update = false;
        if(request()->user()->can(PermissionKey::Comission['permissions']['update']['name']))
            $update = true;
    @endphp
    <h4>Vendedor: {{$venta->asesor()->name." ".$venta->asesor()->last_name}} | Valor de la venta {{$venta->quotation()->detail->precio_final}}</h4>
    @if($venta->comission())
        <form id="form-comision" action="{{ route('panel.comission.update') }}" method="GET" class="row">
    @else
        <form id="form-comision" action="{{ route('panel.comission.store') }}" method="POST" class="row">
    @endif
        {{ csrf_field() }}
        <div class="col-lg-4">
            <div class="form-group">
                <label class="form-control-label" for="payments">* No. pagos</label>
                <input type="number" min="0" max="10" name="payments" id="payments" class="form-control precalculate" autocomplete="off" value="{{$venta->comission()?$venta->comission()->payments:''}}" {{!$update?'readonly':''}}>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="form-group">
                <label class="form-control-label" for="percentage">* % comisión</label>
                <input type="number" min="0" max="100" name="percentage" id="percentage" class="form-control precalculate" autocomplete="off" value="{{$venta->comission()?$venta->comission()->percentage:''}}" {{!$update?'readonly':''}}>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="form-group">
                <label class="form-control-label" for="taxes">* % retención</label>
                <input type="number" min="0" max="100" name="taxes" id="taxes" class="form-control precalculate" autocomplete="off" value="{{$venta->comission()?$venta->comission()->taxes:''}}" {{!$update?'readonly':''}}>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="form-group">
                <label class="form-control-label" for="total_amount">Pago bruto</label>
                <input type="text" name="total_amount" id="total_amount" class="form-control" autocomplete="off" value="{{$venta->comission()?'$'.number_format($venta->comission()->total_amount,2):'$0.00'}}" readonly>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="form-group">
                <label class="form-control-label" for="retentions">Retención</label>
                <input type="text" name="retentions" id="retentions" class="form-control" autocomplete="off" value="{{$venta->comission()?'$'.number_format($venta->comission()->retentions,2):'$0.00'}}" readonly>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="form-group">
                <label class="form-control-label" for="pay_amount">Total a pagar</label>
                <input type="text" name="pay_amount" id="pay_amount" class="form-control" autocomplete="off" value="{{$venta->comission()?'$'.number_format($venta->comission()->pay_amount,2):'$0.00'}}" readonly>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="form-group">
                <label class="form-control-label" for="paid_amount">Total pagado</label>
                <input type="text" name="paid_amount" id="paid_amount" class="form-control" autocomplete="off" value="{{$venta->comission()?'$'.number_format($venta->comission()->paid_amount,2):'$0.00'}}" readonly>
            </div>
        </div>
        <input type="hidden" name="venta_id" id="venta_id" class="form-control" autocomplete="off" value="{{$venta->id}}">
        <input type="hidden" name="type" id="typeS" class="form-control" autocomplete="off" value="ASESOR">
        <div class="col-lg-12">
            <div class="form-group">
                <label class="form-control-label" for="notes">Comentarios</label>
                <textarea name="notes" class="form-control" id="notes" cols="30" rows="10" {{!$update?'readonly':''}}>{{ $venta->comission()?$venta->comission()->notes:"" }}</textarea>
            </div>
        </div>
    </form>
    <div class="col-12 text-center">
        @if($update)
            <button class="btn btn-default " id="guardarComision" title="Guardar">Guardar</button>
        @endif
    </div>
    <br>
    <div class="table-responsive pb-3">
        <table class="table align-items-center table-flush" id="dataTable">
            <thead class="thead-light">
                <tr>
                    <th scope="col" class="sort" data-sort="status">No. Pago</th>
                    <th>Fecha de pago</th>
                    <th>Importe pago</th>
                    <th scope="col" class="sort" data-sort="status">Pagado</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody class="list">
                @if ($venta->comission())
                    @foreach ($venta->comission()->payments() as $key => $item)
                    <tr>
                        <td>{{$key+1}}</td>
                        <td><input type='text' name='payment_date' id="payment_date{{$item->id}}" class='form-control mydatepicker' value="{{date('d/m/Y', strtotime($item->payment_date))}}" autocomplete='off' {{$item->paid?"readonly":(!$update?"readonly":"")}}></td>
                        <td><input type='number' name='payment_amount' id="payment_amount{{$item->id}}" class='form-control' value="{{$item->payment_amount}}" autocomplete='off' {{$item->paid?"readonly":(!$update?"readonly":"")}}></td>
                        <td>
                            @if(!$item->paid)
                                <span class="badge badge-warning">No Pagado</span>
                            @else
                                <span class="badge badge-success">Pagado</span>
                            @endif
                        </td>
                        <td>
                            @if($update)
                                <div class="dropdown">
                                    <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fas fa-ellipsis-v"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                        @if(!$item->paid)
                                            <a class="dropdown-item btn-action" 
                                                data-title="¿Está seguro?"
                                                data-text="De marcar como pagada esta comision"
                                                data-icon="question"
                                                data-axios-method="post"
                                                data-route="{{ route('panel.comission.payment.pay', ['payment_id' => $item->id]) }}"
                                                data-action="location.reload()"
                                                href="javascript:;">
                                                Pagar
                                            </a>
                                            <a class="dropdown-item" href="javascript:;" onclick="update('{{$item->id}}')">
                                                Guardar Cambios
                                            </a>
                                        @else
                                            <a class="dropdown-item" onclick="showModal('#modal-form-evidence','{{$item->id}}')" href="javascript:;">Subir evidencia de pago</a>
                                            @if(!is_null($item->file))
                                                <a class="dropdown-item" href="{{ asset($item->file) }}" target="_blank">Ver evidencia de pago</a>
                                            @endif
                                            <a class="dropdown-item btn-action" 
                                                data-title="¿Está seguro?"
                                                data-text="De revertir el registro de pago de esta comision"
                                                data-icon="question"
                                                data-axios-method="post"
                                                data-route="{{ route('panel.comission.payment.unpay', ['payment_id' => $item->id]) }}"
                                                data-action="location.reload()"
                                                href="javascript:;">
                                                Revertir Pago
                                            </a>
                                        @endif
                                    </div>
                                </div>
                            @endif
                        </td>
                    </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
    </div>
    <div class="modal fade" id="modal-form-evidence" role="dialog" aria-labelledby="modal-form-evidence" aria-hidden="true" style="display: none;">
		<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-body p-0">
					<div class="card bg-secondary border-0 mb-0">
						<div class="card-header bg-transparent pb-5">
							<div class="text-muted text-center mt-3 mb-1">Subir evidencia de pago</div>
							<div class="card-body px-lg-12 py-lg-12">
                                <form id="form" role="form" action="{{ route('panel.comission.payment.file') }}" method="POST" class="needs-validation" novalidate enctype="multipart/form-data">
								    <div class="text-center">
                                        {{ csrf_field() }}
                                        <input type="hidden" id="comission_payment_id" name="comission_payment_id" value=""/>
                                        <div class="col-lg-12 mb-12 text-truncate">
                                            <label class="form-control-label" for="">
                                                <small>Seleccionar un archivo no mayor a 500kb, Formato .jpeg, .jpg</small>
                                            </label>
                                            <div class="input-group input-group-merge input-group-alternative">
                                                <input type="file"  name="file" id="file" class="form-control form-control-alternative" accept="image/jpg,image/jpeg">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 text-center">
                                        <button class="btn btn-default">Confirmar</button>
                                    </div>
                                </form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@push('js')
    <script>
        $(document).ready(function() {

            $('#dataTable').DataTable().order([[0, "asc"]]).draw();
            
            $(".precalculate").on("change",function(e){
                e.preventDefault();
                precalcular();
            });

            $("#guardarComision").on("click",function(){
                $("#form-comision").submit();
            });

            function downloadPaymentHistory(comission_id) {
				window.open("/documentos/pdf/comission/payments/history"+comission_id, '_blank');
			}

            function precalcular(){
                let money_format = Intl.NumberFormat('en-US');
                let percentage  = $("#percentage").val() == null?0:$("#percentage").val();
                let taxes       = $("#taxes").val() == null?0:$("#taxes").val();
                let total       = "{{$venta->quotation()->detail->precio_final}}".replace(/\$|,/g, '');
                let total_bruto = 0
                let total_impuestos = 0;
                let total_a_pagar = 0;
                
                total_bruto = parseFloat((total*percentage)/100).toFixed(2);
                total_impuestos = parseFloat((total_bruto*taxes)/100).toFixed(2);

                total_a_pagar = total_bruto - total_impuestos;

                $("#total_amount").val("$"+money_format.format(total_bruto));
                $("#retentions").val("$"+money_format.format(total_impuestos));
                $("#pay_amount").val("$"+money_format.format(total_a_pagar));
            };
        });

        function showModal(modal,id){
            document.getElementById('comission_payment_id').value = id;
            $(modal).modal("show");
        }

        function update(id){
            let token = "{{csrf_token()}}";
            let date = $("#payment_date"+id).val();
            let amount = $("#payment_amount"+id).val();
            let detalle = {token:token,detalle:{id:id,payment_date:date,payment_amount:amount}};

            $.ajax({
                url:"{{route('panel.comission.payment.update')}}",
                type:'POST',
                data:detalle,
                success: function(result){
                    console.log(result);
                    location.reload();
                }
            });
        }

    </script>
@endpush