<div class="card kanban_card" tabindex="0" data-item="{{$item->client()}}" >
    <div class="card-body">
        {{-- <div class="dropdown card-options">
            <a class="btn" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-ellipsis-v"></i>
            </a>
            <div class="dropdown-menu dropdown-menu-right">
                <a class="dropdown-item" href="#">Edit</a>
                <a class="dropdown-item text-danger" href="#">Archive List</a>
            </div>
        </div> --}}
        
            <a href="{{ route('panel.ventas.edit', ['venta' => $item->id]) }}">           
                <div class="card-title">
                    <h6>Lote - {{ $item->lote()->num_lote }}  </h6>
                    <h6 class="text-small ">{{ $item->client()->name.' '.$item->client()->surname }}</h6>
                </div>
            </a>
        

        <ul class="avatars">
            <li>
                <a href="#" data-toggle="tooltip" title="{{ $item->asesor()->name }}">
                    <img alt="{{ $item->asesor()->name }}" class="avatar" src="{{ asset($item->asesor()->avatar) }}">
                </a>
            </li>
            {{-- <li>
                <a href="#" data-toggle="tooltip" title="Ravi">
                <img alt="Ravi Singh" class="avatar" src="https://pipeline.mediumra.re/assets/img/avatar-male-3.jpg">
                </a>
            </li> --}}
        </ul>
        <div class="card-meta d-flex justify-content-between">
            <span class="text-small">{{ time_passed($item->created_at) }}</span>
        </div>
    </div>
</div>