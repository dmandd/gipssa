<div class="d-flex flex-row align-items-center" style="justify-content: space-evenly;">
    <a href="{{ route('ventas.pdf.corrida', ['venta' => $venta->id]) }}" target="_blank" class="btn btn-lg btn-icon rounded-circle btn-default active" type="button" data-toggle="tooltip" data-placement="top" title="Descargar corrida financiera">
        <span class="btn-inner--icon"><i class="ni ni-cloud-download-95"></i></span>
    </a>
    {{-- @if ($venta->status == 4)
    @endif --}}
</div>
<br><br>
<div class="pl-lg-4">
    @php
    $restante = count($venta->fechas_enganche()->get()) - $venta->recibos('abono-enganche')->count();
    @endphp
    @if($restante > 0)
        <h4>{{($venta->quotation()->detail->plazo->tipo == 'financiamiento')?'Abonos Enganche' : 'Abono Pago(s) de contado'}}
            @can(PermissionKey::Venta['permissions']['create_abono_enganche']['name'])
                @if ($venta->status == 3)
                    <button class="btn btn-icon btn-primary ml-2" data-toggle="modal" data-target="#addAbono" title="Agregar Abono" type="button"><span class="btn-inner--icon"><i class="ni ni-fat-add"></i></span></button>
                @endif
            @endcan
        </h4>
        <h3>Fecha siguiente pago: {{ count($venta->fechas_enganche()->get()) > 0 ? $venta->fechas_enganche()->get()[$venta->recibos('abono-enganche')->count()]->fecha() : "" }}</h3>
        <h4>Monto a abonar: {{ $venta->quotation()->detail->pago_mensual_enganche }}</h4>
        <h4>Pagos restantes: {{$restante}}</h4>
    @else
        <h4>Sin pagos restantes</h4>
    @endif
    
    <br>
    <small>Resúmen movimientos</small>
    <table class="table">
        <thead>
            <tr>
                <th>ID</th>
                <th>ABONO</th>
                <th>SALDO ANTERIOR</th>
                <th>MOVIMIENTO A SALDO</th>
                <th>SALDO</th>
            </tr>
        </thead>
        <tbody>
            @php
                $saldo = getAmmount($venta->quotation()->detail->monto_enganche);
                $abonos = 0;
            @endphp
            @if (($venta->recibos('abono-enganche')) && ($venta->recibos('abono-enganche')->count() > 0))
            @foreach ($venta->recibos('abono-enganche') as $item)
                    @if ($item->status == 'paid')
                        <tr>
                            <td>{{ $item->id }}</td>
                            <td>$ {{ $item->monto() }}</td>
                            <td>$ {{ number_format($saldo, 2) }}</td>
                            <td class="text-red">- $ {{ $item->monto() }}</td>
                            <td>$ {{ number_format(($saldo - $item->monto), 2) }}</td>
                        </tr>
                        @php
                            $abonos += $item->monto;
                            $saldo -= $item->monto;
                        @endphp
                    @endif
                @endforeach
            @endif
        </tbody>
        <tfoot>
            <tr>
                <td></td>
                <td><b>$ {{number_format($abonos, 2) }}</b></td>
                <td></td>
                <td></td>
                <td><b>$ {{ number_format($saldo, 2) }}</b></td>
            </tr>
        </tfoot>
    </table>
    <br><hr>
    <h4>Detalle movimientos</h4>
    @if (($venta->recibos('abono-enganche')) && ($venta->recibos('abono-enganche')->count() > 0))
        @foreach ($venta->recibos('abono-enganche') as $item)
            <hr>
            @if ($item->status !== 'paid')
            <form method="POST" action="{{ route('panel.ventas.recibos.update', ['venta' => $venta->id, 'recibo' => $item->id]) }}" class="needs-validation" novalidate enctype="multipart/form-data">
            @else
            <form method="GET" action="{{ route('panel.facturacion.store', ['venta' => $venta->id, 'recibo' => $item->id]) }}" class="needs-validation" novalidate enctype="multipart/form-data">
            @endif
            
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="recibo_id" value="{{ $item->id }}">
                <div class="row">
                    <div class="col-6">
                        <h4 class="text-muted"># {{ $item->id }}</h4>
                    </div>
                    <div class="col-6 text-right">
                        @if ($venta->status == 3)
                            <a class="btn btn-lg btn-icon rounded-circle btn-default text-white" href="{{ route('ventas.pdf.abono.enganche', ['recibo' => $item->id]) }}" target="_blank" data-toggle="tooltip" data-placement="top" title="Descargar recibo">
                                <span class="btn-inner--icon"><i class="ni ni-cloud-download-95"></i></span>
                            </a>
                            @if ($item->status !== 'paid')
                                @can(PermissionKey::Venta['permissions']['authorize_recibo_enganche']['name'])
                                    <a class="btn btn-lg btn-icon rounded-circle btn-success btn-action text-white"
                                        data-toggle="tooltip" 
                                        data-placement="top"
                                        data-title="¿Está seguro de autorizar?"
                                        data-text="Este registro se autorizará. La operación no puede revertirse"
                                        data-icon="question"
                                        data-axios-method="put"
                                        data-route="{{ route('panel.ventas.recibos.authorize', ['venta' => $venta->id,'recibo' => $item->id]) }}"
                                        data-action="location.reload()"
                                        title="Marcar como pagado">
                                        <span class="btn-inner--icon"><i class="ni ni-check-bold"></i></span>
                                    </a>
                                @endcan
                            @endif
                            @can(PermissionKey::Venta['permissions']['destroy_recibo_enganche']['name'])
                                <a class="btn btn-lg btn-icon rounded-circle btn-danger btn-action text-white" 
                                        type="button"
                                        data-toggle="tooltip"
                                        data-placement="top"
                                        data-title="¿Está seguro de eliminar?"
                                        data-text="Este registro se eliminará. La operación no puede revertirse"
                                        data-icon="question"
                                        data-axios-method="delete"
                                        data-route="{{ route('panel.ventas.recibos.destroy', ['venta' => $venta->id,'recibo' => $item->id]) }}"
                                        data-action="location.reload()"
                                        title="Eliminar">
                                    <span class="btn-inner--icon"><i class="ni ni-fat-remove"></i></span>
                                </a>
                            @endcan
                        @endif
                    </div>
                    <div class="col-6 mb-3 text-truncate">
                        <label for="" class="form-control-label">
                            <span class="text-muted">Monto pagado</span>
                        </label>
                        <input type="text" name="monto" class="form-control" disabled required value="$ {{ number_format($item->monto, 2) }}">
                    </div>
                    <div class="col-6 mb-3 text-truncate">
                        <label for="" class="form-control-label">
                            <span class="text-muted">Fecha de pago</span>
                        </label>
                        <input type="text" name="fecha_pago" class="form-control" disabled required value="{{ $item->fecha_pago }}" autocomplete="off">
                    </div>
                    <div class="col-4 mb-3 text-truncate">
                        <label for="" class="form-control-label">
                            <span class="text-muted">Concepto</span>
                        </label>
                        <select name="dia_pago" id="dia_pago" class="form-control" disabled required>
                            <option value="">Selecciona una opción</option>
                            @if($venta->quotation()->detail->plazo->tipo == 'financiamiento')
                                <option {{ ($item->slug == 'abono-enganche') ? 'selected' : '' }} value="abono-enganche">Abono Enganche</option>
                            @else
                                <option {{ ($item->slug == 'abono-enganche') ? 'selected' : '' }} value="abono-enganche">Abono pago de contado</option>
                            @endif
                            
                        </select>
                    </div>
                    <div class="col-4 mb-3 text-truncate">
                        <label for="" class="form-control-label">
                            <span class="text-muted">Forma de pago</span>
                        </label>
                        <select name="payment_way" id="payment_way" class="form-control" disabled required>
                            <option value="">Selecciona una opción</option>
                            <option {{ ($item->payment_way == '01') ? 'selected' : '' }} value="01">Efectivo</option>
                            <option {{ ($item->payment_way == '02') ? 'selected' : '' }} value="02">Cheque nominativo</option>
                            <option {{ ($item->payment_way == '03') ? 'selected' : '' }} value="03">Transferencia electrónica de fondos</option>
                            <option {{ ($item->payment_way == '04') ? 'selected' : '' }} value="04">Tarjeta de crédito</option>
                            <option {{ ($item->payment_way == '05') ? 'selected' : '' }} value="05">Monedero electrónico</option>
                            <option {{ ($item->payment_way == '06') ? 'selected' : '' }} value="06">Dinero electrónico</option>
                            <option {{ ($item->payment_way == '08') ? 'selected' : '' }} value="08">Vales de despensa</option>
                            <option {{ ($item->payment_way == '12') ? 'selected' : '' }} value="12">Dación en pago</option>
                            <option {{ ($item->payment_way == '13') ? 'selected' : '' }} value="13">Pago por subrogación</option>
                            <option {{ ($item->payment_way == '14') ? 'selected' : '' }} value="14">Pago por consignación</option>
                            <option {{ ($item->payment_way == '15') ? 'selected' : '' }} value="15">Condonación</option>
                            <option {{ ($item->payment_way == '17') ? 'selected' : '' }} value="17">Compensación</option>
                            <option {{ ($item->payment_way == '23') ? 'selected' : '' }} value="23">Novación</option>
                            <option {{ ($item->payment_way == '24') ? 'selected' : '' }} value="24">Confusión</option>
                            <option {{ ($item->payment_way == '25') ? 'selected' : '' }} value="25">Remisión de deuda</option>
                            <option {{ ($item->payment_way == '26') ? 'selected' : '' }} value="26">Prescripción o caducidad</option>
                            <option {{ ($item->payment_way == '27') ? 'selected' : '' }} value="27">A satisfacción del acreedor</option>
                            <option {{ ($item->payment_way == '28') ? 'selected' : '' }} value="28">Tarjeta de débito</option>
                            <option {{ ($item->payment_way == '29') ? 'selected' : '' }} value="29">Tarjeta de servicios</option>
                            <option {{ ($item->payment_way == '30') ? 'selected' : '' }} value="30">Aplicación de anticipos</option>
                            <option {{ ($item->payment_way == '99') ? 'selected' : '' }} value="99">Por definir</option>
                        </select>
                    </div>
                    <div class="col-4 mb-3 text-truncate">
                        <label for="" class="form-control-label">
                            <span class="text-muted">Método de pago</span>
                        </label>
                        <select name="payment_method" id="payment_method" class="form-control" disabled required>
                            <option value="">Selecciona una opción</option>
                            <option {{ ($item->payment_method == 'PUE') ? 'selected' : '' }} value="PUE">PUE-Pago en una sola exhibición</option>
                            <option {{ ($item->payment_method == 'PPD') ? 'selected' : '' }} value="PPD">PPD-Pago en parcialidades o diferido</option>
                        </select>
                    </div>
                    <div class="col mb-3 text-truncate">
                        <label for="" class="form-control-label">
                            <span class="text-muted">Cuenta bancaria</span>
                        </label><br>
                        @if ($item->banco())
                            <input type="text" class="form-control" disabled value="{{ $item->banco()->name }} - {{ $item->banco()->n_cuenta }}">
                        @else
                            <input type="text" class="form-control" disabled value="Error">
                        @endif
                    </div> 
                    <div class="col-12 mb-3 text-truncate">
                        <label for="" class="form-control-label">
                            <span class="text-muted">Referencia/Observaciones</span>
                        </label>
                        <textarea name="message" class="form-control" required rows="3" disabled value="">{{ $item->message }}</textarea>
                    </div>
                    <div class="col-6 offset-3 mb-3 text-truncate">
                        <label for="" class="form-control-label">
                            <span class="text-muted">Comprobante de pago</span>
                        </label><br>
                        @if ($item->status == 'created')
                            <input type="file" name="comprobante_pago" class="form-control" required>
                            <input type="hidden" name="status" value="paid">
                        @else
                            <a class="form-control" href="{{ asset($item->file) }}" target="_blank">Ver Comprobante</a>
                        @endif
                    </div>
                </div>

                @if($item->factura_id == null)
                    @if($item->status !== 'paid')
                        <div class="col-12 text-center">
                            <button class="btn btn-default" type="submit">Actualizar</button>
                        </div>
                    @else
                        @can(PermissionKey::Venta['permissions']['facturar_pagos']['name'])
                            <div class="col-12 text-center">
                                <button class="btn btn-default" type="submit">Facturar</button>
                            </div>
                        @endcan
                    @endif
                @else
                    @can(PermissionKey::Venta['permissions']['facturar_pagos']['name'])
                        <div class="col-12 text-center">
                            <a class="btn btn-icon btn-default text-white" href="{{ route('panel.facturacion.delete', ['recibo_id' => $item->id]) }}" data-toggle="tooltip" data-placement="top" title="Cancelar factura">
                                <span class="btn-inner--icon"> Cancelar factura</span>
                            </a>
                            <button class="btn btn-default"
                                data-title="¿Está seguro de autorizar?"
                                data-text="Este registro se autorizará. La operación no puede revertirse"
                                data-icon="question"
                                data-axios-method="get"
                                data-route="{{ route('panel.ventas.recibos.store', ['venta' => $venta->id,'recibo_id' => $item->id]) }}"
                                data-action="location.reload()"
                                disabled
                                >
                                Enviar factura
                            </button>
                            <a class="btn btn-icon btn-default text-white" href="{{ route('panel.facturacion.download', ['id' => $item->factura_id]) }}" data-toggle="tooltip" data-placement="top" title="Descargar factura">
                                <span class="btn-inner--icon"><i class="ni ni-cloud-download-95"></i>Descargar factura</span>
                            </a>
                        </div>
                    @endcan
                @endif
            </form>
        @endforeach
    @endif
    <hr>
    <br>
    <hr>
    <div class="row">  
        @if ($venta->status == 3)
            @can(PermissionKey::Venta['permissions']['update_status']['name'])
                <div class="col-12 text-center">
                    @if ($saldo > 0)
                        <button class="btn btn-default"
                                data-toggle="tooltip" 
                                data-placement="top" 
                        title="Aún queda un saldo pendiente">Autorizar etapa</button>
                    @else
                        <button class="btn btn-default btn-action"
                                data-toggle="tooltip" 
                                data-placement="top" 
                                data-title="¿Está seguro de autorizar esta etapa?" 
                                data-text="La venta se moverá a pago de contrato" 
                                data-icon="question" 
                                data-axios-method="put" 
                                data-axios-body='{"status": 4}' 
                                data-route="{{ route('panel.ventas.update', ['venta' => $venta->id]) }}" 
                                data-action="location.reload()"         
                        title="Aprobar">Autorizar etapa</button>
                    @endif
                </div>
            @endcan
        @endif  
    </div>
</div>  