<div class="pl-lg-4">
    <h4>Definir Costo Contrato</h4>
    <form action="{{ route('panel.ventas.update', ['venta' => $venta->id]) }}" method="POST">
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="PUT">
        <div class="form-group">
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text">$</span>
              </div>
              <input type="text" class="form-control" name="monto_contrato" value="{{ $venta->monto_contrato }}">
              <div class="input-group-append">
                <button class="btn btn-primary" id="button-addon2">Guardar</button>
              </div>
            </div>
            <small>Formato décimal; Ejemplo: 4500.00</small>
        </div>
    </form>
    <br>
    <h4>Abonos Contrato @if($venta->status == 5)<button class="btn btn-icon btn-primary ml-2" data-toggle="modal" data-target="#addAbono" title="Agregar Abono" type="button"><span class="btn-inner--icon"><i class="ni ni-fat-add"></i></span></button>@endif</h4>
    <br>
    <small>Resúmen movimientos contrato <a href="#" data-toggle="tooltip" data-placement="top" title="El resumen sólo muestra los abonos aprovados"><i class="fas fa-question"></i></a></small>
    <table class="table">
        <thead>
            <tr>
                <th>ID</th>
                <th>ABONO</th>
                <th>SALDO ANTERIOR</th>
                <th>MOVIMIENTO A SALDO</th>
                <th>SALDO</th>
            </tr>
        </thead>
        <tbody>
            @php
                $saldo = $venta->monto_contrato;
                $abonos = 0;
            @endphp
            @if (($venta->recibos('abono-contrato')) && ($venta->recibos('abono-contrato')->count() > 0))
                @foreach ($venta->recibos('abono-contrato') as $item)
                    @if ($item->status == 'paid')
                        <tr>
                            <td>{{ $item->id }}</td>
                            <td>$ {{ $item->monto() }}</td>
                            <td>$ {{ number_format($saldo, 2) }}</td>
                            <td class="text-red">- $ {{ $item->monto() }}</td>
                            <td>$ {{ number_format(($saldo - $item->monto), 2) }}</td>
                        </tr>
                        @php
                            $abonos += $item->monto;
                            $saldo -= $item->monto;
                        @endphp
                    @endif
                @endforeach
            @endif
        </tbody>
        <tfoot>
            <tr>
                <td></td>
                <td><b>$ {{number_format($abonos, 2) }}</b></td>
                <td></td>
                <td></td>
                <td><b>$ {{ number_format($saldo, 2) }}</b></td>
            </tr>
        </tfoot>
    </table>
    <br><hr>
    <h4>Detalle movimientos</h4>
    @if (($venta->recibos('abono-contrato')) && ($venta->recibos('abono-contrato')->count() > 0))
        @foreach ($venta->recibos('abono-contrato') as $item)
            <hr>
            <form method="POST" action="{{ route('panel.ventas.recibos.update', ['venta' => $venta->id, 'recibo' => $item->id]) }}" class="needs-validation" novalidate enctype="multipart/form-data">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="PUT">
                <div class="row">
                    <div class="col-6">
                        <h4 class="text-muted"># {{ $item->id }}</h4>
                    </div>
                    <div class="col-6 text-right">
                        @if ($venta->status == 5)
                            <a class="btn btn-lg btn-icon rounded-circle btn-default text-white" href="{{ route('ventas.pdf.abono.enganche', ['recibo' => $item->id]) }}" target="_blank" data-toggle="tooltip" data-placement="top" title="Descargar recibo">
                                <span class="btn-inner--icon"><i class="ni ni-cloud-download-95"></i></span>
                            </a>
                            @if ($item->status !== 'paid')
                                <a class="btn btn-lg btn-icon rounded-circle btn-success btn-action text-white"
                                    data-toggle="tooltip" 
                                    data-placement="top"
                                    data-title="¿Está seguro de autorizar?"
                                    data-text="Este registro se autorizará. La operación no puede revertirse"
                                    data-icon="question"
                                    data-axios-method="put"
                                    data-route="{{ route('panel.ventas.recibos.authorize', ['venta' => $venta->id,'recibo' => $item->id]) }}"
                                    data-action="location.reload()"
                                    title="Marcar como pagado">
                                    <span class="btn-inner--icon"><i class="ni ni-check-bold"></i></span>
                                </a>
                            @endif
                            <a class="btn btn-lg btn-icon rounded-circle btn-danger btn-action text-white" 
                                    type="button"
                                    data-toggle="tooltip"
                                    data-placement="top"
                                    data-title="¿Está seguro de eliminar?"
                                    data-text="Este registro se eliminará. La operación no puede revertirse"
                                    data-icon="question"
                                    data-axios-method="delete"
                                    data-route="{{ route('panel.ventas.recibos.destroy', ['venta' => $venta->id,'recibo' => $item->id]) }}"
                                    data-action="location.reload()"
                                    title="Eliminar">
                                <span class="btn-inner--icon"><i class="ni ni-fat-remove"></i></span>
                            </a>
                        @endif
                    </div>
                    <div class="col-6 mb-5 text-truncate">
                        <label for="" class="form-control-label">
                            <span class="text-muted">Fecha Pago</span>
                        </label>
                        <input type="text" name="fecha_pago" class="form-control" disabled required value="{{ $item->fecha_pago }}" autocomplete="off">
                    </div>
                    <div class="col-6 mb-5 text-truncate">
                        <label for="" class="form-control-label">
                            <span class="text-muted">Monto Pago</span>
                        </label>
                        <input type="text" name="monto" class="form-control" disabled required value="$ {{ number_format($item->monto, 2) }}">
                    </div>
                    <div class="col mb-5 text-truncate">
                        <label for="" class="form-control-label">
                            <span class="text-muted">Concepto</span>
                        </label>
                        <select name="dia_pago" id="dia_pago" class="form-control" disabled required>
                            <option value="">Selecciona una opción</option>
                            <option {{ ($item->slug == 'abono-contrato') ? 'selected' : '' }} value="abono-contrato">Abono Enganche</option>
                        </select>
                    </div>
                    <div class="col mb-5 text-truncate">
                        <label for="" class="form-control-label">
                            <span class="text-muted">Comprobante Pago</span>
                        </label><br>
                        @if ($item->status == 'created')
                            <input type="file" name="comprobante_pago" class="form-control" required>
                            <input type="hidden" name="status" value="paid">
                        @else
                            @if ($item->file)
                                <a class="form-control" href="{{ asset($item->file) }}" target="_blank">Ver Comprobante</a>
                            @else
                                <input type="file" name="comprobante_pago" class="form-control" required>
                            @endif
                        @endif
                    </div>
                    <div class="col mb-5 text-truncate">
                        <label for="" class="form-control-label">
                            <span class="text-muted">Cuenta Banco</span>
                        </label><br>
                        @if ($item->banco())
                            <input type="text" class="form-control" disabled value="{{ $item->banco()->name }} - {{ $item->banco()->n_cuenta }}">
                        @else
                            <input type="text" class="form-control" disabled value="Error">
                        @endif
                    </div>
                </div>
                @if ($item->status !== 'paid')
                    <div class="col-12 text-center">
                        <button class="btn btn-default" type="submit">Actualizar</button>
                    </div>
                @endif
            </form>
        @endforeach
    @else
        <small class="text-muted">Sin registros</small>
    @endif
    <hr>
    <br>
    <div class="row">  
        @if ($venta->status == 5)
            @can(PermissionKey::Venta['permissions']['update_status']['name'])
                <div class="col-12 text-center">
                    @if ($saldo > 0)
                        <button class="btn btn-default"
                                data-toggle="tooltip" 
                                data-placement="top" 
                        title="Enganche sin saldar">Autorizar etapa</button>
                    @else
                        <button class="btn btn-default btn-action"
                                data-toggle="tooltip" 
                                data-placement="top" 
                                data-title="¿Está seguro de autorizar esta etapa?" 
                                data-text="La venta se moverá a pago de contrato" 
                                data-icon="question" 
                                data-axios-method="put" 
                                data-axios-body='{"status": 5}' 
                                data-route="{{ route('panel.ventas.update', ['venta' => $venta->id]) }}" 
                                data-action="location.reload()"         
                        title="Aprobar">Autorizar etapa</button>
                    @endif
                </div>
            @endcan
        @endif  
    </div>
</div>