@php
    $quotation = $venta->quotation();
@endphp
<div class="d-flex flex-row align-items-center" style="justify-content: space-evenly;">
    @if ($venta->status == 0)
        @can(PermissionKey::Venta['permissions']['authorize_quotation']['name'])
            <button class="btn btn-lg btn-icon btn-default btn-action"
                    type="button" 
                    data-toggle="tooltip" 
                    data-placement="top" 
                    data-title="¿Está seguro de confirmar?" 
                    data-text="La venta se moverá a documentación" 
                    data-icon="question" 
                    data-axios-method="put" 
                    data-axios-body='{"status": 1}' 
                    data-route="{{ route('panel.ventas.update', ['venta' => $venta->id]) }}" 
                    data-action="location.reload()" 
                    title="Confirmar">
                Confirmar Etapa
            </button>
            {{-- <a href="#" data-toggle="modal" data-target="#revokeStep" class="btn btn-lg btn-icon rounded-circle btn-danger" type="button" data-toggle="tooltip" data-placement="top" title="Rechazar">
                <span class="btn-inner--icon"><i class="ni ni-fat-remove"></i></span>
            </a> --}}
        @endcan
    @endif
    {{--<a href="{{ route('ventas.pdf.cotizacion', ['venta' => $venta->id]) }}" target="_blank" class="btn btn-lg btn-icon rounded-circle btn-default text-white" type="button" data-toggle="tooltip" data-placement="top" title="Descargar">
        <span class="btn-inner--icon"><i class="ni ni-cloud-download-95"></i></span>
    </a>
    <a href="#" data-toggle="modal" data-target="#sendMail" class="btn btn-lg btn-icon rounded-circle btn-default" data-toggle="tooltip" data-placement="top" title="Enviar por correo">
        <span class="btn-inner--icon"><i class="ni ni-curved-next"></i></span>
    </a>--}}
    {{-- @if ($venta->status == 0)
        <button class="btn btn-lg btn-icon rounded-circle btn-default btn-action" type="button" 
                data-toggle="tooltip" 
                data-placement="top" 
                title="Solicitar Autorización"
                data-title="¿Está seguro de enviar a revisión?" 
                data-text="La venta será revisada" 
                data-icon="question" 
                data-axios-method="post" 
                data-axios-body='{"status": 0}' 
                data-route="{{ route('panel.ventas.review', ['venta' => $venta->id]) }}" 
                data-action="location.reload()" >
            <span class="btn-inner--icon"><i class="ni ni-bulb-61"></i></span>
        </button>
    @endif --}}
    {{-- <button class="btn btn-lg btn-icon rounded-circle btn-default" type="button" data-toggle="tooltip" data-placement="top" title="Solicitar Documentación">
        <span class="btn-inner--icon"><i class="ni ni-archive-2"></i></span>
    </button> --}}
</div>
<br><div class="px-lg-4">
<div class="accordion" id="accordionExample">
    <div class="card">
        <div class="card-header" id="heading" data-toggle="collapse" data-target="#collapse" aria-expanded="true" aria-controls="collapse">
            <div class="d-flex">
                <h4 class="mb-0">Información Cliente</h4>
                <a href="{{ route('panel.clients.edit', ['cliente' => $venta->client()->id]) }}" data-toggle="tooltip" data-placement="top" title="Modificar Cliente" class="ml-3"><i class="fas fa-pencil-alt"></i></a>
            </div>
        </div>
        <div id="collapse" class="collapse show" aria-labelledby="" data-parent="#heading" style="">
            <div class="card-body">
                <input type="hidden" name="_method" value="PUT">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="form-control-label" for="name">* Nombre</label>
                            <input type="text" name="name" id="name" class="form-control" disabled autocomplete="off" value="{{ (old('name')) ? old('name') : $venta->client()->name }}">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="form-control-label" for="surname">* Apellido</label>
                            <input type="text" name="surname" id="surname" class="form-control" disabled autocomplete="off" value="{{ (old('surname')) ? old('surname') : $venta->client()->surname }}">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="form-control-label" for="phone">* Teléfono</label>
                            <input type="text" name="phone" id="phone" class="form-control" disabled autocomplete="off" value="{{ (old('phone')) ? old('phone') : $venta->client()->phone }}">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label class="form-control-label" for="email">* Email</label>
                            <input type="email" name="email" id="email" class="form-control" disabled autocomplete="off" value="{{ (old('email')) ? old('email') : $venta->client()->email }}">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

    <h4>Información del apartado</h4>
    <div class="row">
        <div class="form-group col-lg-6">
            <label class="form-control-label" for="lote">
                <span class="text-muted">Lote</span>
            </label>
            <div class="input-group input-group-merge input-group-alternative">
                <input id="lote" class="form-control" placeholder="" type="text" name="lote" value="LOTE - {{ $quotation->lote()->num_lote }}" disabled>
            </div>
        </div>
        <div class="form-group mb-3 col-lg-6">
            <label class="form-control-label" for="plazo">
                <span class="text-muted">Plazo</span>
            </label>
            <div class="input-group input-group-merge input-group-alternative">
                <select name="plazo" id="plazo" class="form-control" disabled>
                    @if ($venta->development()->financiamientos()->count() > 0)
                        @foreach ($venta->development()->financiamientos() as $item)
                            <option value="{{ $item->id }}" {{ ($venta->quotation()->detail->plazo->id == $item->id) ? 'selected' : '' }}>{{ $item->nombre }}</option>
                        @endforeach
                    @endif
                </select>
            </div>
        </div>  
        <div class="form-group col-lg-12">
            <label class="form-control-label" for="precio_final">
                <span class="text-muted">Precio Final</span>
            </label>
            <div class="input-group input-group-merge input-group-alternative">
                <input id="precio_final" class="form-control" placeholder="" type="text" name="precio_final" value="{{ $quotation->detail->precio_final }}" disabled>
            </div>
        </div>
        <div class="form-group col-lg-6">
            <label class="form-control-label" for="porc_enganche">
                <span class="text-muted">{{$venta->quotation()->detail->plazo->tipo == 'financiamiento'? 'Porcentaje del enganche':'Porcentaje del pago' }} {{$venta->quotation()->detail->plazo->tipo == 'contado_diferido'? 'a diferir':'' }}</span>
            </label>
            <div class="input-group input-group-merge input-group-alternative">
                <input id="porc_enganche" class="form-control" placeholder="Porcentaje enganche" type="text" name="porc_enganche" value="{{ $quotation->detail->porc_enganche }}" disabled>
            </div>
        </div>
        <div class="form-group col-lg-6" id="enganche-group">
            <label class="form-control-label" for="_enganche">
                <span class="text-muted">{{$venta->quotation()->detail->plazo->tipo == 'financiamiento'? 'Enganche':($venta->quotation()->detail->plazo->tipo == 'contado_diferido'? 'Monto a diferir':'Un pago de')}}</span>
            </label>
            <div class="input-group input-group-merge input-group-alternative">
                <input id="_enganche" class="form-control" placeholder="Enganche" type="text" name="enganche" value="{{ $quotation->detail->monto_enganche }}" disabled>
            </div>
        </div>
        @if($venta->quotation()->detail->plazo->tipo != 'contado')
            <div class="form-group col-lg-6">
                <label class="form-control-label" for="enganche_num_pagos">
                    <span class="text-muted">{{$venta->quotation()->detail->plazo->tipo == 'financiamiento'? 'No. Pagos del enganche':'No. Pagos' }} {{$venta->quotation()->detail->plazo->tipo == 'contado_diferido'? 'diferidos':'' }}</span>
                </label>
                <div class="input-group input-group-merge input-group-alternative">
                    <input id="enganche_num_pagos" class="form-control" placeholder="Porcentaje enganche" type="text" name="enganche_num_pagos" value="{{ $quotation->detail->enganche_num_pagos }}" disabled>
                </div>
            </div>
            <div class="form-group col-lg-6" id="enganche-group">
                <label class="form-control-label" for="pago_mensual_enganche">
                    <span class="text-muted">Pagos Mensuales Diferidos de</span>
                </label>
                <div class="input-group input-group-merge input-group-alternative">
                    <input id="pago_mensual_enganche" class="form-control" placeholder="Enganche" type="text" name="pago_mensual_enganche" value="{{ $quotation->detail->pago_mensual_enganche }}" disabled>
                </div>
            </div>
        @endif
        @if($venta->quotation()->detail->plazo->tipo == 'financiamiento')
            <div class="form-group col-lg-12" id="enganche-group">
                <label class="form-control-label" for="financiamiento">
                    <span class="text-muted">Monto a financiar</span>
                </label>
                <div class="input-group input-group-merge input-group-alternative">
                    <input id="financiamiento" class="form-control" placeholder="Enganche" type="text" name="financiamiento" value="${{ number_format($quotation->detail->financiamiento, 2) }}" disabled>
                </div>
            </div>
        @endif
        
        
        
        @if($venta->quotation()->detail->plazo->tipo == 'financiamiento')
            <div class="form-group col-lg-4">
                <label class="form-control-label" for="mensualidad">
                    <span class="text-muted">No. Pagos Mensuales</span>
                </label>
                <div class="input-group input-group-merge input-group-alternative">
                    <input id="mensualidad" class="form-control" placeholder="" type="text" name="mensualidad" value="{{ $quotation->detail->mensualidad }}" disabled>
                </div>
            </div>
            <div class="form-group col-lg-4">
                <label class="form-control-label" for="pago_mensual">
                    <span class="text-muted">Pago mensual</span>
                </label>
                <div class="input-group input-group-merge input-group-alternative">
                    <input id="pago_mensual" class="form-control" placeholder="" type="text" name="pago_mensual" value="{{ $quotation->detail->pago_mensual }}" disabled>
                </div>
            </div>
            <div class="form-group col-lg-4">
                <label class="form-control-label" for="ultima_mensualidad">
                    <span class="text-muted">Última mensualidad</span>
                </label>
                <div class="input-group input-group-merge input-group-alternative">
                    <input id="ultima_mensualidad" class="form-control" placeholder="" type="text" name="ultima_mensualidad" value="{{ isset($quotation->detail->ultima_mensualidad)?$quotation->detail->ultima_mensualidad:'$0.00' }}" disabled>
                </div>
            </div>
        @else
            <div class="form-group col-lg-12">
                <label class="form-control-label" for="saldo">
                    <span class="text-muted">Saldo</span>
                </label>
                <div class="input-group input-group-merge input-group-alternative">
                    <input id="saldo" class="form-control" placeholder="Saldo" type="text" name="saldo" value="{{ $quotation->detail->saldo }}" disabled>
                </div>
            </div>
        @endif
    </div>
</div>

<div class="modal fade" id="sendMail" tabindex="-1" role="dialog" aria-labelledby="modal-form" aria-hidden="true">
    <div class="modal-dialog modal- modal-dialog-centered modal-sm" role="document">
        <div class="modal-content">   
            <div class="modal-body p-0">                
                <div class="card bg-secondary border-0 mb-0">
                    <div class="card-body px-lg-5 py-lg-5">
                        <div class="text-center text-muted mb-4">
                            <small>Enviar Por Correo</small>
                        </div>
                        <form method="POST" action="{{ route('mail.send.quotation', ['id' => $venta->id]) }}" class="needs-validation" novalidate>
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label class="form-control-label" for="to">* Destino</label>
                                        <input type="text" name="to" id="to" class="form-control" required autocomplete="off" value="{{ $venta->client()->email }}">
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">
                                        <label class="form-control-label" for="subject">* Asunto</label>
                                        <input type="text" name="subject" id="subject" class="form-control" required autocomplete="off" value="{{ old('subject') }}">
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label class="form-control-label">Descripción</label>
                                        <textarea name="description" class="form-control" id="" cols="30" rows="10"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="text-center">
                                <button class="btn btn-primary my-4">Confirmar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>