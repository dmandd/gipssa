<div class="d-flex flex-row align-items-center" style="justify-content: space-evenly;">
    @if ($venta->status == 2)
        <button class="btn btn-lg btn-icon rounded-circle btn-default btn-action" type="button" 
                data-toggle="tooltip" 
                data-placement="top" 
                title="Solicitar Autorización"
                data-title="¿Está seguro de enviar a revisión?" 
                data-text="La venta será revisada" 
                data-icon="question" 
                data-axios-method="post" 
                data-axios-body='{"status": 1}' 
                data-route="{{ route('panel.ventas.review', ['venta' => $venta->id]) }}" 
                data-action="location.reload()" >
            <span class="btn-inner--icon"><i class="ni ni-bulb-61"></i></span>
        </button>
       
        {{-- @if (($venta->development()->clientDocuments()->count() == $venta->documentation(null, ['status' => 'authorized'])->count()))
            <a href="{{ route('ventas.pdf.condicion', ['venta' => $venta->id]) }}" target="_blank" class="btn btn-lg btn-icon rounded-circle btn-default active" type="button" data-toggle="tooltip" data-placement="top" title="Descargar condición de compra">
                <span class="btn-inner--icon"><i class="ni ni-cloud-download-95"></i></span>
            </a>
        @endif --}}
    @endif
    @if (($venta->development()->clientDocuments()->count() == $venta->documentation(null, ['status' => 'authorized'])->count()))
        <a href="{{ route('ventas.word.contrato', ['venta' => $venta->id]) }}" target="_blank" class="btn btn-lg btn-icon rounded btn-default active" type="button" data-toggle="tooltip" data-placement="top" title="Descargar contrato">
            <span class="btn-inner--icon"><i class="ni ni-cloud-download-95"></i> Descargar contrato</span>
        </a>
    @endif
</div>
<br><br>
<form action="{{ route('panel.ventas.documentation.store', ['venta' => $venta->id]) }}" method="POST" class="needs-validation" novalidate enctype="multipart/form-data">
    {{ csrf_field() }}
    <div class="pl-lg-4">
        <div class="row">
            @if (($venta->development()->clientDocuments()) && ($venta->development()->clientDocuments()->count() > 0))
                @foreach ($venta->development()->clientDocuments() as $document)
                    <div class="col-lg-4 mb-6 text-truncate">
                        <label class="form-control-label" for="{{ $document->slug }}">
                            <span class="text-muted">*{{ $document->name }} - <b>{{ (isset($venta->documentation($document->id)->status)) ? $venta->documentation($document->id)->status() : 'Sin registro' }}</b></span><br>
                            @if (isset($venta->documentation($document->id)->file))
                                <small><a href="{{ asset($venta->documentation($document->id)->file) }}" target="_blank">
                                    <img style="width:25%;margin-top:3%;margin-right:3%;" src="{{ asset($venta->documentation($document->id)->file) }}" alt="{{ $document->name }}">                            
                               </a> </small><br>
                               
                               
                                @if ( Str::endsWith(Str::upper($venta->documentation($document->id)->file),'PDF'))
                                <div style="color:#FF0000"> <?php echo 'PDF no podra ser insertado en plantillas'; ?></div>
                                    

                                @endif
                            @endif
                            <small>Seleccionar un archivo no mayor a 500kb, Formato .jpeg, .jpg</small>
                        </label>
                        <div class="input-group input-group-merge input-group-alternative">
                            <input type="file" {{ ((isset($venta->documentation($document->id)->status)) && ($venta->documentation($document->id)->status == 'authorized')) ? 'disabled' : '' }} name="document[{{ $document->id }}]" id="{{ $document->slug }}" class="form-control form-control-alternative" accept="image/jpg,image/jpeg">
                        </div>
                        <br>
                        @can(PermissionKey::Venta['permissions']['authorize_client_docs']['name'])
                            @if ($venta->documentation($document->id))
                                <div class="d-flex flex-row align-items-center" style="justify-content: space-evenly;">
                                    <button class="btn btn-lg btn-icon rounded-circle btn-success btn-action"
                                            type="button" 
                                            data-toggle="tooltip" 
                                            data-placement="top" 
                                            data-title="¿Está seguro de confirmar?" 
                                            data-text="Se aprobará este documento" 
                                            data-icon="question" 
                                            data-axios-method="put" 
                                            data-axios-body='{"status": "authorized"}' 
                                            data-route="{{ route('panel.ventas.documentation.update', ['venta' => $venta->id, 'documento' => $venta->documentation($document->id)]) }}" 
                                            data-action="location.reload()" 
                                    title="Autorizar">
                                        <span class="btn-inner--icon"><i class="ni ni-check-bold"></i></span>
                                    </button>
                                    <a href="#" data-toggle="modal" data-target="#revokeStep" data-route="{{ route('panel.ventas.documentation.update', ['venta' => $venta->id, 'documento' => $venta->documentation($document->id)->id]) }}" class="btn btn-lg btn-icon rounded-circle btn-danger" type="button" data-toggle="tooltip" data-placement="top" title="Rechazar">
                                        <span class="btn-inner--icon"><i class="ni ni-fat-remove"></i></span>
                                    </a>
                                    {{-- <button class="btn btn-lg btn-icon rounded-circle btn-danger btn-action" 
                                            type="button"
                                            data-toggle="tooltip"
                                            data-placement="top"
                                            data-title="¿Está seguro de rechazar?"
                                            data-text="Se rechazará este documento"
                                            data-icon="question"
                                            data-axios-method="put"
                                            data-axios-body='{"status": "rejected"}'
                                            data-route="{{ route('panel.ventas.documentation.update', ['venta' => $venta->id, 'documento' => $venta->documentation($document->id)->id]) }}"
                                            data-action="location.reload()"
                                            title="Rechazar">
                                        <span class="btn-inner--icon"><i class="ni ni-fat-remove"></i></span>
                                    </button> --}}
                                </div>
                            @endif
                        @endcan
                    </div>
                @endforeach
                <div class="col-4">
                    <label class="form-control-label" for="condicion-firmado">
                        <span class="text-muted">* Condición de compra FIRMADO</span><br>
                        @if (isset($venta->recibos('condicion-de-compra')[0]->file))
                            <small><a href="{{ asset($venta->recibos('condicion-de-compra')[0]->file) }}" target="_blank"><b>Ver</b></a> documento subido</small><br>
                        @endif
                        <small>Seleccionar un archivo no mayor a 500kb</small>
                    </label>
                    <div class="input-group input-group-merge input-group-alternative">
                        <input type="file" name="file" id="condicion-firmado" class="form-control form-control-alternative">
                    </div>
                </div>
                @if (($venta->development()->clientDocuments()->count() == $venta->documentation(null, ['status' => 'authorized'])->count()))
                    @if ($venta->status == 2)
                        @can(PermissionKey::Venta['permissions']['update_status']['name'])
                            <div class="col-lg-12 text-center">
                                <button class="btn btn-default btn-action"
                                        data-toggle="tooltip" 
                                        data-placement="top" 
                                        data-title="¿Está seguro de autorizar esta etapa?" 
                                        data-text="La venta se moverá a Pago(s) de Contado y Enganche" 
                                        data-icon="question" 
                                        data-axios-method="put" 
                                        data-axios-body='{"status": 3}' 
                                        data-route="{{ route('panel.ventas.update', ['venta' => $venta->id]) }}" 
                                        data-action="location.reload()"         
                                title="Aprobar">Autorizar etapa</button>
                            </div>
                        @endcan
                    @endif
                @else
                    <div class="col-lg-12 text-center">
                        <button class="btn btn-default">Confirmar</button>
                    </div>
                @endif
            @else
                <h4>Aún no se agregan documentos a verificar para este desarrollo. </h4><br>
                <a href="{{ route('panel.documentation.create', ['desarrollo' => $venta->development_id]) }}"> Agregar</a>
            @endif
        </div>
    </div>
</form>
<div class="modal fade" id="revokeStep" tabindex="-1" role="dialog" aria-labelledby="modal-form" aria-hidden="true">
    <div class="modal-dialog modal- modal-dialog-centered modal-sm" role="document">
        <div class="modal-content">   
            <div class="modal-body p-0">                
                <div class="card bg-secondary border-0 mb-0">
                    <div class="card-body px-lg-5 py-lg-5">
                        <div class="text-center text-muted mb-4">
                            <small>Motivo por el que se rechaza la operación</small>
                        </div>
                        <form method="POST" id="form-revoke-document" action="" class="needs-validation" novalidate>
                            {{ csrf_field() }}
                            <input type="hidden" name="_method" value="PUT">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <input type="hidden" name="status" value="rejected">
                                        <label class="form-control-label">*Motivo</label>
                                        <textarea name="motivo" class="form-control" required id="" cols="30" rows="10"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="text-center">
                                <button class="btn btn-primary my-4">Confirmar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@push('js')
    <script>
        $('#revokeStep').on('shown.bs.modal', function (e) {
            document.getElementById('form-revoke-document').setAttribute('action', e.relatedTarget.dataset.route);
        });
    </script>
@endpush