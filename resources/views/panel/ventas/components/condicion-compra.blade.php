@if (($venta->recibos('condicion-de-compra')) && ($venta->recibos('condicion-de-compra')->count() > 0))
    <form action="{{ route('panel.ventas.recibos.update', ['venta' => $venta->id, 'recibo' => $venta->recibos('condicion-de-compra')[0]->id]) }}" method="POST" enctype="multipart/form-data">    
        <input type="hidden" name="_method" value="PUT">
@else
    <form action="{{ route('panel.ventas.recibos.store', ['venta' => $venta->id]) }}" method="POST" enctype="multipart/form-data">
@endif
    {{ csrf_field() }}
    <input type="hidden" name="venta_id" value="{{ $venta->id }}">
    <div class="pl-lg-4">
        <div class="d-flex flex-row align-items-center mb-4" style="justify-content: space-evenly;">
            {{-- TODO:
            Faltaría agregar el permiso para autorizar --}}
            @if (($venta->recibos('condicion-de-compra')) && ($venta->recibos('condicion-de-compra')->count() > 0))
                @if ($venta->status == 3)
                    <button class="btn btn-lg btn-icon rounded-circle btn-success btn-action"
                            type="button" 
                            data-toggle="tooltip" 
                            data-placement="top" 
                            data-title="¿Está seguro de confirmar?" 
                            data-text="La venta se moverá a Enganche" 
                            data-icon="question" 
                            data-axios-method="put" 
                            data-axios-body='{"status": 4}' 
                            data-route="{{ route('panel.ventas.update', ['venta' => $venta->id]) }}" 
                            data-action="location.reload()" 
                    title="Autorizar">
                        <span class="btn-inner--icon"><i class="ni ni-check-bold"></i></span>
                    </button>
                    <button class="btn btn-lg btn-icon rounded-circle btn-danger" type="button" data-toggle="tooltip" data-placement="top" title="Rechazar">
                        <span class="btn-inner--icon"><i class="ni ni-fat-remove"></i></span>
                    </button>
                @endif
            @endif
            <a href="{{ route('ventas.pdf.condicion', ['venta' => $venta->id]) }}" target="_blank" class="btn btn-lg btn-icon rounded-circle btn-default active" type="button" data-toggle="tooltip" data-placement="top" title="Descargar condición de compra">
                <span class="btn-inner--icon"><i class="ni ni-cloud-download-95"></i></span>
            </a>
        </div>
        <div class="row justify-content-center">
            <div class="col-6 mb-5">
                <label class="form-control-label" for="condicion-firmado">
                    <span class="text-muted">* Condición de compra FIRMADO</span><br>
                    @if (isset($venta->recibos('condicion-de-compra')[0]->file))
                        <small><a href="{{ asset($venta->recibos('condicion-de-compra')[0]->file) }}" target="_blank"><b>Ver</b></a> documento subido</small><br>
                    @endif
                    <small>Seleccionar un archivo no mayor a 500kb</small>
                </label>
                <div class="input-group input-group-merge input-group-alternative">
                    <input type="file" name="file" id="condicion-firmado" class="form-control form-control-alternative">
                </div>
            </div>
            <div class="col-lg-12 text-center">
                <button class="btn btn-default">Confirmar</button>
            </div>
        </div>
    </div>
</form>