@extends('layouts.panel.app')
@section('content')
    <!-- Header -->
    @include('include.panel.header')
    <!-- Page content -->
    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col">
				<div class="card">
				<!-- Card header -->
					<div class="card-header border-0">
                        {{-- <h3 class="mb-0">Nuevo usuario</h3> --}}
                        <h6 class="heading-small text-muted mb-4">Información</h6>
					</div>
                    <!-- Light table -->
                    <div class="card-body">
                        <form action="{{ route('panel.grantors.update', ['desarrollo' => request()->desarrollo, 'grantor' => request()->grantor]) }}" method="POST" class="needs-validation" novalidate>
                            <input type="hidden" name="_method" value="PUT">
                            {{ csrf_field() }}
                            <div class="pl-lg-4">
                                <div class="row">
                                    <div class="col-lg-2">
                                        <div class="form-group">
                                            <label class="form-control-label" for="title">* Titulo</label>
                                            <select class="form-control" name="title" id="title">
                                                <option>Seleccione una opción</option>
                                                <option value="el señor" {{$grantor->title == 'el señor'? 'selected': ''}}>Señor</option>
                                                <option value="la señora" {{$grantor->title == 'la señora'? 'selected': ''}}>Señora</option>
                                                <option value="la señorita" {{$grantor->title == 'la señorita'? 'selected': ''}}>Señorita</option>
                                                <option value="la sociedad" {{$grantor->title == 'la sociedad'? 'selected': ''}}>Sociedad</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-5">
                                        <div class="form-group">
                                            <label class="form-control-label" for="name">* Nombre(s)</label>
                                            <input type="text" name="name" id="name" class="form-control" required autocomplete="off" value="{{ (old('name')) ? old('name') : $grantor->name }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-5">
                                        <div class="form-group">
                                            <label class="form-control-label" for="last_name">* Apellidos</label>
                                            <input type="text" name="last_name" id="last_name" class="form-control" required autocomplete="off" value="{{ (old('last_name')) ? old('last_name') : $grantor->last_name }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="form-control-label" for="celular">Celular</label>
                                            <input type="text" name="celular" id="celular" class="form-control" autocomplete="off" value="{{ (old('celular')) ? old('celular') : $grantor->celular }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="form-control-label" for="email">Correo electrónico</label>
                                            <input type="email" name="email" id="email" class="form-control" autocomplete="off" value="{{ (old('email')) ? old('email') : $grantor->email }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="form-control-label" for="birth_date">Fecha Nacimiento</label>
                                            <input type="text" name="birth_date" id="birth_date" class="form-control mydatepicker" autocomplete="off" value="{{ (old('birth_date')) ? old('birth_date') : date('d/m/Y', strtotime($grantor->birth_date)) }}">
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label class="form-control-label" for="public_notary_description">* Descripción de Fe del Notario público</label>
                                            <textarea name="public_notary_description" id="public_notary_description" class="form-control" rows="3"  required autocomplete="off" value="{{ (old('public_notary_description')) ? old('public_notary_description') : $grantor->public_notary_description }}">{{ (old('public_notary_description')) ? old('public_notary_description') : $grantor->public_notary_description }}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="row">
                                    <div class="col-lg-12 text-center">
                                        <button class="btn btn-default">Confirmar</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
				</div>
			</div>
        </div>
    </div>
@endsection