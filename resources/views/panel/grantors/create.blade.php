@extends('layouts.panel.app')
@section('content')
    <!-- Header -->
    @include('include.panel.header')
    <!-- Page content -->
    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col">
				<div class="card">
				<!-- Card header -->
					<div class="card-header border-0">
                        {{-- <h3 class="mb-0">Nuevo usuario</h3> --}}
                        <h6 class="heading-small text-muted mb-4">Información</h6>
					</div>
                    <!-- Light table -->
                    <div class="card-body">
                        <form action="{{ route('panel.grantors.store', ['desarrollo' => request()->desarrollo]) }}" method="POST" class="needs-validation" novalidate>
                            {{ csrf_field() }}
                            <div class="pl-lg-4">
                                <div class="row">
                                    <div class="col-lg-2">
                                        <div class="form-group">
                                            <label class="form-control-label" for="title">* Titulo</label>
                                            <select class="form-control" name="title" id="title">
                                                <option value="el señor">Señor</option>
                                                <option value="la señora">Señora</option>
                                                <option value="la señorita">Señorita</option>
                                                <option value="la sociedad">Sociedad</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-5">
                                        <div class="form-group">
                                            <label class="form-control-label" for="name">* Nombre(s)</label>
                                            <input type="text" name="name" id="name" class="form-control" required autocomplete="off" value="{{ old('name') }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-5">
                                        <div class="form-group">
                                            <label class="form-control-label" for="last_name">* Apellidos</label>
                                            <input type="text" name="last_name" id="last_name" class="form-control" required autocomplete="off" value="{{ old('last_name') }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="form-control-label" for="celular">Celular</label>
                                            <input type="text" name="celular" id="celular" class="form-control" autocomplete="off" value="{{ old('celular') }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="form-control-label" for="email">Correo electrónico</label>
                                            <input type="email" name="email" id="email" class="form-control" autocomplete="off" value="{{ old('email') }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="form-control-label" for="birth_date">Fecha de nacimiento</label>
                                            <input type="text" name="birth_date" id="birth_date" class="form-control mydatepicker" autocomplete="off" value="{{ old('birth_date') }}">
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label class="form-control-label" for="public_notary_description">* Descripción de Fe del Notario público</label>
                                            <textarea name="public_notary_description" id="public_notary_description" class="form-control" rows="3"  required autocomplete="off" value="{{ old('public_notary_description') }}"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="row">
                                    <div class="col-lg-12 text-center">
                                        <button class="btn btn-default">Confirmar</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
				</div>
			</div>
        </div>
    </div>
@endsection