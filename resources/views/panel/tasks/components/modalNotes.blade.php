<div class="modal fade" id="addNote" tabindex="-1" role="dialog" aria-labelledby="modal-form" aria-hidden="true">
    <div class="modal-dialog modal- modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">   
            <div class="modal-body p-0">                
                <div class="card bg-secondary border-0 mb-0">
                    <div class="card-body px-lg-5 py-lg-5">
                        <div class="text-center text-muted mb-4">
                            <small>Nueva Nota</small>
                        </div>
                        <form method="POST" action="{{ route('panel.notes.store') }}">
                            {{ csrf_field() }}
                            <input type="hidden" name="prospect_id" value="{{ $data->id }}">
                            <input type="hidden" name="admin_id" value="{{ $data->admin_id }}">
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label class="form-control-label" for="title">* Título</label>
                                        <input type="text" name="title" id="title" class="form-control" required autocomplete="off" value="">
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label class="form-control-label" for="description">* Descripción</label>
                                        <textarea name="description" required id="description" class="trumbowyg-panel" cols="30" rows="10"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="text-center">
                                <button class="btn btn-primary my-4">Confirmar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>