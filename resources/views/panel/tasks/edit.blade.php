@extends('layouts.panel.app')
@section('content')
    <!-- Header -->
    @include('include.panel.header')
    <!-- Page content -->
    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col">
				<div class="card">
				<!-- Card header -->
					<div class="card-header border-0">
                        {{-- <h3 class="mb-0">Nuevo usuario</h3> --}}
                        <h6 class="heading-small text-muted mb-4">Información</h6>
					</div>
                    <!-- Light table -->
                    <div class="card-body">
                        <form action="{{ route('panel.tasks.update', ['actividade' => $task]) }}" method="POST" class="needs-validation" novalidate>
                            {{ csrf_field() }}
                            <input type="hidden" name="_method" value="PUT">
                            <input type="hidden" name="id" value="{{$task->id}}">
                            <div class="pl-lg-4">
                                <div class="row">
                                    <div class="col-lg-10">
                                        <div class="form-group">
                                            <label class="form-control-label" for="description">* Descripción</label>
                                            <input type="text" name="description" id="description" class="form-control" required autocomplete="off" value="{{ $task->description }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <div class="form-group">
                                            <label class="form-control-label" for="order">* Orden</label>
                                            <input type="text" name="order" id="order" class="form-control" required autocomplete="off" value="{{ $task->order }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="form-control-label" for="required">* Obligatorio</label>
                                            <select name="required" id="required" class="form-control">
                                                <option value="1" {{ ($task->required == 1) ? 'selected' : '' }}>Sí</option>
                                                <option value="0" {{ ($task->required == 0) ? 'selected' : '' }}>No</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="form-control-label" for="status">* Publicación</label>
                                            <select name="status" id="status" class="form-control">
                                                <option value="visible">Visible</option>
                                                <option value="hidden">Oculto</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="row">
                                    <div class="col-lg-12 text-center">
                                        @can(PermissionKey::Task['permissions']['update']['name'])
                                            <button class="btn btn-default">Confirmar</button>
                                        @endcan
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
				</div>
			</div>
        </div>
    </div>
@endsection