@extends('layouts.panel.app')
@section('content')
    <!-- Header -->
    @include('include.panel.header')
    <!-- Page content -->
    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col">
				<div class="card">
				<!-- Card header -->
					<div class="card-header border-0">
						<div class="row">
                            <div class="col-8">
                                <h6 class="heading-small text-muted mb-4">Información</h6>
                            </div>
                            <div class="col-4 text-right">
                                
                            </div>
                        </div>
					</div>
                    <!-- Light table -->
					<div class="table-responsive pb-3">
						<table class="table align-items-center table-flush" id="dataTable">
							<thead class="thead-light">
								<tr>
									<th width="20px">
                                    </th>
									<th scope="col" class="sort" data-sort="status">Descripción</th>
                                    <th scope="col" class="sort" data-sort="status">Orden</th>
									<th scope="col" class="sort" data-sort="status">Obligatorio</th>
                                    <th scope="col" class="sort" data-sort="status">Visualización</th>
									<th scope="col">Acciones</th>
								</tr>
							</thead>
							<tbody class="list">
								@if ((isset($data)) && (count($data) > 0))
                                    @foreach ($data as $row)
                                        <tr>
                                            <td>
                                            </td>
                                            <td>{{ $row->description }}</td>
                                            <td>{{ $row->order }}</td>
                                            <td>{{ $row->required() }}</td>
                                            <td>{{ $row->status() }}</td>
                                            <td class="text-right">
                                                <div class="dropdown">
                                                    <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <i class="fas fa-ellipsis-v"></i>
                                                    </a>
                                                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                                        <a class="dropdown-item" href="{{ route('panel.tasks.edit', ['actividade' => $row]) }}">Editar</a>
                                                        @can(PermissionKey::Task['permissions']['destroy']['name'])
                                                            <a class="dropdown-item btn-delete" data-axios-method="delete" data-route="{{ route('panel.tasks.destroy', ['actividade' => $row]) }}" data-action="location.reload()" href="javascript:;">Eliminar</a>
                                                        @endcan
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
							</tbody>
						</table>
					</div>
				</div>
			</div>
        </div>
    </div>
    <div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledby="modal-form" aria-hidden="true">
        <div class="modal-dialog modal- modal-dialog-centered modal-sm" role="document">
            <div class="modal-content">   
                <div class="modal-body p-0">                
                    <div class="card bg-secondary border-0 mb-0">
                        <div class="card-body px-lg-5 py-lg-5">
                            <div class="text-center text-muted mb-4">
                                <small>Selecciona el archivo</small>
                            </div>
                            <form method="POST" action="{{ route('panel.prospects.import') }}" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="form-group mb-3">
                                    <div class="input-group input-group-merge input-group-alternative">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="ni ni-folder-17"></i></span>
                                        </div>
                                        <input class="form-control" placeholder="Ingrese el archivo" type="file" name="import" required>
                                    </div>
                                </div>
                                <div class="text-center">
                                    <button class="btn btn-primary my-4">Confirmar</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection