@extends('layouts.panel.app')
@section('content')
    <!-- Header -->
    @include('include.panel.header')
    <!-- Page content -->
    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col">
				<div class="card">
                <!-- Card header -->
					<div class="card-header border-0">
                        <div class="row">
                            <div class="col-8">
                                <h6 class="heading-small text-muted mb-4">Información</h6>
                            </div>
                        </div>
					</div>
                    <!-- Light table -->
                    <div class="card-body">
                        <form action="{{ route('panel.cadastralPlankings.update', ['tablajesCatastrale' => $cadastralPlanking->id]) }}" method="POST" class="needs-validation" novalidate>
                            {{ csrf_field() }}
                            <input type="hidden" name="_method" value="PUT">
                            <div class="pl-lg-4">
                                <div class="row">
                                    <div class="col-4">
                                        <div class="form-group">
                                            <label class="form-control-label" for="name">* Nombre</label>
                                            <input type="text" name="name" id="name" class="form-control" required autocomplete="off" value="{{ (old('name')) ? old('name') : $cadastralPlanking->name }}">
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group">
                                            <label class="form-control-label" for="number">* Número</label>
                                            <input type="text" name="number" id="number" class="form-control" required autocomplete="off" value="{{ (old('number')) ? old('number') : $cadastralPlanking->number }}">
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group">
                                            <label class="form-control-label" for="size">* Medida en m<sup>2</sup></label>
                                            <input type="text" name="size" id="size" class="form-control" required autocomplete="off" value="{{ (old('size')) ? old('size') : $cadastralPlanking->size }}">
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label class="form-control-label" for="stage">* Localidad</label>
                                            <textarea name="location" id="location" class="form-control" rows="3"  required autocomplete="off" value="{{ (old('location')) ? old('location') : $cadastralPlanking->location }}">{{ (old('location')) ? old('location') : $cadastralPlanking->location }}</textarea>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label class="form-control-label" for="stage">* Referencias</label>
                                            <textarea name="reference" id="reference" class="form-control" rows="3"  required autocomplete="off" value="{{ (old('reference')) ? old('reference') : $cadastralPlanking->reference }}">{{ (old('reference')) ? old('reference') : $cadastralPlanking->reference }}</textarea>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label class="form-control-label" for="stage">* Colindancias</label>
                                            <textarea name="adjoining" id="adjoining" class="form-control" rows="3"  required autocomplete="off" value="{{ (old('adjoining')) ? old('adjoining') : $cadastralPlanking->adjoining }}">{{ (old('adjoining')) ? old('adjoining') : $cadastralPlanking->adjoining }}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="row">
                                    <div class="col-lg-12 text-center">
                                        <button class="btn btn-default">Confirmar</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
				</div>
			</div>
        </div>
    </div>
@endsection