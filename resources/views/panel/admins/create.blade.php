@extends('layouts.panel.app')
@section('content')
    <!-- Header -->
    @include('include.panel.header')
    <!-- Page content -->
    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col">
				<div class="card">
				<!-- Card header -->
					<div class="card-header border-0">
                        {{-- <h3 class="mb-0">Nuevo usuario</h3> --}}
                        <h6 class="heading-small text-muted mb-4">Información del usuario</h6>
					</div>
                    <!-- Light table -->
                    <div class="card-body">
                        <form action="{{ route('panel.admins.store') }}" method="POST" class="needs-validation" enctype="multipart/form-data" novalidate>
                            {{ csrf_field() }}
                            {{-- @error('invalid_password')
                                <div class="alert alert-danger" role="alert">
                                    <strong>Ups!</strong> {{ $message }}
                                </div>
                            @enderror --}}
                            <div class="pl-lg-4">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="dropzone-avatar" class="form-control-label">Avatar</label>
                                            <input class="form-control unfocus" type="text" name="avatar" id="avatar" value="{{ old('avatar') }}" data-asset="{{ asset('') }}">
                                            <a href="#modal-media" data-reference="dropzone-avatar" data-dropzone="image" data-toggle="modal" data-target="#modal-media" class="btn btn-default float-right mt-1">Escoger del multimedia</a>
                                            <div id="dropzone-avatar" data-route="{{ route('images.store') }}" data-target="#avatar" class="dropzone" style="border:0px">
                                                <div class="dz-default dz-message">Seleccionar archivo</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label class="form-control-label" for="role">* Rol</label>
                                            <select name="role" id="role" class="form-control" required>
                                                <option value="">Selecciona una opción</option>
                                                @if (($roles) && (count($roles) > 0))
                                                    @foreach ($roles as $rol)
                                                        <option {{ (old('role') == $rol->id) ? 'selected' : '' }} value="{{ $rol->id }}">{{ $rol->name }}</option>                                                        
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="name">* Nombre</label>
                                            <input type="text" name="name" id="name" class="form-control" required autocomplete="off" value="{{ old('name') }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="last_name">* Apellidos</label>
                                            <input type="text" name="last_name" id="last_name" class="form-control" required autocomplete="off" value="{{ old('last_name') }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="email">* Email</label>
                                            <input type="email" id="email" name="email" class="form-control" required autocomplete="off" value="{{ old('email') }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="code">* Código</label>
                                            <input type="text" id="code" name="code" class="form-control" required autocomplete="off" value="{{ old('code') }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="form-control-label" for="password">* Contraseña</label>
                                            <input type="password" name="password" id="password" class="form-control" required autocomplete="off">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="form-control-label" for="confirm-password">* Confirmar contraseña</label>
                                            <input type="password" name="confirm_password" id="confirm-password" class="form-control" required autocomplete="off">
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <h5>Información adicional</h5>
                                <div class="row">
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="comision">Comisión</label>
                                            <input type="text" name="comision" id="comision" class="form-control" autocomplete="off" value="{{ old('comision') }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="birth_date">Fecha Nacimiento</label>
                                            <input type="text" name="birth_date" id="birth_date" class="form-control mydatepicker" autocomplete="off" value="{{ old('birth_date') }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="celular">Celular</label>
                                            <input type="text" name="celular" id="celular" class="form-control" autocomplete="off" value="{{ old('celular') }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="direccion">Dirección</label>
                                            <input type="text" name="direccion" id="direccion" class="form-control" autocomplete="off" value="{{ old('direccion') }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="cp">CP</label>
                                            <input type="text" name="cp" id="cp" class="form-control" autocomplete="off" value="{{ old('cp') }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="lugar_nacimiento">Lugar Nacimiento</label>
                                            <input type="text" name="lugar_nacimiento" id="lugar_nacimiento" class="form-control" autocomplete="off" value="{{ old('lugar_nacimiento') }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="identification_type">Tipo identificación</label>
                                            <select name="identification_type" id="identification_type" class="form-control">
                                                <option value="0" selected >Seleccione una opción</option>
                                                <option value="1" >INE</option>
                                                <option value="2" >IFE</option>
                                                <option value="3" >VISA</option>
                                                <option value="4" >FM2</option>
                                                <option value="5" >Cédula Profesional</option>
                                                <option value="6" >Pasaporte</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="identificacion">Identificación</label>
                                            <input type="text" name="identificacion" id="identificacion" class="form-control" autocomplete="off" value="{{ old('identificacion') }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="licencia_manejo">Licencia Manejo</label>
                                            <input type="text" name="licencia_manejo" id="licencia_manejo" class="form-control" autocomplete="off" value="{{ old('licencia_manejo') }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="documento_permiso">Documento o permiso de trabajo</label>
                                            <input type="text" name="documento_permiso" id="documento_permiso" class="form-control" autocomplete="off" value="{{ old('documento_permiso') }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="curp">CURP</label>
                                            <input type="text" name="curp" id="curp" class="form-control" autocomplete="off" value="{{ old('curp') }}">
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <h5>Información de salud</h5>
                                <div class="row">
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="numero_seguridad">Número de seguridad social</label>
                                            <input type="text" name="numero_seguridad" id="numero_seguridad" class="form-control" autocomplete="off" value="{{ old('numero_seguridad') }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="poliza_seguro">Póliza Seguro</label>
                                            <input type="text" name="poliza_seguro" id="poliza_seguro" class="form-control" autocomplete="off" value="{{ old('poliza_seguro') }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="estado_actual">Estado de salud actual</label>
                                            <select name="estado_actual" id="" class="form-control">
                                                <option value="Bueno">Bueno</option>
                                                <option value="Regular">Regular</option>
                                                <option value="Malo">Malo</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="tipo_sangre">Tipo de sangre</label>
                                            <input type="text" name="tipo_sangre" id="tipo_sangre" class="form-control" autocomplete="off" value="{{ old('tipo_sangre') }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label class="form-control-label" for="enfermedad_cronica">Padece alguna enfermedad crónica/medicado; Describa</label>
                                            <textarea name="enfermedad_cronica" id="" cols="30" rows="10" class="form-control"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <div class="form-group">
                                            <label class="form-control-label" for="estatura">Estatura</label>
                                            <input type="text" name="estatura" id="estatura" class="form-control" autocomplete="off" value="{{ old('estatura') }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <div class="form-group">
                                            <label class="form-control-label" for="peso">Peso</label>
                                            <input type="text" name="peso" id="peso" class="form-control" autocomplete="off" value="{{ old('peso') }}">
                                        </div>
                                    </div>
                                    
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="form-control-label" for="alergia">Alergia</label>
                                            <input type="text" name="alergia" id="alergia" class="form-control" autocomplete="off" value="{{ old('alergia') }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="form-control-label" for="enfermedades">Enfermedades</label>
                                            <input type="text" name="enfermedades" id="enfermedades" class="form-control" autocomplete="off" value="{{ old('enfermedades') }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label class="form-control-label" for="contacto">Contacto de emergencia</label>
                                            <input type="text" name="contacto" id="contacto" class="form-control" autocomplete="off" value="{{ old('contacto') }}">
                                        </div>
                                    </div>

                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="dropzone-ine1" class="form-control-label">* Ine Adelante</label>
                                            <input class="form-control unfocus" type="text" name="ine1" id="ine1" value="{{ old('ine1') }}"
                                             data-asset="{{ asset('') }}" required>
                                            <a href="#modal-media" data-reference="dropzone-ine1" data-dropzone="image" 
                                            data-toggle="modal" data-target="#modal-media" class="btn btn-default float-right mt-1">Escoger del multimedia</a>
                                            <div class="clearfix"></div>
                                            <div id="dropzone-ine1" data-route="{{ route('images.store') }}" data-target="#ine1" class="dropzone" style="border:0px">
                                                <div class="dz-default dz-message">Seleccionar archivo</div>
                                            </div>
                                        </div>
                                    </div>    

                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="dropzone-ine2" class="form-control-label">* Ine Atrás</label>
                                            <input class="form-control unfocus" type="text" name="ine2" id="ine2"
                                             value="{{ old('ine2') }}"
                                             data-asset="{{ asset('') }}" required>
                                            <a href="#modal-media" data-reference="dropzone-ine2" data-dropzone="image" 
                                            data-toggle="modal" data-target="#modal-media" class="btn btn-default float-right mt-1">Escoger del multimedia</a>
                                            <div class="clearfix"></div>
                                            <div id="dropzone-ine2" data-route="{{ route('images.store') }}" data-target="#ine2"
                                             class="dropzone" style="border:0px">
                                                <div class="dz-default dz-message">Seleccionar archivo</div>
                                            </div>
                                        </div>
                                    </div>    


                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="row">
                                    <div class="col-lg-12 text-center">
                                        <button class="btn btn-default">Confirmar</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
				</div>
			</div>
        </div>
    </div>
    @include('include.panel.media')
@endsection