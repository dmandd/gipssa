@extends('layouts.panel.app')
@section('content')
    <!-- Header -->
    @include('include.panel.header')
    <!-- Page content -->
    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col">
				<div class="card">
				<!-- Card header -->
					<div class="card-header border-0">
						{{-- <h3 class="mb-0">Light table</h3> --}}
					</div>
                    <!-- Light table -->
					<div class="table-responsive pb-3">
						<table class="table align-items-center table-flush" id="dataTable">
							<thead class="thead-light">
								<tr>
									<th width="20px">
                                        {{-- <input type="checkbox" class="" id="customCheck1"> --}}
                                        {{-- <div class="custom-control custom-checkbox">
                                            <label class="custom-control-label" for="customCheck1"></label>
                                        </div> --}}
                                    </th>
									<th scope="col" class="sort" data-sort="budget">Avatar</th>
                                    <th scope="col" class="sort" data-sort="budget">Leads</th>
									<th scope="col" class="sort" data-sort="status">Nombre</th>
									<th scope="col">Rol</th>
									<th scope="col">Correo</th>
                                    <th scope="col">Clientes Asignados</th>
									<th scope="col">Acciones</th>
								</tr>
							</thead>
							<tbody class="list">
								@if ((isset($data)) && (count($data) > 0))
                                    @foreach ($data as $row)
                                        <tr>
                                         
                                            <td>
                                                <input type="checkbox" class="" id=""><span style="opacity:0;">{{ $row->id}}</span>
                                            </td>
                                            <td><img width="20px" src="{{ asset($row->avatar) }}" alt=""></td>
                                            <td>{{ $row->prospectos()->count() }}</td>


                                            <td>
                                            @if ($row->hide==1)
                                                <del>                                                
                                            @endif                                                
                                                {{ $row->name }} {{$row->attorney?"\n\n Apoderado":''}}
                                            @if ($row->hide==1)
                                                <del>                                                
                                            @endif
                                            </td>

                                            <td>{{ $row->currentRole() }}</td>
                                            <td>{{ $row->email }}</td>
                                            <!-- Mostrar listado de Usuarios para transferir -->
                                            <td>
                                                
                                                    
                                                    <form action="{{ route('panel.admins.reasign', ['id' => $row->id]) }}" method="POST"  enctype="multipart/form-data" novalidate>
                                                        @method('PUT')
                                                        @csrf  
                                                        <div>     
                                                        <a class="btn btn-default" href="{{ route('panel.prospects.index',['vista'=>'tabla','usuario'=>$row->id]) }}">Abrir Leads</a>  <br>                                                           
                                                        
                                                        <!-- Clientes:{{ $row->client()->count() }} <br>       
                                                        Prospectos:{{ $row->prospectos()->count() }}<br>                                                                
                                                        Contactos:{{ $row->contacts()->count() }}<br>
                                                        Desarrollos:{{ $row->developments()->count() }}<br>
                                                        Propiedades:{{ $row->propertys()->count() }}<br>
                                                        Cotizaciones:{{ $row->quotations()->count() }}<br> -->
                                                        
                                                        <label for="Asignar">Reasignar a:</label>
                                                        <select name="admin_to" id="admin_to">
                                                        @foreach ($data as $adm )
                                                            @if($adm->id!=$row->id)
                                                                <option value="{{$adm->id}}">{{$adm->name}}</option>   
                                                            @endif                                                        
                                                        @endforeach  
                                                        </select>           
                                                                                              
                                                            <input type="submit" value="Asignar">
                                                        </div>   
                                                        Enlaces Internos:{{ $row->client()->count() + $row->prospectos()->count()+
                                                             $row->contacts()->count()+ $row->developments()->count()+ $row->propertys()->count()+ $row->quotations()->count() }}<br> 
                                                         
                                                    </form>
                                               
                                            </td>
                                            <!-- Mostrar listado de Usuarios para transferir -->
                                          
                                            <td class="text-right">
                                                <div class="dropdown">
                                                    <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <i class="fas fa-ellipsis-v"></i>
                                                    </a>
                                                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                                        <a class="dropdown-item" href="{{ route('panel.admins.edit', ['id' => $row->id]) }}">Editar</a>    
                                                     

                                                       <!--  <a class="dropdown-item" href="{{ route('panel.admins.hide', ['id' => $row->id]) }}">
                                                            @if ($row->hide==1)
                                                                Activar
                                                            @else
                                                                Desactivar
                                                            @endif    
                                                         </a> -->
                                                        
                                                          
                                                        



                                                         <a class="dropdown-item btn-delete" data-axios-method="delete" data-route="{{ route('panel.admins.destroy', ['id' => $row->id]) }}" data-action="location.reload()" href="javascript:;">Eliminar</a> 
                                                        <!-- Agregar el listado de las ventas asociadas con el cliente 17 de Junio 2022-->    
                                                        @if ($row->client()->count()>0)                                                           
                                                            <a class="dropdown-item" > Clientes Asignados </a>                                                                            
                                                            @foreach ($row->client() as $client)
                                                               <a class="dropdown-item"
                                                                    @can(PermissionKey::Client['permissions']['update']['name'])
                                                                        href="{{ route('panel.clients.edit', ['cliente' => $client->id]) }}"
                                                                     @endcan
                                                                 >  {{$client->name}} </a>         
                                                                
                                                                                                           
                                                            @endforeach                                  
                                                        @endif
                                                        <!--  -->   




                                                    </div>
                                                </div>
                                            </td>
                                            
                                        </tr>
                                    @endforeach
                                @endif
							</tbody>
						</table>
					</div>
				</div>
			</div>
        </div>
    </div>
@endsection