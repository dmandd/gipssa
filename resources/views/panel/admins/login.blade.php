@extends('layouts.panel.login')
@section('content')
    <!-- Header -->
    <style focused>

        .bg-gipssa{
            background-image: url("{{ asset('panel/img/brand/LOGO-CRM.png') }}"), url("{{ asset('panel/img/MARCA-AGUA-CRM-EXT.png') }}"), url("{{ asset('panel/img/FONDO-CRM-EXT.png') }}");
            background-position: left top,right bottom, center;
            background-repeat: no-repeat;
            background-size: 65vh,100vh,cover;
        }

        .login{
            padding: 28vh 6vh;
        }
        @media only screen and (max-width: 600px) {
            .bg-gipssa{
                background-repeat: no-repeat;
                background-size: 40vh, 90vh, cover;
            }
        }
    </style>
    <div class="login bg-gipssa">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-5 col-md-7">
                    <h1 class="text-white text-center">Iniciar Sesión</h1>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-lg-5 col-md-7">
                    <div class="card bg-secondary border-0 mb-0">
                        <div class="card-body px-lg-5 py-lg-5">
                            <form role="form" method="POST" action="{{ route('panel.admins.login') }}">
                                {{ csrf_field() }}
                                @if ($errors->any())
                                    <div class="alert alert-danger" role="alert">
                                        <strong>Ups!</strong> El correo o contraseña son inválidos!
                                    </div>
                                @endif
                                <div class="form-group mb-3">
                                    <div class="input-group input-group-merge input-group-alternative">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="ni ni-email-83"></i></span>
                                        </div>
                                    <input name="email" class="form-control" placeholder="Email" type="email" value="{{ (old('email')) ? old('email') : '' }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group input-group-merge input-group-alternative">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                                        </div>
                                        <input name="password" class="form-control" placeholder="Contraseña" type="password">
                                    </div>
                                </div>
                                <div class="custom-control custom-control-alternative custom-checkbox">
                                    <input class="custom-control-input" name="remember" id=" customCheckLogin" type="checkbox">
                                    <label class="custom-control-label" for=" customCheckLogin">
                                        <span class="text-muted">Recordarme</span>
                                    </label>
                                </div>
                                <div class="text-center">
                                    <button class="btn btn-primary my-4">Continuar</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-6">
                            <a href="{{ route('panel.admins.password.reset') }}" class="text-light"><small>Recuperar Contraseña?</small></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Page content -->
    
@endsection