@extends('layouts.panel.app')
@section('content')
    <!-- Header -->
    @include('include.panel.header')
    <!-- Page content -->
    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col">
				<div class="card">
				<!-- Card header -->
					<div class="card-header border-0">
                        <div class="row">
                            <div class="col-8">
                                <h6 class="heading-small text-muted mb-4">Información del usuario</h6>
                            </div>
                            @can(PermissionKey::Admin['permissions']['update_password']['name'])
                                <div class="col-4 text-right">
                                    <div class="dropdown">
                                        <a href="#!" class="btn btn-sm btn-default" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Opciones</a>
                                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow" style="">
                                            <a class="dropdown-item" href="{{ route('panel.admins.edit.password', ['id' => $admin->id]) }}">Actualizar contraseña</a>
                                        </div>
                                    </div>
                                </div>
                            @endcan
                        </div>
					</div>
                    <!-- Light table -->
                    <div class="card-body">
                        <form action="{{ route('panel.admins.update', ['id' => $admin->id]) }}" method="POST" class="needs-validation" enctype="multipart/form-data" novalidate>
                            {{ csrf_field() }}
                            <input type="hidden" name="_method" value="PUT">
                            <div class="pl-lg-4">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="dropzone-avatar" class="form-control-label">Avatar</label>
                                            <input class="form-control unfocus" type="text" name="avatar" id="avatar" value="{{ (old('avatar')) ? old('avatar') : $admin->avatar }}" data-asset="{{ asset('') }}" required>
                                            <a href="#modal-media" data-dropzone="image" data-toggle="modal" data-target="#modal-media" class="btn btn-default float-right mt-1">Escoger del multimedia</a>
                                            <div id="dropzone-avatar" data-route="{{ route('images.store') }}" data-target="#avatar" class="dropzone" style="border:0px">
                                                <div class="dz-default dz-message">Seleccionar archivo</div>
                                            </div>
                                        </div>
                                    </div>
                                    @can(PermissionKey::Role['permissions']['assign_role']['name'])
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label class="form-control-label" for="role">* Rol</label>
                                                <select name="role" id="role" class="form-control" required>
                                                    <option value="">Selecciona una opción</option>
                                                    @if ((isset($roles)) && (count($roles) > 0))
                                                        @foreach ($roles as $role)
                                                            <option value="{{ $role->id }}" {{ ($admin->hasRole($role->id)) ? 'selected' : '' }}>{{ $role->name }}</option>
                                                        @endforeach
                                                    @else
                                                        <option value="">sin contenido...</option>
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                    @endcan
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="name">* Nombre</label>
                                            <input type="text" name="name" id="name" class="form-control" required autocomplete="off" value="{{ $admin->name }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="last_name">* Apellidos</label>
                                            <input type="text" name="last_name" id="last_name" class="form-control" required autocomplete="off" value="{{ $admin->last_name }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="email">* Email</label>
                                            <input type="email" id="email" name="email" class="form-control" required autocomplete="off" value="{{ $admin->email }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="code">* Código</label>
                                            <input type="text" id="code" name="code" class="form-control" required autocomplete="off" value="{{ $admin->code }}">
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <h5>Información adicional</h5>
                                <div class="row">
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="comision">Comisión</label>
                                            <input type="text" name="comision" id="comision" class="form-control" autocomplete="off" value="{{ (old('comision')) ? old('comision') : $admin->comision }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="birth_date">Fecha Nacimiento</label>
                                            <input type="text" name="birth_date" id="birth_date" class="form-control mydatepicker" autocomplete="off" value="{{ (old('birth_date')) ? old('birth_date') : date('d/m/Y', strtotime($admin->birth_date)) }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="celular">Celular</label>
                                            <input type="text" name="celular" id="celular" class="form-control" autocomplete="off" value="{{ (old('celular')) ? old('celular') : $admin->celular }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="direccion">Dirección</label>
                                            <input type="text" name="direccion" id="direccion" class="form-control" autocomplete="off" value="{{ (old('direccion')) ? old('direccion') : $admin->direccion }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="cp">CP</label>
                                            <input type="text" name="cp" id="cp" class="form-control" autocomplete="off" value="{{ $admin->cp }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="lugar_nacimiento">Lugar Nacimiento</label>
                                            <input type="text" name="lugar_nacimiento" id="lugar_nacimiento" class="form-control" autocomplete="off" value="{{ (old('lugar_nacimiento')) ? old('lugar_nacimiento') : $admin->lugar_nacimiento }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="identification_type">Tipo identificación</label>
                                            <select name="identification_type" id="identification_type" class="form-control">
                                                <option value="0" {{ ($admin->identification_type == '0') ? 'selected' : '' }}>Seleccione una opción</option>
                                                <option value="1" {{ ($admin->identification_type == '1') ? 'selected' : '' }}>INE</option>
                                                <option value="2" {{ ($admin->identification_type == '2') ? 'selected' : '' }}>IFE</option>
                                                <option value="3" {{ ($admin->identification_type == '3') ? 'selected' : '' }}>VISA</option>
                                                <option value="4" {{ ($admin->identification_type == '4') ? 'selected' : '' }}>FM2</option>
                                                <option value="5" {{ ($admin->identification_type == '5') ? 'selected' : '' }}>Cédula Profesional</option>
                                                <option value="6" {{ ($admin->identification_type == '6') ? 'selected' : '' }}>Pasaporte</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="identificacion">Identificación</label>
                                            <input type="text" name="identificacion" id="identificacion" class="form-control" autocomplete="off" value="{{ (old('identificacion')) ? old('identificacion') : $admin->identificacion }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="licencia_manejo">Licencia Manejo</label>
                                            <input type="text" name="licencia_manejo" id="licencia_manejo" class="form-control" autocomplete="off" value="{{ (old('licencia_manejo')) ? old('licencia_manejo') : $admin->licencia_manejo }}">
                                        </div>
                                    </div>
                                    
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="documento_permiso">Documento o permiso de trabajo</label>
                                            <input type="text" name="documento_permiso" id="documento_permiso" class="form-control" autocomplete="off" value="{{ (old('documento_permiso')) ? old('documento_permiso') : $admin->documento_permiso }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="curp">CURP</label>
                                            <input type="text" name="curp" id="curp" class="form-control" autocomplete="off" value="{{ (old('curp')) ? old('curp') : $admin->curp }}">
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <h5>Información de salud</h5>
                                <div class="row">
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="numero_seguridad">Número de seguridad social</label>
                                            <input type="text" name="numero_seguridad" id="numero_seguridad" class="form-control" autocomplete="off" value="{{ (old('numero_seguridad')) ? old('numero_seguridad') : $admin->numero_seguridad }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="poliza_seguro">Póliza Seguro</label>
                                            <input type="text" name="poliza_seguro" id="poliza_seguro" class="form-control" autocomplete="off" value="{{ (old('poliza_seguro')) ? old('poliza_seguro') : $admin->poliza_seguro }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="estado_actual">Estado de salud actual</label>
                                            <select name="estado_actual" id="" class="form-control">
                                                <option value="Bueno" {{ ($admin->estado_actual == 'Bueno') ? 'selected' : '' }}>Bueno</option>
                                                <option value="Regular" {{ ($admin->estado_actual == 'Regular') ? 'selected' : '' }}>Regular</option>
                                                <option value="Malo" {{ ($admin->estado_actual == 'Malo') ? 'selected' : '' }}>Malo</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="tipo_sangre">Tipo de sangre</label>
                                            <input type="text" name="tipo_sangre" id="tipo_sangre" class="form-control" autocomplete="off" value="{{ (old('tipo_sangre')) ? old('tipo_sangre') : $admin->tipo_sangre }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label class="form-control-label" for="enfermedad_cronica">Padece alguna enfermedad crónica/medicado; Describa</label>
                                            <textarea name="enfermedad_cronica" id="" cols="30" rows="10" class="form-control">{{ $admin->enfermedad_cronica }}</textarea>
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <div class="form-group">
                                            <label class="form-control-label" for="estatura">Estatura</label>
                                            <input type="text" name="estatura" id="estatura" class="form-control" autocomplete="off" value="{{ (old('estatura')) ? old('estatura') : $admin->estatura }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <div class="form-group">
                                            <label class="form-control-label" for="peso">Peso</label>
                                            <input type="text" name="peso" id="peso" class="form-control" autocomplete="off" value="{{ (old('peso')) ? old('peso') : $admin->peso }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="form-control-label" for="alergia">Alergia</label>
                                            <input type="text" name="alergia" id="alergia" class="form-control" autocomplete="off" value="{{ (old('alergia')) ? old('alergia') : $admin->alergia }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="form-control-label" for="enfermedades">Enfermedades</label>
                                            <input type="text" name="enfermedades" id="enfermedades" class="form-control" autocomplete="off" value="{{ (old('enfermedades')) ? old('enfermedades') : $admin->enfermedades }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label class="form-control-label" for="contacto">Contacto de emergencia</label>
                                            <input type="text" name="contacto" id="contacto" class="form-control" autocomplete="off" value="{{ (old('contacto')) ? old('contacto') : $admin->contacto }}">
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <h5>Información fiscal</h5>
                                <div class="row">
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="rfc">RFC</label>
                                            <input type="text" name="rfc" id="rfc" class="form-control" autocomplete="off" value="{{ (old('rfc')) ? old('rfc') : $admin->rfc }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <label class="form-control-label" for="csd_cer">* Archivo CSD .cer</label>
                                        <div class="input-group input-group-merge input-group-alternative">
                                            <input type="file" name="csd_cer" id="csd_cer" class="form-control form-control-alternative" accept=".cer">
                                        </div>
                                        @if ($admin->csd_cer)
                                            <small><b>Actualizar</b> documento subido</small><br>
                                        @endif
                                        <small>Seleccionar un archivo no mayor a 500kb, Formato .cer</small>
                                    </div>
                                    <div class="col-lg-3">
                                        <label class="form-control-label" for="csd_key">* Archivo CSD .key</label>
                                        <div class="input-group input-group-merge input-group-alternative">
                                            <input type="file" name="csd_key" id="csd_key" class="form-control form-control-alternative" accept=".key">
                                        </div>
                                        @if ($admin->csd_key)
                                            <small><b>Actualizar</b> documento subido</small><br>
                                        @endif
                                        <small>Seleccionar un archivo no mayor a 500kb, Formato .key</small>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="csd_password">* Contraseña CSD .key </label>
                                            <input type="password" name="csd_password" id="csd_password" class="form-control" value="{{ (old('csd_password')) ? old('csd_password') : $admin->csd_password }}" autocomplete="off">
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label class="form-control-label" for="attorney"></label>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" name="attorney" {{ $admin->attorney?'checked':'' }} value="{{ $admin->attorney }}" id="attorney">
                                                <label class="custom-control-label" for="attorney">Apoderado legal</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="dropzone-ine1" class="form-control-label">* Ine Adelante</label>
                                            <input class="form-control unfocus" type="text" name="ine1" id="ine1"
                                             value="{{ (old('ine1'))
                                                 ? old('ine1') : $admin->ine1 }}" data-asset="{{ asset('') }}"
                                                  required>
                                            <a href="#modal-media" data-reference="dropzone-ine1" data-dropzone="image"
                                             data-toggle="modal" data-target="#modal-media" class="btn btn-default float-right mt-1">Escoger del multimedia</a>
                                            <div class="clearfix"></div>
                                            <div id="dropzone-ine1" data-route="{{ route('images.store') }}" data-target="#ine1"
                                             class="dropzone" style="border:0px">
                                                <div class="dz-default dz-message">Seleccionar archivo</div>
                                            </div>
                                        </div>
                                    </div>    

                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="dropzone-ine2" class="form-control-label">* Ine Atrás</label>
                                            <input class="form-control unfocus" type="text" name="ine2" id="ine2"
                                             value="{{ (old('ine2'))
                                                 ? old('ine2') : $admin->ine2 }}" data-asset="{{ asset('') }}"
                                                  required>
                                            <a href="#modal-media" data-reference="dropzone-ine2" data-dropzone="image"
                                             data-toggle="modal" data-target="#modal-media" class="btn btn-default float-right mt-1">Escoger del multimedia</a>
                                            <div class="clearfix"></div>
                                            <div id="dropzone-ine2" data-route="{{ route('images.store') }}" 
                                            data-target="#ine2"
                                             class="dropzone" style="border:0px">
                                                <div class="dz-default dz-message">Seleccionar archivo</div>
                                            </div>
                                        </div>
                                    </div>   


                            </div>
                            <div class="card-footer">
                                @can(PermissionKey::Admin['permissions']['update']['name'])
                                    <div class="row">
                                        <div class="col-lg-12 text-center">
                                            <button class="btn btn-default">Confirmar</button>
                                        </div>
                                    </div>
                                @endcan
                            </div>
                        </form>
                    </div>
				</div>
			</div>
        </div>
    </div>
    @include('include.panel.media')
@endsection