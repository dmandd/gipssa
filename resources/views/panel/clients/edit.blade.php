@extends('layouts.panel.app')
@section('content')
    <!-- Header -->
    @include('include.panel.header')
    <!-- Page content -->
    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col">
				<div class="card">
				<!-- Card header -->
					<div class="card-header border-0">
                        <div class="row">
                            <div class="col-8">
                                <h6 class="heading-small text-muted mb-4">Información</h6>
                            </div>
                            <div class="col-4 text-right">
                                <div class="dropdown">
                                    <a href="#!" class="btn btn-default" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Cotizar</a>
                                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow" style="">
                                        @foreach ($developments as $development)
                                            <a class="dropdown-item" href="{{ route('panel.ventas.create', ['desarrollo' => $development->id, 'client_id' => $client->id]) }}">{{ $development->name }}</a>
                                        @endforeach
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
					</div>
                    <!-- Light table -->
                    <div class="card-body">
                        <form action="{{ route('panel.clients.update', ['cliente' => $client->id]) }}" method="POST" class="needs-validation" novalidate>
                            {{ csrf_field() }}
                            <input type="hidden" name="_method" value="PUT">
                            <div class="pl-lg-4">
                                <div class="row">



                                {{-- @can(PermissionKey::Client['permissions']['assign']) --}}
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label class="form-control-label" for="admin_id">Asignar Agente</label>
                                                <select name="admin_id" id="admin_id">
                                                    @if ((isset($admins)) && (count($admins) > 0))
                                                        @foreach ($admins as $admin)
                                                            <option value="{{ $admin->id }}" {{ ($admin->id == $client->admin_id) ? 'selected' : '' }}>{{ $admin->name }}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                    {{--@endcan --}}

                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="form-control-label" for="name">* Nombre</label>
                                            <input type="text" name="name" id="name" class="form-control" required autocomplete="off" value="{{ (old('name')) ? old('name') : $client->name }}">
                                        </div>
                                    </div>


                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="form-control-label" for="surname">* Apellido</label>
                                            <input type="text" name="surname" id="surname" class="form-control" required autocomplete="off" value="{{ (old('surname')) ? old('surname') : $client->surname }}">
                                        </div>
                                    </div>

                                 

                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="form-control-label" for="phone">* Teléfono</label>
                                            <input type="text" name="phone" id="phone" class="form-control" required autocomplete="off" value="{{ (old('phone')) ? old('phone') : $client->phone }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="form-control-label" for="email">* Email</label>
                                            <input type="email" name="email" id="email" class="form-control" required autocomplete="off" value="{{ (old('email')) ? old('email') : $client->email }}">
                                        </div>
                                    </div>
                              
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="form-control-label" for="lugar_nacimiento">Lugar de nacimiento</label>
                                            <input type="text" name="lugar_nacimiento" id="lugar_nacimiento" class="form-control" autocomplete="off" value="{{ $client->lugar_nacimiento }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="form-control-label" for="fecha_nacimiento">Fecha de nacimiento</label>
                                            <input type="text" name="fecha_nacimiento" id="fecha_nacimiento" class="form-control mydatepicker" autocomplete="off" value="{{ $client->fecha_nacimiento() }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="form-control-label" for="ocupation">Ocupación</label>
                                            <input type="text" name="ocupation" id="ocupation" class="form-control" autocomplete="off" value="{{ $client->ocupation }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="form-control-label" for="address">Domicilio actual</label>
                                            <input type="text" name="address" id="address" class="form-control" autocomplete="off" value="{{ $client->address }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="form-control-label" for="municipio">Municipio</label>
                                            <input type="text" name="municipio" id="municipio" class="form-control" autocomplete="off" value="{{ $client->municipio }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="form-control-label" for="estado">Estado</label>
                                            <input type="text" name="estado" id="estado" class="form-control" autocomplete="off" value="{{ $client->estado }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="form-control-label" for="cp">CP</label>
                                            <input type="text" name="cp" id="cp" class="form-control" autocomplete="off" value="{{ $client->cp }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <div class="form-group">
                                            <label class="form-control-label" for="identification_type">Tipo identificación</label>
                                            <select name="identification_type" id="identification_type" class="form-control">
                                                <option value="0" {{ ($client->identification_type == '0') ? 'selected' : '' }}>Seleccione una opción</option>
                                                <option value="1" {{ ($client->identification_type == '1') ? 'selected' : '' }}>INE</option>
                                                <option value="2" {{ ($client->identification_type == '2') ? 'selected' : '' }}>IFE</option>
                                                <option value="3" {{ ($client->identification_type == '2') ? 'selected' : '' }}>VISA</option>
                                                <option value="4" {{ ($client->identification_type == '2') ? 'selected' : '' }}>FM2</option>
                                                <option value="5" {{ ($client->identification_type == '2') ? 'selected' : '' }}>Cédula Profesional</option>
                                                <option value="6" {{ ($client->identification_type == '3') ? 'selected' : '' }}>Pasaporte</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <div class="form-group">
                                            <label class="form-control-label" for="identificacion">Identificación</label>
                                            <input type="text" name="identificacion" id="identificacion" class="form-control" autocomplete="off" value="{{ $client->identificacion }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="form-control-label" for="curp">CURP</label>
                                            <input type="text" name="curp" id="curp" class="form-control" autocomplete="off" value="{{ $client->curp }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="form-control-label" for="rfc">R.F.C.:</label>
                                            <input type="text" name="rfc" id="rfc" class="form-control" autocomplete="off" value="{{ $client->rfc }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="form-control-label" for="nacionalidad">Nacionalidad</label>
                                            <input type="text" name="nacionalidad" id="nacionalidad" class="form-control" autocomplete="off" value="{{ $client->nacionalidad }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="form-control-label" for="extranjero"></label>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" name="extranjero" {{ $client->extranjero?'checked':'' }} value="{{ $client->extranjero }}" id="extranjero">
                                                <label class="custom-control-label" for="extranjero">Marcar como extranjero</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <h5 class="heading-small text-muted mb-4">Referido</h5>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label class="form-control-label" for="title_second">Título contacto</label>
                                            <input type="text" name="title_second" id="title_second" class="form-control" autocomplete="off" value="{{ (old('title_second')) ? old('title_second') : $client->title_second }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="form-control-label" for="name_second">Nombre</label>
                                            <input type="text" name="name_second" id="name_second" class="form-control" autocomplete="off" value="{{ (old('name_second')) ? old('name_second') : $client->name_second }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="form-control-label" for="surname_second">Apellido</label>
                                            <input type="text" name="surname_second" id="surname_second" class="form-control" autocomplete="off" value="{{ (old('surname_second')) ? old('surname_second') : $client->surname_second }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="form-control-label" for="phone_second">Teléfono</label>
                                            <input type="text" name="phone_second" id="phone_second" class="form-control" autocomplete="off" value="{{ (old('phone_second')) ? old('phone_second') : $client->phone_second }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="form-control-label" for="email_second">Email</label>
                                            <input type="email" name="email_second" id="email_second" class="form-control" autocomplete="off" value="{{ (old('email_second')) ? old('email_second') : $client->email_second }}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                           
                            
                            <div class="card-footer">
                                <div class="row">
                                    @can(PermissionKey::Client['permissions']['update']['name'])
                                        <div class="col-lg-12 text-center">
                                            <button class="btn btn-default">Confirmar</button>
                                        </div>
                                    @endcan
                                </div>
                            </div>
                        </form>
                    </div>
				</div>
			</div>
        </div>
    </div>
@endsection