@extends('layouts.panel.app')
@section('content')
    <!-- Header -->
    @include('include.panel.header')
    <!-- Page content -->
    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col">
				<div class="card">
				<!-- Card header -->
					<div class="card-header border-0">
                        {{-- <h3 class="mb-0">Nuevo usuario</h3> --}}
                        <h6 class="heading-small text-muted mb-4">Información</h6>
					</div>
                    <!-- Light table -->
                    <div class="card-body">
                        <form action="{{ route('panel.clients.store') }}" method="POST" class="needs-validation" novalidate>
                            {{ csrf_field() }}
                            <div class="pl-lg-4">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="form-control-label" for="name">* Nombre</label>
                                            <input type="text" name="name" id="name" class="form-control" required autocomplete="off" value="{{ old('name') }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="form-control-label" for="surname">* Apellido</label>
                                            <input type="text" name="surname" id="surname" class="form-control" required autocomplete="off" value="{{ old('surname') }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="form-control-label" for="phone">* Teléfono</label>
                                            <input type="text" name="phone" id="phone" class="form-control" required autocomplete="off" value="{{ old('phone') }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="form-control-label" for="email">* Email</label>
                                            <input type="email" name="email" id="email" class="form-control" required autocomplete="off" value="{{ old('email') }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="form-control-label" for="lugar_nacimiento">Lugar de nacimiento</label>
                                            <input type="text" name="lugar_nacimiento" id="lugar_nacimiento" class="form-control" autocomplete="off" value="{{ old('lugar_nacimiento') }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="form-control-label" for="fecha_nacimiento">Fecha de nacimiento</label>
                                            <input type="text" name="fecha_nacimiento" id="fecha_nacimiento" class="form-control mydatepicker" autocomplete="off" value="{{ old('fecha_nacimiento') }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="form-control-label" for="ocupation">Ocupación</label>
                                            <input type="text" name="ocupation" id="ocupation" class="form-control" autocomplete="off" value="{{ old('ocupation') }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="form-control-label" for="address">Domicilio actual</label>
                                            <input type="text" name="address" id="address" class="form-control" autocomplete="off" value="{{ old('address') }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="form-control-label" for="municipio">Municipio</label>
                                            <input type="text" name="municipio" id="municipio" class="form-control" autocomplete="off" value="{{ old('municipio') }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="form-control-label" for="estado">Estado</label>
                                            <input type="text" name="estado" id="estado" class="form-control" autocomplete="off" value="{{ old('estado') }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="form-control-label" for="cp">CP</label>
                                            <input type="text" name="cp" id="cp" class="form-control" autocomplete="off" value="{{ old('cp') }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="form-control-label" for="nacionalidad">Nacionalidad</label>
                                            <input type="text" name="nacionalidad" id="nacionalidad" class="form-control" autocomplete="off" value="Mexicana">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="form-control-label" for="extranjero"></label>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" name="extranjero" value="0" id="extranjero">
                                                <label class="custom-control-label" for="extranjero">Marcar como extranjero</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <h5 class="heading-small text-muted mb-4">Segundo contacto</h5>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label class="form-control-label" for="title_second">Título contacto</label>
                                            <input type="text" name="title_second" id="title_second" class="form-control" autocomplete="off" value="{{ old('title_second') }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="form-control-label" for="name_second">Nombre</label>
                                            <input type="text" name="name_second" id="name_second" class="form-control" autocomplete="off" value="{{ old('name_second') }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="form-control-label" for="surname_second">Apellido</label>
                                            <input type="text" name="surname_second" id="surname_second" class="form-control" autocomplete="off" value="{{ old('surname_second') }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="form-control-label" for="phone_second">Teléfono</label>
                                            <input type="text" name="phone_second" id="phone_second" class="form-control" autocomplete="off" value="{{ old('phone_second') }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="form-control-label" for="email_second">Email</label>
                                            <input type="email" name="email_second" id="email_second" class="form-control" autocomplete="off" value="{{ old('email_second') }}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="row">
                                    <div class="col-lg-12 text-center">
                                        <button class="btn btn-default">Confirmar</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
				</div>
			</div>
        </div>
    </div>
@endsection