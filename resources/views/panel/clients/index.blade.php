@extends('layouts.panel.app')
@section('content')
    <!-- Header -->
    @include('include.panel.header')
    <!-- Page content -->
    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col">
				<div class="card">
				<!-- Card header -->
					<div class="card-header border-0">
						{{-- <h3 class="mb-0">Light table</h3> --}}
					</div>
                    <!-- Light table -->
					<div class="table-responsive pb-3">
						<table class="table align-items-center table-flush" id="dataTable">
							<thead class="thead-light">
								<tr>
									<th width="20px">
                                    </th>
									<th scope="col" class="sort" data-sort="status">Nombre</th>
									<th scope="col" class="sort" data-sort="status">Apellido</th>
									<th scope="col" class="sort" data-sort="status">Teléfono</th>
									<th scope="col" class="sort" data-sort="status">Correo eléctronico</th>
									<th scope="col" class="sort" data-sort="status">Responsable</th>
                                    <th scope="col" class="sort" data-sort="status">Número de Leads</th>
									<th scope="col">Acciones</th>
								</tr>
							</thead>
							<tbody class="list">
								@if ((isset($data)) && (count($data) > 0))
                                    @foreach ($data as $row)
                                        <tr>
                                            <td>
                                                <input type="checkbox" class="" id=""><span style="opacity:0;">{{ $row->id}}</span>
                                            </td>
                                            <td>{{ $row->name }}</td>
                                            <td>{{ $row->surname }}</td>
                                            <td>{{ $row->phone }}</td>
                                            <td>{{ $row->email }}</td>
                                            <td>{{ $row->admin()->name }}</td>
                                            <td>{{ $row->ventas()->count() }}</td>
                                            <td class="text-right">
                                                <div class="dropdown">
                                                    <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <i class="fas fa-ellipsis-v"></i>
                                                    </a>
                                                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                                        <a class="dropdown-item" href="{{ route('panel.clients.edit', ['cliente' => $row->id]) }}">Editar</a>

                                                         <a class="dropdown-item btn-delete" data-axios-method="delete" data-route="{{ route('panel.clients.destroy',
                                                             ['cliente' => $row->id]) }}" data-action="location.reload()" href="javascript:;">Eliminar</a>


                                                        <!-- 
                                                        <a href="{{ route('panel.clients.destroy',['cliente' => $row->id])}}" class="btn btn-danger" onclick="
                                                            var result = confirm('Are you sure you want to delete this record?');
                                                            
                                                            if(result){
                                                                event.preventDefault();
                                                                document.getElementById('delete-form').submit();
                                                            }">
                                                            Delete
                                                        </a> -->

            

                                                             <form method="POST" id="delete-form" action="{{route('panel.clients.destroy',  ['cliente' => $row->id])}}">
                                                            {{csrf_field()}}
                                                            <input type="hidden" name="_method" value="DELETE">
                                                            </form>  
                                                            
                                                        
                                                             <!-- Agregar el listado de las ventas asociadas con el cliente 17 de Junio 2022-->                                                                                
                                                        @foreach ($row->ventas() as $lead)
                                                            <a class="dropdown-item" >
                                                                    {{$lead->development()->name}} Lt 
                                                                    {{$lead->lote()->name}} 
                                                                    
                                                                
                                                                </a>                                                        
                                                        @endforeach                                  
                                                        <!--  -->                    


                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
							</tbody>
						</table>
					</div>
				</div>
			</div>
        </div>
    </div>
    
@endsection