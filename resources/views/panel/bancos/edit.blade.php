@extends('layouts.panel.app')
@section('content')
    <!-- Header -->
    @include('include.panel.header')
    <!-- Page content -->
    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col">
				<div class="card">
				<!-- Card header -->
					<div class="card-header border-0">
                        {{-- <h3 class="mb-0">Nuevo usuario</h3> --}}
                        <h6 class="heading-small text-muted mb-4">Información</h6>
					</div>
                    <!-- Light table -->
                    <div class="card-body">
                        <form action="{{ route('panel.bancos.update', ['desarrollo' => request()->desarrollo, 'banco' => request()->banco]) }}" method="POST" class="needs-validation" novalidate>
                            <input type="hidden" name="_method" value="PUT">
                            {{ csrf_field() }}
                            <div class="pl-lg-4">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="form-control-label" for="name">* Nombre</label>
                                            <input type="text" name="name" id="name" class="form-control" required autocomplete="off" value="{{ (old('name')) ? old('name') : $banco->name }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="form-control-label" for="n_cuenta">* Número de cuenta</label>
                                            <input type="text" name="n_cuenta" id="n_cuenta" class="form-control" required autocomplete="off" value="{{ (old('n_cuenta')) ? old('n_cuenta') : $banco->n_cuenta }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="form-control-label" for="referencia">* Referencia</label>
                                            <input type="text" name="referencia" id="referencia" class="form-control" required autocomplete="off" value="{{ (old('referencia')) ? old('referencia') : $banco->referencia }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="form-control-label" for="status">* Publicación</label>
                                            <select name="status" id="status" class="form-control">
                                                <option value="visible" {{ ($banco->status == 'visible') ? 'selected' : '' }}>Visible</option>
                                                <option value="hidden" {{ ($banco->status == 'hidden') ? 'selected' : '' }}>Oculto</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="row">
                                    <div class="col-lg-12 text-center">
                                        <button class="btn btn-default">Confirmar</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
				</div>
			</div>
        </div>
    </div>
@endsection