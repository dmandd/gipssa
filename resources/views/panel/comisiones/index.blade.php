@extends('layouts.panel.app')
@section('content')
    <!-- Header -->
    @include('include.panel.header')
    <!-- Page content -->
    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col">
				<div class="card">
				<!-- Card header -->
					<div class="card-header border-0">
						{{-- <h3 class="mb-0">Light table</h3> --}}
					</div>
                    <!-- Light table -->
                    <form id="form-filter">
                        <div class="pl-lg-4">
                            <div class="row">
                                @if(request()->user()->can(PermissionKey::Comission['permissions']['show_sidebar']['name']) && request()->user()->can(PermissionKey::ComissionSupervision['permissions']['show_sidebar']['name']))

                                    <div class="col-lg-2">
                                        <div class="form-group">
                                            <label class="form-control-label" for="type">Tipo</label>
                                            <select name="type" id="type" class="form-control">
                                                <option value="">Selecciona una opción</option>
                                                <option value="{{App\Comission::TYPE['ASESOR']}}" {{ (App\Comission::TYPE['ASESOR'] == request('type')) ? 'selected' : '' }}>Asesores</option>
                                                <option value="{{App\Comission::TYPE['SUPERVISOR']}}" {{ (App\Comission::TYPE['SUPERVISOR'] == request('type')) ? 'selected' : '' }}>Supervisores</option>
                                            </select>
                                        </div>
                                    </div>
                                @endif
                                @can(PermissionKey::Comission['permissions']['index']['name'])
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="form-control-label" for="admin_id">Asesor</label>
                                            <select name="admin_id" id="admin_id" class="form-control">
                                                <option value="">Selecciona una opción</option>
                                                @if (($admins) && (count($admins) > 0))
                                                    @foreach ($admins as $admin)
                                                        <option value="{{ $admin->id }}" {{ ($admin->id == request('admin_id')) ? 'selected' : '' }}>{{ $admin->name." ".$admin->last_name }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                @endcan
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label class="form-control-label" for="desarrollo">Desarrollo</label>
                                        <select class="form-control" name="desarrollo" id="desarrollo">
                                            <option value="">Selecciona una opción</option>
                                            @foreach (Development::all() as $dev)
                                                <option value="{{ $dev->id }}" {{ ($dev->id == request('desarrollo')) ? 'selected' : '' }}>{{ $dev->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label class="form-control-label" for="fecha">Periodo</label>
                                        <input type="text" placeholder="mm/aaaa" name="fecha" id="fecha" class="form-control mydatepicker2" value="{{ request('fecha') }}" autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <label class="form-control-label" for="corte">Corte</label>
                                        <select class="form-control" name="corte" id="corte">
                                            <option value="">Selecciona una opción</option>
                                            <option value="1" {{ "1" == request('corte') ? 'selected' : '' }}>Corte de 15 al 30 de este mes</option>
                                            <option value="2" {{ "2" == request('corte') ? 'selected' : '' }}>Corte del 1 al 15 del mes siguiente</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 text-center">
                                <button class="btn btn-default">Aplicar filtros</button>
                                <button class="btn btn-default" type="button" id="limpiar">Limpiar</button>
                            </div>
                        </div>
                    </form>
					<div class="table-responsive pb-3">
                    
                        

						<table class="table align-items-center table-flush" id="comissionDataTable">
							<thead class="thead-light">
								<tr>
                                    <th width="10px"></th>
									<th>Nombre</th>
                                    <th>No. de lote</th>
                                    <th>Desarrollo</th>
									<th>Etapa</th>
									<th>F. Apartado</th>
                                    <th>F. Primera mensualidad</th>
                                    <th>Comision neta</th>
                                    <th>F. Pago No.1</th>
                                    <th>F. Pago No.2</th>
                                    <th>F. Pago No.3</th>
                                    <th>F. Pago No.4</th>
                                    <th>F. Pago No.5</th>
                                    <th>F. Pago No.6</th>
                                    <th>F. Pago No.7</th>
                                    <th>F. Pago No.8</th>
                                    <th>F. Pago No.9</th>
                                    <th>F. Pago No.10</th>
                                    <th>Nota</th>
									<th>Acciones</th>
								</tr>
							</thead>
							<tbody class="list">
							</tbody>
						</table>
					</div>
                    <a class="dropdown-item btn-action"  style="display:none;" id="lockComission" data-title="¿Está seguro?" data-text="Este registro ya no sera visible en este modulo" data-icon="question" data-axios-method="get" data-action="location.reload()" href="javascript:;">Crear cliente</a>
				</div>
			</div>
        </div>
    </div>
    
@endsection
@push('js')
    <script>
        function lockComission(url){
            $("#lockComission").attr('data-route', url);
            let button = document.getElementById('lockComission');
            button.click();
        }

        $( document ).ready(function() {
            const getFechaPago = (row) =>{
                let payments = row.comission_payments;
                let fechaFormateada = "No aplica";
                if(payments.length > 0){
                    let payment = payments.shift();
                    let fecha = new Date(payment.payment_date);
                    let dia = fecha.getDate();
                    let mes = fecha.getMonth() + 1; 
                    let anio = fecha.getFullYear();

                    fechaFormateada = dia + '/' + mes + '/' + anio;
                    if(payment.paid){
                        fechaFormateada = `<span style="color:green;">${fechaFormateada}</span>`;
                    }
                }
                return fechaFormateada;
            }

            $(document).on('click','#limpiar',function(){
                window.location.href = "{{asset('')}}admin/comisiones";
            });

            $(".mydatepicker2").datepicker( {
                format: "mm/yyyy",
                startView: "months", 
                minViewMode: "months",
                autoclose: true
            });

            $('#comissionDataTable').DataTable({
                "language": {
                    "paginate": {
                    "previous": "<",
                    "next": ">"
                    },
                    "emptyTable": "No hay contenido disponible",
                    "info": "Mostrando desde _START_ hasta _END_ de _TOTAL_ registros",
                    "infoEmpty": "Mostrando desde 0 hasta 0 de 0 registros",
                    "lengthMenu": "Mostrar _MENU_ registros",
                    "loadingRecords": "Cargando...",
                    "processing":     "Procesando...",
                    "search":         "Buscar:",
                    "zeroRecords":    "No se encontraron registros",
                },
                "columnDefs": [ {
                    "orderable": false,
                    "className": 'select-checkbox',
                    "targets":   [0,8,9,10,11,12,13,14,15,16,17,19]
                } ],
                "select": {
                    "style": 'os',
                    "selector": 'td:first-child'
                },
                "order": [
                    [18, 'desc']
                ],
                processing: true,
                serverSide: true,
                ajax: {
                    url: "{{route('panel.comissions.getData')}}",
                    type: 'POST',
                },
                columns:[
                    {
                        data: null,
                        render: function(data, type, row) {
                            return '';
                        }
                    },
                    { 
                        data: null,
                        render: function(data, type, row) {
                            let name = row.name_admin;
                            if(row.type == 2)
                                name = name+"<br>"+"Supervisor: "+row.name_admin_supervisor;

                            return name
                        }
                    },
                    {
                        data: 'name_lote' 
                    },
                    {
                        data: 'name_desarrollo' 
                    },
                    { 
                        data: 'stage' 
                    },
                    { 
                        data: null,
                        render: function(data, type, row) {
                            let fechaOriginal = row.fecha_bloqueo;

                            let fecha = new Date(fechaOriginal);

                            let dia = fecha.getDate();
                            let mes = fecha.getMonth() + 1; 
                            let anio = fecha.getFullYear();

                            let fechaFormateada = dia + '/' + mes + '/' + anio;

                            return fechaFormateada;
                        }
                    },
                    { 
                        data: null,
                        render: function(data, type, row) {
                            let fechaOriginal = row.dia_pago;

                            let fecha = new Date(fechaOriginal);

                            let dia = fecha.getDate();
                            let mes = fecha.getMonth() + 1; 
                            let anio = fecha.getFullYear();

                            let fechaFormateada = dia + '/' + mes + '/' + anio;

                            return fechaFormateada;
                        }
                    },
                    { 
                        data: null,
                        render: function(data, type, row) {

                            const formatter = new Intl.NumberFormat('es-MX', {
                                style: 'currency',
                                currency: 'MXN',
                            });

                            let comision = formatter.format(row.pay_amount);

                            return comision;
                        }
                    },
                    { 
                        data: null,
                        render: function(data, type, row) {
                            return getFechaPago(row);
                        }
                    },
                    { 
                        data: null,
                        render: function(data, type, row) {
                            return getFechaPago(row);
                        }
                    },
                    { 
                        data: null,
                        render: function(data, type, row) {
                            return getFechaPago(row);
                        }
                    },
                    { 
                        data: null,
                        render: function(data, type, row) {
                            return getFechaPago(row);
                        }
                    },
                    { 
                        data: null,
                        render: function(data, type, row) {
                            return getFechaPago(row);
                        }
                    },
                    { 
                        data: null,
                        render: function(data, type, row) {
                            return getFechaPago(row);
                        }
                    },
                    { 
                        data: null,
                        render: function(data, type, row) {
                            return getFechaPago(row);
                        }
                    },
                    { 
                        data: null,
                        render: function(data, type, row) {
                            return getFechaPago(row);
                        }
                    },
                    { 
                        data: null,
                        render: function(data, type, row) {
                            return getFechaPago(row);
                        }
                    },
                    { 
                        data: null,
                        render: function(data, type, row) {
                            return getFechaPago(row);
                        }
                    },
                    { 
                        data: 'notes' 
                    },
                    {
    data: null,
    render: function(data, type, row) {
        let tipo = row.type;
        let buttons = '';

        if (tipo === 1) {
            // Add buttons for ASESOR tipo
            // Assuming lockComission function exists and works as intended
            buttons = `@can(PermissionKey::Comission['permissions']['edit']['name'])
                            <a class="dropdown-item" href="{{asset('')}}admin/ventas/${row['venta_id']}/edit">Ver</a>
                        @endcan
                        @can(PermissionKey::Comission['permissions']['destroy']['name'])
                            <a class="dropdown-item" href="javascript:;" onclick="lockComission('{{asset('')}}admin/comisiones/lock/${row['comission_id']}')">Bloquear comision</a>
                        @endcan`;
        } else if (tipo === 2) {
            // Add buttons for SUPERVISOR type
            buttons = `@can(PermissionKey::ComissionSupervision['permissions']['edit']['name'])
                            <a class="dropdown-item" href="{{asset('')}}admin/ventas/${row['venta_id']}/edit">Ver</a>
                        @endcan
                        @can(PermissionKey::ComissionSupervision['permissions']['destroy']['name'])
                            <a class="dropdown-item" href="javascript:;" onclick="lockComission('{{asset('')}}admin/comisiones/lock/${row['comission_id']}')">Bloquear comision</a>
                        @endcan`;
        }
        return `
            <div class="text-right">
                <div class="dropdown">
                    <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-ellipsis-v"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                        ${buttons}
                    </div>
                </div>
            </div>
        `;
    } 
}

                ],
            });

            
        });
    </script>
@endpush