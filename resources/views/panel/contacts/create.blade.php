@extends('layouts.panel.app')
@section('content')
    <!-- Header -->
    @include('include.panel.header')
    <!-- Page content -->
    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col">
				<div class="card">
				<!-- Card header -->
					<div class="card-header border-0">
                        {{-- <h3 class="mb-0">Nuevo usuario</h3> --}}
                        <h6 class="heading-small text-muted mb-4">Información</h6>
					</div>
                    <!-- Light table -->
                    <div class="card-body">
                        <form action="{{ route('panel.contacts.store') }}" method="POST" class="needs-validation" novalidate>
                            {{ csrf_field() }}
                            <div class="pl-lg-4">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="form-control-label" for="name">* Nombre</label>
                                            <input type="text" name="name" id="name" class="form-control" required autocomplete="off" value="{{ old('name') }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="form-control-label" for="type">* Tipo</label>
                                            <select name="type" id="type" class="form-control" required>
                                                <option value="">Selecciona una opción</option>
                                                <option value="propietario">Propietario</option>
                                                <option value="abogado">Abogado</option>
                                                <option value="representante">Representante</option>
                                                <option value="otro">Otro</option>
                                            </select>
                                        </div>
                                    </div>
                                    @can(PermissionKey::Contact['permissions']['assign']['name'])
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label class="form-control-label" for="admin_id">* Asignado</label>
                                                <select name="admin_id" id="admin_id" class="form-control" required>
                                                    <option value="">Selecciona una opción</option>
                                                    @if (($admins) && (count($admins) > 0))
                                                        @foreach ($admins as $admin)
                                                            <option value="{{ $admin->id }}" >{{ $admin->name }} {{ $admin->last_name }}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                    @endcan
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="form-control-label" for="phone">* Teléfono</label>
                                            <input type="text" name="phone" id="phone" class="form-control" required autocomplete="off" value="{{ old('phone') }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="form-control-label" for="email">* Correo</label>
                                            <input type="email" name="email" id="email" class="form-control" required autocomplete="off" value="{{ old('email') }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="form-control-label" for="address">* Dirección</label>
                                            <input type="text" name="address" id="address" class="form-control" required autocomplete="off" value="{{ old('address') }}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="row">
                                    <div class="col-lg-12 text-center">
                                        <button class="btn btn-default">Confirmar</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
				</div>
			</div>
        </div>
    </div>
@endsection