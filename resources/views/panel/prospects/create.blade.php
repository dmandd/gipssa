@extends('layouts.panel.app')
@section('content')
    <!-- Header -->
    @include('include.panel.header')
    <!-- Page content -->
    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col">
				<div class="card">
				<!-- Card header -->
					<div class="card-header border-0">
                        {{-- <h3 class="mb-0">Nuevo usuario</h3> --}}
                        <h6 class="heading-small text-muted mb-4">Información</h6>
					</div>
                    <!-- Light table -->
                    <div class="card-body">
                        <form action="{{ route('panel.prospects.store') }}" method="POST" class="needs-validation" novalidate>
                            {{ csrf_field() }}
                            <div class="pl-lg-4">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="form-control-label" for="name">* Nombre</label>
                                            <input type="text" name="name" id="name" class="form-control" required autocomplete="off" value="{{ old('name') }}">
                                        </div>
                                    </div>
                                    @can(PermissionKey::Prospect['permissions']['assign']['name'])
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label class="form-control-label" for="admin_id">* Asignado</label>
                                                <select name="admin_id" id="admin_id" class="form-control" required>
                                                    <option value="">Selecciona una opción</option>
                                                    @if (($admins) && (count($admins) > 0))
                                                        @foreach ($admins as $admin)
                                                            <option value="{{ $admin->id }}">{{ $admin->name }}</option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                    @endcan
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="form-control-label" for="phone">* Teléfono</label>
                                            <input type="text" name="phone" id="phone" class="form-control" required autocomplete="off" value="{{ old('phone') }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="form-control-label" for="email">* Correo</label>
                                            <input type="email" name="email" id="email" class="form-control" required autocomplete="off" value="{{ old('email') }}">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="form-control-label" for="priority">Prioridad</label>
                                            <select class="form-control" name="priority" id="priority">
                                                <option value="">Sin definir</option>
                                                <option value="high">Alta</option>
                                                <option value="medium">Media</option>
                                                <option value="low">Baja</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="form-control-label" for="avance">Avance</label>
                                            <select class="form-control" name="avance" id="avance">
                                                <option value="gris">Nuevo</option>
                                                <option value="verde">Potencial Cliente</option>
                                                <option value="amarillo">En seguimiento</option>
                                                <option value="rojo">Sin interés</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="form-control-label" for="desarrollo">Desarrollo</label>
                                            <select class="form-control" name="desarrollo" id="desarrollo">
                                                @foreach (Development::all() as $dev)
                                                    <option value="{{ $dev->slug() }}">{{ $dev->name }}</option>
                                                @endforeach
                                                <option value="inmobiliaria">Inmobiliaria</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="form-control-label" for="procedence">Procedencia</label>
                                            <select class="form-control" name="procedence" id="procedence">
                                                <option value="">Selecciona una opción</option>
                                                <option value="FB">Facebook</option>
                                                <option value="WA">WhatsApp</option>
                                                <option value="OR">Orgánico</option>
                                                <option value="RF">Referido</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label class="form-control-label" for="notes">Notas</label>
                                            <textarea name="notes" class="form-control" id="" cols="30" rows="10">{{ old('notes') }}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="row">
                                    <div class="col-lg-12 text-center">
                                        <button class="btn btn-default">Confirmar</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
				</div>
			</div>
        </div>
    </div>
@endsection