@extends('layouts.panel.app')
@section('content')
    <!-- Header -->
    @include('include.panel.header')
    <!-- Page content -->
    <div class="container-fluid mt--6">
        <div class="row">
            <div class="col">
				<div class="card">
				<!-- Card header -->
					<div class="card-header border-0">
						<div class="row">
                            <div class="col-8">
                                <h6 class="heading-small text-muted mb-4">Información</h6>
                            </div>
                            <div class="col-4 text-right">


                            @can(PermissionKey::Prospect['permissions']['index']['name'])
                                    <div class="dropdown">
                                           
                                        <a class="btn btn-default font-normal" href="{{ route('panel.prospects.index',['vista'=>'tabla','usuario'=>Auth::id(),'todos'=>'SI']) }}">Listado</a>  <br>                                                           
                                        
                                      
                                    </div>
                                @endcan
                        

                                @can(PermissionKey::Prospect['permissions']['import']['name'])
                                    <div class="dropdown">
                                        <a href="#!" class="btn btn-default" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Opciones</a>
                                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow" >
                                           
                                        
                                        
                                            <button type="button" class="dropdown-item" data-toggle="modal" data-target="#modal-form">Importar</button>
                                            <a class="dropdown-item" href="{{ route('panel.prospects.export', ['admin_id' => 0]) }}">Exportar</a>
                                        </div>
                                    </div>
                                @endcan
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="row">
                            @foreach ($users as $user)
                              @if ($user->prospects()->count()>=0)      <!-- Solo si tienen asiganos leads-->                            
                                <div class="col-lg-3 col-12">
                                    <a href="{{ route('panel.prospects.index', ['usuario' => $user->id]) }}">
                                        <div class="card card-stats">
                                            <!-- Card body -->
                                            <div class="card-body">
                                                <div class="row">
                                                    <div class="col">
                                                        <h5 class="card-title text-uppercase text-muted mb-0">{{ $user->name }} {{ $user->last_name }}</h5>
                                                        <span class="h2 font-weight-bold mb-0">{{ $user->prospects()->count() }} leads asignados</span>
                                                    </div>
                                                    <div class="col-auto">
                                                        <div class="icon icon-shape bg-secondary text-white rounded-circle shadow">
                                                            <img src="{{ asset($user->avatar) }}" alt="avatar" 
                                                            
                                                            style="width:100%;">
                                                        </div>
                                                    </div>
                                                </div>
                                                    <p class="mt-3 mb-0 text-sm">
                                                        <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> {{ $user->rendimiento() }} %</span>
                                                        <span class="text-nowrap">Desde el último mes</span>
                                                    </p>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                @endif  
                            @endforeach
                        </div>
                    </div>
				</div>
			</div>
        </div>
    </div>
    <div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledby="modal-form" aria-hidden="true">
        <div class="modal-dialog modal- modal-dialog-centered modal-sm" role="document">
            <div class="modal-content">   
                <div class="modal-body p-0">                
                    <div class="card bg-secondary border-0 mb-0">
                        <div class="card-body px-lg-5 py-lg-5">
                            <div class="text-center text-muted mb-4">
                                <small>Selecciona el archivo</small>
                            </div>
                            <form method="POST" action="{{ route('panel.prospects.import') }}" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="form-group mb-3">
                                    <label for="">Usuario</label>
                                    <select name="admin_id" id="admin_id" class="form-control" required>
                                        <option value="">Selecciona una opción</option>
                                        @foreach (Admin::all() as $admin)
                                            <option value="{{ $admin->id }}">{{ $admin->name }} {{ $admin->last_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group mb-3">
                                    <div class="input-group input-group-merge input-group-alternative">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="ni ni-folder-17"></i></span>
                                        </div>
                                        <input class="form-control" placeholder="Ingrese el archivo" type="file" name="import" required>
                                    </div>
                                </div>
                                <div class="text-center">
                                    <button class="btn btn-primary my-4">Confirmar</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection