<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Panel Administrativo para controlar los recursos">
    <meta name="author" content="Publica mi empresa">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Panel</title>
    <!-- Favicon -->
    <link rel="icon" href="{{asset('panel/img/brand/favicon.svg')}}" type="image/svg">
    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
    <!-- Icons -->
    <link rel="stylesheet" href="{{asset('panel/vendor/nucleo/css/nucleo.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('panel/vendor/@fortawesome/fontawesome-free/css/all.min.css')}}" type="text/css">
    <!-- Argon CSS -->
    <link rel="stylesheet" href="{{asset('panel/css/main.css?v=1.2.0')}}" type="text/css">
    @stack('style')
</head>

<body>
    @include('include.panel.sidebar')
    <!-- Main content -->
    <div class="main-content" id="panel">
        @yield('content')
        <div id="bin-delete" class="position-fixed" style="right:35px;bottom:20px;display:none;">
            <button class="btn btn-lg btn-icon btn-danger" type="button" style="border-radius:50px;font-size: 35px;padding: 8px 19px;" title="Eliminar">
                <span class="btn-inner--icon">
                    <i class="fa fa-trash"></i>
                </span>
            </button>
        </div>
    </div>
    <script>
        const PATH = '{{ asset('') }}';
    </script>
    <script src="{{asset('panel/js/main.js?v=1.2.3')}}"></script>
    @stack('js')
</body>

</html>