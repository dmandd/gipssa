<!--header starts-->
<header class="header transparent scroll-hide screenContent">
    <!--Main Menu starts-->
    <div class="site-navbar-wrap {{ ((request()->is('aviso-privacidad')) || (request()->is('propiedades/*')) || (request()->is('contacto'))) ? 'v1' : 'v1' }}">
        <div class=" row">
            <div class="site-navbar col-lg-8">
            </div>
            <div class="col-lg-2 displayMobil mt-2">
                <a class="g-header-social mr-1" href="{{ (request()->is('propiedades')) ? 'https://www.facebook.com/GipssaInmobiliaria' : 'https://www.facebook.com/gipssadesarrollos'}} " target="_blank" ><i class="g-header-social-fb fab fa-facebook-f fa-2x"></i></a>
                <a class="g-header-social" href="{{ (request()->is('propiedades')) ? 'https://www.instagram.com/gipssainmobiliaria/':'https://www.instagram.com/gipssadesarrollos/'}} " target="_blank" ><i class="g-header-social-ig fab fa-instagram fa-2x"></i></a>
            </div>
        </div>
    </div>
    <!--Main Menu ends-->
</header>
<!--Header ends-->