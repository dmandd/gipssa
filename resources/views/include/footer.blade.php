<!--Footer Starts-->
<div class="footer-wrapper v1 g-grey-color-area screenContent">
        
    <div class="footer-top-area" id="nosotros">
        <div class="container">
           <div class="row">
                <div class="col-lg-12 center-block">
                    <div class="text-center g-footer-title">
                        <h2>CONTÁCTANOS</h2>
                        <hr class="col-lg-4 g-footer-hr"/>
                    </div>
                </div>
                <div class="col-lg-12 pt-5 pb-5">
                    <div class="row pb-5">
                        <div class="col-lg-4 text-center g-footer-subtitle pt-3">
                                <i class="fas fa-mobile-alt fa-2x mb-3"></i>
                                <h3>TELÉFONOS</h3>
                                <div class="center-block">
                                    <div class="contact-info">
                                        <div><i class="fas fa-phone-alt mr-2"></i><a href="tel:9994310411">9994 31 04 11</a></div>
                                    </div>
                                    <div class="contact-info">
                                        <div><i class="fas fa-mobile-alt mr-2"></i><a href="tel:9994707744">9994 70 77 44</a></div>
                                    </div>
                                </div>
                        </div>
                        <div class="col-lg-4 d-flex">
                                <hr class="g-footer-vr"/>
                                <div class="col-lg-12 text-center g-footer-subtitle pt-3">
                                    <i class="fas fa-map-marker-alt fa-2x mb-3"></i>
                                    <h3>ENCUÉNTRANOS</h3>
                                    <p>Calle 36 Núm. 287 por 33 y 35,<br>Col. San Ramón Norte.<br>C.P. 97117</p>
                                </div>
                                
                                <hr class="g-footer-vr"/>
                            </div>
                            <div class="col-lg-4 text-center g-footer-subtitle pt-3">
                                <i class="fas fa-envelope fa-2x mb-3"></i>
                                <h3>ESCRÍBENOS</h3>
                                <div class="g-footer-mail">
                                    <a href="mailto:contacto@gipssa.com"><p>contacto@gipssa.com</a>
                                </div>
                            </div>
                    </div>
                </div>
           </div>
        </div>
    </div>
</div>
<div class="footer-wrapper v1 screenContent" style="background-image: url(https://crm.gipssa.com/assets/images/FONDO5.jpg); width='100%'">
        
    <div class="footer-top-area">
        <div class="container">
            <div class="col-lg-12 g-footer-contact">
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <img src="https://crm.gipssa.com/assets/images/inicio-logo-contacto.png" alt="logo" class="img-fluid">
                    </div>
                    <div class="col-lg-6 offset-lg-3 text-center pt-3">
                        <h2>Si tienes alguna duda<br>¡Escribenos!</h2>
                    </div>
                    <div class="col-lg-4 offset-lg-4">
                        <form action="{{ route('sendcustomermail') }}" method="post"  class="c">
                            <div class="form-control-wrap">
                                <div id="message" class="alert alert-danger alert-dismissible fade"></div>
                                <div class="form-group">
                                    <input type="text" class="form-control" id="fname" placeholder="Nombre*" name="fname">
                                </div>
                                <div class="form-group">
                                    <input type="number" class="form-control" id="fphone" placeholder="Teléfono" name="fphone">
                                </div>
                                <div class="form-group">
                                    <input type="email" class="form-control" id="email_address" placeholder="Correo electrónico*" name="email">
                                </div>
                                <div class="form-group">
                                    <textarea class="form-control" rows="8" name="comment" id="comment" placeholder="Mensaje"></textarea>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="col-lg-8 offset-lg-2 btn ">Enviar</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
    <div class="footer-bottom-area g-grey-color-area" >
        <div class="container pr-5 pl-5">
            <div class="row">
                <div class="col-md-8 offset-md-2 g-footer-privacy  ">
                    <p>
                        © gipssa.com 2022. Todos los derechos reservados. 
                        <a href="{{ route('aviso') }}">Aviso de privacidad</a>
                    </p>

                </div>
                <div class="col-lg-2 col-md-12 col-sm-12 text-center">
                    <a class="g-footer-social mr-1" href="{{ (request()->is('propiedades')) ? 'https://www.facebook.com/GipssaInmobiliaria' : 'https://www.facebook.com/gipssadesarrollos'}}" target="_blank" ><i class="g-footer-social-fb fab fa-facebook-f fa-2x"></i></a>
                    <a class="g-footer-social" href="{{ (request()->is('propiedades')) ? 'https://www.instagram.com/gipssainmobiliaria/':'https://www.instagram.com/gipssadesarrollos/'}}" target="_blank" ><i class="g-footer-social-ig fab fa-instagram fa-2x"></i></a>
                </div>
            </div>
        </div>
    </div>
</div>
<!--Footer ends-->