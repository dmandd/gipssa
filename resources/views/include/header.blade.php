<!--header starts-->
<header class="header transparent scroll-hide screenContent">
    <!--Main Menu starts-->
    <div class="site-navbar-wrap {{ ((request()->is('aviso-privacidad')) || (request()->is('propiedades/*')) || (request()->is('contacto'))) ? 'v1' : 'v1' }}">
        <div class=" row">
            <div class="site-navbar col-lg-8">
                <div class="row ">
                    {{--<div class="col-lg-2 col-md-6 col-9 order-2 order-xl-1 order-lg-1 order-md-1">
                        @if ((request()->is('aviso-privacidad')) || (request()->is('propiedades/*')) || (request()->is('contacto')))
                            <a class="navbar-brand" href="{{ route('home') }}"><img src="{{ asset('assets/images/logo-verde.png') }}" alt="logo" class="img-fluid"></a>
                        @else
                            <a class="navbar-brand" href="{{ route('home') }}"><img src="{{ asset('assets/images/logo.png') }}" alt="logo" class="img-fluid"></a>
                        @endif
                    </div>--}}
                    <div class="col-lg-11 pl-xs-0">
                        <nav class="site-navigation text-right">
                            <div class="">
                                <ul class="site-menu js-clone-nav d-none d-lg-block g-menu" >
                                    <li class="has-children">
                                        <a href="{{ route('home') }}">INICIO</a>
                                    </li>
                                    <li class="has-children">
                                        <a href="{{ route('aboutUs') }}">NOSOTROS</a>
                                    </li>
                                    {{-- <li class="has-children">
                                        <a href="#about-us">Nosotros</a>
                                        <ul class="dropdown">
                                            <li class="has-children">
                                                <a href="#">List Layout</a>
                                                <ul class="dropdown sub-menu">
                                                    <li><a href="list-fullwidth.html">Full Width</a></li>
                                                    <li><a href="list-fullwidth-map.html">Fullwidth with google map</a></li>
                                                    <li><a href="list-fullwidth-leaflet-map.html">Fullwidth with Leaflet map</a></li>
                                                    <li><a href="list-left-sidebar.html">Left Sidebar</a></li>
                                                    <li><a href="list-right-sidebar.html">Right Sidebar</a></li>
                                                </ul>
                                            </li>
                                            <li class="has-children">
                                                <a href="#">Grid Layout</a>
                                                <ul class="dropdown sub-menu">
                                                    <li><a href="grid-fullwidth.html">Full Width</a></li>
                                                    <li><a href="grid-fullwidth-map.html">Fullwidth with google map</a></li>
                                                    <li><a href="grid-fullwidth-leaflet-map.html">Fullwidth with leaflet map</a></li>
                                                    <li><a href="grid-two-column.html">Grid two column</a></li>
                                                    <li><a href="grid-left-sidebar.html">Left Sidebar</a></li>
                                                    <li><a href="grid-right-sidebar.html">Right Sidebar</a></li>
                                                </ul>
                                            </li>
                                            <li class="has-children">
                                                <a href="#">Map Layout - google</a>
                                                <ul class="dropdown sub-menu">
                                                    <li><a href="list-left-sidemap.html">List Left sidemap</a></li>
                                                    <li><a href="list-right-sidemap.html">List Right sidemap</a></li>
                                                    <li><a href="grid-left-sidemap.html">Grid Left sidemap</a></li>
                                                    <li><a href="grid-right-sidemap.html">Grid Right sidemap</a></li>
                                                    <li><a href="grid-search-half-map.html">advanced search</a></li>
                                                </ul>
                                            </li>
                                            <li class="has-children">
                                                <a href="#">Tab Layout</a>
                                                <ul class="dropdown sub-menu">
                                                    <li><a href="tab-fullwidth.html">Tab fullwidth</a></li>
                                                    <li><a href="tab-left-sidebar.html">Tab left sidebar</a></li>
                                                    <li><a href="tab-right-sidebar.html">Tab right sidebar</a></li>
                                                </ul>
                                            </li>
                                            <li class="has-children">
                                                <a href="#">Map Layout - Leaflet</a>
                                                <ul class="dropdown sub-menu">
                                                    <li><a href="list-left-leaflet-sidemap.html">List Left sidemap</a></li>
                                                    <li><a href="list-right-leaflet-sidemap.html">List Right sidemap</a></li>
                                                    <li><a href="grid-left-leaflet-sidemap.html">Grid Left sidemap</a></li>
                                                    <li><a href="grid-right-leaflet-sidemap.html">Grid Right sidemap</a></li>
                                                    <li><a href="grid-search-half-leafletmap.html">advanced search</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </li> --}}
                                    <li class="has-children">
                                        <a href="{{ route('propiedades') }}">INMOBILIARIA</a>
                                        <ul class="dropdown sub-menu">
                                            @if ($types = PropertyType::where('status', 'visible')->get())
                                                @foreach ($types as $_t)
                                                    <li><a href="{{ route('propiedades', ['tipo' => $_t->slug]) }}">{{ $_t->name }} </a></li>
                                                @endforeach
                                            @endif
                                        </ul>
                                    </li>
                                    <li class="has-children">
                                        <a href="{{ route('desarrollo')}}">DESARROLLOS</a>
                                        <ul class="dropdown sub-menu">
                                            <li><a href="https://landing.gipssa.com/kukulkan/adw" target="_blank">Kukulkán</a></li>
                                            
                                            <li><a href="http://grupocs.gipssa.com/nerea/" target="_blank">Nerea</a></li>
                                            
                                            <li><a href="http://grupocs.gipssa.com/punta-venado/" target="_blank">Punta venado</a></li>
                                        </ul>
                                    </li>
                                    {{--<li class="has-children">
                                        <a href="#">CONSTRUCCIÓN</a>
                                        <ul class="dropdown sub-menu">
                                            <li><a href="{{ route('building') }}">Edificio</a></li>
                                            <li><a href="{{ route('building') }}">Construcción</a></li>
                                            <li><a href="{{ route('building') }}">Remodelación</a></li>
                                        </ul>
                                    </li>--}}
                                    <li class="has-children">
                                        <a href="{{ route('contact') }}">CONTACTO</a>
                                    </li>
                                </ul>
                            </div>
                        </nav>
                        <div class="d-lg-none sm-left ml-4 ">
                            <a href="#" class="mobile-bar js-menu-toggle">
                                <span class="lnr lnr-menu"></span>
                            </a>
                        </div>
                        <!--mobile-menu starts -->
                        <div class="site-mobile-menu">
                            <div class="site-mobile-menu-header">
                                <div class="site-mobile-menu-close  js-menu-toggle">
                                    <span class="lnr lnr-cross"></span> </div>
                            </div>
                            <div class="site-mobile-menu-body"></div>
                        </div>
                        <!--mobile-menu ends-->
                    </div>
                    
                </div>
                
            </div>
            <div class="col-lg-2 displayMobil mt-2">
                        <a class="g-header-social mr-1" href="{{ (request()->is('propiedades')) ? 'https://www.facebook.com/GipssaInmobiliaria' : 'https://www.facebook.com/gipssadesarrollos'}} " target="_blank" ><i class="g-header-social-fb fab fa-facebook-f fa-2x"></i></a>
                        <a class="g-header-social" href="{{ (request()->is('propiedades')) ? 'https://www.instagram.com/gipssainmobiliaria/':'https://www.instagram.com/gipssadesarrollos/'}} " target="_blank" ><i class="g-header-social-ig fab fa-instagram fa-2x"></i></a>
                    </div>
        </div>
    </div>
    <!--Main Menu ends-->
</header>
<!--Header ends-->