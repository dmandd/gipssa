<!-- Sidenav -->
@php
	use App\Configuration;
	$maintenance = Configuration::find(1);
@endphp
<style focused>
	.bg-side-menu{
		background-image: url("{{ asset('panel/img/MARCA-DE-AGUA-CRM-INT.png') }}"), url("{{ asset('panel/img/FONDO-MENU-CRM-INT.png') }}");
		background-position: left bottom, center;
		background-repeat: no-repeat;
		background-size: 100%,cover;
	}
</style>
<nav class="sidenav navbar navbar-vertical  fixed-left  navbar-expand-xs navbar-light bg-side-menu" id="sidenav-main">
	<div class="scrollbar-inner">
	<!-- Brand -->
	<div class="sidenav-header  align-items-center">
		<a class="navbar-brand" href="javascript:void(0)">
			<img src="{{asset('panel/img/brand/logo-verde.png')}}" class="navbar-brand-img" alt="...">
		</a>
	</div>
	<div class="navbar-inner">
	
		<!-- New Menu -->
		<div class="collapse navbar-collapse" id="sidenav-collapse-main">
			<!-- Nav items -->
			<ul class="navbar-nav">

			    @can(PermissionKey::Event['permissions']['show_sidebar']['name'])
					<li class="nav-item">
						<a class="nav-link {{ (request()->is('admin/eventos*')) ? 'active' : '' }}" href="{{ route('panel.events.index') }}">
						<i class="ni ni-calendar-grid-58 text-default"></i>
						<span class="nav-link-text">Calendario</span>
						</a>
					</li>
				@endcan

		<!-- Desarrollos -->		

		<li class="nav-item">
						<a class="nav-link {{ (request()->is('admin/desarrollos*') || request()->is('admin/clientes*') || request()->is('admin/prospectos*')) ? 'active' : '' }}"
						 href="#navbar-catalogos" data-toggle="collapse" role="button" 
						 aria-expanded="{{ (request()->is('admin/clientes*') || request()->is('admin/desarrollos*') || request()->is('admin/prospectos*')) ? 'true' : 'false' }}" 
						 aria-controls="navbar-catalogos">
							<i class="ni ni-books text-default"></i>
							<span class="nav-link-text">Desarrollos</span>
						</a>
			<div class="collapse" id="navbar-catalogos">
				<ul class="nav nav-sm flex-column">
				<!-- Ejecutivos -->
				@if(request()->user()->can(PermissionKey::Venta['permissions']['index']['name']))

				


					<li class="nav-item">
						<a class="nav-link" 
						href="#navbar-ejecutivos" data-toggle="collapse" 
						role="button" aria-expanded="{{ (request()->is('admin/clientes*') || request()->is('admin/desarrollos*') || request()->is('admin/prospectos*')) ? 'true' : 'false' }}" 
						aria-controls="navbar-catalogos">
							<i class="ni ni-folder-17 text-default"></i>
							<span class="nav-link-text">Ejecutivos</span>
						</a>

						<div class="collapse {{ (request()->is('admin/desarrollos*') || request()->is('admin/clientes*') || request()->is('admin/prospectos*')) ? 'show' : '' }}" id="navbar-ejecutivos">
							<ul class="nav nav-sm flex-column">
								@can(PermissionKey::Venta['permissions']['internos']['name'])
									<li class="nav-item">
										<a class="nav-link {{ (request()->is('venta/cuentas/usuarios*')) ? 'active' : '' }}" href="{{ route('panel.ejecutives.index') }}">
											<span class="nav-link-text">Internos</span>
										</a>
									</li>
								@endcan
								<!-- @can(PermissionKey::Client['permissions']['index']['name'])
									<li class="nav-item">
										<a class="nav-link {{ (request()->is('admin/clientes*')) ? 'active' : '' }}" href="{{ route('panel.clients.index') }}">
											<span class="nav-link-text">Externos</span>
										</a>
									</li>
								 @endcan -->
								
							</ul>
						</div>
						
					</li>
				@endif
				<!--Ventas  -->
				@if(request()->user()->can(PermissionKey::Development['permissions']['index']['name']) 
				|| request()->user()->can(PermissionKey::Client['permissions']['index']['name']))
					<li class="nav-item">
						<a class="nav-link {{ (request()->is('admin/desarrollos*') || request()->is('admin/clientes*') || request()->is('admin/prospectos*')) ? 'active' : '' }}" href="#navbar-ventas" data-toggle="collapse" role="button" aria-expanded="{{ (request()->is('admin/clientes*') || request()->is('admin/desarrollos*') || request()->is('admin/prospectos*')) ? 'true' : 'false' }}" aria-controls="navbar-catalogos">
							<i class="ni ni-folder-17 text-default"></i>
							<span class="nav-link-text">Ventas</span>
						</a>
						<div class="collapse {{ (request()->is('admin/desarrollos*') || request()->is('admin/clientes*') || request()->is('admin/prospectos*')) ? 'show' : '' }}" id="navbar-ventas">
							<ul class="nav nav-sm flex-column">
								<!-- @can(PermissionKey::Prospect['permissions']['show_sidebar']['name']) -->
									<li class="nav-item">
										<a class="nav-link {{ (request()->is('admin/prospectos*')) ? 'active' : '' }}" href="{{ route('panel.prospects.index') }}">
											<span class="nav-link-text">Leads</span>
										</a>
									</li>
								<!-- @endcan -->
								<!-- @can(PermissionKey::Client['permissions']['index']['name']) -->
									<li class="nav-item">
										<a class="nav-link {{ (request()->is('admin/clientes*')) ? 'active' : '' }}" href="{{ route('panel.clients.index') }}">
											<span class="nav-link-text">Clientes</span>
										</a>
									</li>
								<!-- @endcan -->
					
								
							</ul>
						</div>
					</li>
				@endif
				<!-- Disponibilidad -->										
				@can(PermissionKey::Venta['permissions']['show_sidebar']['name'])
										<li class="nav-item">
											<a class="nav-link {{ ((request()->is('admin/ventas*'))) ? 'active' : '' }}" href="#navbar-disponibilidad" 
											data-toggle="collapse" role="button" aria-expanded="{{ ((request()->is('admin/ventas*'))) ? 'true' : 'false' }}" aria-controls="navbar-ventas">
												<i class="ni ni-folder-17 text-default"></i>
												<span class="nav-link-text">Disponibilidad</span>
											</a>
											<div class="collapse {{ ((request()->is('admin/ventas*'))) ? 'show' : '' }}" id="navbar-disponibilidad">
												<ul class="nav nav-sm flex-column">
													@foreach (Development::where('status', 'visible')->get() as $desarrollo)
														<li class="nav-item">
															
																<ul class="nav nav-sm flex-column">
																	<li class="nav-item">
																		<a href="{{ route('panel.ventas.create', ['desarrollo' => $desarrollo->id]) }}" class="nav-link">
																			<span class="nav-link-text"> {{ mb_strtoupper($desarrollo->name) }} ETAPA {{ mb_strtoupper($desarrollo->stage) }}</span>
																		</a>
																		
																	</li>
																</ul>
														</li>
													@endforeach
												</ul>
											</div>
										</li>
				@endcan
				<!--Seguimiento  -->
				@if(request()->user()->can(PermissionKey::Development['permissions']['index']['name']) 
				|| request()->user()->can(PermissionKey::Client['permissions']['index']['name']))
					<li class="nav-item">
						<a class="nav-link {{ (request()->is('admin/desarrollos*') || request()->is('admin/clientes*') || request()->is('admin/prospectos*')) ? 'active' : '' }}" href="#navbar-seguimiento" data-toggle="collapse" role="button" aria-expanded="{{ (request()->is('admin/clientes*') || request()->is('admin/desarrollos*') || request()->is('admin/prospectos*')) ? 'true' : 'false' }}" aria-controls="navbar-catalogos">
							<i class="ni ni-folder-17 text-default"></i>
							<span class="nav-link-text">Seguimiento</span>
						</a>
						<div class="collapse {{ (request()->is('admin/desarrollos*') || request()->is('admin/clientes*') || request()->is('admin/prospectos*')) ? 'show' : '' }}" id="navbar-seguimiento">
							<ul class="nav nav-sm flex-column">
								<!-- @can(PermissionKey::Prospect['permissions']['show_sidebar']['name']) -->
									
								@foreach (Development::where('status', 'visible')->get() as $desarrollo)
														<li class="nav-item">
															
																<ul class="nav nav-sm flex-column">
																	<li class="nav-item">
																		
																		<a href="{{ route('panel.ventas.index', ['desarrollo' => $desarrollo->id]) }}" class="nav-link">
																			<span class="nav-link-text">{{ mb_strtoupper($desarrollo->name) }} ETAPA {{ mb_strtoupper($desarrollo->stage) }}</span>
																		</a>
																		
																	</li>
																</ul>
														</li>
													@endforeach
									
								<!-- @endcan -->
								
								
							</ul>
						</div>
					</li>
				@endif

				@if(request()->user()->can(PermissionKey::Comission['permissions']['show_sidebar']['name']) || request()->user()->can(PermissionKey::ComissionSupervision['permissions']['show_sidebar']['name']))

					@php
						$type = App\Comission::TYPE['ASESOR'];
						if(!request()->user()->can(PermissionKey::Comission['permissions']['show_sidebar']['name']) && request()->user()->can(PermissionKey::ComissionSupervision['permissions']['show_sidebar']['name'])){
							$type = App\Comission::TYPE['SUPERVISOR'];
						}
					@endphp

					<li class="nav-item">
						<a class="nav-link {{ (request()->is('admin/comisiones*')) ? 'active' : '' }}" href="{{ route('panel.comisiones.index', ['type' => $type ]) }}">
							<i class="ni ni-folder-17 text-default"></i>
							<span class="nav-link-text">Historial de comisiones</span>
						</a>
					</li>
				@endif

		
						<!--Publicidad  -->
				@if(request()->user()->can(PermissionKey::Development['permissions']['index']['name']) 
				|| request()->user()->can(PermissionKey::Client['permissions']['index']['name']))
					<li class="nav-item">
						<a class="nav-link {{ (request()->is('admin/desarrollos*') || request()->is('admin/clientes*') || request()->is('admin/prospectos*')) ? 'active' : '' }}"
						 href="#navbar-publicidad" data-toggle="collapse" role="button" aria-expanded="{{ (request()->is('admin/clientes*') || request()->is('admin/desarrollos*') || request()->is('admin/prospectos*')) ? 'true' : 'false' }}" aria-controls="navbar-catalogos">
							<i class="ni ni-folder-17 text-default"></i>
							<span class="nav-link-text">Publicidad</span>
						</a>
						<div class="collapse {{ (request()->is('admin/desarrollos*') || request()->is('admin/clientes*') || request()->is('admin/prospectos*')) ? 'show' : '' }}" id="navbar-publicidad">
							<ul class="nav nav-sm flex-column">
								<!-- @can(PermissionKey::Prospect['permissions']['show_sidebar']['name']) -->
									<li class="nav-item pointer-events:none">
										<a class="nav-link {{ (request()->is('admin/prospectos*')) ? 'active' : '' }}"  href="" >
											<span class="nav-link-text">Brochure</span>
										</a>
									</li>
								<!-- @endcan -->
								<!-- @can(PermissionKey::Client['permissions']['index']['name']) -->
									<li class="nav-item">
										<a class="nav-link {{ (request()->is('admin/clientes*')) ? 'active' : '' }}" href="#">
											<span class="nav-link-text">Imagenes</span>
										</a>
									</li>
								<!-- @endcan -->
									<!-- @can(PermissionKey::Client['permissions']['index']['name']) -->
									<li class="nav-item">
										<a class="nav-link {{ (request()->is('admin/clientes*')) ? 'active' : '' }}" href="#">
											<span class="nav-link-text">Promocionales</span>
										</a>
									</li>
								<!-- @endcan -->
								<!-- @can(PermissionKey::Client['permissions']['index']['name']) -->
								<li class="nav-item">
										<a class="nav-link {{ (request()->is('admin/clientes*')) ? 'active' : '' }}" href="#">
											<span class="nav-link-text">Videos</span>
										</a>
									</li>
								<!-- @endcan -->
								
							</ul>
						</div>
					</li>
				@endif
				
																
				</ul>
			</div>
		</li>
			
							
				
				
				



			
				

						

				<!--Inmobiliria -->
				@if(request()->user()->can(PermissionKey::Property['permissions']['show_sidebar']['name']) || 
				request()->user()->can(PermissionKey::Zone['permissions']['index']['name']) ||
				request()->user()->can(PermissionKey::Operation['permissions']['index']['name']) ||
				request()->user()->can(PermissionKey::Tag['permissions']['index']['name']) ||
				request()->user()->can(PermissionKey::Contact['permissions']['index']['name']) ||
				request()->user()->can(PermissionKey::PropertyType['permissions']['index']['name']) ||
				request()->user()->can(PermissionKey::Equipment['permissions']['index']['name']))
					<li class="nav-item">
						<a class="nav-link {{ ((request()->is('admin/propiedades*')) || (request()->is('admin/zonas*')) || (request()->is('admin/etiquetas*')) || (request()->is('admin/areas*')) || (request()->is('admin/equipamiento*')) || (request()->is('admin/contactos*'))) ? 'active' : '' }}" href="#navbar-propiedades" data-toggle="collapse" role="button" aria-expanded="{{ ((request()->is('admin/propiedades*')) || (request()->is('admin/zonas*')) || (request()->is('admin/etiquetas*')) || (request()->is('admin/areas*')) || (request()->is('admin/equipamiento*')) || (request()->is('admin/contactos*'))) ? 'true' : 'false' }}" aria-controls="navbar-propiedades">
							<i class="ni ni-building text-default"></i>
							<span class="nav-link-text">Inmobiliaria</span>
						</a>
						<div class="collapse {{ ((request()->is('admin/propiedades*')) || (request()->is('admin/zonas*')) || (request()->is('admin/etiquetas*')) || (request()->is('admin/areas*')) || (request()->is('admin/equipamiento*')) || (request()->is('admin/contactos*'))) ? 'show' : '' }}" id="navbar-propiedades">
							<ul class="nav nav-sm flex-column">
								@can(PermissionKey::Zone['permissions']['index']['name'])
									<li class="nav-item">
										<a class="nav-link {{ (request()->is('admin/zonas*')) ? 'active' : '' }}" href="{{ route('panel.zones.index') }}">
											<span class="nav-link-text">Zonas</span>
										</a>
									</li>
								@endcan
								@can(PermissionKey::PropertyType['permissions']['index']['name'])
									<li class="nav-item">
										<a class="nav-link {{ (request()->is('admin/tipos*')) ? 'active' : '' }}" href="{{ route('panel.types.index') }}">
											<span class="nav-link-text">Tipos</span>
										</a>
									</li>
								@endcan
								@can(PermissionKey::Operation['permissions']['index']['name'])
									<li class="nav-item">
										<a class="nav-link {{ (request()->is('admin/operaciones*')) ? 'active' : '' }}" href="{{ route('panel.operations.index') }}">
											<span class="nav-link-text">Operaciones</span>
										</a>
									</li>
								@endcan
								@can(PermissionKey::Tag['permissions']['index']['name'])
									<li class="nav-item">
										<a class="nav-link {{ (request()->is('admin/etiquetas*')) ? 'active' : '' }}" href="{{ route('panel.tags.index') }}">
											<span class="nav-link-text">Etiquetas</span>
										</a>
									</li>
								@endcan
								@can(PermissionKey::Equipment['permissions']['index']['name'])
									<li class="nav-item">
										<a class="nav-link {{ (request()->is('admin/equipamiento*')) ? 'active' : '' }}" href="{{ route('panel.equipment.index') }}">
											<span class="nav-link-text">Equipamiento</span>
										</a>
									</li>
								@endcan
								@can(PermissionKey::Property['permissions']['show_sidebar']['name'])
									<li class="nav-item">
										<a class="nav-link {{ (request()->is('admin/propiedades/index*')) ? 'active' : '' }}" href="{{ route('panel.propierties.index') }}">
											<span class="nav-link-text">Catálogo</span>
										</a>
									</li>
								@endcan
								@can(PermissionKey::Contact['permissions']['index']['name'])
									<li class="nav-item">
										<a class="nav-link {{ (request()->is('admin/contactos*')) ? 'active' : '' }}" href="{{ route('panel.contacts.index') }}">
											<span class="nav-link-text">Contactos</span>
										</a>
									</li>
								@endcan
						
				@endif			
			
			</ul>


					<!--Configuracion  -->
					@can(PermissionKey::Admin['permissions']['show_sidebar']['name'])
					<li class="nav-item">
						<a class="nav-link {{ (request()->is('admin/cuentas*') || request()->is('admin/pagos*') || request()->is('admin/tablajesCatastrales*')) ? 'active' : '' }}" href="#navbar-cuentas" data-toggle="collapse" role="button" aria-expanded="{{ (request()->is('admin/cuentas*') || request()->is('admin/pagos*') || request()->is('admin/tablajesCatastrales*')) ? 'true' : 'false' }}" aria-controls="navbar-cuentas">
							<i class="ni ni-single-02 text-default"></i>
							<span class="nav-link-text">Administración</span>
						</a>
						<div class="collapse {{ (request()->is('admin/cuentas*') || request()->is('admin/pagos*') || request()->is('admin/tablajesCatastrales*')) ? 'show' : '' }}" id="navbar-cuentas">
							<ul class="nav nav-sm flex-column">
								@can(PermissionKey::Role['permissions']['index']['name'])
									<li class="nav-item">
										<a class="nav-link {{ (request()->is('admin/cuentas/roles*')) ? 'active' : '' }}" href="{{ route('panel.roles.index') }}">
											<span class="nav-link-text">Roles</span>
										</a>
									</li>
								@endcan
								@can(PermissionKey::Admin['permissions']['index']['name'])
									<li class="nav-item">
										<a class="nav-link {{ (request()->is('admin/cuentas/usuarios*')) ? 'active' : '' }}" href="{{ route('panel.admins.index') }}">
											<span class="nav-link-text">Usuarios</span>
										</a>
									</li>
								@endcan
								@can(PermissionKey::Payment['permissions']['display_menu']['name'])
									<li class="nav-item">
										<a class="nav-link {{ (request()->is('admin/pagos*')) ? 'active' : '' }}" href="{{ route('panel.payments.index') }}">
											<span class="nav-link-text">Caja Chica</span>
										</a>
									</li>
								@endcan
								@can(PermissionKey::CadastralPlanking['permissions']['index']['name'])
									<li class="nav-item">
										<a class="nav-link {{ (request()->is('admin/tablajesCatastrales*')) ? 'active' : '' }}" href="{{ route('panel.cadastralPlankings.index') }}">
											<span class="nav-link-text">Tablajes Catastrales</span>
										</a>
									</li> 
								@endcan
								@can(PermissionKey::Development['permissions']['index']['name'])
									<li class="nav-item">
										<a class="nav-link {{ ( (request()->is('admin/desarrollos*'))) ? 'active' : '' }}" href="{{ route('panel.developments.index') }}">
											<span class="nav-link-text">Desarrollos</span>
										</a>
									</li> 
								@endcan
								@can(PermissionKey::Configuration['permissions']['maintenance']['name'])
									<li class="nav-item">
										<hr>
										<div class="container">
											<h3>Sitio web</h3>
											<form id="maintenanceForm">
												<div class="btn-group btn-toggle" data-toggle="buttons">
														<label class="btn btn-sm btn-outline-success active">
															<input type="radio" hidden name="active" id="option1" value="off" {{!$maintenance->active?'checked':''}}> On
														</label>
														<label class="btn btn-sm btn-outline-dark">
															<input type="radio" hidden name="active" id="option2" value="on" {{$maintenance->active?'checked':''}}> Off
														</label>
													</div>
												</div>
											</form>
										</div>
									</li>
								@endcan
							</ul>
						</div>
					</li>
				@endcan	

		</div>
	</div>
</nav>
@push('js')
<script>
	 $(document).ready(function () {
            $('.btn-toggle').on("click",function (event) {
				let form = $("#maintenanceForm").serialize();
				event.preventDefault();

				$.ajax({
                    url:'{{asset("admin/configuration/maintenance")}}',
                    type:'POST',
					data:form,
					success: function(result){
						console.log(result);
				  }});
			});
        });
</script>
@endpush