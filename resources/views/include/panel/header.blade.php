<style focused>
	.bg-banner{
		background-image: url("{{ asset('panel/img/LOGOS-BANNER-CRM-INT.png') }}"), url("{{ asset('panel/img/FONDO-BANNER-CRM-INT.png') }}");
		background-position: top, center;
		background-repeat: no-repeat;
		background-size: 60%,cover;
	}
</style>
<div class="header bg-banner pb-6">
    <nav class="navbar navbar-top navbar-expand navbar-dark ">
        <div class="container-fluid">
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Navbar links -->
            <ul class="navbar-nav align-items-center  ml-md-auto ">
            <li class="nav-item d-xl-none">
                <!-- Sidenav toggler -->
                <div class="pr-3 sidenav-toggler sidenav-toggler-dark" data-action="sidenav-pin" data-target="#sidenav-main">
                <div class="sidenav-toggler-inner">
                    <i class="sidenav-toggler-line"></i>
                    <i class="sidenav-toggler-line"></i>
                    <i class="sidenav-toggler-line"></i>
                </div>
                </div>
            </li>

            
            </ul>
            <ul class="navbar-nav align-items-center  ml-auto ml-md-0 ">
            <li class="nav-item dropdown">
                <a class="nav-link pr-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <div class="media align-items-center">
                    <span class="avatar avatar-sm rounded-circle">
                    <img alt="Image placeholder" src="{{asset(Auth::guard('admin')->user()->avatar)}}">
                    </span>
                    <div class="media-body  ml-2  d-none d-lg-block">
                    <span class="mb-0 text-sm  font-weight-bold">{{ Auth::guard('admin')->user()->name }}</span>
                    </div>
                </div>
                </a>
                <div class="dropdown-menu  dropdown-menu-right ">
                    <a href="{{ route('panel.admins.edit', ['id' => Auth::guard('admin')->user()->id]) }}" class="dropdown-item">
                        <i class="ni ni-single-02"></i>
                        <span>Mi perfil</span>
                    </a>
                    <div class="dropdown-divider"></div>
                    <form action="{{ route('panel.admins.logout') }}" method="POST">
                        {{ csrf_field() }}
                        <button class="dropdown-item">
                            <i class="ni ni-user-run"></i>
                            <span>Salir</span>
                        </button>
                    </form>
                </div>
            </li>
            </ul>
        </div>
        </div>
    </nav>
    <div class="container-fluid">
        <div class="header-body">
        <div class="row align-items-center py-4">
            <div class="col-lg-6 col-7">
                <h6 class="h2 text-white d-inline-block mb-0">{{ $title }}</h6>
                <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                    <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                        @if ((isset($breadcrumb)) && (count($breadcrumb) > 0))
                            @foreach ($breadcrumb as $b)
                                @if (isset($b['disabled']))
                                    @if (!$b['disabled'])
                                        <li class="breadcrumb-item {{ ((isset($b['active'])) && ($b['active'])) ? 'active' : '' }}">
                                            @if (isset($b['params']))
                                                <a href="{{ (isset($b['route'])) ? route($b['route'], $b['params']) : '' }}">{{ $b['title'] }}</a>
                                            @else
                                                <a href="{{ (isset($b['route'])) ? route($b['route']) : '' }}">{{ $b['title'] }}</a>
                                            @endif
                                        </li>
                                    @endif
                                @else
                                    <li class="breadcrumb-item {{ ((isset($b['active'])) && ($b['active'])) ? 'active' : '' }}">
                                        @if (isset($b['params']))
                                            @if (isset($b['route']))
                                                <a href="{{ (isset($b['route'])) ? route($b['route'], $b['params']) : '' }}">{{ $b['title'] }}</a>
                                            @else
                                                {{ $b['title'] }}
                                            @endif
                                        @else
                                            @if (isset($b['route']))
                                                <a href="{{ (isset($b['route'])) ? route($b['route']) : '' }}">{{ $b['title'] }}</a>
                                            @else
                                                {{ $b['title'] }}
                                            @endif
                                        @endif
                                    </li>
                                @endif
                            @endforeach
                        @endif
                    </ol>
                </nav>
            </div>
            <div class="col-lg-6 col-5 text-right">
                @if ((isset($buttons)) && (count($buttons) > 0))
                    @foreach ($buttons as $item)
                        @if (isset($item['params']))
                            <a href="{{ route($item['route'], $item['params']) }}" class="btn btn-sm btn-secondary">{{ $item['title']  }}</a>    
                        @else
                            <a href="{{ route($item['route']) }}" class="btn btn-sm btn-secondary">{{ $item['title']  }}</a>
                        @endif
                    @endforeach
                @endif
            </div>
        </div>
        </div>
        @if (session('success'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <span class="alert-icon"><i class="ni ni-like-2"></i></span>
                <span class="alert-text"><strong>Correcto!</strong> {{ session('success') }}</span>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
        @error('invalid')
            <div class="alert alert-danger" role="alert">
                <strong>Ups!</strong> {{ $message }}
            </div>
        @enderror
        @error('document')
            <div class="alert alert-danger" role="alert">
                <strong>Ups!</strong> El documento debe ser de tipo: jpeg, jpg.
            </div>
        @enderror
    </div>
</div>