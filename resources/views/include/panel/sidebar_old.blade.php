<!-- Sidenav -->
<nav class="sidenav navbar navbar-vertical  fixed-left  navbar-expand-xs navbar-light bg-white" id="sidenav-main">
	<div class="scrollbar-inner">
	<!-- Brand -->
	<div class="sidenav-header  align-items-center">
		<a class="navbar-brand" href="javascript:void(0)">
			<img src="{{asset('panel/img/brand/logo-verde.png')}}" class="navbar-brand-img" alt="...">
		</a>
	</div>
	<div class="navbar-inner">
		<!-- Collapse -->
		<div class="collapse navbar-collapse" id="sidenav-collapse-main">
			<!-- Nav items -->
			<ul class="navbar-nav">
				@if(request()->user()->can(PermissionKey::Development['permissions']['index']['name']) || request()->user()->can(PermissionKey::Client['permissions']['index']['name']))
					<li class="nav-item">
						<a class="nav-link {{ (request()->is('admin/desarrollos*') || request()->is('admin/clientes*') || request()->is('admin/prospectos*')) ? 'active' : '' }}" href="#navbar-catalogos" data-toggle="collapse" role="button" aria-expanded="{{ (request()->is('admin/clientes*') || request()->is('admin/desarrollos*') || request()->is('admin/prospectos*')) ? 'true' : 'false' }}" aria-controls="navbar-catalogos">
							<i class="ni ni-books text-default"></i>
							<span class="nav-link-text">General</span>
						</a>
						<div class="collapse {{ (request()->is('admin/desarrollos*') || request()->is('admin/clientes*') || request()->is('admin/prospectos*')) ? 'show' : '' }}" id="navbar-catalogos">
							<ul class="nav nav-sm flex-column">
								@can(PermissionKey::Prospect['permissions']['show_sidebar']['name'])
									<li class="nav-item">
										<a class="nav-link {{ (request()->is('admin/prospectos*')) ? 'active' : '' }}" href="{{ route('panel.prospects.index') }}">
											<span class="nav-link-text">Leads</span>
										</a>
									</li>
								@endcan
								@can(PermissionKey::Client['permissions']['index']['name'])
									<li class="nav-item">
										<a class="nav-link {{ (request()->is('admin/clientes*')) ? 'active' : '' }}" href="{{ route('panel.clients.index') }}">
											<span class="nav-link-text">Clientes</span>
										</a>
									</li>
								@endcan
								@can(PermissionKey::Development['permissions']['index']['name'])
									<li class="nav-item">
										<a class="nav-link {{ ( (request()->is('admin/desarrollos*'))) ? 'active' : '' }}" href="{{ route('panel.developments.index') }}">
											<span class="nav-link-text">Desarrollos</span>
										</a>
									</li> 
								@endcan
							</ul>
						</div>
					</li>
				@endif
				@can(PermissionKey::Venta['permissions']['show_sidebar']['name'])
					<li class="nav-item">
						<a class="nav-link {{ ((request()->is('admin/ventas*'))) ? 'active' : '' }}" href="#navbar-ventas" data-toggle="collapse" role="button" aria-expanded="{{ ((request()->is('admin/ventas*'))) ? 'true' : 'false' }}" aria-controls="navbar-ventas">
							<i class="ni ni-credit-card text-default"></i>
							<span class="nav-link-text">Ventas</span>
						</a>
						<div class="collapse {{ ((request()->is('admin/ventas*'))) ? 'show' : '' }}" id="navbar-ventas">
							<ul class="nav nav-sm flex-column">
								@foreach (Development::where('status', 'visible')->get() as $desarrollo)
									<li class="nav-item">
										<a class="nav-link {{ (request()->is('admin/ventas?development='.$desarrollo->id.'')) ? 'active' : '' }}" href="#desarrollo-{{ $desarrollo->id }}" data-toggle="collapse" role="button" aria-expanded="{{ ((request()->is('admin/ventas/create?desarrollo='.$desarrollo->id))) ? 'true' : 'false' }}">
											<span class="nav-link-text">{{ mb_strtoupper($desarrollo->name) }} ETAPA {{ mb_strtoupper($desarrollo->stage) }}</span>
										</a>
										<div class="collapse {{ ((request()->is('admin/ventas/create?desarrollo='.$desarrollo->id))) ? 'show' : '' }}" id="desarrollo-{{ $desarrollo->id }}">
											<ul class="nav nav-sm flex-column">
												<li class="nav-item">
													<a href="{{ route('panel.ventas.create', ['desarrollo' => $desarrollo->id]) }}" class="nav-link">
														<span class="nav-link-text">Disponibilidad</span>
													</a>
													<a href="{{ route('panel.ventas.index', ['desarrollo' => $desarrollo->id]) }}" class="nav-link">
														<span class="nav-link-text">Seguimiento</span>
													</a>
													@can(PermissionKey::Venta['permissions']['seguimiento_pagos']['name'])
														<a href="{{ route('panel.pagos', ['desarrollo' => $desarrollo->id]) }}" class="nav-link">
															<span class="nav-link-text">Cuentas por cobrar</span>
														</a>
													@endcan
												</li>
											</ul>
										</div>
									</li>
								@endforeach
							</ul>
						</div>
					</li>
				@endcan
				@if(request()->user()->can(PermissionKey::Property['permissions']['show_sidebar']['name']) || 
				request()->user()->can(PermissionKey::Zone['permissions']['index']['name']) ||
				request()->user()->can(PermissionKey::Operation['permissions']['index']['name']) ||
				request()->user()->can(PermissionKey::Tag['permissions']['index']['name']) ||
				request()->user()->can(PermissionKey::Contact['permissions']['index']['name']) ||
				request()->user()->can(PermissionKey::PropertyType['permissions']['index']['name']) ||
				request()->user()->can(PermissionKey::Equipment['permissions']['index']['name']))
					<li class="nav-item">
						<a class="nav-link {{ ((request()->is('admin/propiedades*')) || (request()->is('admin/zonas*')) || (request()->is('admin/etiquetas*')) || (request()->is('admin/areas*')) || (request()->is('admin/equipamiento*')) || (request()->is('admin/contactos*'))) ? 'active' : '' }}" href="#navbar-propiedades" data-toggle="collapse" role="button" aria-expanded="{{ ((request()->is('admin/propiedades*')) || (request()->is('admin/zonas*')) || (request()->is('admin/etiquetas*')) || (request()->is('admin/areas*')) || (request()->is('admin/equipamiento*')) || (request()->is('admin/contactos*'))) ? 'true' : 'false' }}" aria-controls="navbar-propiedades">
							<i class="ni ni-building text-default"></i>
							<span class="nav-link-text">Propiedades</span>
						</a>
						<div class="collapse {{ ((request()->is('admin/propiedades*')) || (request()->is('admin/zonas*')) || (request()->is('admin/etiquetas*')) || (request()->is('admin/areas*')) || (request()->is('admin/equipamiento*')) || (request()->is('admin/contactos*'))) ? 'show' : '' }}" id="navbar-propiedades">
							<ul class="nav nav-sm flex-column">
								@can(PermissionKey::Zone['permissions']['index']['name'])
									<li class="nav-item">
										<a class="nav-link {{ (request()->is('admin/zonas*')) ? 'active' : '' }}" href="{{ route('panel.zones.index') }}">
											<span class="nav-link-text">Zonas</span>
										</a>
									</li>
								@endcan
								@can(PermissionKey::PropertyType['permissions']['index']['name'])
									<li class="nav-item">
										<a class="nav-link {{ (request()->is('admin/tipos*')) ? 'active' : '' }}" href="{{ route('panel.types.index') }}">
											<span class="nav-link-text">Tipos</span>
										</a>
									</li>
								@endcan
								@can(PermissionKey::Operation['permissions']['index']['name'])
									<li class="nav-item">
										<a class="nav-link {{ (request()->is('admin/operaciones*')) ? 'active' : '' }}" href="{{ route('panel.operations.index') }}">
											<span class="nav-link-text">Operaciones</span>
										</a>
									</li>
								@endcan
								@can(PermissionKey::Tag['permissions']['index']['name'])
									<li class="nav-item">
										<a class="nav-link {{ (request()->is('admin/etiquetas*')) ? 'active' : '' }}" href="{{ route('panel.tags.index') }}">
											<span class="nav-link-text">Etiquetas</span>
										</a>
									</li>
								@endcan
								@can(PermissionKey::Equipment['permissions']['index']['name'])
									<li class="nav-item">
										<a class="nav-link {{ (request()->is('admin/equipamiento*')) ? 'active' : '' }}" href="{{ route('panel.equipment.index') }}">
											<span class="nav-link-text">Equipamiento</span>
										</a>
									</li>
								@endcan
								@can(PermissionKey::Property['permissions']['show_sidebar']['name'])
									<li class="nav-item">
										<a class="nav-link {{ (request()->is('admin/propiedades/index*')) ? 'active' : '' }}" href="{{ route('panel.propierties.index') }}">
											<span class="nav-link-text">Catálogo</span>
										</a>
									</li>
								@endcan
								@can(PermissionKey::Contact['permissions']['index']['name'])
									<li class="nav-item">
										<a class="nav-link {{ (request()->is('admin/contactos*')) ? 'active' : '' }}" href="{{ route('panel.contacts.index') }}">
											<span class="nav-link-text">Contactos</span>
										</a>
									</li>
								@endcan
							</ul>
						</div>
					</li>
				@endif
				@can(PermissionKey::Event['permissions']['show_sidebar']['name'])
					<li class="nav-item">
						<a class="nav-link {{ (request()->is('admin/eventos*')) ? 'active' : '' }}" href="{{ route('panel.events.index') }}">
						<i class="ni ni-calendar-grid-58 text-default"></i>
						<span class="nav-link-text">Calendario</span>
						</a>
					</li>
				@endcan
				@can(PermissionKey::Admin['permissions']['show_sidebar']['name'])
					<li class="nav-item">
						<a class="nav-link {{ (request()->is('admin/cuentas*') || request()->is('admin/pagos*') || request()->is('admin/tablajesCatastrales*')) ? 'active' : '' }}" href="#navbar-cuentas" data-toggle="collapse" role="button" aria-expanded="{{ (request()->is('admin/cuentas*') || request()->is('admin/pagos*') || request()->is('admin/tablajesCatastrales*')) ? 'true' : 'false' }}" aria-controls="navbar-cuentas">
							<i class="ni ni-single-02 text-default"></i>
							<span class="nav-link-text">Administración</span>
						</a>
						<div class="collapse {{ (request()->is('admin/cuentas*') || request()->is('admin/pagos*') || request()->is('admin/tablajesCatastrales*')) ? 'show' : '' }}" id="navbar-cuentas">
							<ul class="nav nav-sm flex-column">
								@can(PermissionKey::Role['permissions']['index']['name'])
									<li class="nav-item">
										<a class="nav-link {{ (request()->is('admin/cuentas/roles*')) ? 'active' : '' }}" href="{{ route('panel.roles.index') }}">
											<span class="nav-link-text">Roles</span>
										</a>
									</li>
								@endcan
								@can(PermissionKey::Admin['permissions']['index']['name'])
									<li class="nav-item">
										<a class="nav-link {{ (request()->is('admin/cuentas/usuarios*')) ? 'active' : '' }}" href="{{ route('panel.admins.index') }}">
											<span class="nav-link-text">Usuarios</span>
										</a>
									</li>
								@endcan
								@can(PermissionKey::Payment['permissions']['display_menu']['name'])
									<li class="nav-item">
										<a class="nav-link {{ (request()->is('admin/pagos*')) ? 'active' : '' }}" href="{{ route('panel.payments.index') }}">
											<span class="nav-link-text">Caja Chica</span>
										</a>
									</li>
								@endcan
								@can(PermissionKey::CadastralPlanking['permissions']['index']['name'])
									<li class="nav-item">
										<a class="nav-link {{ (request()->is('admin/tablajesCatastrales*')) ? 'active' : '' }}" href="{{ route('panel.cadastralPlankings.index') }}">
											<span class="nav-link-text">Tablajes Catastrales</span>
										</a>
									</li> 
								@endcan
							</ul>
						</div>
					</li>
				@endcan
				@if (request()->user()->can(PermissionKey::Task['permissions']['display_menu']['name']))
					<li class="nav-item">
						<a class="nav-link {{ (request()->is('admin/actividades*')) ? 'active' : '' }}" href="#navbar-settings" data-toggle="collapse" role="button" aria-expanded="{{ (request()->is('admin/actividades*')) ? 'true' : 'false' }}" aria-controls="navbar-settings">
							<i class="ni ni-settings-gear-65 text-default"></i>
							<span class="nav-link-text">Configuraciones</span>
						</a>
						<div class="collapse {{ (request()->is('admin/actividades*')) ? 'show' : '' }}" id="navbar-settings">
							<ul class="nav nav-sm flex-column">
								@can(PermissionKey::Task['permissions']['index']['name'])
									<li class="nav-item">
										<a class="nav-link {{ (request()->is('admin/actividades*')) ? 'active' : '' }}" href="{{ route('panel.tasks.index') }}">
											<span class="nav-link-text">Actividades</span>
										</a>
									</li>
								@endcan
							</ul>
						</div>
					</li>
				@endif
			</ul>
		</div>
	</div>
	</div>
</nav>