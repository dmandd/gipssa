<div class="col-xl-4 col-md-6 col-sm-12">
    <div class="single-property-box">
        <div class="property-item">
            <a class="property-img" href="{{ route('propiedades.detalle', ['id' => $_property->id, 'slug' => $_property->slug()]) }}">
                {{-- Portada --}}
                <img src="{{ asset($_property->cover) }}" alt="#">
            </a>
            <ul class="feature_text">
                @if ($_property->tag())
                    <li class="feature_cb"><span>{{ $_property->tag()->name }}</span></li>
                @endif
                @if ($_property->outstanding)
                    <li class="feature_or"><span style="text-transform: capitalize;">Destacado</span></li>
                @endif
            </ul>
            <div class="property-author-wrap">
                {{-- Agente --}}
                <a href="#" class="property-author">
                    <img src="{{ $_property->admin()->avatar }}" alt="...">
                    <span>{{ $_property->admin()->name }}</span>
                </a>
                {{-- Agente --}}
            </div>

        </div>
        <div class="property-title-box">
            <h4><a href="{{ route('propiedades.detalle', ['id' => $_property->id, 'slug' => $_property->slug()]) }}">{{ $_property->name }}</a></h4>
            @if ($_property->location)
                <div class="property-location">
                    <i class="fa fa-map-marker-alt"></i>
                    <p><a href="{{ $_property->location }}" target="_blank">Ver ubicación</a></p>
                </div>
            @endif
            <ul class="property-feature">
                @if ($_property->bedrooms)
                    <li> <i class="fas fa-bed"></i>
                        <span>{{ $_property->bedrooms }} Recámaras</span>
                    </li>
                @endif
                @if ($_property->bathrooms)
                    <li> <i class="fas fa-bath"></i>
                        <span>{{ $_property->bathrooms }} Baños</span>
                    </li>
                @endif
                <li> <i class="fas fa-arrows-alt"></i>
                    <span>{{ $_property->terrain_m2 }} m&sup2; (Terreno)</span>
                </li>
            </ul>
            <div class="trending-bottom">
                <a class="trend-right float-right">
                    <div class="trend-open">
                        <p><span class="per_sale">Precio</span> ${{ $_property->first_price }}</p>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>