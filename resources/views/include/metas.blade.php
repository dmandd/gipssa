<!-- Metas -->
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="author" content="gipssa.com" />
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-55BRZVC');</script>
<!-- End Google Tag Manager -->
<meta name="instapage-verification" content="1614874132421-IC54" />
<meta name="facebook-domain-verification" content="8by2s9plw5pv8ehsmk43a6czlfimm1" />
<meta name="google-site-verification" content="rOXOf1LLZZCAeUYu18zHC0wGweeEIRpT5W_vt46iqXc" />
<!-- Links -->
<link rel="icon" href="{{asset('panel/img/brand/favicon.svg')}}" type="image/svg">
<!-- google fonts-->
<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet">
<!-- Plugins CSS -->
<link href="{{asset('assets/css/plugin.css')}}" rel="stylesheet" />
<!-- style CSS -->
<link href="{{asset('assets/css/style.css')}}" rel="stylesheet" />
<link href="{{asset('assets/css/custom.css')}}?v=1.2.1" rel="stylesheet" />
@if (isset($property))
    <meta property="og:title" content="{{ $property->name }}">
    <meta property="og:description" content="Increíble propiedad">
    <meta property="og:image" content="{{ asset($property->cover) }}">
    <meta property="og:url" content="{{ route('propiedades.detalle', ['id' => $property->id, 'slug' => $property->slug()]) }}">
    <meta property="product:price:amount" content="{{ $property->first_price }}">   
    <meta property="product:price:currency" content="MXN">
    <script type="application/ld+json">
    {   "@context": "http://schema.org/",   
        "@type": "Product",   
        "image": "{{ asset($property->cover) }}",     
        "name": "{{ $property->name }}",   
        "description": "Increíble propiedad",     
        "offers": {     
            "@type": "Offer",     
            "priceCurrency": "MXN",     
            "price": "{{ $property->first_price }}"   
        } 
    } </script>
    <span itemscope itemtype="http://schema.org/Product" class="microdata">
      <meta itemprop="image" content="{{ asset($property->cover) }}">   
      <meta itemprop="name" content="{{ $property->name }}">   
      <meta itemprop="description" content="Increíble propiedad">   
      <span itemprop="offers" itemscope itemtype="http://schema.org/Offer">     
        <meta itemprop="price" content="{{ $property->first_price }}">     
        <meta itemprop="priceCurrency" content="MXN">   
      </span> 
    </span>
@else
    <meta property="og:title" content="Gipssa">
    <meta property="og:description" content="Gipssa">
    <meta property="og:url" content="{{ route('home') }}">
@endif
<!-- Start of HubSpot Embed Code -->
  <script type="text/javascript" id="hs-script-loader" async defer src="//js.hs-scripts.com/19562588.js"></script>
<!-- End of HubSpot Embed Code -->



@stack('link')
