@component('mail::message')
# Solicitud Revisión Venta #{{ $venta->id }}

{{ $venta->asesor()->name }} {{ $venta->asesor()->last_name }} ha solicitado la revisión para la venta con el folio #{{ $venta->id }}

@component('mail::button', ['url' => route('panel.ventas.edit', ['venta' => $venta->id])])
Ver
@endcomponent

@endcomponent
