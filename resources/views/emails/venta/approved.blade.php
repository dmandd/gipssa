@component('mail::message')
# Solicitud Revisión Venta #{{ $venta->id }} ACEPTADA

@switch($venta->status)
    @case(1)
        Se ha aprobado tu solicitud de autorización, la venta se encuentra en DEFINIR PARAMETROS DE BLOQUEO
    @break
    @case(2)
        Se ha aprobado tu solicitud de autorización, la venta se encuentra en DOCUMENTACIÓN
    @break
    @case(3)
        Se ha autorizado la documentación, la venta se encuentra en ENGANCHE. Se puede generar la condición de compra, recibo de pago de contrato, descargar el contrato.
    @break;
    @case(4)
        Se ha aprobado tu solicitud de autorización, la venta se encuentra en CONTRATO
    @break;
    @default
        Se ha aprobado tu solicitud de autorización
    @break;
@endswitch


@component('mail::button', ['url' => route('panel.ventas.edit', ['venta' => $venta->id])])
Ver
@endcomponent

@endcomponent
