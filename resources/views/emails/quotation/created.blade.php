@component('mail::message')
# Cotización Creada

- {{ $description }}

@component('mail::button', ['url' => $url])
Ver
@endcomponent

@endcomponent
