@component('mail::message')
# Cotización Rechazada

La venta con el folio: {{ $venta->id }} ha sido rechazada.

Motivo: {{ $motivo }}

@endcomponent
