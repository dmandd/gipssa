@component('mail::message')
# Este cliente quiere que lo contacten 

-Nombre: {{ $description->fname }}
<br>
-Teléfono: {{ $description->fphone }}
<br>
-Correo electrónico: {{ $description->email }}
<br><br>
-Mensaje: {{ $description->comment }}

@endcomponent
