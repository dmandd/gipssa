@component('mail::message')
# Documento Rechazado

{{-- Documento: {{ $document->document()->name }} --}}

Ha sido rechazado por un Supervisor, favor de cambiarlo

Motivo: {{ $motivo }}

{{-- @component('mail::button', ['url' => route('panel.venta.edit', ['venta' => $venta->id])])
Ver
@endcomponent --}}

@endcomponent
