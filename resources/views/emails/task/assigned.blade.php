@component('mail::message')
# Nueva Tarea

Se te ha asignado una nueva tarea.

- {{ $task->description }}

@component('mail::button', ['url' => ''])
Ver
@endcomponent

@endcomponent
