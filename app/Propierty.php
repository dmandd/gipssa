<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\PropertyGallery;
use App\PropertyType;
use App\Operation;
use Illuminate\Support\Str;

class Propierty extends Model
{
    protected $fillable = [
        'admin_id',
        'contact_id',
        'outstanding',
        'code',
        'cover',
        'name',
        'type',
        'zone_id',
        'city_id',
        'tag_id',
        'operation',
        'location',
        'colony',
        'street',
        'terrain_m2',
        'front_m2',
        'depth_m2',
        'build_m2',
        'other_m2',
        'level',
        'bedrooms',
        'bathrooms',
        'first_price',
        'last_price',
        'other_price',
        'status',
        'address',
        'internal_number',
        'external_number',
        'accross',
        'year',
        'update',
        'usage',
        'paper',
        'plano_catastral',
        'cedula_catastral',
        'libertad_gravamen',
        'hipoteca',
        'contrato_renta',
        'deuda',
        'habitable',
        'habitable_porc',
        'habitable_notes',
        'delivery_time',
        'accepted_credit',
        'maintenance_quote',
        'extra_quote',
        'other_quote',
        'comission_type',
        'comission',
        'comission_share',
        'comission_note',
        'enganche_type',
        'enganche',
        'enganche_note',
        'references'
    ];

    public function type(){
        switch ($this->type) {
            case 'casa':
                return 'Casa';
            break;
            case 'departamento':
                return 'Departamento';
            break;
            case 'townhouse':
                return 'Townhouse';
            break;
            case 'loft':
                return 'Loft';
            break;
            case 'hacienda':
                return 'Hacienda';
            break;
            case 'oficina':
                return 'Oficina';
            break;
            case 'local':
                return 'Local';
            break;
            case 'desarrollo':
                return 'Desarrollo';
            break;
            case 'terreno':
                return 'Terreno';
            break;
            
            default:
                if($this->belongsTo('App\PropertyType', 'type', 'id')->first()){
                    return $this->belongsTo('App\PropertyType', 'type', 'id')->first()->name;
                }else{
                    return 'Sin asignar';
                }
            break;
        }
    }

    public function operation(){
        switch ($this->operation) {
            case 'venta':
                return 'Venta';
            break;
            case 'traspaso':
                return 'Traspaso';
            break;
            case 'renta_venta':
                return 'Renta y venta';
            break;
            case 'renta':
                return "Renta";
            break;
            case 'otro':
                return 'Otro';
            break;
            
            default:
                if($this->belongsTo('App\Operation', 'operation', 'id')->first()){
                    return $this->belongsTo('App\Operation', 'operation', 'id')->first()->name;
                }else{
                    return 'Sin asignar';
                }
            break;
        }
    }

    public function zone(){
        return $this->belongsTo('App\Zone')->first();
    }

    public function areas(){
        return $this->hasMany('App\Area', 'property_id');
    }

    public function contact(){
        return $this->belongsTo('App\Contact');
    }

    public function addImage($image){
        $gallery = PropertyGallery::create([
            'propierty_id' => $this->id,
            'image' => $image->file_name,
        ]);
    }

    public function gallery(){
        return $this->hasMany('App\PropertyGallery')->get();
    }

    public function tag(){
        return $this->belongsTo('App\Tag')->first();
    }

    public function admin(){
        return $this->belongsTo('App\Admin')->first();
    }

    public function slug(){
        return Str::slug($this->name);
    }

    public static function code($request){
        $type = PropertyType::find($request->type);
        //Obtenemos la operación
        $operation = Operation::find($request->operation);
        // $operation = ...;
        /**
         * El código se define de la siguiente manera:
         * Todos los códigos empiezan 
         * -	G  Gipssa
         * -	I    Inmobiliaria
         *  Aquí el tercer ítem es el prefijo del tipo de la propiedad
         *  Prefijo del tipo de negociación
         *  Prefijo Vendedor
         *  Código Vendedor 
         *  Número de propiedad asignada al vendedor (Es auto incremental)
         */
        //Incrementamos el numero actual de propiedad del usuario
        if(isset($request->user()->adminProperty()->total)){
            $request->user()->adminProperty()->increment('total');
        }else{
            AdminProperty::create(['admin_id' => $request->user()->id, 'total' => 0]);
        }
        $totalProperty = $request->user()->adminProperty()->total;
        $consecutivo = $totalProperty < 10 ? "0".$totalProperty:$totalProperty;
        return $type->prefix.$operation->prefix.$request->user()->code.$consecutivo;
    }
}
