<?php

namespace App\Http\Controllers;

use App\Equipment;
use Illuminate\Http\Request;

class EquipmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $info = [
            'title' => 'Equipamiento',
            'breadcrumb' => [
                [
                    'title' => 'Todos',
                    'route' => 'panel.equipment.index',
                    'active' => true
                ]
            ],
            'buttons' => [
                [
                    'title' => 'Agregar Nuevo',
                    'route' => 'panel.equipment.create'
                ]
            ]
        ];
        $info['data'] = Equipment::all()->sortByDesc('id');
        return view('panel.equipment.index', $info);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $info = [
            'title' => 'Equipamiento',
            'breadcrumb' => [
                [
                    'title' => 'Todos',
                    'route' => 'panel.equipment.index',
                ],
                [
                    'title' => 'Nuevo',
                    'route' => 'panel.equipment.create',
                    'active' => true
                ]
            ]
        ];
        return view('panel.equipment.create', $info);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $equipment = Equipment::create($request->toArray());
        return redirect()->route('panel.equipment.index')->with('success', 'Operación exitosa');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Equipment  $equipment
     * @return \Illuminate\Http\Response
     */
    public function show(Equipment $equipment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Equipment  $equipment
     * @return \Illuminate\Http\Response
     */
    public function edit(Int $id)
    {
        $info = [
            'title' => 'Equipamiento',
            'breadcrumb' => [
                [
                    'title' => 'Todos',
                    'route' => 'panel.equipment.index',
                ],
                [
                    'title' => 'Editar',
                    'route' => 'panel.equipment.edit',
                    'params' => ['equipamiento' => $id],
                    'active' => true
                ]
            ]
        ];
        if($info['equipment'] = Equipment::find($id)){
            return view('panel.equipment.edit', $info);
        }else{
            return redirect()->route('panel.equipment.index');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Equipment  $equipment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Int $id)
    {
        $equipment = Equipment::find($id)->update($request->toArray());
        if(isset($arr['api'])){
            return response(['success' => true], 200);
        }else{
            return redirect()->route('panel.equipment.edit', ['equipamiento' => $id])
                    ->with('success', 'Elemento actualizado');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Equipment  $equipment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Int $id)
    {
        if(Equipment::find($id)){
            Equipment::destroy($id);
            return response(['success' => true], 200);
        }else{
            return response(['success' => false], 200);
        }
    }
}
