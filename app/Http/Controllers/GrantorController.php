<?php

namespace App\Http\Controllers;

use App\Grantor;
use App\Development;
use Illuminate\Http\Request;

class GrantorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $desarrollo = Development::find($request->desarrollo);
        $info = [
            'title' => 'Poderdantes',
            'breadcrumb' => [
                [
                    'title' => 'Todos',
                    'route' => 'panel.developments.index',
                ],
                [
                    'title' => $desarrollo->name." Etapa ".$desarrollo->stage,
                    'route' => 'panel.developments.edit',
                    'params' => ['desarrollo' => $request->desarrollo],
                ],
                [
                    'title' => 'Poderdantes',
                    'route' => 'panel.grantors.index',
                    'params' => ['desarrollo' => $request->desarrollo],
                    'active' => true
                ]
            ],
            'buttons' => [
                [
                    'title' => 'Agregar Nuevo',
                    'route' => 'panel.grantors.create',
                    'params' => ['desarrollo' => $request->desarrollo]
                ]
            ]
        ];
        $info['data'] = Grantor::where('development_id', $request->desarrollo)->get()->sortByDesc('id');
        return view('panel.grantors.index', $info);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $desarrollo = Development::find($request->desarrollo);
        $info = [
            'title' => 'Poderdantes',
            'breadcrumb' => [
                [
                    'title' => 'Todos',
                    'route' => 'panel.developments.index',
                ],
                [
                    'title' => $desarrollo->name." Etapa ".$desarrollo->stage,
                    'route' => 'panel.developments.edit',
                    'params' => ['desarrollo' => $request->desarrollo],
                ],
                [
                    'title' => 'Poderdantes',
                    'route' => 'panel.grantors.index',
                    'params' => ['desarrollo' => $request->desarrollo],
                    'active' => true
                ],
                [
                    'title' => 'Nuevo',
                    'route' => 'panel.grantors.create',
                    'params' => ['desarrollo' => $request->desarrollo],
                ]
            ]
        ];
        return view('panel.grantors.create', $info);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
        $this->validate($request, [
            'name' => 'required',
            'last_name' => 'required',
            'public_notary_description' => 'required'
        ]);

        $arr = $request->toArray();
        $arr['birth_date'] = str_replace('/', '-', $arr['birth_date']);
        $arr['birth_date'] = date('Y-m-d', strtotime($arr['birth_date']));
        $arr['development_id'] = $request->desarrollo;
        $grantor = Grantor::create($arr);
        return redirect()->route('panel.grantors.index', ['desarrollo' => $request->desarrollo])->with('success', 'Operación exitosa');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Grantor  $grantor
     * @return \Illuminate\Http\Response
     */
    public function show(Grantor $grantor)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Grantor  $grantor
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $desarrollo = Development::find($request->desarrollo);
        $info = [
            'title' => 'Poderdantes',
            'breadcrumb' => [
                [
                    'title' => 'Todos',
                    'route' => 'panel.developments.index',
                ],
                [
                    'title' => $desarrollo->name." Etapa ".$desarrollo->stage,
                    'route' => 'panel.developments.edit',
                    'params' => ['desarrollo' => $request->desarrollo],
                ],
                [
                    'title' => 'Poderdantes',
                    'route' => 'panel.grantors.index',
                    'params' => ['desarrollo' => $request->desarrollo],
                    'active' => true
                ],
                [
                    'title' => 'Editar',
                    'route' => 'panel.grantors.edit',
                    'params' => ['desarrollo' => $request->desarrollo, 'grantor' => $request->grantor],
                ]
            ]
        ];
        $info['grantor'] = Grantor::find($request->grantor);
        return view('panel.grantors.edit', $info);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Grantor  $grantor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'last_name' => 'required',
            'public_notary_description' => 'required'
        ]);

        if($grantor = Grantor::find($request->grantor)){
            $arr = $request->toArray();  
                      
            $arr['birth_date'] = str_replace('/', '-', $arr['birth_date']);
            $arr['birth_date'] = date('Y-m-d', strtotime($arr['birth_date']));
            $grantor->update($arr);
            return redirect()->back()->with('success', 'Información actualizada');
        }else{
            return redirect()->back()->withErrors(['invalid' => 'Lo sentimos, algo salió mal']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Grantor  $grantor
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        if(Grantor::find($request->grantor)){
            Grantor::destroy($request->grantor);
            return response(['success' => true]);
        }else{
            return response(['success' => false]);
        }
    }
}
