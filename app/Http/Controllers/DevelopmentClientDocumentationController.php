<?php

namespace App\Http\Controllers;

use App\DevelopmentClientDocumentation;
use App\Development;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class DevelopmentClientDocumentationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Int $id)
    {
        $info = [
            'title' => 'Documentación',
            'breadcrumb' => [
                [
                    'title' => 'Todos',
                    'route' => 'panel.developments.index',
                ],
                [
                    'title' => Development::find($id)->name,
                    'route' => 'panel.developments.edit',
                    'params' => ['desarrollo' => $id],
                ],
                [
                    'title' => 'Documentación',
                    'route' => 'panel.documentation.index',
                    'params' => ['desarrollo' => $id],
                    'active' => true
                ],
            ],
            'buttons' => [
                [
                    'title' => 'Agregar Nuevo',
                    'route' => 'panel.documentation.create',
                    'params' => ['desarrollo' => $id],
                ]
            ]
        ];
        $info['data'] = DevelopmentClientDocumentation::where('development_id', $id)->get();
        return view('panel.documentation.index', $info);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Int $id)
    {
        $info = [
            'title' => 'Documentación',
            'breadcrumb' => [
                [
                    'title' => 'Todos',
                    'route' => 'panel.developments.index',
                ],
                [
                    'title' => Development::find($id)->name,
                    'route' => 'panel.developments.edit',
                    'params' => ['desarrollo' => $id],
                ],
                [
                    'title' => 'Documentación',
                    'route' => 'panel.documentation.index',
                    'params' => ['desarrollo' => $id],
                ],
                [
                    'title' => 'Nuevo Documento',
                    'route' => 'panel.documentation.create',
                    'params' => ['desarrollo' => $id],
                    'active' => true
                ],
            ],
        ];
        return view('panel.documentation.create', $info);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Int $desarrollo)
    {
        $arr = $request->toArray();
        $arr['slug'] = Str::slug($arr['name']);
        $arr['development_id'] = $desarrollo;
        $documentation = DevelopmentClientDocumentation::create($arr);
        return redirect()->route('panel.documentation.edit', ['desarrollo' => $desarrollo, 'documentacion' => $documentation->id])->with('success', 'Operación exitosa');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DevelopmentClientDocumentation  $developmentClientDocumentation
     * @return \Illuminate\Http\Response
     */
    public function show(DevelopmentClientDocumentation $developmentClientDocumentation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DevelopmentClientDocumentation  $developmentClientDocumentation
     * @return \Illuminate\Http\Response
     */
    public function edit(Int $desarrollo, Int $id)
    {
        $info = [
            'title' => 'Documentación',
            'breadcrumb' => [
                [
                    'title' => 'Todos',
                    'route' => 'panel.developments.index',
                ],
                [
                    'title' => Development::find($desarrollo)->name,
                    'route' => 'panel.developments.edit',
                    'params' => ['desarrollo' => $desarrollo],
                ],
                [
                    'title' => 'Documentación',
                    'route' => 'panel.documentation.index',
                    'params' => ['desarrollo' => $desarrollo],
                ],
                [
                    'title' => 'Modificar Documento',
                    'route' => 'panel.documentation.edit',
                    'params' => ['desarrollo' => $desarrollo, 'documentacion' => $id],
                    'active' => true
                ],
            ],
        ];
        $info['documentation'] = DevelopmentClientDocumentation::find($id);
        return view('panel.documentation.edit', $info);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DevelopmentClientDocumentation  $developmentClientDocumentation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Int $desarrollo, Int $id)
    {
        $arr = $request->toArray();
        $arr['slug'] = Str::slug($arr['name']);
        $documentation = DevelopmentClientDocumentation::find($id);
        $arr['mostrar_condicion']=intval(isset($arr['mostrar_condicion']));
        $arr['show_contract']=intval(isset($arr['show_contract']));
        $arr['fijo']=intval(isset($arr['fijo']));
        $documentation->update($arr);
        return redirect()->back()->with('success', 'Operación exitosa');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DevelopmentClientDocumentation  $developmentClientDocumentation
     * @return \Illuminate\Http\Response
     */
    public function destroy(Int $desarrollo, Int $id)
    {
        if(DevelopmentClientDocumentation::find($id)){
            DevelopmentClientDocumentation::destroy($id);
            return response(['success' => true], 200);
        }else{
            return response(['success' => false], 200);
        }
    }
}
