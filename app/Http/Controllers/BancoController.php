<?php

namespace App\Http\Controllers;

use App\Banco;
use App\Development;
use Illuminate\Http\Request;

class BancoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $info = [
            'title' => 'C. de Banco',
            'breadcrumb' => [
                [
                    'title' => 'Todos',
                    'route' => 'panel.developments.index',
                ],
                [
                    'title' => Development::find($request->desarrollo)->name,
                    'route' => 'panel.developments.edit',
                    'params' => ['desarrollo' => $request->desarrollo],
                ],
                [
                    'title' => 'Cuentas de banco',
                    'route' => 'panel.bancos.index',
                    'params' => ['desarrollo' => $request->desarrollo],
                    'active' => true
                ]
            ],
            'buttons' => [
                [
                    'title' => 'Agregar Nuevo',
                    'route' => 'panel.bancos.create',
                    'params' => ['desarrollo' => $request->desarrollo]
                ]
            ]
        ];
        $info['data'] = Banco::where('development_id', $request->desarrollo)->get()->sortByDesc('id');
        return view('panel.bancos.index', $info);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $info = [
            'title' => 'C. de Banco',
            'breadcrumb' => [
                [
                    'title' => 'Todos',
                    'route' => 'panel.developments.index',
                ],
                [
                    'title' => Development::find($request->desarrollo)->name,
                    'route' => 'panel.developments.edit',
                    'params' => ['desarrollo' => $request->desarrollo],
                ],
                [
                    'title' => 'Cuentas de banco',
                    'route' => 'panel.bancos.index',
                    'params' => ['desarrollo' => $request->desarrollo],
                ],
                [
                    'title' => 'Nuevo',
                    'route' => 'panel.bancos.create',
                    'params' => ['desarrollo' => $request->desarrollo],
                ]
            ]
        ];
        return view('panel.bancos.create', $info);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'n_cuenta' => 'required',
            'referencia' => 'required',
            'status' => 'required'
        ]);
        $arr = $request->toArray();
        $arr['development_id'] = $request->desarrollo;
        $banco = Banco::create($arr);
        return redirect()->route('panel.bancos.index', ['desarrollo' => $request->desarrollo])->with('success', 'Operación exitosa');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Banco  $banco
     * @return \Illuminate\Http\Response
     */
    public function show(Banco $banco)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Banco  $banco
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $info = [
            'title' => 'C. de Banco',
            'breadcrumb' => [
                [
                    'title' => 'Todos',
                    'route' => 'panel.developments.index',
                ],
                [
                    'title' => Development::find($request->desarrollo)->name,
                    'route' => 'panel.developments.edit',
                    'params' => ['desarrollo' => $request->desarrollo],
                ],
                [
                    'title' => 'Cuentas de banco',
                    'route' => 'panel.bancos.index',
                    'params' => ['desarrollo' => $request->desarrollo],
                ],
                [
                    'title' => 'Editar',
                    'route' => 'panel.bancos.edit',
                    'params' => ['desarrollo' => $request->desarrollo, 'banco' => $request->banco],
                ]
            ]
        ];
        $info['banco'] = Banco::find($request->banco);
        return view('panel.bancos.edit', $info);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Banco  $banco
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'n_cuenta' => 'required',
            'referencia' => 'required',
            'status' => 'required'
        ]);

        if($banco = Banco::find($request->banco)){
            $banco->update($request->toArray());
            return redirect()->back()->with('success', 'Información actualizada');
        }else{
            return redirect()->back()->withErrors(['invalid' => 'Lo sentimos, algo salió mal']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Banco  $banco
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        if(Banco::find($request->banco)){
            Banco::destroy($request->banco);
            return response(['success' => true]);
        }else{
            return response(['success' => false]);
        }
    }
}
