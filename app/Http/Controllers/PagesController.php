<?php

namespace App\Http\Controllers;

use App\Configuration;
use Illuminate\Http\Request;
use App\Propierty;
use App\PropertyType;
use App\Operation;
use App\Zone;

class PagesController extends Controller
{
    public function index(){
        $maintenance = Configuration::find(1);
        if($maintenance->active){
            return view('pages.maintenance');
        }
        $data['outstanding'] = Propierty::where('outstanding', 1)->where('status', 'visible')->inRandomOrder()->take(3)->get();
        $data['types'] = PropertyType::where('status', 'visible')->get();
        if(($data['types']) && ($data['types']->count() > 0)){
            foreach($data['types'] as $type){
                $data['outstanding_type'][$type->slug] = Propierty::where('outstanding', 1)->where('type', $type->id)->where('status', 'visible')->inRandomOrder()->take(3)->get();    
            }
        }
        $data['news'] = Propierty::where('status', 'visible')->orderBy('created_at', 'desc')->inRandomOrder()->take(9)->get();
        $data['operations'] = Operation::where('status', 'visible')->get();
        
        return view('pages.index', $data);
    }
    public function propiedades(Request $request){
        $maintenance = Configuration::find(1);
        if($maintenance->active){
            return view('pages.maintenance');
        }
        //Recuperamos las zonas
        $data['zones'] = Zone::where('status', 'visible')->get();
        $data['operations'] = Operation::where('status', 'visible')->get();
        $data['types'] = PropertyType::where('status', 'visible')->get();

        $data['properties'] = Propierty::where('status', 'visible');
        
        if(isset($request->zona)){
            $data['properties'] = $data['properties']->where('zone_id', $request->zona);
        }

        if(isset($request->search)){
            $data['properties'] = $data['properties']->where('name', $request->search)->orWhere('code', $request->search);
        }

        if(isset($request->operacion) && ($request->operacion !== 'Operación')){
            $data['properties'] = $data['properties']->where('operation', $request->operacion);
        }

        if(isset($request->tipo) && ($request->tipo !== 'Tipo de propiedad')){
            $type = PropertyType::where('slug', $request->tipo)->first();
            $data['properties'] = $data['properties']->where('type', $type->id);
        }

        if(isset($request->nivel)){
            if($request->nivel == '5+'){
                dd('entra');
                $data['properties'] = $data['properties']->where('level', '>=', $request->nivel);
            }else{
                $data['properties'] = $data['properties']->where('level', $request->nivel);
            }
        }

        if(isset($request->recamara)){
            if($request->recamara == '5+'){
                $data['properties'] = $data['properties']->where('bedrooms', '>=', $request->recamara);
            }else{
                $data['properties'] = $data['properties']->where('bedrooms', $request->recamara);
            }
        }

        if(isset($request->baño)){
            if($request->baño == '5+'){
                $data['properties'] = $data['properties']->where('bathrooms', '>=', $request->baño);
            }else{
                $data['properties'] = $data['properties']->where('bathrooms', $request->baño);
            }
        }

        if(isset($request->sort)){
            
        }

        $data['total'] = $data['properties']->get()->count();
        $data['properties'] = $data['properties']->paginate(15);
        return view('pages.propiedades', $data);
    }
    public function desarrollo(Request $request){
        $maintenance = Configuration::find(1);
        if($maintenance->active){
            return view('pages.maintenance');
        }
        return view('pages.desarrollo');
    }
    public function contact(Request $request){
        $maintenance = Configuration::find(1);
        if($maintenance->active){
            return view('pages.maintenance');
        }
        $data['zones'] = Zone::where('status', 'visible')->get();
        $data['operations'] = Operation::where('status', 'visible')->get();
        $data['types'] = PropertyType::where('status', 'visible')->get();
        return view('pages.contacto', $data);
    }

    public function aboutUs(Request $request){
        $maintenance = Configuration::find(1);
        if($maintenance->active){
            return view('pages.maintenance');
        }
        return view('pages.nosotros');
    }

    public function building(Request $request){
        $maintenance = Configuration::find(1);
        if($maintenance->active){
            return view('pages.maintenance');
        }
        return view('pages.building');
    }

    public function mailResponse(Request $request){
        return view('pages.contact-form-response');
    }

    public function aviso(){
        $maintenance = Configuration::find(1);
        if($maintenance->active){
            return view('pages.maintenance');
        }
        return view('pages.aviso-privacidad');
    }
    public function detalle(Int $id, String $slug){
        $maintenance = Configuration::find(1);
        if($maintenance->active){
            return view('pages.maintenance');
        }
        $data['property'] = Propierty::find($id);
        $data['news'] = Propierty::where('status', 'visible')->where('id', '!=', $id)->orderBy('created_at', 'desc')->inRandomOrder()->take(3)->get();
        $data['outstanding'] = Propierty::where('outstanding', 1)->where('status', 'visible')->where('id', '!=', $id)->inRandomOrder()->take(5)->get();
        $data['similar'] = Propierty::where('type', $data['property']->type)->where('status', 'visible')->where('id', '!=', $id)->inRandomOrder()->take(9)->get();
        return view('pages.detalle-propiedad', $data);
    }
}
