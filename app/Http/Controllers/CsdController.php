<?php

namespace App\Http\Controllers;

use App\Venta;
use App\Admin;
use App\VentaRecibo;
use App\VentaDocumentation;
use App\Development;
use App\Quotation;
use App\Client;
use App\Lote;
use App\Financiamiento;
use App\VentaPagoEnganche;
use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade as PDF;
use App\Notifications\RevokeQuotation;
use App\Notifications\ReviewStep;
use App\Notifications\ReviewAccepted;
use Illuminate\Notifications\Notification;
use App\Providers\PermissionKey;
use Illuminate\Support\Str;
use PhpOffice\PhpWord\PhpWord as PhpWord;
use PhpOffice\PhpWord\TemplateProcessor as TemplateProcessor;
use Illuminate\Support\Facades\Storage;

class CsdController extends Controller
{
    const URL = 'https://api.facturama.mx/';
    private const HEADERS = [
      'Authorization: Basic R2lwc3NhOmZpbmdpcDIwMjE=',
      'Content-Type: application/json'
    ];

    public function setHeaders($headers) {
        $this->headers = $headers;
    }

    public static function createCSD(array $data){
      //https://apisandbox.facturama.mx/api-lite/csds POST
      //dd($data);
      $key = base64_encode(($data['csd_key'])->get());
      $cer = base64_encode(($data['csd_cer'])->get());
      
      $csd = array(
        "Rfc" => $data['rfc'],
        "Certificate" => $cer,
        "PrivateKey" => $key,
        "PrivateKeyPassword" => $data['csd_password']
      );

      $json = json_encode($csd);

      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, self::URL.'api-lite/csds');
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_HTTPHEADER, self::HEADERS);
      curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
      $result = curl_exec($ch);
      
      if ($result === FALSE) {
          die('Curl failed: ' . curl_error($ch));
      }
      curl_close($ch);

      return json_decode($result);
    }

    public static function updateCSD(array $data){
      //https://www.api.facturama.com.mx/api-lite/csds/{rfc} PUT
      //dd($data);
      $key = base64_encode(($data['csd_key'])->get());
      $cer = base64_encode(($data['csd_cer'])->get());
     
      $csd = array(
        "Rfc" => $data['rfc'],
        "Certificate" => $cer,
        "PrivateKey" => $key,
        "PrivateKeyPassword" => $data['csd_password']
      );

      $json = json_encode($csd);
//dd($json);
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, self::URL.'api-lite/csds/'.$data['rfc']);
      curl_setopt($ch, CURLOPT_HTTPHEADER, self::HEADERS);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
      curl_setopt($ch, CURLOPT_POSTFIELDS, $json);

      $result = curl_exec($ch);
      //dd(json_decode($result));
      if ($result === FALSE) {
          die('Curl failed: ' . curl_error($ch));
      }
      curl_close($ch);

      return json_decode($result);
    }

    public static function getCSD(String $rfc){
      //https://apisandbox.facturama.mx/api-lite/csds/{rfc} GET

      $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, self::URL.'api-lite/csds/'.$rfc);
        curl_setopt($ch, CURLOPT_HTTPHEADER, self::HEADERS);
        curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);

        $result = curl_exec($ch);
        //dd($result);
        //dd(isset(json_decode($result)->Rfc));
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        
        curl_close($ch);
        return json_decode($result);
        
    }

    public function getCSDS(){
      //https://apisandbox.facturama.mx/api-lite/csds GET

      $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, self::URL.'api-lite/csds/');
        curl_setopt($ch, CURLOPT_HTTPHEADER, self::HEADERS);
        curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $result = curl_exec($ch);
        dd($result);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        curl_close($ch);
    }

    public static function deleteCSD(String $rfc){
      //https://apisandbox.facturama.mx/api-lite/csds/{rfc} DELETE
      //dd($rfc);
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, self::URL.'api-lite/csds/'.$rfc);
      curl_setopt($ch, CURLOPT_HTTPHEADER, self::HEADERS);
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
      curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
    
      $result = curl_exec($ch);
      //dd(json_decode($result));
      if ($result === FALSE) {
          die('Curl failed: ' . curl_error($ch));
      }
      curl_close($ch);

      return json_decode($result);
    }
}
