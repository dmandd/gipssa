<?php

namespace App\Http\Controllers;

use App\Comission;
use App\Venta;
use Exception;
use App\VentaRecibo;
use Illuminate\Support\Str;
use Illuminate\Http\Request;

class VentaReciboController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $arr = $request->toArray();
        
        if(isset($arr['title'])){
            $arr['slug'] = Str::slug($arr['title']);
        }
        if(isset($arr['fecha_pago'])){
            $arr['fecha_pago'] = date('Y-m-d', strtotime(str_replace('/', '-', $arr['fecha_pago'])));
        }
        if((isset($arr['slug']))){
            //Calculamos que el abono que se intente guardar no sobrepase el adeudo
            $venta = Venta::find($request->venta_id);
            
            switch ($arr['slug']) {
                case 'abono-enganche':
                    $abonos = $venta->recibos('abono-enganche', false)->where('status', 'paid')->get()->sum('monto');
                    $saldo = getAmmount($venta->quotation()->detail->monto_enganche);
                break;
                case 'abono-contrato':
                    $abonos = $venta->recibos('abono-contrato', false)->where('status', 'paid')->get()->sum('monto');
                    $saldo = $venta->monto_contrato;
                break;
                case 'mensualidad':
                    $abonos = $venta->recibos('mensualidad', false)->where('status', 'paid')->get()->sum('monto');
                    $saldo = $venta->quotation()->detail->financiamiento;
                break;
                case 'saldo':
                    $abonos = $venta->recibos('saldo', false)->where('status', 'paid')->get()->sum('monto');
                    $saldo =  getAmmount($venta->quotation()->detail->saldo);
                break;
                
                default:
                    $abonos = $venta->recibos($arr['slug'], false)->where('status', 'paid')->get()->sum('monto');
                    $saldo = $venta->monto_contrato;
                break;
            }
            if(($abonos + $arr['monto']) > $saldo){
                return redirect()->back()->withErrors(['invalid' => 'Monto invalido. Los abonos no puede sobrepasar el valor del saldo']);
            }
        }
        
        $ventaRecibo = VentaRecibo::create($arr);
        if($ventaRecibo){
            return redirect()->back()->with('success', 'Operación exitosa');
        }else{
            return redirect()->back()->withInput($request->input())->withErrors(['invalid' => 'Error. Algo salió mal']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\VentaRecibo  $ventaRecibo
     * @return \Illuminate\Http\Response
     */
    public function show(VentaRecibo $ventaRecibo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\VentaRecibo  $ventaRecibo
     * @return \Illuminate\Http\Response
     */
    public function edit(VentaRecibo $ventaRecibo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\VentaRecibo  $ventaRecibo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Int $venta_id, Int $id)
    {
        $arr = $request->toArray();
        if($request->file('file')){
            $path_file = $request->file('file')->store('public/media');
            $_exploded = explode('/', $path_file);
            $_exploded[0] = 'storage';
            $path_file = implode('/', $_exploded);
            $arr['file'] = $path_file;
        }
        $ventaRecibo = VentaRecibo::find($id);
        $ventaRecibo->update($arr);
        if($ventaRecibo){
            return redirect()->back()->with('success', 'Operación exitosa');
        }else{
            return redirect()->back()->withInput($request->input())->withErrors(['invalid' => 'Error. Algo salió mal']);
        }
    }

    public function my_authorize(Request $request, Int $venta_id, Int $id){
        if($ventaRecibo = VentaRecibo::find($id)){
            if($ventaRecibo->slug == 'mensualidad'){
                //Actualizamos la fecha del siguiente pago
                $venta = Venta::find($ventaRecibo->venta_id);
                $fecha = date("Y-m-d", strtotime($venta->fecha_siguiente_pago."+1 month"));
                $venta->update(['fecha_siguiente_pago' => $fecha]);
            }
            $ventaRecibo->update(['status' => 'paid']);
            return response(['success' => true], 200);
        }else{
            return response(['success' => false], 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\VentaRecibo  $ventaRecibo
     * @return \Illuminate\Http\Response
     */
    public function destroy(Int $venta_id, Int $id)
    {
        if(VentaRecibo::find($id)){
            $ventaRecibo = VentaRecibo::find($id);
            if(($ventaRecibo->slug == 'mensualidad') && ($ventaRecibo->status == 'paid')){
                //Regresamos la fecha del siguiente pago para un mes antes
                $venta = Venta::find($ventaRecibo->venta_id);
                $fecha = date("Y-m-d", strtotime($venta->fecha_siguiente_pago."-1 month"));
                $venta->update(['fecha_siguiente_pago' => $fecha]);
            }
            VentaRecibo::destroy($id);
            return response(['success' => true], 200);
        }else{            
            return response(['success' => false], 200);
        }
    }
}
