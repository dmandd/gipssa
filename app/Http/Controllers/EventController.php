<?php

namespace App\Http\Controllers;

use Calendar;
use App\Event;
use App\Propierty;
use App\Prospect;
use App\Client;
use App\Venta;
use Illuminate\Http\Request;
use App\Providers\PermissionKey;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $info = [
            'title' => 'Calendario - Eventos',
            'breadcrumb' => [
                [
                    'title' => 'Todos',
                    'route' => 'panel.events.index',
                    'active' => true
                ]
            ],
            'events' => []
        ];
        if($request->user()->can(PermissionKey::Event['permissions']['index']['name'])){
            $data = Event::all();
        }else{
            $data = Event::where('admin_id', $request->user()->id)->get();
        }
        if($data->count()) {
            foreach ($data as $key => $value) {
                $info['events'][] = Calendar::event(
                    $value->title,
                    false,
                    new \DateTime($value->start_date),
                    new \DateTime($value->end_date),
                    null,
                    // Add color and link on event
	                [
                        'action' => route('panel.events.update', ['evento' => $value->id]),
                        'color' => $value->color,
                        'description' => $value->description,
                        'admin_id' => $value->admin_id,
                        'contact_id' => $value->contact_id.'-'.$value->contact_type,
                        'delete' => route('panel.events.destroy', ['evento' => $value->id]),
	                ]
                );
            }
        }
        //Agregamos los cumpleaños de los clientes
        if($request->user()->can(PermissionKey::Client['permissions']['all']['name'])){
            $clients = Client::all();
        }else{
            $clients = Client::where('admin_id', $request->user()->id)->get();
        }
        if($clients->count() > 0){
            foreach($clients as $client){
                $mes = date('m', strtotime($client->fecha_nacimiento));
                $dia = date('d', strtotime($client->fecha_nacimiento));
                $fecha = date('Y-'.$mes.'-'.$dia);
                $info['events'][] = Calendar::event(
                    'Cumpleaños de '.$client->name.' '.$client->surname,
                    false,
                    new \DateTime($fecha),
                    new \DateTime($fecha),
                    null,
                    [
                        'action' => '',
                        'color' => '#45ce89',
                        'description' => 'Cumpleaños de '.$client->name.' '.$client->surname,
                        'admin_id' => 1,
                        'contact_id' => $client->id.'-'.Client::class,
                        'delete' => '',
                    ]
                );
            }
        }
        
        //Agregamos los pagos
        if($request->user()->can(PermissionKey::Venta['permissions']['seguimiento_pagos']['name'])){
            if(Venta::where('status', 4)->get()->count() > 0){
                $ventas = Venta::where('status', 4)->get();
                foreach($ventas as $venta){
                    $info['events'][] = Calendar::event(
                        'Pago Abono Venta '.$venta->lote()->name.', Desarrollo: '.$venta->development()->name,
                        false,
                        new \DateTime($venta->fecha_siguiente_pago),
                        new \DateTime($venta->fecha_siguiente_pago),
                        null,
                        [
                            'action' => '',
                            'color' => '#45ce89',
                            'description' => 'Pago Abono Venta '.$venta->lote()->name.', Desarrollo: '.$venta->development()->name,
                            'admin_id' => 1,
                            'contact_id' => $venta->client()->id.'-'.Client::class,
                            'delete' => '',
                        ]
                    );
                }
            }
        }

        Calendar::setCallbacks([
            'eventRender' => 'function(event, element) {
                element.popover({
                  animation: true,
                  title: event.title,
                  content: (event.description) ? event.description : "Sin descripción",
                  trigger: "hover",
                  placement: "top"
                });
            }'
        ]);
        $info['calendar'] = Calendar::addEvents($info['events']);
        return view('panel.events.index', $info);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $arr = $request->toArray();
        $arr['start_date'] = date('Y-m-d H:i:s', strtotime($request->start_date.' '.$request->start_time));
        $arr['end_date'] = date('Y-m-d H:i:s', strtotime($request->start_date.' '.$request->start_time.' + '.$request->length.' minute'));
        if(!isset($request->admin_id)){
            $arr['admin_id'] = $request->user()->id;
        }
        if(isset($arr['contact_id'])){
            $exploded = explode('-', $arr['contact_id']);
            $arr['contact_id'] = $exploded[0];
            $arr['contact_type'] = $exploded[1];
        }
        //Verificamos sí la fecha de inicio colinda con otro evento de este usuario
        if(Event::where('admin_id', $arr['admin_id'])->where('start_date', $arr['start_date'])->get()->count() > 0){
            return redirect()->back()->withErrors(['invalid' => 'Ya existe un evento para la fecha seleccionada']);
        }else{
            $event = Event::create($arr);
            return redirect()->route('panel.events.index')->with('success', 'Operación exitosa');
        }
    }

    public function agendar(Request $request, Int $property_id){
        $arr = $request->toArray();
        $arr['start_date'] = date('Y-m-d H:i:s', strtotime($request->start_date.' '.$request->start_time));
        $arr['end_date'] = date('Y-m-d H:i:s', strtotime($request->start_date.' '.$request->start_time.' + 30 minute'));
        $arr['admin_id'] = Propierty::find($property_id)->admin_id;
        $arr['title'] = 'Cita Propiedad clave: '.Propierty::find($property_id)->code;
        //Creamos prospecto
        if($prospect = Prospect::where('email', $arr['email'])->first()){
            $arr['contact_id'] = $prospect->id;
            $arr['contact_type'] = $prospect->model();
        }else{
            $prospect = Prospect::create($arr);
            $arr['contact_id'] = $prospect->id;
            $arr['contact_type'] = $prospect->model();
        }
        //Verificamos sí la fecha de inicio colinda con otro evento de este usuario
        if(Event::where('admin_id', $arr['admin_id'])->where('start_date', $arr['start_date'])->get()->count() > 0){
            return redirect()->back()->withErrors(['invalid' => 'Ya existe un evento para la fecha seleccionada']);
        }else{
            $event = Event::create($arr);
            return redirect()->back()->with('success', 'Operación exitosa');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function show(Event $event)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function edit(Event $event)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Int $id)
    {
        if($event = Event::find($id)){
            $arr = $request->toArray();
            $arr['start_date'] = date('Y-m-d H:i:s', strtotime($request->start_date.' '.$request->start_time));
            $arr['end_date'] = date('Y-m-d H:i:s', strtotime($request->start_date.' '.$request->start_time.' + '.$request->length.' minute'));
            if(!isset($request->admin_id)){
                $arr['admin_id'] = $event->admin_id;
            }
            if(Event::where('admin_id', $arr['admin_id'])->where('start_date', $arr['start_date'])->get()->count() > 0){
                return redirect()->back()->withErrors(['invalid' => 'Ya existe un evento para la fecha seleccionada']);
            }else{
                $event->update($arr);
                return redirect()->route('panel.events.index')->with('success', 'Operación exitosa');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function destroy(Int $id)
    {
        if(Event::find($id)){
            Event::destroy($id);
            return response(['success' => true]);
        }else{
            return response(['success' => false]);
        }
    }
}
