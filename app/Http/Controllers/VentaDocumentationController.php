<?php

namespace App\Http\Controllers;

use App\VentaDocumentation;
use App\Venta;
use App\VentaRecibo;
use Illuminate\Http\Request;
use App\Notifications\RevokeDocument;

class VentaDocumentationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Int $venta)
    {
        // $validatedData = $request->validate([
        //     'document' => 'mimes:jpeg,jpg',
        // ]);
        if((isset($request->document)) && (count($request->document) > 0)){
            foreach ($request->document as $key => $doc) {
                if($doc->getSize() > 2000000){
                    return redirect()->back()->withInputs($request->input())->withErrors(['invalid' => 'El tamaño máximo de los archivos es de 1.9MB']);
                }else{
                    //Procedemos a guardar el archivo
                    $path_file = $doc->store('public/documents');
                    $_exploded = explode('/', $path_file);
                    $_exploded[0] = 'storage';
                    $path_file = implode('/', $_exploded);
                    //Validamos sí es un nuevo documento
                    if($document = VentaDocumentation::where('development_client_documentation_id', $key)->where('venta_id', $venta)->first()){
                        $document->update([
                            'file' => $path_file,
                            'status' => 'created',
                        ]);
                    }else{
                        $document = VentaDocumentation::create([
                            'venta_id' => $venta,
                            'development_client_documentation_id' => $key,
                            'file' => $path_file,
                            'status' => 'created',
                        ]);
                    }
                }
            }
            return redirect()->back()->with('success', 'Operación exitosa');
        }else{
            if($request->file('file')){
                $path_file = $request->file('file')->store('public/media');
                $_exploded = explode('/', $path_file);
                $_exploded[0] = 'storage';
                $path_file = implode('/', $_exploded);
                $arr['file'] = $path_file;
                $arr['venta_id'] = $venta;
                if($ventaRecibo = VentaRecibo::where('venta_id', $venta)->where('slug', 'condicion-de-compra')->first()){
                    $ventaRecibo->update($arr);
                }else{
                    $ventaRecibo = VentaRecibo::create($arr);
                }
                return redirect()->back()->with('success', 'Operación exitosa');
            }else{
                return redirect()->back()->withErrors(['invalid' => 'No se realizó operación']);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\VentaDocumentation  $ventaDocumentation
     * @return \Illuminate\Http\Response
     */
    public function show(VentaDocumentation $ventaDocumentation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\VentaDocumentation  $ventaDocumentation
     * @return \Illuminate\Http\Response
     */
    public function edit(VentaDocumentation $ventaDocumentation)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\VentaDocumentation  $ventaDocumentation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Int $venta, Int $documento)
    {
        if($document = VentaDocumentation::find($documento)){
            if($request->status == 'rejected'){
                $_venta = Venta::find($venta);
                $document = VentaDocumentation::find($documento);
                $_venta->asesor()->notify(new RevokeDocument($_venta, $document, $request->motivo));
            }
            $document->update($request->toArray());
            if($request->ajax()){
                return response(['success' => true], 200);
            }else{
                return redirect()->back()->with('success', 'Operación exitosa');
            }
        }else{
            if($request->ajax()){
                return response(['success' => false], 200);
            }else{
                return redirect()->back();
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\VentaDocumentation  $ventaDocumentation
     * @return \Illuminate\Http\Response
     */
    public function destroy(VentaDocumentation $ventaDocumentation)
    {
        //
    }
}
