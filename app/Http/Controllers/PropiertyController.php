<?php

namespace App\Http\Controllers;

use App\Propierty;
use App\Zone;
use App\Tag;
use App\Area;
use App\AreaEquipment;
use App\Contact;
use App\Operation;
use App\PropertyGallery;
use App\PropertyType;
use Illuminate\Http\Request;
use App\DataTables\PropiertiesDataTable;
use App\Providers\PermissionKey;
use App\Admin;

class PropiertyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $info = [
            'title' => 'Propiedades',
            'breadcrumb' => [
                [
                    'title' => 'Todos',
                    'route' => 'panel.propierties.index',
                    'active' => true
                ]
            ],
            'buttons' => []
        ];

        if($request->user()->can(PermissionKey::Property['permissions']['create']['name']))
            array_push($info['buttons'],[
                'title' => 'Agregar Nuevo',
                'route' => 'panel.propierties.create'
            ]);

        if($request->user()->can(PermissionKey::Property['permissions']['index']['name'])){
            $info['data'] = Propierty::all()->sortByDesc('id');
        }else{
            $info['data'] = Propierty::where('admin_id', $request->user()->id)->get();
        }
        return view('panel.propierties.index', $info);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $info = [
            'title' => 'Propiedades',
            'breadcrumb' => [
                [
                    'title' => 'Todos',
                    'route' => 'panel.propierties.index',
                ],
                [
                    'title' => 'Nuevo',
                    'route' => 'panel.propierties.create',
                    'active' => true
                ]
            ]
        ];
        $info['zones'] = Zone::where('status', 'visible')->get();
        $info['tags'] = Tag::where('status', 'visible')->get();
        $info['areas'] = Area::where('status', 'visible')->get();
        $info['types'] = PropertyType::where('status', 'visible')->get();
        $info['operations'] = Operation::where('status', 'visible')->get();
        $info['contacts'] = [];
        $info['emails'] = [];
        $info['phones'] = [];
        $contacts = Contact::all();
        foreach($contacts as $contact){
            $info['contacts'][$contact->name] = $contact;
            $info['emails'][$contact->email] = $contact;
            $info['phones'][$contact->phone] = $contact;
        }
        return view('panel.propierties.create', $info);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->contact['id']){
            $contact = Contact::find($request->contact['id']);
        }else{
            //Validamos que el contacto no exista (en teoría)
            $this->validate($request, [
                'email' => 'unique:contacts',
                'phone' => 'unique:contacts'
            ]);
            if($contact = Contact::where('email', $request->contact['email'])->orWhere('phone', $request->contact['phone'])->first()){
                return redirect()->back()->withInput($request->input())->withErrors(['invalid' => 'Información de contacto duplicado']);
            }else{
                //Creamos el contacto
                $contact = Contact::create($request->toArray()['contact']);
            }
        }
        //Creamos la propiedad
        $arr = $request->toArray();
        $arr['admin_id'] = $request->user()->id;
        $arr['contact_id'] = $contact->id;
        $arr['city_id'] = 1;
        $arr['code'] = Propierty::code($request);
        $property = Propierty::create($arr);
        $property->update(['code' => $arr['code']]);
        return redirect()->route('panel.propierties.edit', ['propiedade' => $property->id])->with('success', 'Operación exitosa');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Propierty  $propierty
     * @return \Illuminate\Http\Response
     */
    public function show(Propierty $propierty)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Propierty  $propierty
     * @return \Illuminate\Http\Response
     */
    public function edit(Int $id)
    {
        $info = [
            'title' => 'Propiedades',
            'breadcrumb' => [
                [
                    'title' => 'Todos',
                    'route' => 'panel.propierties.index',
                ],
                [
                    'title' => 'Editar',
                    'route' => 'panel.propierties.edit',
                    'params' => ['propiedade' => $id],
                    'active' => true
                ]
            ]
        ];
        $info['zones'] = Zone::where('status', 'visible')->get();
        $info['tags'] = Tag::where('status', 'visible')->get();
        $info['areas'] = Area::where('status', 'visible')->get();
        $info['types'] = PropertyType::where('status', 'visible')->get();
        $info['operations'] = Operation::where('status', 'visible')->get();
        $info['property'] = Propierty::find($id);
        $info['admins'] = Admin::all();
        return view('panel.propierties.edit', $info);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Propierty  $propierty
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Int $id)
    {
        if($property = Propierty::find($id)){
            if(isset($request->gallery)){
                $gallery = json_decode($request->gallery);
                foreach ($gallery as $key => $image) {
                    $property->addImage($image);
                }
            }
            $arr = $request->toArray();
            $arr['code'] = Propierty::code($request);
            $property->update($request->toArray());
            return redirect()->back()->with('success', 'Operación exitosa');
        }else{
            return redirect()->back()->with('invalid', 'Lo sentimos algo salió mal');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Propierty  $propierty
     * @return \Illuminate\Http\Response
     */
    public function destroy(Int $id)
    {
        if(Propierty::find($id)){
            //Eliminamos las áreas asignadas a esta propiedad
            $areas = Area::where('property_id', $id)->get();
            foreach($areas as $area){
                AreaEquipment::where('area_id', $area->id)->delete();
            }
            Area::where('property_id', $id)->delete();
            Propierty::destroy($id);
            return response(['success' => true], 200);
        }else{
            return response(['success' => false], 200);
        }
    }

    public function removeImage(Request $request, Int $id){
        if($image = PropertyGallery::where('propierty_id', $id)->where('image', $request->image)->first()){
            PropertyGallery::destroy($image->id);
            return response(['success' => true]);
        }else{
            return response(['success' => false]);
        }
    }
}
