<?php

namespace App\Http\Controllers;

use App\Task;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $info = [
            'title' => 'Prospectos',
            'breadcrumb' => [
                [
                    'title' => 'Todos',
                    'route' => 'panel.tasks.index',
                    'active' => true
                ]
            ],
            'buttons' => [
                [
                    'title' => 'Agregar Nuevo',
                    'route' => 'panel.tasks.create'
                ],
            ]
        ];
        //dd(Task::where('default', 1)->orderBy('order','asc')->get());
        $info['data'] = Task::where('default', 1)->orderBy('order','asc')->get();
        return view('panel.tasks.index', $info);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $info = [
            'title' => 'Actividades',
            'breadcrumb' => [
                [
                    'title' => 'Todos',
                    'route' => 'panel.tasks.index',
                ],
                [
                    'title' => 'Nuevo',
                    'active' => true
                ]
            ],
        ];
        return view('panel.tasks.create', $info);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(Task::where('order','=',$request->order)->get()->count() > 0)
            return redirect()->back()->withInput($request->input())->withErrors(['invalid' => 'Este orden ya está en uso, por favor intenta con otro.']);
        
        $inputs = $request->input();
        $inputs['default'] = 1;
        $task = Task::create($inputs);
        if($task)
            return redirect()->route('panel.tasks.index');
        else
            return redirect()->back()->withErrors(['invalid' => 'Algo salió mal']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function show(Task $task)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Task $task)
    {
        $info = [
            'title' => 'Actividades',
            'breadcrumb' => [
                [
                    'title' => 'Todos',
                    'route' => 'panel.tasks.index',
                ],
                [
                    'title' => 'Modificar',
                    // 'route' => 'panel.tasks.edit',
                    'params' => ['actividade' => Task::find($request->actividade)],
                    'active' => true
                ]
            ],
        ];
        $info['task'] = Task::find($request->actividade);
        return view('panel.tasks.edit', $info);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Task $task)
    {   
        //dd($request->id);
        if(Task::where([['id','!=',$request->id],['order','=',$request->order]])->get()->count() > 0)
            return redirect()->back()->withInput($request->input())->withErrors(['invalid' => 'Este orden ya está en uso, por favor intenta con otro.']);

        $task = Task::find($request->actividade);
        $task->update($request->toArray());
        return redirect()->back()->with('success', 'Operación exitosa');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function destroy(Task $task)
    {
        //
    }
}
