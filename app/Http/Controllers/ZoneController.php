<?php

namespace App\Http\Controllers;

use App\Zone;
use Illuminate\Http\Request;

class ZoneController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $info = [
            'title' => 'Zonas',
            'breadcrumb' => [
                [
                    'title' => 'Todos',
                    'route' => 'panel.zones.index',
                    'active' => true
                ]
            ],
            'buttons' => [
                [
                    'title' => 'Agregar Nuevo',
                    'route' => 'panel.zones.create'
                ]
            ]
        ];
        $info['data'] = Zone::all()->sortByDesc('id');
        return view('panel.zones.index', $info);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $info = [
            'title' => 'Zonas',
            'breadcrumb' => [
                [
                    'title' => 'Todos',
                    'route' => 'panel.zones.index',
                    'active' => true
                ]
            ]
        ];
        return view('panel.zones.create', $info);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $zone = Zone::create($request->toArray());
        return redirect()->route('panel.zones.edit', ['zona' => $zone->id])->with('success', 'Operación exitosa');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Zone  $zone
     * @return \Illuminate\Http\Response
     */
    public function show(Zone $zone)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Zone  $zone
     * @return \Illuminate\Http\Response
     */
    public function edit(Int $id)
    {
        $info = [
            'title' => 'Zonas',
            'breadcrumb' => [
                [
                    'title' => 'Todos',
                    'route' => 'panel.zones.index',
                    'active' => true
                ]
            ]
        ];
        $info['zone'] = Zone::find($id);
        return view('panel.zones.edit', $info);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Zone  $zone
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Int $id)
    {
        $zone = Zone::find($id)->update($request->toArray());
        if(isset($arr['api'])){
            return response(['success' => true], 200);
        }else{
            return redirect()->route('panel.zones.edit', ['zona' => $id])
                            ->with('success', 'Elemento actualizado');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Zone  $zone
     * @return \Illuminate\Http\Response
     */
    public function destroy(Int $id)
    {
        if(Zone::find($id)){
            Zone::destroy($id);
            return response(['success' => true], 200);
        }else{
            return response(['success' => false], 200);
        }
    }
}
