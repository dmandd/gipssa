<?php

namespace App\Http\Controllers;

use App\Area;
use App\Equipment;
use App\AreaEquipment;
use Illuminate\Http\Request;

class AreaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $info = [
            'title' => 'Propiedades',
            'breadcrumb' => [
                [
                    'title' => 'Todos',
                    'route' => 'panel.propierties.index',
                ],
                [
                    'title' => 'Editar',
                    'route' => 'panel.propierties.edit',
                    'params' => ['propiedade' => $id],
                    'active' => true
                ],
                [
                    'title' => 'Áreas',
                    'route' => 'panel.areas.index',
                    'params' => ['propiedade' => $id],
                    'active' => true
                ]
            ],
            'buttons' => [
                [
                    'title' => 'Agregar Nuevo',
                    'route' => 'panel.areas.create',
                    'params' => ['propiedade' => $id],
                ]
            ]
        ];
        $info['data'] = Area::where('property_id', $id)->get()->sortByDesc('id');
        return view('panel.areas.index', $info);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Int $id)
    {
        $info = [
            'title' => 'Áreas',
            'breadcrumb' => [
                [
                    'title' => 'Todos',
                    'route' => 'panel.propierties.index',
                ],
                [
                    'title' => 'Editar',
                    'route' => 'panel.propierties.edit',
                    'params' => ['propiedade' => $id],
                    'active' => true
                ],
                [
                    'title' => 'Áreas',
                    'route' => 'panel.areas.index',
                    'params' => ['propiedade' => $id],
                    'active' => true
                ],
                [
                    'title' => 'Nuevo',
                    'route' => 'panel.areas.create',
                    'params' => ['propiedade' => $id],
                    'active' => true
                ]
            ]
        ];
        $info['equipment'] = Equipment::where('status', 'visible')->get();
        return view('panel.areas.create', $info);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Int $id)
    {
        //Registramos el área
        $arr = $request->toArray();
        $arr['property_id'] = $id;
        $area = Area::create($arr);
        //Sincronizamos el equipamiento de esta área
        if(isset($request->equipment)){
            $area->syncEquipment($request->equipment);
        }
        return redirect()->route('panel.areas.index', ['propiedade' => $id])->with('success', 'Operación exitosa');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Area  $area
     * @return \Illuminate\Http\Response
     */
    public function show(Area $area)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Area  $area
     * @return \Illuminate\Http\Response
     */
    public function edit(Int $property_id, Int $id)
    {
        $info = [
            'title' => 'Áreas',
            'breadcrumb' => [
                [
                    'title' => 'Todos',
                    'route' => 'panel.propierties.index',
                ],
                [
                    'title' => 'Editar',
                    'route' => 'panel.propierties.edit',
                    'params' => ['propiedade' => $property_id],
                    'active' => true
                ],
                [
                    'title' => 'Áreas',
                    'route' => 'panel.areas.index',
                    'params' => ['propiedade' => $property_id],
                    'active' => true
                ],
                [
                    'title' => 'Editar',
                    'route' => 'panel.areas.edit',
                    'params' => ['propiedade' => $property_id, 'area' => $id],
                    'active' => true
                ]
            ]
        ];
        $info['area'] = Area::find($id);
        $info['equipment'] = Equipment::where('status', 'visible')->get();
        return view('panel.areas.edit', $info);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Area  $area
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Int $property_id, Int $id)
    {
        if($area = Area::find($id)){
            $area->update($request->toArray());
            //Sincronizamos el equipamiento de esta área
            if(isset($request->equipment)){
                $area->syncEquipment($request->equipment);
            }
            return redirect()->route('panel.areas.edit', ['propiedade' => $property_id, 'area' => $id])->with('success', 'Operación exitosa');
        }else{
            return redirect()->back()->with('invalid', 'Lo sentimos, algo salió mal.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Area  $area
     * @return \Illuminate\Http\Response
     */
    public function destroy(Int $prop_id, Int $id)
    {
        if($area = Area::find($id)){
            AreaEquipment::where('area_id', $id)->delete();
            Area::destroy($id);
            return response(['success' => true], 200);
        }else{
            return response(['success' => false], 200);
        }
    }
}
