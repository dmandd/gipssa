<?php

namespace App\Http\Controllers;

use App\Venta;
use App\Admin;
use App\VentaRecibo;
use App\VentaDocumentation;
use App\Development;
use App\Quotation;
use App\Client;
use App\Lote;
use App\Financiamiento;
use App\VentaPagoEnganche;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Barryvdh\DomPDF\Facade as PDF;
use App\Notifications\RevokeQuotation;
use App\Notifications\ReviewStep;
use App\Notifications\ReviewAccepted;
use Illuminate\Notifications\Notification;
use App\Providers\PermissionKey;
use Illuminate\Support\Str;
use PhpOffice\PhpWord\PhpWord as PhpWord;
use PhpOffice\PhpWord\TemplateProcessor as TemplateProcessor;
use Illuminate\Support\Facades\Storage;

class FacturacionController extends Controller
{
    const URL = 'https://api.facturama.mx/';
    
    private const HEADERS = [
        'Authorization: Basic R2lwc3NhOmZpbmdpcDIwMjE=',
        'Content-Type: application/json'
    ];

    public function setHeaders($headers) {
        $this->headers = $headers;
    }

    public function getCFDIS(){
        //https://apisandbox.facturama.mx/api-lite/cfdis?type=issued&folioStart=100&folioEnd=200&rfcIssuer=EKU9003173C9&rfc=XAXX&taxEntityName=Publico&dateStart=01/01/2019&dateEnd=15/02/2019&status=active GET

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, self::URL."api-lite/cfdis");
        curl_setopt($ch, CURLOPT_HTTPHEADER, self::HEADERS);
        curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $result = curl_exec($ch);
        dd($result);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        curl_close($ch);
    }

    public function findCFDI(Request $request, String $id){
        //https://apisandbox.facturama.mx/api-lite/cfdis/{id} GET

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, self::URL."api-lite/cfdis/".$id);
        curl_setopt($ch, CURLOPT_HTTPHEADER, self::HEADERS);
        curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $result = curl_exec($ch);
        //dd($result);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        curl_close($ch);
    }

    public function sendCFDI(Request $request){
        //https://www.api.facturama.com.mx/cfdi?cfdiType={cfdiType}&cfdiId={cfdiId}&email={email}&subject={subject}&comments={comments} POST

        $data = [
          "cfdiType" => "",
          "cfdiId" => "",
          "email" => "",
          "subject" => "",
          "comments" => "",
        ];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, self::URL."cfdi");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, self::HEADERS);
        curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        $result = curl_exec($ch);
        //dd($result);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        curl_close($ch);
    }

    public function createCFDI(Request $request){
        //https://www.api.facturama.com.mx/2/cfdis POST
        /*$csd = app('App\Http\Controllers\CsdController')->getCSD(null,"EKU9003173C9");
            dd($csd["Rfc"]);*/
            //dd($request);
        try{
          $recibo = VentaRecibo::find($request->recibo_id);
          if($recibo){
            if($recibo->status === "paid"){
              $venta  = Venta::find($recibo->venta_id);
              if($venta){
                $lote = $venta->lote();
                $cliente = $venta->client();
                $corrida = $venta->corrida();
                $desarrollo = $venta->development();
                
                $serie = !is_null($desarrollo->serie)?$desarrollo->serie:"";
                $folio = "";
                $concepto_pago = "";
                $pagos = "";

                $folio = VentaRecibo::where("serie",$serie)->get('folio')->last();
                if($folio){
                  $folio = ($folio->folio) + 1;
                }else{
                  $folio = 1;
                }
                
                $taxes = array();
                $unit_price = 0;
                $subtotal = 0;

                if($recibo->slug == "pago-bloqueo"){
                  $concepto_pago = "Contrato";
                  $pagos = "1/1";
                  array_push($taxes,array(
                    "Total" =>number_format((float)($recibo->monto - ($recibo->monto / 1.16)), 2, '.', ''),
                    "Name" => "IVA",
                    "Base" => number_format((float)($recibo->monto / 1.16), 2, '.', ''),
                    "Rate" => "0.16",
                    "IsRetention" => false
                  ));
                  $unit_price = number_format((float)($recibo->monto / 1.16), 2, '.', '');
                  $subtotal = number_format((float)($recibo->monto / 1.16), 2, '.', '');
                  
                }else if($recibo->slug == "abono-enganche"){
                  $concepto_pago = "Enganche";
                  $cantidad_pagos = $venta->fechas_enganche()->get();
                  $cantidad_pagados = $venta->recibos('abono-enganche');
                  $pagos = count($cantidad_pagados)."/".count($cantidad_pagos);
                  array_push($taxes,array(
                    "Total" =>0.00,
                    "Name" => "IVA Exento",
                    "Base" => number_format((float)($recibo->monto), 2, '.', ''),
                    "Rate" => 0.00,
                    "IsRetention" => false
                  ));
                  $unit_price = number_format((float)($recibo->monto), 2, '.', '');
                  $subtotal = number_format((float)($recibo->monto), 2, '.', '');
                }else if($recibo->slug == "mensualidad"){
                  $concepto_pago = "Abono a capital";
                  $cantidad_pagos = $venta->recibos('mensualidad');
                  $pagos = count($cantidad_pagos)."/".$corrida->num_pagos;
                  array_push($taxes,array(
                    "Total" =>0,
                    "Name" => "IVA Exento",
                    "Base" => number_format((float)($recibo->monto), 2, '.', ''),
                    "Rate" => 0,
                    "IsRetention" => false
                  ));
                  $unit_price = number_format((float)($recibo->monto), 2, '.', '');
                  $subtotal = number_format((float)($recibo->monto), 2, '.', '');
                }
        
                $factura=array(
                  "Serie" => $serie,
                  "Currency" => "MXN",
                  "ExpeditionPlace" => "97133",
                  "PaymentConditions" => "Los pagos no serán reembolsables por cancelar la opración.",
                  "Folio" => $folio > 9?$folio:"0".$folio,
                  "CfdiType" => "I",
                  "PaymentForm" => $recibo->payment_way,
                  "PaymentMethod" => $recibo->payment_method,
                  "LogoUrl" =>"https://crm.gipssa.com/assets/images/inicio-logo.png",
                  "Issuer" => array(
                    "FiscalRegime" => "612",
                    "Rfc" => $desarrollo->admin()->rfc,
                    "Name" => $desarrollo->admin()->name." ".$desarrollo->admin()->last_name
                  ),
                  "Receiver" => array(
                    "Rfc" => $cliente->rfc?$cliente->rfc:"XAXX010101000",
                    "Name" => $cliente->name." ".$cliente->surname,
                    "CfdiUse" => "P01"
                  ),
                  "Items" => array(
                    array(
                      "ProductCode" => "95101500",
                      "IdentificationNumber" => "000",
                      "Description" => $concepto_pago." ".$pagos." - Desarrollo ".$desarrollo->name." - ".$desarrollo->stage."a. Etapa - Lote ".$lote->name,
                      "Unit" => "NO APLICA",
                      "UnitCode" => "ACT",
                      "UnitPrice" => $unit_price,
                      "Quantity" => 1.0,
                      "Subtotal" => $subtotal,
                      "Discount" => 0.0,
                      "Taxes" => $taxes,
                      "Total" => number_format((float)($recibo->monto), 2, '.', '')
                    )
                  ),
                  "Observations" => $recibo->message
                );
                //dd(($factura));
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, self::URL.'api-lite/2/cfdis');
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_HTTPHEADER, self::HEADERS);
                curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($factura));
                $result = curl_exec($ch);
                //dd(curl_exec($ch));
                if ($result === FALSE) {
                    die('Curl failed: ' . curl_error($ch));
                }
                curl_close($ch);
                
                $recibo->factura_id = json_decode($result)->Id;
                $recibo->serie = $serie;
                $recibo->folio = $folio;
                $recibo->update();
                if($recibo){
                    return redirect()->back()->with('success', 'Operación exitosa');
                }else{
                    return redirect()->back()->withInput($request->input())->withErrors(['invalid' => 'Error. Algo salió mal']);
                }
              }
            }
          }
        }catch (\Exception $ex){
          Log::info("Error");
          Log::error($ex);
          dd($ex);
        }
    }

    public function cancelCFDI(Request $request){
        //https://apisandbox.facturama.mx/api-lite/cfdis/{id} DELETES
      try{
        $recibo = VentaRecibo::find($request->recibo_id);

        if($recibo){
          $ch = curl_init();
          curl_setopt($ch, CURLOPT_URL, self::URL."api-lite/cfdis/".$recibo->factura_id);
          curl_setopt($ch, CURLOPT_HTTPHEADER, self::HEADERS);
          curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
      
          $result = curl_exec($ch);
          
          if ($result === FALSE) {
              die('Curl failed: ' . curl_error($ch));
          }

          curl_close($ch);

          $arrRecibo = $recibo->toArray();
          unset($arrRecibo['id']);
          unset($arrRecibo['created_at']);
          unset($arrRecibo['updated_at']);
          $arrRecibo['status'] = 'canceled';
          $ventaRecibo = VentaRecibo::create($arrRecibo);
          if($ventaRecibo){
            $recibo->factura_id = null;
            $recibo->serie = null;
            $recibo->folio = null;
            $recibo->update();
            return redirect()->back()->with('success', 'Factura cancelada correctamente');
          }else{
            return redirect()->back()->withInput($request->input())->withErrors(['invalid' => 'Error. Algo salió mal']);
          }
      }
          
      }catch (\Exception $ex){
        Log::info("Error");
        Log::error($ex);
        dd($ex);
      }
    }

    public function downloadCFDI(Request $request, String $id){
        //https://apisandbox.facturama.mx/cfdi/{format}/{type}/{id} GET
        $format = "pdf";
        $type = "issuedLite";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, self::URL."Cfdi/".$format."/".$type."/".$id);
        curl_setopt($ch, CURLOPT_HTTPHEADER, self::HEADERS);
        curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);

        $result = curl_exec($ch);
        
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        //curl_close($ch);
        //dd(json_decode($result)->Content);
          /*foreach($response as $item => $value){
              echo "$index:$value";
          }*/
          $file = 'Factura.pdf';
          file_put_contents($file, base64_decode(json_decode($result)->Content));

          if (file_exists($file)) {
              header('Content-Description: File Transfer');
              header('Content-Type: application/octet-stream');
              header('Content-Disposition: attachment; filename="'.basename($file).'"');
              header('Expires: 0');
              header('Cache-Control: must-revalidate');
              header('Pragma: public');
              header('Content-Length: ' . filesize($file));
              readfile($file);
              exit;
          }
          curl_close($ch);
 
        
        
    }

}
