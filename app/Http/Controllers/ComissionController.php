<?php

namespace App\Http\Controllers;

use App\Admin;
use App\Venta;
use Exception;
use App\Comission;
use Carbon\Carbon;
use App\ComissionPayment;
use Illuminate\Http\Request;
use App\Providers\PermissionKey;
use App\Exports\ComissionsExport;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Concerns\ToArray;

class ComissionController extends Controller
{
  public function store(Request $request){
      try{
        $data = $request->all();
        $validacion = $this->validator($data);
        if( $validacion  !== true){
          throw new Exception($validacion->original);
        } 

        $venta = Venta::find($data["venta_id"]);

        if($venta){
          DB::beginTransaction();
          $comission = new Comission();
          $data["sale_amount"] = getAmmount($venta->quotation()->detail->precio_final);
          $data["pay_amount"] = getAmmount($data["pay_amount"]);
          $data["total_amount"] = getAmmount($data["total_amount"]);
          $data["retentions"] = getAmmount($data["retentions"]);
          $data["paid_amount"] = getAmmount($data["paid_amount"]);
          $data["type"] = Comission::TYPE[$data["type"]];
          $comission_id = $comission->create($data)->id;

          if($comission_id){
            $pay_date = new Carbon($venta->fecha_bloqueo);
            $payment_amount = tofloat($data["pay_amount"]/$data["payments"]);
            for($i=0;$i<$data["payments"];$i++){
              $comissionPayment = new ComissionPayment();
              $comissionPayment->comission_id = $comission_id;
              $comissionPayment->payment_date = $pay_date->format('Y-m-d H:i:s');
              $comissionPayment->payment_amount = $payment_amount;
              $comissionPayment->save();
              $pay_date = $pay_date->addMonth();
            }
          }
        }else
          throw new Exception("La venta no existe");

        DB::commit();
        return redirect()->back()->with('success', 'Operación exitosa');
      }catch(Exception $ex){
        DB::rollBack();
        return redirect()->back()->withErrors(['invalid' => $ex->getMessage()]);
      }
  }

  public function update(Request $request){
    try{
      $data = $request->all();
      $validacion = $this->validator($data);
      if( $validacion  !== true){
        throw new Exception($validacion->original);
      } 

      $venta = Venta::find($data["venta_id"]);

      if($venta){
        DB::beginTransaction();
        $comission = Comission::where(["venta_id"=>$venta->id, "type"=>Comission::TYPE[$data["type"]]])->first();
        $data["sale_amount"] = getAmmount($venta->quotation()->detail->precio_final);
        $data["pay_amount"] = getAmmount($data["pay_amount"]);
        $data["total_amount"] = getAmmount($data["total_amount"]);
        $data["retentions"] = getAmmount($data["retentions"]);
        $data["paid_amount"] = getAmmount($data["paid_amount"]);
        $data["type"] = Comission::TYPE[$data["type"]];
        $comission->update($data);

        if($comission){
          if(($data["pay_amount"]-$data["paid_amount"]) < 0)
            throw new Exception("Movimiento cancelado: La cuenta ya se encuentra cubierta.");

          $pay_date = new Carbon($venta->fecha_bloqueo);

          $paidPayments = ComissionPayment::where(['comission_id'=>$comission->id,'paid'=>1])->get();
          if(count($paidPayments)>0){
            $lastPayment = $paidPayments->last();
            $pay_date = new Carbon($lastPayment->payment_date);
            $pay_date = $pay_date->addMonth();
          }

          $deletePayments = ComissionPayment::where(['comission_id'=>$comission->id,'paid'=>0])->delete();
          $data["payments"] = $data["payments"] - count($paidPayments);

          if($data["payments"]>0){
            $payment_amount = tofloat(($data["pay_amount"]-$data["paid_amount"])/$data["payments"]);
            if($payment_amount <= 0)
              throw new Exception("Movimiento cancelado: No se pueden crear mas pagos si no hay saldo por pagar.");


            for($i=0;$i<$data["payments"];$i++){
              $comissionPayment = new ComissionPayment();
              $comissionPayment->comission_id = $comission->id;
              $comissionPayment->payment_date = $pay_date->format('Y-m-d H:i:s');
              $comissionPayment->payment_amount = $payment_amount;
              $comissionPayment->save();
              $pay_date = $pay_date->addMonth();
            }
          }else if($data["payments"]<0){
            throw new Exception("La cantidad de pagos no puede ser menor que los pagos ya realizados");
          }
        }
      }else
        throw new Exception("La venta no existe");

        DB::commit();
        return redirect()->back()->with('success', 'Operación exitosa');
      }catch(Exception $ex){
        DB::rollBack();
        return redirect()->back()->withErrors(['invalid' => $ex->getMessage()]);
      }
  }

  public function delete($id){
    try{
        $comission = Comission::find($id);
        $comission->payments->delete();
        $comission->delete();
      }catch(Exception $ex){
        return redirect()->back()->withErrors(['invalid' => $ex->getMessage()]);
      }
  }

  public function paymentHistory(Int $id){
    if($comission = Comission::find($id)){
        $venta = $comission->venta();
        $tipo = "Asesor";
        if($comission->type == "2")
          $tipo = "Supervision";
        $comission->tipo = $tipo;
        $info['venta'] = $venta;
        $info['development'] = $venta->development();
        $info['client'] = $venta->client();
        $info['quotation'] = $venta->quotation();
        $info['admin'] = $comission->admin();
        $info['supervisor'] = Comission::where(["venta_id"=>$comission->venta_id,"type"=>Comission::TYPE['SUPERVISOR']])->first();
        $info['comission'] = $comission;

        return PDF::loadView('panel.ventas.pdf.comissionHistory', $info)->stream('Historial de pago de comisiones - '.$tipo.' - Venta '.$venta->id);
    }else{
        abort(404);
    }
  }

  public function getData(Request $request){   
      $origin_params = [];
      $referer = $request->server('HTTP_REFERER');

      if (!empty($referer)) {
          $parsedUrl = parse_url($referer);
          if (isset($parsedUrl['query'])) {
              parse_str($parsedUrl['query'], $params);
              $origin_params= ($params);
          }
      }

      $sortableColumns = [
        0=>'comissions.venta_id',
        1=>'admins.name',
        2=>'lotes.name',
        3=>'developments.name',
        4=>'developments.stage',
        5=>'ventas.fecha_bloqueo',
        6=>'ventas.dia_pago',
        7=>'comissions.pay_amount',
        17=>'comissions.notes',
        18=>'comissions.type',
      ];
      
      $draw = $request->input('draw');
      $columnIndex = $request->input('order.0.column');
      $column = $sortableColumns[$columnIndex];
      $dir = $request->input('order.0.dir');
      $start = $request->input('start', 0);
      $length = $request->input('length', 10); // Ensure default length is set
      
      $searchValue = $request->input('search.value');
      
      $datos = Comission::where('comissions.status',1)->join('ventas', 'comissions.venta_id', '=', 'ventas.id')
      ->join('quotations', 'ventas.quotation_id', '=', 'quotations.id')
      ->join('admins', 'quotations.admin_id', '=', 'admins.id')
      ->join('developments', 'ventas.development_id', '=', 'developments.id')
      ->join('lotes', 'ventas.lote_id', '=', 'lotes.id')
      ->select('comissions.id as comission_id', 'comissions.admin_id as comission_admin_id','comissions.type','comissions.venta_id', 'comissions.pay_amount', 'comissions.notes',
              DB::raw('CONCAT(admins.name, " ", admins.last_name) as name_admin'),
              'developments.name as name_desarrollo','developments.stage','lotes.name as name_lote','ventas.fecha_bloqueo','ventas.dia_pago');
      
      if ($origin_params) {
        if (isset($origin_params['admin_id']) && $origin_params['admin_id'] != "") {
            $datos->where('quotations.admin_id', $origin_params['admin_id']);
        }
        if (isset($origin_params['desarrollo']) && $origin_params['desarrollo'] != "") {
          $datos->where('ventas.development_id', $origin_params['desarrollo']);
        }
      }

      if(request()->user()->can(PermissionKey::Comission['permissions']['show_sidebar']['name']) && request()->user()->can(PermissionKey::ComissionSupervision['permissions']['show_sidebar']['name'])){
        if ($origin_params) {
          if (isset($origin_params['type']) && $origin_params['type'] != "") {
            $datos->where('comissions.type', $origin_params['type']);
          }
        }

        if (!$request->user()->can(PermissionKey::ComissionSupervision['permissions']['index']['name']) && !$request->user()->can(PermissionKey::Comission['permissions']['index']['name'])) {
          $datos->where(function ($query) use ($request,$origin_params) {
            $query->where('comissions.admin_id', $request->user()->id)
                  ->orWhere('quotations.admin_id', $request->user()->id);
                  if ($origin_params) {
                    if (isset($origin_params['admin_id']) && $origin_params['admin_id'] != "") {
                        $query->where('quotations.admin_id', $origin_params['admin_id']);
                    }
                    if (isset($origin_params['desarrollo']) && $origin_params['desarrollo'] != "") {
                      $query->where('ventas.development_id', $origin_params['desarrollo']);
                    }
                  }
          }); 
        }else{
          if(!$request->user()->can(PermissionKey::Comission['permissions']['index']['name'])){
            $datos->where('comissions.type', Comission::TYPE['SUPERVISOR']);
            $datos->orWhere(function ($query) use ($request,$origin_params) {
              $query->where('quotations.admin_id', $request->user()->id);
              if ($origin_params) {
                if (isset($origin_params['admin_id']) && $origin_params['admin_id'] != "") {
                    $query->where('quotations.admin_id', $origin_params['admin_id']);
                }
                if (isset($origin_params['desarrollo']) && $origin_params['desarrollo'] != "") {
                  $query->where('ventas.development_id', $origin_params['desarrollo']);
                }
              }
            }); 
          }else if(!$request->user()->can(PermissionKey::ComissionSupervision['permissions']['index']['name'])){
            $datos->where('comissions.type', Comission::TYPE['ASESOR']);
            $datos->orWhere(function ($query) use ($request,$origin_params) {
              $query->where('comissions.admin_id', $request->user()->id);
              if ($origin_params) {
                if (isset($origin_params['admin_id']) && $origin_params['admin_id'] != "") {
                    $query->where('quotations.admin_id', $origin_params['admin_id']);
                }
                if (isset($origin_params['desarrollo']) && $origin_params['desarrollo'] != "") {
                  $query->where('ventas.development_id', $origin_params['desarrollo']);
                }
              }
            }); 
          }
        }
      }else if(request()->user()->can(PermissionKey::Comission['permissions']['show_sidebar']['name']))
      {
        $datos->where('comissions.type', Comission::TYPE['ASESOR']);
        if (!$request->user()->can(PermissionKey::Comission['permissions']['index']['name'])) {
          $datos->where('quotations.admin_id', $request->user()->id);
        }
      }else
      {
        $datos->where('comissions.type', Comission::TYPE['SUPERVISOR']);
        if (!$request->user()->can(PermissionKey::ComissionSupervision['permissions']['index']['name'])) {
          $datos->where('comissions.admin_id', $request->user()->id);
        }
      }
      
      if (!empty($searchValue)) {
          $datos->where(function ($query) use ($searchValue) {
              $query->orWhere('developments.name', 'LIKE', '%' . $searchValue . '%')
                  ->orWhere('developments.stage', 'LIKE', '%' . $searchValue . '%')
                  ->orWhere('admins.name', 'LIKE', '%' . $searchValue . '%')
                  ->orWhere('lotes.name', 'LIKE', '%' . $searchValue . '%')
                  ->orWhere('comissions.notes', 'LIKE', '%' . $searchValue . '%');
          });
      }

      if($origin_params){
        if ((isset($origin_params['fecha']) && isset($origin_params['corte'])) && ($origin_params['fecha'] != "" && $origin_params['corte'] != "")) {
          $fecha = "01/".$origin_params['fecha'];

          $fecha = Carbon::createFromFormat('d/m/Y', $fecha);
          
          if($origin_params['corte'] == 1){
            $fecha->endOfMonth();
            $end_date = $fecha->format("Y-m-d");
            $fecha->day(15);
            $start_date = $fecha->format("Y-m-d");
            
          }else if($origin_params['corte'] == 2){
            $fecha->addMonth()->startOfMonth();
            $start_date = $fecha->format("Y-m-d");
            $fecha->day(15);
            $end_date = $fecha->format("Y-m-d");
          }
         
          $datos->whereHas('comissionPayments', function ($query) use ($start_date,$end_date) {
            $query->where('payment_date', '>=', $start_date)  // Día mayor o igual a 1
                  ->where('payment_date', '<=', $end_date);
          });
        }else if (isset($origin_params['fecha']) && $origin_params['fecha'] != "") {
          $fecha = "01/".$origin_params['fecha'];

          $fecha = Carbon::createFromFormat('d/m/Y', $fecha);
          
          $fecha->endOfMonth();
          $end_date = $fecha->format("Y-m-d");
          $fecha->day(1);
          $start_date = $fecha->format("Y-m-d");
         
          $datos->whereHas('comissionPayments', function ($query) use ($start_date,$end_date) {
            $query->where('payment_date', '>=', $start_date)  // Día mayor o igual a 1
                  ->where('payment_date', '<=', $end_date);
          });
        }
      }
      
      $totalRecords = $datos->count(); // Total records before filtering
      
      $datos = $datos->with('comissionPayments')->orderBy($column, $dir)
              ->offset($start)
              ->limit($length)
              ->get(); // Convertir a un array
      
      $datos = $datos->toArray();

      foreach($datos as $key => $item){
        $supervisor = Admin::find($item['comission_admin_id']);
        $payments = ComissionPayment::where('comission_id',$item['comission_id'])->get()->toArray();
        $datos[$key]['name_admin_supervisor'] = $supervisor?$supervisor->name." ".$supervisor->last_name:"";
        $datos[$key]['comission_payments'] = $payments;
      }
              
      $response = [
          'draw' => $draw,
          'recordsTotal' => $totalRecords,
          'recordsFiltered' => $totalRecords, // For now, until filtering is applied
          'data' => $datos,
      ];
      
      return response()->json($response);
      
  }

  /**
     * Display the specified resource.
     *
     * @param  \App\Comission $comission
     * @return \Illuminate\Http\Response
     */
    public function show(Comission $comission)
    {
        //
    }

  public function locker(Int $id){
    try{
      DB::beginTransaction();
      $comission = Comission::find($id);
      $status = 1;
      if($comission->status){
        $status = 0;
      }
      $comission->update(['status'=>$status]);
      DB::commit();
      return redirect()->back()->with('success', 'Operación exitosa');
    }catch(Exception $ex){
      DB::rollBack();
      return redirect()->back()->withErrors(['invalid' => $ex->getMessage()]);
    }
  }

  protected function validator(array $data)
  {
      $validator =  Validator::make($data, [
          'payments' => ['required'],
          'percentage' => ['required'],
          'taxes' => ['required'],
          'pay_amount' => ['required'],
          'total_amount' => ['required'],
          'retentions' => ['required'],
      ]);

      if($validator->fails())
          return response()->json($validator->errors());

      return true;
  }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

      if(!request()->user()->can(PermissionKey::Comission['permissions']['show_sidebar']['name']) && !request()->user()->can(PermissionKey::ComissionSupervision['permissions']['show_sidebar']['name']))
        return redirect()->route('panel.events.index');

      $origin_params = [];
      $referer = $request->server('HTTP_REFERER');

      if (!empty($referer)) {
          $parsedUrl = parse_url($referer);
          if (isset($parsedUrl['query'])) {
              parse_str($parsedUrl['query'], $params);
              $origin_params= ($params);
          }
      }

        $info = [
            'title' => 'Historial de comisiones',
            'breadcrumb' => [
                [
                    'title' => 'Todos',
                    'route' => 'panel.comisiones.index',
                    'active' => true
                ]
            ],
            'buttons' => [
                
            ]
        ];

        $showDownload = false;
        
        if(request()->user()->can(PermissionKey::Comission['permissions']['download']['name']) || request()->user()->can(PermissionKey::ComissionSupervision['permissions']['download']['name'])){
          $showDownload = true;
        }
        
        if($showDownload) {
          array_push($info['buttons'],[
            'title' => 'Descargar',
            'route' => 'panel.comissions.export',
            'params' => $origin_params,
          ]);
        }

        $info['admins'] = Admin::all();
        return view('panel.comisiones.index', $info);
    }

    public function export(Request $request)
    {
      try{
      $origin_params = [];
        $referer = $request->server('HTTP_REFERER');

        if (!empty($referer)) {
            $parsedUrl = parse_url($referer);
            if (isset($parsedUrl['query'])) {
                parse_str($parsedUrl['query'], $params);
                $origin_params= ($params);
            }
        }
   
            return Excel::download(new ComissionsExport($origin_params,$request), 'Comisiones generadas '.date('d-m-Y').'.xlsx');
        }catch(Exception $ex){
            return redirect()->back()->withInput($request->input())->withErrors(['invalid' => $ex->getMessage()]);
        }
     
    }

}

