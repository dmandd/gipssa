<?php

namespace App\Http\Controllers;

use App\Lote;
use App\Development;
use Illuminate\Http\Request;
use App\Imports\LotesImport;
use App\Exports\LotesExport;
use Maatwebsite\Excel\Facades\Excel;

class LoteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Int $id)
    {
        $info = [
            'title' => 'Lotes',
            'breadcrumb' => [
                [
                    'title' => 'Todos',
                    'route' => 'panel.developments.index',
                ],
                [
                    'title' => Development::find($id)->name,
                    'route' => 'panel.developments.edit',
                    'params' => ['desarrollo' => $id],
                ],
                [
                    'title' => 'Lotes',
                    'route' => 'panel.lotes.index',
                    'params' => ['desarrollo' => $id],
                    'active' => true
                ],
            ],
        ];
        $info['data'] = Lote::where('development_id', $id)->get();
        return view('panel.lotes.index', $info);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Lote  $lote
     * @return \Illuminate\Http\Response
     */
    public function show(Lote $lote)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Lote  $lote
     * @return \Illuminate\Http\Response
     */
    public function edit(Int $desarrollo, Int $id)
    {
        $info = [
            'title' => 'Lotes',
            'breadcrumb' => [
                [
                    'title' => 'Todos',
                    'route' => 'panel.developments.index',
                    'params' => ['desarrollo' => $desarrollo]
                ],
                [
                    'title' => 'Editar',
                    'route' => 'panel.lotes.edit',
                    'params' => ['desarrollo' => $desarrollo, 'lote' => $id],
                    'active' => true
                ]
            ]
        ];
        $info['lote'] = Lote::find($id);
        return view('panel.lotes.edit', $info);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Lote  $lote
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Int $development_id, Int $id)
    {
        $lote = Lote::find($id);
        $lote->update($request->input());
        return redirect()->route('panel.lotes.index', ['desarrollo' => $development_id])->with('success', 'Operación exitosa');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Lote  $lote
     * @return \Illuminate\Http\Response
     */
    public function destroy(Int $desarrollo, Int $id)
    {
        $lote = Lote::where(['development_id'=>$desarrollo,'id'=>$id])->first();
        if($lote){
            $lote->delete();
            return response(['success' => true]);
        }else{
            return response(['success' => false]);
        }
    }

    public function import(Request $request) 
    {
        if($request->file('import')){
            Excel::import(new LotesImport($request->development_id), $request->file('import'));
        }
        return redirect()->back()->with('success', 'Operación exitosa');
        // return redirect('/')->with('success', 'All good!');
    }

    public function export(Request $request){
        return Excel::download(new LotesExport($request->desarrollo), 'lotes.xlsx');
        return redirect()->back()->with('success', 'Operación exitosa');
    }

    public function assign(Int $id){
        $info = [
            'title' => 'Lotes',
            'breadcrumb' => [
                [
                    'title' => 'Todos',
                    'route' => 'panel.developments.index',
                ],
                [
                    'title' => Development::find($id)->name,
                    'route' => 'panel.developments.edit',
                    'params' => ['desarrollo' => $id],
                ],
                [
                    'title' => 'Lotes',
                    'route' => 'panel.lotes.index',
                    'params' => ['desarrollo' => $id],
                    'active' => true
                ],
            ],
        ];
        $info['development'] = Development::find($id);
        return view('panel.lotes.assign', $info);
    }
}
