<?php

namespace App\Http\Controllers;

use App\PropertyType;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class PropertyTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $info = [
            'title' => 'Tipos',
            'breadcrumb' => [
                [
                    'title' => 'Todos',
                    'route' => 'panel.types.index',
                    'active' => true
                ]
            ],
            'buttons' => [
                [
                    'title' => 'Agregar Nuevo',
                    'route' => 'panel.types.create'
                ]
            ]
        ];
        $info['data'] = PropertyType::all()->sortByDesc('id');
        return view('panel.types.index', $info);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $info = [
            'title' => 'Tipos',
            'breadcrumb' => [
                [
                    'title' => 'Todos',
                    'route' => 'panel.types.index',
                    'active' => true
                ],
                [
                    'title' => 'Nuevo',
                    'route' => 'panel.types.create',
                    'active' => true
                ]
            ],
            'buttons' => [
                [
                    'title' => 'Agregar Nuevo',
                    'route' => 'panel.types.create'
                ]
            ]
        ];

        return view('panel.types.create', $info);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $arr = $request->toArray();
        $arr['slug'] = Str::slug($request->name);
        if(PropertyType::where('slug', $arr['slug'])->first()){
            return redirect()->back()->withInput($request->input())->withErrors(['invalid' => 'Error, Nombre duplicado']);
        }else{
            $type = PropertyType::create($arr);
            return redirect()->route('panel.types.index')->with('success', 'Operación exitosa');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PropertyType  $propertyType
     * @return \Illuminate\Http\Response
     */
    public function show(PropertyType $propertyType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PropertyType  $propertyType
     * @return \Illuminate\Http\Response
     */
    public function edit(Int $id)
    {
        $info = [
            'title' => 'Tipos',
            'breadcrumb' => [
                [
                    'title' => 'Todos',
                    'route' => 'panel.types.index',
                ],
                [
                    'title' => 'Editar',
                    'route' => 'panel.types.edit',
                    'params' => ['tipo' => $id],
                    'active' => true
                ]
            ]
        ];
        $info['type'] = PropertyType::find($id);
        return view('panel.types.edit', $info);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PropertyType  $propertyType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Int $id)
    {
        $arr = $request->toArray();
        $arr['slug'] = Str::slug($request->name);
        if(PropertyType::where('slug', $arr['slug'])->where('id', '!=', $id)->first()){
            return redirect()->back()->withInput($request->input())->withErrors(['invalid' => 'Error, Nombre duplicado']);
        }else{
            PropertyType::find($id)->update($arr);
            return redirect()->route('panel.types.edit', ['tipo' => $id])->with('success', 'Elemento Actualizado');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PropertyType  $propertyType
     * @return \Illuminate\Http\Response
     */
    public function destroy(PropertyType $propertyType)
    {
        //
    }
}
