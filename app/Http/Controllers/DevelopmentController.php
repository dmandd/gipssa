<?php

namespace App\Http\Controllers;

use App\Development;
use App\Admin;
use App\CadastralPlanking;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class DevelopmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $info = [
            'title' => 'Desarrollos',
            'breadcrumb' => [
                [
                    'title' => 'Todos',
                    'route' => 'panel.developments.index',
                    'active' => true
                ]
            ],
            'buttons' => [
                [
                    'title' => 'Agregar Nuevo',
                    'route' => 'panel.developments.create'
                ]
            ]
        ];
        $info['data'] = Development::all();
        return view('panel.developments.index', $info);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $info = [
            'title' => 'Desarrollos',
            'breadcrumb' => [
                [
                    'title' => 'Todos',
                    'route' => 'panel.developments.index',
                ],
                [
                    'title' => 'Nuevo',
                    'route' => 'panel.developments.create',
                    'active' => true
                ]
            ]
        ];

        $info['attorneys'] = Admin::where("attorney",true)->get();
        $info['cadastralPlankings'] = CadastralPlanking::all();
        
        return view('panel.developments.create', $info);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $development = Development::create($request->toArray());
        return redirect()->route('panel.developments.index')->with('success', 'Operación exitosa');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Development  $development
     * @return \Illuminate\Http\Response
     */
    public function show(Development $development)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Development  $development
     * @return \Illuminate\Http\Response
     */
    public function edit(Int $id)
    {
        $info = [
            'title' => 'Desarrollos',
            'breadcrumb' => [
                [
                    'title' => 'Todos',
                    'route' => 'panel.developments.index',
                ],
                [
                    'title' => 'Editar',
                    'route' => 'panel.developments.edit',
                    'params' => ['desarrollo' => $id],
                    'active' => true
                ]
            ]
        ];

        $info['attorneys'] = Admin::where("attorney",true)->get();
        $info['cadastralPlankings'] = CadastralPlanking::all();

        if($info['development'] = Development::find($id)){
            return view('panel.developments.edit', $info);
        }else{
            return redirect()->route('panel.developments.index');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Development  $development
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Int $id)
    {
        $development = Development::find($id)->update($request->toArray());
        if(isset($arr['api'])){
            return response(['success' => true], 200);
        }else{
            return redirect()->route('panel.developments.edit', ['desarrollo' => $id])
                    ->with('success', 'Elemento actualizado');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Development  $development
     * @return \Illuminate\Http\Response
     */
    public function destroy(Int $id)
    {
        $desarrollo = Development::find($id);
        if($desarrollo){
            $desarrollo->delete();
            return response(['success' => true]);
        }else{
            return response(['success' => false]);
        }
    }

    public function assign(Request $request, Int $desarrollo){
        if($development = Development::find($desarrollo)){
            $explode = explode('/', $development->plano);
            $filename = end($explode);
            Storage::put('public/media/'.$filename, $request->svg);
            return redirect()->back()->with('success', 'Operación exitosa');
        }else{
            return redirect()->back()->withErrors(['invalid' => 'Lo sentimos, algo salió mal']);
        }
    }

    public function disponibilidad(Request $request){
        $development = null;
        if(Development::get()->count() > 0){
            foreach(Development::all() as $d){
                if(Str::slug($d->name).'-etapa-'.$d->stage == $request->slug){
                    $development = $d;
                }
            }
        }
        if($development){
            return view('panel.ventas.disponibilidad', ['desarrollo' => $development]);
        }else{
            abort(404);
        }
    }
}
