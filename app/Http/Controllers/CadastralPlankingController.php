<?php

namespace App\Http\Controllers;

use App\CadastralPlanking;
use Illuminate\Http\Request;

class CadastralPlankingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $info = [
            'title' => 'Tablajes Catastrales',
            'breadcrumb' => [
                [
                    'title' => 'Todos',
                    'route' => 'panel.cadastralPlankings.index',
                    'active' => true
                ]
            ],
            'buttons' => [
                [
                    'title' => 'Agregar Nuevo',
                    'route' => 'panel.cadastralPlankings.create'
                ]
            ]
        ];
        $info['data'] = CadastralPlanking::all();
        return view('panel.cadastralPlankings.index', $info);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $info = [
            'title' => 'Tablaje Catastral',
            'breadcrumb' => [
                [
                    'title' => 'Todos',
                    'route' => 'panel.cadastralPlankings.index',
                ],
                [
                    'title' => 'Nuevo',
                    'route' => 'panel.cadastralPlankings.create',
                    'active' => true
                ]
            ]
        ];
        return view('panel.cadastralPlankings.create', $info);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $cadastralPlanking = CadastralPlanking::create($request->toArray());
        return redirect()->route('panel.cadastralPlankings.index')->with('success', 'Operación exitosa');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CadastralPlanking  $cadastralPlanking
     * @return \Illuminate\Http\Response
     */
    public function show(CadastralPlanking $cadastralPlanking)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CadastralPlanking  $cadastralPlanking
     * @return \Illuminate\Http\Response
     */
    public function edit(Int $id)
    {
        $info = [
            'title' => 'Tablajes Catastrales',
            'breadcrumb' => [
                [
                    'title' => 'Todos',
                    'route' => 'panel.cadastralPlankings.index',
                ],
                [
                    'title' => 'Editar',
                    'route' => 'panel.cadastralPlankings.edit',
                    'params' => ['tablajesCatastrale' => $id],
                    'active' => true
                ]
            ]
        ];
        if($info['cadastralPlanking'] = CadastralPlanking::find($id)){
            return view('panel.cadastralPlankings.edit', $info);
        }else{
            return redirect()->route('panel.cadastralPlankings.index');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CadastralPlanking  $cadastralPlanking
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Int $id)
    {
        $cadastralPlanking = CadastralPlanking::find($id)->update($request->toArray());
        if(isset($arr['api'])){
            return response(['success' => true], 200);
        }else{
            return redirect()->route('panel.cadastralPlankings.edit', ['tablajesCatastrale' => $id])
                    ->with('success', 'Elemento actualizado');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CadastralPlanking  $cadastralPlanking
     * @return \Illuminate\Http\Response
     */
    public function destroy(Int $id)
    {
        if(CadastralPlanking::find($id)){
            //quitar relacion con desarrollo
            CadastralPlanking::destroy($id);
            return response(['success' => true]);
        }else{
            return response(['success' => false]);
        }
    }
   
}
