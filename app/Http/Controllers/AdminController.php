<?php
namespace App\Http\Controllers;

use App\Note;
use App\Admin;
use App\Event;
use App\Venta;
use App\Client;
use App\Contact;
use App\Prospect;
use App\Propierty;
use App\Quotation;
use App\Development;
use App\TaskProspect;
use App\AdminProperty;
use Illuminate\Http\Request;
use App\Providers\PermissionKey;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\CsdController;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Mail\Message;

class AdminController extends Controller
{
    use AuthenticatesUsers;
    use AuthenticatesUsers {
        logout as doLogout;
    }
    protected $redirectTo = '/cuentas';
    protected $redirectAfterLogout = '/admin';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $info = [
            'title' => 'Administradores',
            'breadcrumb' => [
                [
                    'title' => 'Todos',
                    'route' => 'panel.admins.index',
                    'active' => true
                ]
            ],
            'buttons' => [
                [
                    'title' => 'Agregar Nuevo',
                    'route' => 'panel.admins.create'
                ]
            ]
        ];
        
            $info['data'] = Admin::all()->sortByDesc('id');

            //$info['data'] = Admin::all()->where('comision','>','0')
        //->sortByDesc('id');
           
           
        
        return view('panel.admins.index', $info);

        
    }

    public function index_ejecutivos()
    {
        
        $info = [
            'title' => 'Administradores',
            'breadcrumb' => [
                [
                    'title' => 'Todos',
                    'route' => 'panel.admins.index',
                    'active' => true
                ]
            ],
            'buttons' => [
                [
                    'title' => 'Agregar Nuevo',
                    'route' => 'panel.admins.create'
                ]
            ]
        ];
        
            //$info['data'] = Admin::all()->sortByDesc('id');

           
       $info['data'] = Admin::permission('prospects.menu')->get()
       ->sortByDesc('name');

      // $users = User::role('writer')->get(); // Returns only users with the role 'writer'
           
        
        return view('panel.admins.index', $info);

        
    }


   

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $info = [
            'title' => 'Administradores',
            'breadcrumb' => [
                [
                    'title' => 'Todos',
                    'route' => 'panel.admins.index',
                ],
                [
                    'title' => 'Nuevo',
                    'route' => 'panel.admins.create',
                    'active' => true
                ]
            ]
        ];
        $info['roles'] = Role::all();
        return view('panel.admins.create', $info);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Sí las constraseñas no son iguales regresamos al paso anterior
        if($request->password != $request->confirm_password){
            return redirect()->back()->withInput($request->input())->withErrors(['invalid' => 'Las contraseñas deben ser iguales']);
        }
        $arr = $request->toArray();
        $arr['birth_date'] = str_replace('/', '-', $arr['birth_date']);
        $arr['birth_date'] = date('Y-m-d', strtotime($arr['birth_date']));
        if($arr['avatar'] === null)
            unset($arr['avatar']);
        $arr['password'] = Hash::make($request->password);
        if(Admin::where('email', $arr['email'])->get()->count() > 0){
            return redirect()->back()->withInput($request->input())->withErrors(['invalid' => 'El correo ya está en uso']);
        }else{
            $admin = Admin::create($arr);
            $admin->assignRole($arr['role']);
            return redirect()->route('panel.admins.edit', ['id' => $admin->id]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function show(Admin $admin)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Int $id)
    {
        $info = [
            'title' => 'Administradores',
            'breadcrumb' => [
                [
                    'title' => 'Todos',
                    'route' => 'panel.admins.index'
                ],
                [
                    'title' => 'Editar',
                    'route' => 'panel.admins.edit',
                    'params' => ['id' => $id],
                    'active' => true
                ]
            ]
        ];
        if(!$request->user()->can(PermissionKey::Admin['permissions']['index']['name']))
            unset($info['breadcrumb'][0]['route']);
        $info['admin'] = Admin::find($id);
        $info['roles'] = Role::all();
        $info['admins']= Admin::all(); // Ampliado el 21 de Junio
        return view('panel.admins.edit', $info);
    }

    public function editPassword($id){
        $info = [
            'title' => 'Usuarios',
            'breadcrumb' => [
                [
                    'title' => 'Todos',
                    'route' => 'panel.admins.index',
                ],
                [
                    'title' => 'Editar',
                    'route' => 'panel.admins.edit',
                    'params' => ['id' => $id]
                ],
                [
                    'title' => 'Contraseña',
                    'active' => true
                ]
            ],
        ];
        $info['admin'] = Admin::find($id);
        return view('panel.admins.editPassword', $info);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Int $id)
    {
        $arr = $request->toArray();
        
        //Verificamos que el nuevo correo esté disponible
        if(Admin::where('id', '!=', $id)->where('email', $request->email)->get()->count() > 0){
            return redirect()->back()->withInput($request->input())->withErrors(['invalid' => 'Lo sentimos ese correo ya está en uso']);
        } 

        $arr['birth_date'] = str_replace('/', '-', $arr['birth_date']);
        $arr['birth_date'] = date('Y-m-d', strtotime($arr['birth_date']));
        $arr['attorney']=intval(isset($arr['attorney']));

        if($arr['attorney']){
            if(!is_null($arr['rfc']) && !is_null($arr['csd_password'])){
                if(Admin::where('id', '!=', $id)->where('rfc', $arr['rfc'])->get()->count() > 0){
                    return redirect()->back()->withInput($request->input())->withErrors(['invalid' => 'Lo sentimos ese RFC ya está en uso']);
                } 

                if(isset($arr['csd_cer']) && isset($arr['csd_key'])){
                    $admin = Admin::find($id);
                    $data = array(
                        "rfc" => $arr['rfc'],
                        "csd_cer" => $arr['csd_cer'],
                        "csd_key" => $arr['csd_key'],
                        "csd_password" => $arr['csd_password']
                    );
                    if($admin->csd_cer && $admin->csd_key){
                        if($admin->rfc != $arr['rfc']){
                            $saveCSD = CsdController::createCSD($data);
                            $message = "";
                            if(!is_null($saveCSD)){
                                foreach($saveCSD as $key=>$value){
                                    if($key == "Message")
                                        $message.= $value;
                                    if($key == "ModelState"){
                                        $message = "";
                                        foreach($value as $error=>$val){
                                            foreach($val as $msg=>$v){
                                                $message .= " - ".$v;
                                            }
                                        }
                                    }
                                }
                                return redirect()->back()->withInput($request->input())->withErrors(['invalid' => 'La información no se actualizó.'.$message]);
                            }else{
                                $deleteCSD = CsdController::deleteCSD($admin->rfc);
                            }
                        }else{
                            $saveCSD = CsdController::updateCSD($data);
                        }
                    }else{
                        $saveCSD = CsdController::createCSD($data);
                    }
                   
                    $message = "";
                    //dd($saveCSD);
                    if(!is_null($saveCSD)){
                        foreach($saveCSD as $key=>$value){
                            if($key == "Message")
                                $message.= $value;
                            if($key == "ModelState"){
                                $message = "";
                                foreach($value as $error=>$val){
                                    foreach($val as $msg=>$v){
                                        $message .= " - ".$v;
                                    }
                                }
                            }
                        }
                        return redirect()->back()->withInput($request->input())->withErrors(['invalid' => 'La información no se actualizó.'.$message]);
                    }else{
                        $arr['csd_cer']=1;
                        $arr['csd_key']=1;
                    }
                }else{
                    if(!isset($arr['csd_cer']) && !isset($arr['csd_key'])){
                        $admin = Admin::find($id);

                        if($admin->csd_cer && $admin->csd_key){
                            if($admin->rfc != $arr['rfc'] || $admin->csd_password != $arr['csd_password']){
                                return redirect()->back()->withInput($request->input())->withErrors(['invalid' => 'Para cambiar el RFC o la contraseña CSD .key se tienen que enviar los archivos CSD correspondientes.']);
                            }
                        }
                    }else if(isset($arr['csd_cer']) || isset($arr['csd_key'])){
                        return redirect()->back()->withInput($request->input())->withErrors(['invalid' => 'Ambos archivos CSD deben ser enviados.']);
                    }
                }
            }else{
                return redirect()->back()->withInput($request->input())->withErrors(['invalid' => 'Para actualizar los campos del Apoderado legal se tienen que enviar todos los campos de Información fiscal.']);
            }
        }else{
            $admin = Admin::find($id);
            if($admin->attorney){
                //borrar csd 
                $deleteCSD = CsdController::deleteCSD($admin->rfc);
                if(is_null($deleteCSD)){
                    $arr['rfc'] = null;
                    $arr['csd_cer'] = 0;
                    $arr['csd_key'] = 0;
                    $arr['csd_password'] = null;
                }
            }else{
                if(!is_null($arr['rfc']) || isset($arr['csd_cer']) || isset($arr['csd_key']) || !is_null($arr['csd_password'])){
                    return redirect()->back()->withInput($request->input())->withErrors(['invalid' => 'Para actualizar los campos de Información fiscal seleccione el privilegio de Apoderado legal.']);
                }
            }
            
        }
        
        //dd($arr);
        $user = Admin::find($id);
        $user->update($arr);
        //Le asignamos el rol
        if(isset($arr['role']))
            $user->syncRoles([$arr['role']]);
        return redirect()->route('panel.admins.edit', ['id' => $id])->with('success', 'Información actualizada');
    }

    public function updatePassword(Request $request, $id){
        $admin = Admin::find($id);
        if($admin){
            // if(Hash::check($request->password, $admin->password)){
                if($request->new_password == $request->confirm_password){
                    if(Hash::needsRehash($request->new_password)){
                        $admin->password = Hash::make($request->new_password);
                    }else{
                        $admin->password = $request->new_password;
                    }
                    // $admin->password = Hash::make($request->new_password);
                    $admin->save();
                    return redirect()->route('panel.admins.edit', ['id' => $id])->with('success', 'Información actualizada');
                }else{
                    return redirect()->back()->withInput($request->input())->withErrors(['invalid' => 'La nueva contraseña no coincide, favor de verificar']);
                }
            // }else{
            //     return redirect()->back()->withInput($request->input())->withErrors(['invalid' => 'Contraseña incorrecta']);
            // }
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function hide(Int $id)
    {
        if(Admin::find($id)){
            $admin = Admin::find($id);              
            
            $admin->hide=$admin->hide==1 ? '0': '1';            
            $admin->save();
            Client::where('admin_id',$id)->update(['admin_id' => '2']); // Quitarle todas sus ventas y pasarlas al usaurio 2
            

            return redirect()->route('panel.admins.index', ['id' => $id])->with('success', 'Información actualizada');
        }else{
            return redirect()->back()->withErrors(['invalid' => 'La nueva contraseña no coincide, favor de verificar']);
        }
    }

     /**
     * reasignar ventas a otro para poder borrar
     *
     * @param  \App\Admin  $admin
     * @return \Illuminate\Http\Response
     */

    public function reasign(Request $request,int $id)
    {
        
        
        if(Admin::find($id)){           
            Client::where('admin_id',$id)->update(['admin_id' =>$request->admin_to]); // Pasar ventas a otro admin     
            Prospect::where('admin_id',$id)->update(['admin_id' =>$request->admin_to]); // Pasar ventas a otro admin     
            Contact::where('admin_id',$id)->update(['admin_id' =>$request->admin_to]); // Pasar ventas a otro admin     
            Quotation::where('admin_id',$id)->update(['admin_id' =>$request->admin_to]); // Pasar ventas a otro admin     
            
            

            return redirect()->route('panel.admins.index', ['id' => $id])->with('success', 'Reasignación correcta');
        }else{
            return redirect()->back()->withErrors(['invalid' => 'No fué psoible la reasignación']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function destroy(Int $id)
    {
        try {
            DB::beginTransaction();
        
            $user = Admin::find($id);
            $receptorUser = Admin::where('attorney',true)->where('id', '!=', $id)->first();

            if(!$receptorUser){
                $receptorUser = Admin::where('id', '!=', $id)->first();
            }

            if($receptorUser->id != $user->id){
                AdminProperty::where('admin_id', $user->id)->update(['admin_id'=>$receptorUser->id]);
                Propierty::where('admin_id', $id)->update(['admin_id' => $receptorUser->id]);
                Contact::where('admin_id', $user->id)->update(['admin_id' => $receptorUser->id]);
                Client::where('admin_id', $user->id)->update(['admin_id' => $receptorUser->id]);
                Development::where('admin_id', $user->id)->update(['admin_id' => $receptorUser->id]);
                Event::where('admin_id', $user->id)->update(['admin_id' => $receptorUser->id]);
                Prospect::where('admin_id', $user->id)->update(['admin_id' => $receptorUser->id]);
                TaskProspect::where('admin_id', $user->id)->update(['admin_id' => $receptorUser->id]);
                Note::where('admin_id', $user->id)->update(['admin_id' => $receptorUser->id]);
                $quotation = Quotation::where('admin_id', $user->id)->get();
                foreach($quotation as $quote){
                    $developmentIdAtorrney = $quote->lote()->development_id;
                    $developmentAtorrneyId = Development::find($developmentIdAtorrney)->admin_id;
                    $quote->update(['admin_id'=>$developmentAtorrneyId]);
                }
                $user->delete();
    
                if($user){
                    DB::commit();
                    return response(['success' => true,
                                'message' => 'Usuario eliminado con exito'], 200);
                }else{
                    throw('No se pudo eliminar al usuario');
                }
            }else{
                throw('No se puede eliminar a si mismo');
            }
            
        } catch (\Exception $e) {
            DB::rollback();
            return response(['success' => false,
                            'message'=>$e->getMessage()], 200);
        }
        
        
    }

    public function unauthenticated(){
        return view('panel.admins.login');
    }

    public function login(Request $request){
        //Attempt to log in the seller
        if(Auth::guard('admin')->attempt(['email' => $request->email ,'password' => $request->password], $request->remember)){
            return redirect()->route('panel.events.index');
        }else{
            //return redirect()->route('panel.events.index');
            return redirect()->back()->withInput()->withErrors(['message' => 'Correo o contraseña invalida']);
        }
    }

    public function logout(Request $request){
        $this->guard()->logout();
        $request->session()->invalidate();
        return redirect('/admin');
    }
}