<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contact;
use App\Admin;
use App\Providers\PermissionKey;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $info = [
            'title' => 'Contactos',
            'breadcrumb' => [
                [
                    'title' => 'Todos',
                    'route' => 'panel.contacts.index',
                ],
            ],
        ];
        if($request->user()->can(PermissionKey::Contact['permissions']['create']['name'])){
            $info['buttons'] = [ ['title' => 'Agregar Nuevo', 'route' => 'panel.contacts.create'] ];
        }
        if($request->user()->can(PermissionKey::Contact['permissions']['index']['name'])){
            $info['data'] = Contact::all();
        }else{
            $info['data'] = Contact::where('admin_id', $request->user()->id)->get();
        }
        return view('panel.contacts.index', $info);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $info = [
            'title' => 'Contactos',
            'breadcrumb' => [
                [
                    'title' => 'Todos',
                    'route' => 'panel.contacts.index',
                ],
                [
                    'title' => 'Nuevo',
                    'route' => 'panel.contacts.create',
                    'active' => true
                ]
            ]
        ];
        $info['admins'] = Admin::all();
        return view('panel.contacts.create', $info);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Verificamos que el correo o el telefono esten duplicados
        if(Contact::where('email', $request->email)->orWhere('phone', $request->phone)->first()){
            return redirect()->back()->withInput($request->input())->withErrors(['invalid' => 'Correo o teléfono duplicado']);
        }else{
            $contact = Contact::create($request->toArray());
            return redirect()->route('panel.contacts.index')->with('success', 'Operación exitosa');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Int $id)
    {
        $info = [
            'title' => 'Contactos',
            'breadcrumb' => [
                [
                    'title' => 'Todos',
                    'route' => 'panel.contacts.index',
                    'active' => true
                ],
                [
                    'title' => 'Editar',
                    'route' => 'panel.contacts.edit',
                    'params' => ['contacto' => $id],
                    'active' => true
                ]
            ]
        ];
        $info['contact'] = Contact::find($id);
        $info['admins'] = Admin::all();
        return view('panel.contacts.edit', $info);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $contact = Contact::find($id)->update($request->toArray());
        return redirect()->route('panel.contacts.edit', ['contacto' => $id])
                ->with('success', 'Datos actualizados');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Contact::find($id)){
            Contact::destroy($id);
        }else{
            return response(['success' => false]);
        }
    }
}
