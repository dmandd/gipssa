<?php

namespace App\Http\Controllers;

use App\Payment;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $info = [
            'title' => 'Caja Chica',
            'breadcrumb' => [
                [
                    'title' => 'Todos',
                    'route' => 'panel.payments.index',
                    'active' => true
                ]
            ],
            'buttons' => [
                [
                    'title' => 'Agregar Nuevo',
                    'route' => 'panel.payments.create'
                ]
            ]
        ];
        if(isset($request->filter)){
            $filter = explode(';', $request->filter);
            $info['data'] = Payment::where(function($query) use ($filter){
                foreach ($filter as $key => $filt) {
                    if($key == 0){
                        $query->where('tipo', $filt);
                    }else{
                        $query->orWhere('tipo', $filt);
                    }
                }
            })->get();
            $info['ingreso'] = Payment::where(function($query) use ($filter){
                foreach ($filter as $key => $filt) {
                    if($key == 0){
                        $query->where('tipo', $filt);
                    }else{
                        $query->orWhere('tipo', $filt);
                    }
                }
            })->where('movimiento', 'Ingreso')->sum('monto');
            $info['egreso'] = Payment::where(function($query) use ($filter){
                foreach ($filter as $key => $filt) {
                    if($key == 0){
                        $query->where('tipo', $filt);
                    }else{
                        $query->orWhere('tipo', $filt);
                    }
                }
            })->where('movimiento', 'Egreso')->sum('monto');
            $info['total'] = $info['ingreso'] - $info['egreso'];
        }else{
            $info['data'] = Payment::all();
            $info['ingreso'] = Payment::where('movimiento', 'Ingreso')->sum('monto');
            $info['egreso'] = Payment::where('movimiento', 'Egreso')->sum('monto');
            $info['total'] = $info['ingreso'] - $info['egreso'];
        }
        return view('panel.payments.index', $info);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $info = [
            'title' => 'Caja chica',
            'breadcrumb' => [
                [
                    'title' => 'Todos',
                    'route' => 'panel.payments.index',
                ],
                [
                    'title' => 'Nuevo',
                    'route' => 'panel.payments.create',
                    'active' => true
                ]
            ]
        ];
        return view('panel.payments.create', $info);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $inputs = $request->input();
        $inputs['saldo'] = 0;
        //Le damos formato a la fecha
        $inputs['fecha'] = str_replace('/', '-', $inputs['fecha']);
        $inputs['fecha'] = date('Y-m-d', strtotime($inputs['fecha']));
        $payment = Payment::create($inputs);
        return redirect()->route('panel.payments.index')->with('success', 'Operación exitosa');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Payment  $payment
     * @return \Illuminate\Http\Response
     */
    public function show(Payment $payment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Payment  $payment
     * @return \Illuminate\Http\Response
     */
    public function edit(Payment $payment)
    {
        $pago = Payment::find(request()->pago);
        $info = [
            'title' => 'Caja chica',
            'breadcrumb' => [
                [
                    'title' => 'Todos',
                    'route' => 'panel.payments.index',
                ],
                [
                    'title' => 'Editar',
                    'route' => 'panel.payments.edit',
                    'params' => ['pago' => $pago],
                    'active' => true
                ]
            ]
        ];
        $info['payment'] = $pago;
        return view('panel.payments.edit', $info);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Payment  $payment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Payment $payment)
    {
        $payment = Payment::find($request->pago);
        $inputs = $request->input();
        $inputs['saldo'] = 0;

        $inputs['fecha'] = str_replace('/', '-', $inputs['fecha']);
        $inputs['fecha'] = date('Y-m-d', strtotime($inputs['fecha']));
        $payment->update($inputs);
        return redirect()->back()->with('success', 'Operación exitosa');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Payment  $payment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Payment $payment)
    {
        //
    }
}
