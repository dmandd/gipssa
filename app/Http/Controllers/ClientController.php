<?php

namespace App\Http\Controllers;

use App\Client;
use App\Admin;
use App\Development;
use Illuminate\Http\Request;
use App\Providers\PermissionKey;
use App\Http\Controllers\VentaController;
use App\Prospect;
use App\Quotation;
use App\Venta;
use Illuminate\Support\Facades\DB;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $info = [
            'title' => 'Clientes',
            'breadcrumb' => [
                [
                    'title' => 'Todos',
                    'route' => 'panel.clients.index',
                    'active' => true
                ]
            ],
            'buttons' => [
                [
                    'title' => 'Agregar Nuevo',
                    'route' => 'panel.clients.create'
                ]
            ]
        ];
        //Veremos sí puede visualizar todos los clientes, de lo contrario solo visualiza los que tiene asignado
        if($request->user()->can(PermissionKey::Client['permissions']['all']['name'])){
            $info['data'] = Client::all();
        }else{
            $info['data'] = $request->user()->client();
        }
        return view('panel.clients.index', $info);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $info = [
            'title' => 'Clientes',
            'breadcrumb' => [
                [
                    'title' => 'Todos',
                    'route' => 'panel.clients.index',
                ],
                [
                    'title' => 'Nuevo',
                    'route' => 'panel.clients.create',
                    'active' => true
                ]
            ]
        ];
        return view('panel.clients.create', $info);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $arr = $request->toArray();
        
        if(Client::where('email', $arr['email'])->get()->count() > 0 || Prospect::where('email', $arr['email'])->get()->count() > 0){
            return redirect()->back()->withInput($request->input())->withErrors(['invalid' => 'Lo sentimos el correo '.$arr['email'].' ya está en uso intente con otro.']);
        } 

        $arr['extranjero']=isset($arr['extranjero'])?$arr['extranjero']:$arr['nacionalidad'] != 'Mexicana'; 
        $arr['fecha_nacimiento'] = isset($arr['fecha_nacimiento']) ? formatDate($arr['fecha_nacimiento']) : null;
        $arr['admin_id'] = $request->user()->id;
       
        $client = Client::create($arr);

        if($client){
            if(isset($arr['desarrollo'])){ 
                return redirect()->route('panel.ventas.create',['desarrollo'=>$arr['desarrollo'],'client_id'=>$client->id]);
            }else{
                return redirect()->route('panel.clients.index')->with('success', '¡Operación exitosa!');
            }
        }
        
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function show(Client $client)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function edit(Int $id)
    {
        $info = [
            'title' => 'Clientes',
            'breadcrumb' => [
                [
                    'title' => 'Todos',
                    'route' => 'panel.clients.index',
                ],
                [
                    'title' => 'Edit',
                    'route' => 'panel.clients.edit',
                    'params' => ['cliente' => $id],
                    'active' => true
                ]
            ]
        ];
        $info['client'] = Client::find($id);
        $info['developments'] = Development::where('status','visible')->get();
        if(PermissionKey::Client['permissions']['assign']){
            $info['admins'] = Admin::all();
        }
        return view('panel.clients.edit', $info);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Int $id)
    {
        if($client = Client::find($id)){
            //Verificamos que el nuevo correo esté disponible
            if(Client::where('id', '!=', $id)->where('email', $request->email)->get()->count() > 0 || Prospect::where('email', $request->email)->get()->count() > 0){
                return redirect()->back()->withInput($request->input())->withErrors(['invalid' => 'Lo sentimos el correo '.$request->email.' ya está en uso intente con otro.']);
            } 
            $arr = $request->toArray();
            $arr['extranjero']=isset($arr['extranjero'])?$arr['extranjero']:$arr['nacionalidad'] != 'Mexicana';
            $arr['fecha_nacimiento'] = formatDate($arr['fecha_nacimiento']);
            $client->update($arr);
            return redirect()->back()->with('success', 'Operación exitosa');
        }else{
            return redirect()->back()->withErrors(['invalid' => 'Algo salió mal']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function destroy(Int $id)
    {
        if(Client::find($id)){
            // Falta entender que hay que borrar
            DB::table('ventas')
            ->where('client_id', $id)->delete();
            
            //Venta::where("client_id",$id)->update(['client_id'=>'2'])  ;;     
            Client::destroy($id);
            return response(['success' => true], 200);
        }else{
            
            return response(['success' => false], 200);
        }
    }
}
