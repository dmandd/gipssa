<?php

namespace App\Http\Controllers;

use App\Operation;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class OperationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $info = [
            'title' => 'Operaciones',
            'breadcrumb' => [
                [
                    'title' => 'Todos',
                    'route' => 'panel.operations.index',
                    'active' => true
                ]
            ],
            'buttons' => [
                [
                    'title' => 'Agregar Nuevo',
                    'route' => 'panel.operations.create'
                ]
            ]
        ];
        $info['data'] = Operation::all()->sortByDesc('id');
        return view('panel.operations.index', $info);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $info = [
            'title' => 'Operaciones',
            'breadcrumb' => [
                [
                    'title' => 'Todos',
                    'route' => 'panel.operations.index',
                    'active' => true
                ],
                [
                    'title' => 'Nuevo',
                    'route' => 'panel.operations.create',
                    'active' => true
                ]
            ],
            'buttons' => [
                [
                    'title' => 'Agregar Nuevo',
                    'route' => 'panel.operations.create'
                ]
            ]
        ];

        return view('panel.operations.create', $info);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $arr = $request->toArray();
        $arr['slug'] = Str::slug($request->name);
        if(Operation::where('slug', $arr['slug'])->first()){
            return redirect()->back()->withInput($request->input())->withErrors(['invalid' => 'Error, Nombre duplicado']);
        }else{
            $operation = Operation::create($arr);
            return redirect()->route('panel.operations.index')->with('success', 'Operación exitosa');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Operation  $operation
     * @return \Illuminate\Http\Response
     */
    public function show(Operation $operation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Operation  $operation
     * @return \Illuminate\Http\Response
     */
    public function edit(Operation $operation)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Operation  $operation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Operation $operation)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Operation  $operation
     * @return \Illuminate\Http\Response
     */
    public function destroy(Operation $operation)
    {
        //
    }
}
