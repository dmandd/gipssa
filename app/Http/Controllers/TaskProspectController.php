<?php

namespace App\Http\Controllers;

use App\Task;
use App\Admin;
use App\TaskProspect;
use Illuminate\Http\Request;
use App\Notifications\AssignedTask;

class TaskProspectController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $inputs = $request->input();
        $task = Task::create($inputs);
        //Create TaskProspect
        if($request->admin_id){
            $taskProspect = TaskProspect::create([
                'prospect_id' => $request->prospect_id,
                'admin_id' => $request->admin_id,
                'task_id' => $task->id
            ]);
            //Debemos notificar al usuario responsable
            $responsable = Admin::find($request->admin_id);
            $responsable->notify(new AssignedTask($task, $request->prospect_id));
            return redirect()->back()->with('success', 'Operación exitosa');
        }else{
            return redirect()->back()->withErrors(['invalid' => 'El prospecto debe estar asignado a un vendedor']);
        }
    }

    public function update(Request $request){
        $inputs = $request->input();
        if(count($inputs['tasks']) > 0){
            foreach($inputs['tasks'] as $task){
                $tp = TaskProspect::where('task_id', $task)->where('prospect_id', $request->prospect_id)->first();
                if($tp){
                    $tp->update(['complete' => 1]);
                }else{
                    $tp = TaskProspect::create([
                        'prospect_id' => $request->prospect_id,
                        'admin_id' => $request->user()->id,
                        'task_id' => $task,
                        'complete' => 1
                    ]);
                }
            }
            return redirect()->back()->with('success', 'Operación exitosa');
        }else{
            return redirect()->back();
        }
    }

    public function destroy(Int $id){
        if(Task::find($id)){
            TaskProspect::where('task_id', $id)->delete();
            Task::destroy($id);
            return response(['success' => true], 200);
        }else{
            return response(['success' => false], 200);
        }
    }
}
