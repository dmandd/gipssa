<?php

namespace App\Http\Controllers;

use App\Financiamiento;
use App\Development;
use Illuminate\Http\Request;

class FinanciamientoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Int $desarrollo)
    {
        $info = [
            'title' => 'Planes de financiamiento',
            'breadcrumb' => [
                [
                    'title' => 'Todos',
                    'route' => 'panel.developments.index'
                ],
                [
                    'title' => Development::find($desarrollo)->name,
                    'route' => 'panel.developments.edit',
                    'params' => ['desarrollo' => $desarrollo],
                ],
                [
                    'title' => 'Planes de financiamiento',
                    'route' => 'panel.financiamiento.index',
                    'params' => ['desarrollo' => $desarrollo],
                    'active' => true
                ],
            ],
            'buttons' => [
                [
                    'title' => 'Agregar Nuevo',
                    'route' => 'panel.financiamiento.create',
                    'params' => ['desarrollo' => $desarrollo]
                ]
            ]
        ];
        $info['data'] = Financiamiento::where('development_id', $desarrollo)->get();
        return view('panel.financiamiento.index', $info);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Int $desarrollo)
    {
        $info = [
            'title' => 'Planes de financiamiento',
            'breadcrumb' => [
                [
                    'title' => 'Todos',
                    'route' => 'panel.developments.index'
                ],
                [
                    'title' => Development::find($desarrollo)->name,
                    'route' => 'panel.developments.edit',
                    'params' => ['desarrollo' => $desarrollo],
                ],
                [
                    'title' => 'Planes de financiamiento',
                    'route' => 'panel.financiamiento.index',
                    'params' => ['desarrollo' => $desarrollo],
                ],
                [
                    'title' => 'Nuevo',
                    'route' => 'panel.financiamiento.create',
                    'params' => ['desarrollo' => $desarrollo],
                    'active' => true
                ],
            ],
        ];
        $info['desarrollo'] =  Development::find($desarrollo);
        return view('panel.financiamiento.create', $info);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Int $desarrollo)
    {
        $arr = $request->toArray();
        
        $arr['development_id'] = $desarrollo;
        /*if(($arr['mensualidad'] === null) || ($arr['mensualidad_num_pagos'] === null)){
            unset($arr['mensualidad']);
            unset($arr['mensualidad_num_pagos']);
        }
*///dd($arr); 
        switch($arr['tipo']){
            case "contado":
                foreach($arr['contado'] as $key => $value){
                    $arr[$key] = $value;
                }
            break;
            case "contado_diferido":
                foreach($arr['contado_diferido'] as $key => $value){
                    $arr[$key] = $value;
                }
            break;
            case "financiamiento":
                foreach($arr['financiamiento'] as $key => $value){
                    $arr[$key] = $value;
                }
            break;
        } 

        unset($arr['contado']);
        unset($arr['contado_diferido']);
        unset($arr['financiamiento']);
        //dd($arr);

        $financiamiento = Financiamiento::create($arr);
        return redirect()->route('panel.financiamiento.index', ['desarrollo' => $desarrollo])->with('success', 'Operación exitosa');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Financiamiento  $financiamiento
     * @return \Illuminate\Http\Response
     */
    public function show(Financiamiento $financiamiento)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Financiamiento  $financiamiento
     * @return \Illuminate\Http\Response
     */
    public function edit(Int $desarrollo, Int $financiamiento)
    {
        $info = [
            'title' => 'Planes de financiamiento',
            'breadcrumb' => [
                [
                    'title' => 'Todos',
                    'route' => 'panel.developments.index'
                ],
                [
                    'title' => Development::find($desarrollo)->name,
                    'route' => 'panel.developments.edit',
                    'params' => ['desarrollo' => $desarrollo],
                ],
                [
                    'title' => 'Planes de financiamiento',
                    'route' => 'panel.financiamiento.index',
                    'params' => ['desarrollo' => $desarrollo],
                ],
                [
                    'title' => 'Editar',
                    'route' => 'panel.financiamiento.edit',
                    'params' => ['desarrollo' => $desarrollo, 'financiamiento' => $financiamiento],
                    'active' => true
                ],
            ],
        ];
        $info['financiamiento'] = Financiamiento::find($financiamiento);
        $info['desarrollo'] =  Development::find($desarrollo);
        return view('panel.financiamiento.edit', $info);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Financiamiento  $financiamiento
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Int $desarrollo, Int $id)
    {
        if($financiamiento = Financiamiento::find($id)){
            $arr = $request->toArray();

            switch($arr['tipo']){
                case "contado":
                    foreach($arr['contado'] as $key => $value){
                        $arr[$key] = $value;
                    }
                    $arr['mensualidad'] = 0;
                    $arr['mensualidad_num_pagos'] = 0;
                break;
                case "contado_diferido":
                    foreach($arr['contado_diferido'] as $key => $value){
                        $arr[$key] = $value;
                    }
                    $arr['mensualidad'] = 0;
                    $arr['mensualidad_num_pagos'] = 0;
                break;
                case "financiamiento":
                    foreach($arr['financiamiento'] as $key => $value){
                        $arr[$key] = $value;
                    }
                break;
            } 
    
            unset($arr['contado']);
            unset($arr['contado_diferido']);
            unset($arr['financiamiento']);

            $financiamiento->update($arr);

            return redirect()->back()->with('success', 'Información actualizada');
        }else{
            return abort(404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Financiamiento  $financiamiento
     * @return \Illuminate\Http\Response
     */
    public function destroy(Financiamiento $financiamiento)
    {
        //
    }
}
