<?php

namespace App\Http\Controllers;

use App\Admin;
use Illuminate\Http\Request;
use App\Providers\PermissionKey;
use App\Http\Controllers\Controller;

class EjecutiveController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $info = [
            'title' => 'Ejecutivos',
            'breadcrumb' => [
                [
                    'title' => 'Todos',
                    'route' => 'panel.prospects.index',
                    'active' => true
                ]
            ],
        ];
        
        if($request->user()->can(PermissionKey::Admin['permissions']['create']['name'])){
            $info['buttons'] = [
                [
                    'title' => 'Agregar Nuevo',
                    'route' => 'panel.admins.create'
                ],
            ];
        }
        if(isset($request->usuario)){
            if($request->user()->can(PermissionKey::Prospect['permissions']['index']['name'])){
                $info['user'] = $request->usuario;
            }else{
                $info['user'] = $request->user()->id;
            }

            if(isset($request->vista)){
                return view('panel.prospects.indexTable', $info);
            }else{
                return view('panel.prospects.index', $info);
            }
              
        }else{
            if($request->user()->can(PermissionKey::Prospect['permissions']['index']['name'])){
                $users = Admin::all();
                $managers = [];
                foreach($users as $user){
                    if($user->can(PermissionKey::Admin['permissions']['agent']['name']))
                        array_push($managers,$user);
                }
                
                $info['users'] = $managers; 
            }else{
                $info['users'] = Admin::where('id', $request->user()->id)->get();
            }
            return view('panel.prospects.usuarios', $info);   
        }
       
    }
}
