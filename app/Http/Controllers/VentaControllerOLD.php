<?php

namespace App\Http\Controllers;

use Illuminate\Support\Carbon;
use App\Venta;
use App\Admin;
use App\VentaRecibo;
use App\VentaDocumentation;
use App\Development;
use App\Quotation;
use App\Client;
use App\Lote;
use App\Financiamiento;
use App\VentaPagoEnganche;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Barryvdh\DomPDF\Facade as PDF;
use App\Notifications\RevokeQuotation;
use App\Notifications\ReviewStep;
use App\Notifications\ReviewAccepted;
use Illuminate\Notifications\Notification;
use App\Providers\PermissionKey;
use Illuminate\Support\Str;
use PhpOffice\PhpWord\PhpWord as PhpWord;
use PhpOffice\PhpWord\TemplateProcessor as TemplateProcessor;
use Illuminate\Support\Facades\Storage;

class VentaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $info = [
            'title' => 'Ventas - '.Development::find($request->desarrollo)->name,
            'breadcrumb' => [
                [
                    'title' => 'Todos',
                    'route' => 'panel.ventas.index',
                    'active' => true
                ]
            ]
        ];
        $info['desarrollo'] = Development::find($request->desarrollo);
        return view('panel.ventas.index', $info);
    }

    public function pagos(Request $request){
        $info = [
            'title' => 'Ventas - '.Development::find($request->desarrollo)->name,
            'breadcrumb' => [
                [
                    'title' => 'Ventas',
                    'route' => 'panel.ventas.index',
                    'params' => ["desarrollo"=>$request->desarrollo],
                    'active' => true
                ]
            ]
        ];
        $info['desarrollo'] = Development::find($request->desarrollo);
        $ventas = Venta::where([['status',">",2],'development_id'=>$request->desarrollo])->orderBy("status","asc")->get();
        $pagosPendientes = [];
        $totalPorCobrar = 0;
        foreach($ventas as $venta){
            //enganche y pagos de contado
            for($i=$venta->recibos('abono-enganche')->count();$i<count($venta->fechas_enganche()->get());$i++){
                $fecha = $venta->fechas_enganche()->get()[$i]->fecha();
                $fecha2 = explode("/",$fecha);
                $fecha2 = $fecha2[2]."-".$fecha2[1]."-".$fecha2[0];
                if($fecha2 <= now()->toDateString()){
                    array_push($pagosPendientes,[
                        "id" => $venta->id,
                        "cliente" => $venta->client()->name." ".$venta->client()->surname,
                        "lote" => $venta->lote()->name,
                        "fecha" => $fecha,
                        "monto" => $venta->quotation()->detail->pago_mensual_enganche,
                        "tipo" => $venta->quotation()->detail->plazo->tipo == 'financiamiento'?"Pago de enganche":"Pago"
                    ]);
                    $totalPorCobrar = $totalPorCobrar + getAmmount($venta->quotation()->detail->pago_mensual_enganche);
                }
            }

            //finaciaminetos y saldos
            if($venta->quotation()->detail->plazo->tipo == 'financiamiento'){
                $corrida = $venta->corrida($venta->fecha_siguiente_pago);
                $abonos_pagados = 0;
            
                foreach ($venta->recibos('mensualidad') as $k => $item){
                    if ($item->status == 'paid'){
                        $abonos_pagados++;
                    }
                }
                for($i = 0; $i < ($corrida->num_pagos - ($abonos_pagados)); $i++){
                    $fecha = date('d/m/Y', strtotime($corrida->fecha));
                    $fecha2 = explode("/",$fecha);
                    $fecha2 = $fecha2[2]."-".$fecha2[1]."-".$fecha2[0];
                    if($fecha2 <= now()->toDateString()){
                        array_push($pagosPendientes,[
                            "id" => $venta->id,
                            "cliente" => $venta->client()->name." ".$venta->client()->surname,
                            "lote" => $venta->lote()->name,
                            "fecha" => $fecha,
                            "monto" => $venta->quotation()->detail->pago_mensual,
                            "tipo" => "Mensualidad"
                        ]);
                        $totalPorCobrar = $totalPorCobrar + getAmmount($venta->quotation()->detail->pago_mensual);
                    }else
                        break;
                    $corrida->fecha = date("Y-m-".$corrida->dia, strtotime($corrida->fecha."+1 month"));
                }
            }
            
        }
        $info['data'] = $pagosPendientes;
        $info['total'] = "$".number_format($totalPorCobrar);
       
        return view('panel.ventas.pagos', $info);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $info = [
            'title' => 'Cotizar / Apartar - '.mb_strtoupper(Development::find($request->desarrollo)->name).' ETAPA '.mb_strtoupper(Development::find($request->desarrollo)->stage),
            'breadcrumb' => [
                [
                    'title' => 'Nuevo',
                    'route' => 'panel.ventas.create',
                    'active' => true
                ]
            ]
        ];
        $info['desarrollo'] = Development::find($request->desarrollo);
        if(isset($request->client_id)){
            $info['client'] = Client::find($request->client_id);
        }
        $info['clients'] = [];
        $clients = [];
        if($request->user()->client()->count() > 0){
            foreach($request->user()->client() as $client){
                $name = $client->name.' '.$client->surname;
                $clients[$name] = $client;
            }
        }
        $info['clients'] = $clients;
        return view('panel.ventas.create', $info);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validamos que el lote no este apartado y lo apartamos
        if(Lote::find($request->lote_id)->status == 'apartado'){
            return redirect()->back()->withErrors(['invalid' => 'El lote ya se encuentra aparatado']);
        }

        $venta_id = null;
       
        $venta_id = $request->venta_id;
        
        $arr = $request->toArray();
        $arr['admin_id'] = $request->user()->id;
        //Generamos la cotización
        $arr['detail']['plazo'] = Financiamiento::find($arr['plazo'])->toArray();
        $arr['detail'] = json_encode($arr['detail']);
        $cotizacion = Quotation::create($arr);
        
        //Generamos la venta en status apartado
        //(intval($request->detail['cliente']));
        $venta = Venta::create([
            'development_id' => Lote::find($request->lote_id)->development()->id,
            'lote_id' => $request->lote_id,
            'client_id' => intval($request->cliente),
            'quotation_id' => $cotizacion->id,
            'total' => '5000'
        ]);
        
        if($venta){
            if((isset($request->document)) && (count($request->document) > 0)){
                foreach ($request->document as $key => $doc) {
                    if($doc->getSize() > 2000000){
                        return redirect()->back()->withInputs($request->input())->withErrors(['invalid' => 'El tamaño máximo de los archivos es de 1.9MB']);
                    }else{
                        //Procedemos a guardar el archivo
                        $path_file = $doc->store('public/documents');
                        $_exploded = explode('/', $path_file);
                        $_exploded[0] = 'storage';
                        $path_file = implode('/', $_exploded);
                        //Validamos sí es un nuevo documento
                        if($document = VentaDocumentation::where('development_client_documentation_id', $key)->where('venta_id', $venta->id)->first()){
                            $document->update([
                                'file' => $path_file,
                                'status' => 'created',
                            ]);
                        }else{
                            $document = VentaDocumentation::create([
                                'venta_id' => $venta->id,
                                'development_client_documentation_id' => $key,
                                'file' => $path_file,
                                'status' => 'created',
                            ]);
                            if(!is_null($venta->id)){
                                
                            }
                        }
                    }
                }
                //return redirect()->back()->with('success', 'Operación exitosa');
            }else{
                if($request->file('file')){
                    $path_file = $request->file('file')->store('public/media');
                    $_exploded = explode('/', $path_file);
                    $_exploded[0] = 'storage';
                    $path_file = implode('/', $_exploded);
                    $arr['file'] = $path_file;
                    $arr['venta_id'] = $venta->id;
                    if($ventaRecibo = VentaRecibo::where('venta_id', $venta->id)->where('slug', 'condicion-de-compra')->first()){
                        $ventaRecibo->update($arr);
                    }else{
                        $ventaRecibo = VentaRecibo::create($arr);
                    }
                // return redirect()->back()->with('success', 'Operación exitosa');
                }else{
                    return redirect()->back()->withErrors(['invalid' => 'No se realizó operación']);
                }
            }

            $venta->update(['status' => 1]);
            $venta->lote()->update(['status' => 'apartado']);
            $venta->asesor()->notify(new ReviewAccepted($venta));
        }
        return redirect()->route('panel.ventas.index', ['desarrollo' => $venta->development_id])->with('success', 'Apartado creado.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Venta  $venta
     * @return \Illuminate\Http\Response
     */
    public function show(Venta $venta)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Venta  $venta
     * @return \Illuminate\Http\Response
     */
    public function edit(Int $id)
    {
        $info = [
            'title' => 'Venta',
            'breadcrumb' => [
                [
                    'title' => 'Todos',
                    'route' => 'panel.ventas.index',
                    'params' => ['desarrollo' => Venta::find($id)->development_id],
                ],
                [
                    'title' => 'Detalle',
                    'route' => 'panel.ventas.edit',
                    'params' => ['venta' => $id],
                    'active' => true
                ]
            ]
        ];
        $info['venta'] = Venta::find($id);
        return view('panel.ventas.edit', $info);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Venta  $venta
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Int $id)
    {
        try{
            if($venta = Venta::find($id)){
                $arr = $request->all();
                if(isset($arr['fecha_bloqueo'])){
                    $arr['fecha_bloqueo'] = str_replace('/', '-', $arr['fecha_bloqueo']);
                    $arr['fecha_bloqueo'] = date('Y-m-d', strtotime($arr['fecha_bloqueo']));
                }
                if(isset($arr['fecha_firma_contrato'])){
                    $arr['fecha_firma_contrato'] = str_replace('/', '-', $arr['fecha_firma_contrato']);
                    $arr['fecha_firma_contrato'] = date('Y-m-d', strtotime($arr['fecha_firma_contrato']));
                }
                if(isset($arr['dia_pago'])){
                    $arr['dia_pago'] = str_replace('/', '-', $arr['dia_pago']);
                    $arr['dia_pago'] = date('Y-m-d', strtotime($arr['dia_pago']));
                }
                if(isset($arr['contrato_text'])){
                    $arr['contrato'] = $arr['contrato_text'];
                }
                if($request->file('contrato')){
                    $arr['contrato'] = upload_file($request->file('contrato'));
                    $arr['status'] = 5;
                }

                if((isset($request->title)) && ($request->title == "Pago Bloqueo")){
                    $e = [];
                    $e['venta_id'] = $venta->id;
                    $e['title'] = $request->title;
                    $e['slug'] = Str::slug($e['title']);
                    $e['fecha_pago'] = $arr['fecha_bloqueo'];
                    $e['monto'] = $arr['monto_bloqueo'];
                    $e['banco_id'] = $arr['banco_id'];
                    $e['message'] = $arr['message'];
                    $e['payment_way'] = $arr['payment_way'];
                    $e['payment_method'] = $arr['payment_method'];

                    // $path_file = $request->comprobante_pago->store('public/documents');
                    // $_exploded = explode('/', $path_file);
                    // $_exploded[0] = 'storage';
                    // $path_file = implode('/', $_exploded);
                    // $e['file'] = $path_file;

                    $e['status'] = 'paid';
                    if($ventaRecibo = VentaRecibo::where('venta_id', $venta->id)->where('slug', $e['slug'])->first()){
                        $ventaRecibo->update($e);
                    }else{
                        VentaRecibo::create($e);
                    }
                }
                
                if(isset($arr['dia_pago_enganche'])){
                    VentaPagoEnganche::where('venta_id', $venta->id)->delete();
                    foreach ($arr['dia_pago_enganche'] as $key => $value) {
                        $value = str_replace('/', '-', $value);
                        $value = date('Y-m-d', strtotime($value));
                        VentaPagoEnganche::create([
                            'venta_id' => $venta->id,
                            'fecha' => $value,
                            'monto' => $request->monto_enganche[$key]
                        ]);
                    }
                }

                $venta->update($arr);

                if(isset($arr['status'])){
                    if($arr['status'] == 3){
                        //Notificamos a administración y jurídico
                        $users = Admin::all();
                        foreach ($users as $key => $user) {
                            if($user->can(PermissionKey::Venta['permissions']['authorize_recibo_enganche']['name'])){
                                $user->notify(new ReviewAccepted($venta));
                            }
                        }
                    }
                    //Se autorizó el pago del enganche
                    if($arr['status'] == 4){
                        //Marcamos el lote como vendido
                        $venta->lote()->update(['status' => 'vendido']);
                        //Acutaliamos la fecha de la primera mensualidad
                        $venta->fecha_siguiente_pago = $venta->dia_pago;
                        $venta->save();
                    }
                    if($arr['status'] < 3){
                        $venta->asesor()->notify(new ReviewAccepted($venta));
                    }
                }
            }
            if($request->ajax()){
                return response(['success' => true, 'message' => 'Element updated', 'data' => $venta->toArray()], 200);
            }else{
                return redirect()->back()->with('success', 'Operación exitosa');
            }
        }catch (\Exception $ex){
            Log::info("Error");
            Log::error($ex);
            dd($ex);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Venta  $venta
     * @return \Illuminate\Http\Response
     */
    public function destroy(Int $id)
    {
        if($venta = Venta::find($id)){
            $venta->lote()->update(['status' => 'disponible']);
            VentaRecibo::where('venta_id', $id)->delete();
            VentaDocumentation::where('venta_id', $id)->delete();
            VentaPagoEnganche::where('venta_id', $id)->delete();
            Venta::destroy($id);
            return response(['success' => true], 200);
        }else{
            return response(['success' => false], 200);
        }
    }

    public function cotizar(Request $request)
    {
        //validamos que el lote no este apartado y lo apartamos
        if(Lote::find($request->lote_id)->status == 'apartado'){
            return redirect()->back()->withErrors(['invalid' => 'El lote ya se encuentra aparatado']);
        }
        $arr = $request->toArray();
        $arr['admin_id'] = Client::find($arr['cliente'])->admin_id;
        //Generamos la cotización
        $arr['detail']['plazo'] = Financiamiento::find($arr['plazo'])->toArray();
        $arr['detail'] = json_encode($arr['detail']);
        $cotizacion = Quotation::create($arr);
        
        if($cotizacion){
            $venta = Venta::create([
                'development_id' => Lote::find($request->lote_id)->development()->id,
                'lote_id' => $request->lote_id,
                'client_id' => intval($request->cliente),
                'quotation_id' => $cotizacion->id,
                'total' => '5000',
            ]);
            if($venta){
                return $venta->id;
            }
        }
    }

    public function cotizacion(Int $id){
        
        if($venta = Venta::find($id)){
            
            $info['venta'] = $venta;
            $info['development'] = $venta->development();
            $info['client'] = $venta->client();
            $info['quotation'] = $venta->quotation();
            return PDF::loadView('panel.ventas.pdf.cotizacion', $info)->stream('cotizacion-'.$id.'.pdf');
        }else{
            abort(404);
        }
    }

    public function condicion(Int $id){
        if($venta = Venta::find($id)){
            // return view('panel.ventas.pdf.condicion', $venta);
            $info['venta'] = $venta;
            $info['development'] = $venta->development();
            $info['client'] = $venta->client();
            $info['quotation'] = $venta->quotation();
            return PDF::loadView('panel.ventas.pdf.condicion', $info)->stream('condicion-compra-'.$id.'.pdf');
        }else{
            abort(404);
        }
    }

    public function corrida(Int $id){
        if($venta = Venta::find($id)){
            $info['venta'] = $venta;
            $info['development'] = $venta->development();
            $info['client'] = $venta->client();
            $info['quotation'] = $venta->quotation();
            $customPaper = array(0,0,21.59/2.54*72,35.36/2.54*72);
            return PDF::loadView('panel.ventas.pdf.corrida', $info)->setPaper($customPaper, 'portair')->stream('corrida-venta-'.$id.'.pdf');
        }else{
            abort(404);
        }
    }

    public function abono(Int $id){
        if($ventaRecibo = VentaRecibo::find($id)){
            $info['venta'] = Venta::find($ventaRecibo->venta_id);
            $info['development'] = $info['venta']->development();
            $info['client'] = $info['venta']->client();
            $info['quotation'] = $info['venta']->quotation();
            $info['recibo'] = $ventaRecibo;
            return PDF::loadView('panel.ventas.pdf.abono', $info)->stream('abono-enganche-'.$id.'.pdf');
        }else{
            abort(404);
        }
    }

    public function bloqueo(Int $id){
        $info = [];
        if($info['venta'] = Venta::find($id)){
            $info['development'] = $info['venta']->development();
            $info['client'] = $info['venta']->client();
            $info['quotation'] = $info['venta']->quotation();
            return PDF::loadView('panel.ventas.pdf.bloqueo', $info)->stream('bloqueo-'.$id.'.pdf');
        }else{
            abort(404);
        }
    }

    public function contrato(Int $id){
        setlocale(LC_TIME, "spanish");
        if($venta = Venta::find($id)){
           /* if($venta->quotation()->detail->plazo->tipo == "contado"){
                $templateProcessor = new TemplateProcessor(Storage::path('public/contrato/machote_contado.docx'));
            }else{
                $templateProcessor = new TemplateProcessor(Storage::path('public/contrato/machote_financiado.docx'));
            }*/
            $templateProcessor = new TemplateProcessor(Storage::path('public/contrato/nuevo_machote_contrato.docx'));
           // $templateProcessor->setValue('dia', num2letras(date('j', strtotime($venta->fecha_firma_contrato)), false, false, false));
            //Pondremos el mes de la fecha de la firma del contrato
            //dd($venta->development()->cadastralPlanking()->number);
            //fecha venta
            $templateProcessor->setValue('fecha_venta',date('d', strtotime($venta->fecha_firma_contrato))." de ".monthTraductor(date('m', strtotime($venta->fecha_firma_contrato)))." de ".num2letras(date('Y'), false, false, false));
            //cliente
            $templateProcessor->setValue('nombre_cliente', mb_strtoupper($venta->client()->name.' '.$venta->client()->surname));
            $templateProcessor->setValue('dia_nacimiento_cliente', date('d', strtotime($venta->client()->fecha_nacimiento)));
            $templateProcessor->setValue('descripcion_dia_nacimiento_cliente', descriptionNumber(date('d', strtotime($venta->client()->fecha_nacimiento))));
            $templateProcessor->setValue('mes_nacimiento_cliente',  monthTraductor(date('m', strtotime($venta->client()->fecha_nacimiento))));
            $templateProcessor->setValue('anio_nacimiento_cliente', date('Y', strtotime($venta->client()->fecha_nacimiento)));
            $templateProcessor->setValue('descripcion_anio_nacimiento_cliente', descriptionNumber(date('Y', strtotime($venta->client()->fecha_nacimiento))));
            $templateProcessor->setValue('ine_cliente', $venta->client()->identificacion);
            $templateProcessor->setValue('nacionalidad_cliente', strtolower($venta->client()->nacionalidad));
            $templateProcessor->setValue('curp_cliente', strtoupper($venta->client()->curp));

            $array = str_split(strtoupper($venta->client()->curp));
            $array = array_map('documentToLetras', $array);
            $descripcion_curp_cliente = implode(",",$array);

            $templateProcessor->setValue('descripcion_curp_cliente', $descripcion_curp_cliente);
            
            $array = str_split(strtoupper($venta->client()->rfc));
            $array = array_map('documentToLetras', $array);
            $descripcion_rfc_cliente = implode(",",$array);

            $templateProcessor->setValue('rfc_cliente', strtoupper($venta->client()->rfc)." (".$descripcion_rfc_cliente.")");

            $array = str_split(strtoupper($venta->client()->identificacion));
            $array = array_map('documentToLetras', $array);
            $identificacion = strtoupper($venta->client()->identificacion)." (".implode(",",$array).")";

            $templateProcessor->setValue('identificacion_cliente', $identificacion);
            $templateProcessor->setValue('instituto_expediente_identificacion_cliente',  identificationExtend($venta->client()->identification_type));

            $array = explode(" ",$venta->client()->address);
            $array = array_map('fraseNumToLetras', $array);
            $direccion_cliente = implode(" ",$array);

            $templateProcessor->setValue('direccion_cliente', $direccion_cliente);
            $templateProcessor->setValue('cp_cliente', $venta->client()->cp." (".descriptionNumber($venta->client()->cp).")"); 
            //clientes extranjeros
            $clausula = "";
            $descripcion_clausula = "";
            if($venta->client()->extranjero){
                $clausula = "DECIMA OCTAVA.- CLAUSULA ESPECIAL.";
                $descripcion_clausula = "---En atención a que la nacionalidad del prominente comprador es de un país extranjero que mantienen relaciones diplomáticas con México; y, que el predio motivo del presente contrato se encuentra ubicado dentro de las áreas restringidas que prevé el primer párrafo del Artículo 27-1 Constitucional; así como de conformidad con lo establecido en la Ley de Inversión Extranjera, publicada en Diciembre de 1993, la que permite a los extranjeros adquirir un bien inmueble dentro de la zona restringida, por medio de fideicomiso del cual “EL PROMINENTE COMPRADOR” se hace responsable de todo tramite de la gestión y seguimiento, renunciando a la protección de su gobierno en relación con tal adquisición, quedando el prominente vendedor sin responsabilidad debido al incumplimiento de la obligación de la presente clausula si como fuere el no perfeccionamiento de la escritura correspondiente de traslación de dominio.---";
            }
            $templateProcessor->setValue('clausula_extranjeros', $clausula);
            $templateProcessor->setValue('descripcion_clausula_extranejros', $descripcion_clausula);
            //apoderado
            $templateProcessor->setValue('nombre_apoderado', mb_strtoupper($venta->development()->admin()->name.' '.$venta->development()->admin()->last_name));
            
            $array = str_split(strtoupper($venta->development()->admin()->identificacion));
            $array = array_map('documentToLetras', $array);
            $identificacion = strtoupper($venta->development()->admin()->identificacion)." (".implode(",",$array).")";
            
            $templateProcessor->setValue('identificacion_apoderado', $identificacion);
            $templateProcessor->setValue('instituto_expediente_identificacion_apoderado',  identificationExtend($venta->development()->admin()->identification_type));
            $templateProcessor->setValue('lugar_nacimiento_apoderado_May', mb_strtoupper($venta->development()->admin()->lugar_nacimiento));
            $templateProcessor->setValue('lugar_nacimiento_apoderado_Min', $venta->development()->admin()->lugar_nacimiento);
            $templateProcessor->setValue('dia_nacimiento_apoderado', date('d', strtotime($venta->development()->admin()->birth_date)));
            $templateProcessor->setValue('descripcion_dia_nacimiento_apoderado', descriptionNumber(date('d', strtotime($venta->development()->admin()->birth_date))));
            $templateProcessor->setValue('mes_nacimiento_apoderado',  monthTraductor(date('m', strtotime($venta->development()->admin()->birth_date))));
            $templateProcessor->setValue('anio_nacimiento_apoderado', date('Y', strtotime($venta->development()->admin()->birth_date)));
            $templateProcessor->setValue('descripcion_anio_nacimiento_apoderado', descriptionNumber(date('Y', strtotime($venta->development()->admin()->birth_date))));

            $array = explode(" ",$venta->development()->admin()->direccion);
            $array = array_map('fraseNumToLetras', $array);
            $direccion_apoderado = implode(" ",$array);

            $templateProcessor->setValue('direccion_apoderado', $direccion_apoderado);
            $templateProcessor->setValue('cp_apoderado', $venta->development()->admin()->cp." (".descriptionNumber($venta->development()->admin()->cp).")"); 
            $templateProcessor->setValue('curp_apoderado', strtoupper($venta->development()->admin()->curp));

            $array = str_split(strtoupper($venta->development()->admin()->curp));
            $array = array_map('documentToLetras', $array);
            $descripcion_curp_apoderado = implode(",",$array);

            $templateProcessor->setValue('descripcion_curp_apoderado', $descripcion_curp_apoderado);
            
            $array = str_split(strtoupper($venta->development()->admin()->rfc));
            $array = array_map('documentToLetras', $array);
            $descripcion_rfc_apoderado = implode(",",$array);

            $templateProcessor->setValue('descripcion_rfc_apoderado', strtoupper($venta->development()->admin()->rfc)." (".$descripcion_rfc_apoderado.")");
            //tablaje catastral
            $templateProcessor->setValue('folio_tablaje_catastral', $venta->development()->cadastralPlanking()->number);
            $templateProcessor->setValue('folio_tablaje_catastral_descripcion', descriptionNumber($venta->development()->cadastralPlanking()->number));
            $templateProcessor->setValue('localidad_tablaje_catastral', $venta->development()->cadastralPlanking()->location);
            $templateProcessor->setValue('referencias_tablaje_catastral', $venta->development()->cadastralPlanking()->reference);
            $templateProcessor->setValue('medida_tablaje_catastral', number_format($venta->development()->cadastralPlanking()->size,2)." m2 (".descriptionNumber(number_format($venta->development()->cadastralPlanking()->size,2),false,true,false).")");
            $templateProcessor->setValue('colindancias_tablaje_catastral', $venta->development()->cadastralPlanking()->adjoining);
            //poderdantes
            $poderdante = "";
            $poderdante_completo = ""; 
            foreach($venta->development()->grantor() as $item){
                
                $poderdante .= $item->title." ".ucwords($item->name." ".ucwords($item->last_name));
                $poderdante_completo .= $item->title." ".ucwords($item->name)." ".ucwords($item->last_name).", ".$item->public_notary_description;
            }

            $templateProcessor->setValue('poderdante', $poderdante);
            $templateProcessor->setValue('poderdante_completo', $poderdante_completo);
            //lote
            $templateProcessor->setValue('medida_lote', number_format($venta->lote()->area,2)." m2 (".descriptionNumber(number_format($venta->lote()->area,2),false,false,false).")");
            $templateProcessor->setValue('numero_lote', $venta->lote()->num_lote);
            $templateProcessor->setValue('numero_lote_descripcion', descriptionNumber($venta->lote()->num_lote));

            $linderos =  [];
            if(!is_null($venta->lote()->norte))
                array_push($linderos,"al Norte con ".number_format($venta->lote()->norte,2)." m (".descriptionNumber(number_format($venta->lote()->norte,2),false,false,false).") metros lineales ".$venta->lote()->lindancia_norte);
            if(!is_null($venta->lote()->nororiente))
                array_push($linderos,"al Nororiente con ".number_format($venta->lote()->nororiente,2)." m (".descriptionNumber(number_format($venta->lote()->nororiente,2),false,false,false).") metros lineales ".$venta->lote()->lindancia_nororiente);
            if(!is_null($venta->lote()->oriente))
                array_push($linderos,"al Oriente con ".number_format($venta->lote()->oriente,2)." m (".descriptionNumber(number_format($venta->lote()->oriente,2),false,false,false).") metros lineales ".$venta->lote()->lindancia_oriente);
            if(!is_null($venta->lote()->suroriente))
                array_push($linderos,"al Suroriente con ".number_format($venta->lote()->suroriente,2)." m (".descriptionNumber(number_format($venta->lote()->suroriente,2),false,false,false).") metros lineales ".$venta->lote()->lindancia_suroriente);
            if(!is_null($venta->lote()->sur))
                array_push($linderos,"al Sur con ".number_format($venta->lote()->sur,2)." m (".descriptionNumber(number_format($venta->lote()->sur,2),false,false,false).") metros lineales ".$venta->lote()->lindancia_sur);
            if(!is_null($venta->lote()->surponiente))
                array_push($linderos,"al Surponiente con ".number_format($venta->lote()->surponiente,2)." m (".descriptionNumber(number_format($venta->lote()->surponiente,2),false,false,false).") metros lineales ".$venta->lote()->lindancia_surponiente);
            if(!is_null($venta->lote()->poniente))
                array_push($linderos,"al Poniente con ".number_format($venta->lote()->poniente,2)." m (".descriptionNumber(number_format($venta->lote()->poniente,2),false,false,false).") metros lineales ".$venta->lote()->lindancia_poniente);
            if(!is_null($venta->lote()->norponiente))
                array_push($linderos,"al Norponiente con ".number_format($venta->lote()->norponiente,2)." m (".descriptionNumber(number_format($venta->lote()->norponiente,2),false,false,false).") metros lineales ".$venta->lote()->lindancia_norponiente);

            $linderos_lote = "";
            foreach($linderos as $key=>$value){
                if(count($linderos)-1 == $key){
                    $linderos_lote .= $value.".";
                }else{
                    if(count($linderos)-2 == $key){
                        $linderos_lote .= $value." y ";
                    }else{
                        $linderos_lote .= $value.", ";
                    }
                }
            }

            $templateProcessor->setValue('linderos_lote', $linderos_lote);
            //desarrollo
            $templateProcessor->setValue('nombre_desarrollo', ucwords($venta->development()->name));
            $templateProcessor->setValue('nombre_desarrollo_mayuscula', mb_strtoupper($venta->development()->name));
            $templateProcessor->setValue('etapa_desarrollo', $venta->development()->stage);
            $templateProcessor->setValue('ubicacion_desarrollo_mayuscula', mb_strtoupper($venta->development()->location));
            $templateProcessor->setValue('ubicacion_desarrollo', ucwords($venta->development()->location));
            //venta
           // dd($venta->quotation()->detail->precio_final." M.N SON:(".num2letras(getAmmount($venta->quotation()->detail->precio_final),false,false,true).")");
           // dd(num2letras(getAmmount($venta->quotation()->detail->precio_final),false,false),num2letras(getAmmount($venta->quotation()->detail->monto_enganche),false,false),num2letras(getAmmount($venta->quotation()->detail->saldo),false,false));
            
            $templateProcessor->setValue('monto_venta', $venta->quotation()->detail->precio_final." M.N SON: (".num2letras(getAmmount($venta->quotation()->detail->precio_final),false,false,true).")");
            //$templateProcessor->setValue('descripcion_monto_venta', mb_strtoupper(num2letras(getAmmount($venta->quotation()->detail->precio_final), false, false)));
            $plazo = "";
            if($venta->quotation()->detail->plazo->tipo == 'contado'){
                $plazo .= "--- Ambas partes convienen que el “ PROMITENTE COMPRADOR”  realizará un NUMERO PAGO PRIMER PAGO por la cantidad de ".$venta->quotation()->detail->monto_enganche." M.N SON: (".num2letras(getAmmount($venta->quotation()->detail->monto_enganche),false,false,true).")"." con fecha del día FECHA PAGO XX (CERO, CERO)  del mes de  XXXXX del año 2021 (DOS, CERO, DOS, UNO), y el pago de SALDO CONTRA ESCRITURA de ".$venta->quotation()->detail->saldo." M.N SON: (".num2letras(getAmmount($venta->quotation()->detail->saldo),false,false,true).")"." con fecha del día FECHA PAGO SALDO XX (XXXXXXXX) de XXXXX del año 2024 (DOS, CERO, DOS, CUATRO) de forma directa y sin intermediarios correspondiente al PAGO DE CONTADO del lote motivo de esta operación, desglosado en el recibo correspondiente, que al efecto se le otorga y que forma parte integrante de este contrato, por lo que se le tiene por reproducido como si fuere insertado a la letra en esta cláusula, por lo que no se hace responsable “ EL PROMITENTE VENDEDOR” de haber sido entregado dicho monto, a terceros sin autorización expresa y por escrito para que pueda ser recepcionado por orden.------------";
                $plazo .= "--- Se considera PAGO DE CONTADO aquel que se realice con el 90 % del valor total estipulado y el 10 % sea saldado a la entrega de la escritura en una sola exhibición en el caso de no ser así, se considerara como financiamiento cobrándose el costo correspondiente al valor del predio a tratar. -----------------------------------------------------------";
            }
            if($venta->quotation()->detail->plazo->tipo == 'contado_diferido'){
                $plazo .= "--- Ambas partes convienen que el “ PROMITENTE COMPRADOR”  realizará un NUMERO PAGO PRIMER PAGO por la cantidad de ".$venta->quotation()->detail->monto_enganche." M.N SON: (".num2letras(getAmmount($venta->quotation()->detail->monto_enganche),false,false,true).")"." con fecha del día FECHA PAGO XX (CERO, CERO)  del mes de  XXXXX del año 2021 (DOS, CERO, DOS, UNO), y el pago de SALDO CONTRA ESCRITURA de ".$venta->quotation()->detail->saldo." M.N SON: (".num2letras(getAmmount($venta->quotation()->detail->saldo),false,false,true).")"." con fecha del día FECHA PAGO SALDO XX (XXXXXXXX) de XXXXX del año 2024 (DOS, CERO, DOS, CUATRO) de forma directa y sin intermediarios correspondiente al PAGO DE CONTADO del lote motivo de esta operación, desglosado en el recibo correspondiente, que al efecto se le otorga y que forma parte integrante de este contrato, por lo que se le tiene por reproducido como si fuere insertado a la letra en esta cláusula, por lo que no se hace responsable “ EL PROMITENTE VENDEDOR” de haber sido entregado dicho monto, a terceros sin autorización expresa y por escrito para que pueda ser recepcionado por orden.------------";
                $plazo .= "--- Se considera PAGO DE CONTADO aquel que se realice con el 90 % del valor total estipulado y el 10 % sea saldado a la entrega de la escritura en una sola exhibición en el caso de no ser así, se considerara como financiamiento cobrándose el costo correspondiente al valor del predio a tratar. -----------------------------------------------------------";
                $plazo .= "En  CONTADO EXTENDIDO es el plazo máximo de 90 días considerando el párrafo anterior.---------------------------------------------------------------------------------------------------------";
            }
            if($venta->quotation()->detail->plazo->tipo == 'financiamiento'){
                $plazo .= "---Ambas partes convienen que el precio restante de la operación deberá pagarse mediante los pagos mensuales y consecutivos por las cantidades que se especifican en la CORRIDA FINANCIERA desglosado en el ANEXO A correspondiente, que al efecto se le otorga y que forma parte integrante de este contrato, por lo que se le tiene por reproducido como si fuere insertado a la letra en esta cláusula.---------------------";
                $plazo .= "--- EL “ PROMITENTE COMPRADOR”  realizará el pago correspondiente al   ENGANCHE  por la cantidad de ".$venta->quotation()->detail->monto_enganche." M.N SON: (".num2letras(getAmmount($venta->quotation()->detail->monto_enganche),false,false,true).")"." con fecha del día FECHA PAGO ENGANCHEXX (CERO,CERO)  del mes de  XXXXXXXXX del año XXXX (DOS, CERO, DOS, CERO), de forma directa y sin intermediarios correspondiente al PLAN DE FINANCIAMIENTO del lote motivo de esta operación, desglosado en el recibo correspondiente, que al efecto se le otorga y que forma parte integrante de este contrato, por lo que se le tiene por reproducido como si fuere insertado a la letra en esta cláusula, por lo que no se hace responsable “ EL PROMITENTE VENDEDOR” de haber sido entregado dicho monto, a terceros sin autorización expresa y por escrito para que pueda ser recepcionado por orden.------------";
            }

            $templateProcessor->setValue('monto_enganche', num2letras(getAmmount($venta->quotation()->detail->monto_enganche),false,false));
            /*$templateProcessor->setValue('monto_saldo', num2letras(getAmmount($venta->quotation()->detail->saldo),false,false));dd("hola");*/
            $templateProcessor->setValue('plazo_venta', $plazo);
            $templateProcessor->setValue('precio_real', "$".number_format($venta->lote()->price,2)." M.N SON: (".num2letras($venta->lote()->price,false,false,true).")");
            
            $endDate=date('Y-m-d', strtotime('+36 months', strtotime($venta->fecha_firma_contrato)));

            $templateProcessor->setValue('dia_vencimiento', date('d', strtotime($endDate)));
            $templateProcessor->setValue('descripcion_dia_vencimiento', descriptionNumber(date('d', strtotime($endDate))));
            $templateProcessor->setValue('mes_vencimiento',  monthTraductor(date('m', strtotime($endDate))));
            $templateProcessor->setValue('anio_vencimiento', date('Y', strtotime($endDate)));
            $templateProcessor->setValue('descripcion_anio_vencimiento', descriptionNumber(date('Y', strtotime($endDate))));
            //$templateProcessor->setValue('descripcion_monto_enganche', mb_strtoupper(num2letras(getAmmount($venta->quotation()->detail->monto_enganche))));
            $templateProcessor->setValue('plazo_financiamiento', $venta->quotation()->detail->mensualidad);
            //$templateProcessor->setValue('plazo_financiamiento_letra', num2letras($venta->quotation()->detail->mensualidad, false, false, false));
            $count = 0;
            foreach($venta->documentation() as $doc){
                if($doc->development_client_documentation->show_contract){
                    $templateProcessor->setImageValue('identificacion_cliente_'.$count, ['path' => asset($doc->file), 'width' => 300, 'height' => 300, 'ratio' => true]);
                    $count++;
                }
            }
//dd($templateProcessor);
            $templateProcessor->saveAs(Storage::path('public/contrato/contrato_venta_'.$id.'.docx'));
            return response()->download(Storage::path('public/contrato/contrato_venta_'.$id.'.docx'))->deleteFileAfterSend(true);

            // $phpWord = \PhpOffice\PhpWord\IOFactory::load(Storage::path('public/contrato/temp_machote_nerea_financiado.docx')); // Read the temp file
            // $xmlWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
            // $xmlWriter->save(Storage::path('public/contrato/contrato_venta_'.$id.'.docx'));


            // $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($templateProcessor, 'Word2007');
            // $objWriter->save();
        }else{
            abort(404);
        }
    }

    public function revoke(Request $request, Int $id_venta){
        $venta = Venta::find($id_venta);
        $venta->asesor()->notify(new RevokeQuotation($venta, $request->motivo));
        return redirect()->back()->with('success', 'Operación exitosa');
    }

    public function review(Request $request, Int $id_venta){
        $venta = Venta::find($id_venta);
        $users = Admin::all();
        foreach ($users as $key => $user) {
            if($user->can(PermissionKey::Venta['permissions']['authorize_client_docs']['name'])){
                $user->notify(new ReviewStep($venta));
            }
        }
        return response(['success' => true, 'message' => 'Element updated', 'data' => $venta->toArray()], 200);
    }
}
