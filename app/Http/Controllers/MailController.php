<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Mail;
use App\Mail\QuotationCreated;
use App\Mail\ContactForm;

use Illuminate\Http\Request;

class MailController extends Controller
{
    public function sendQuotation(Request $request, Int $id){
        $url = route('ventas.pdf.cotizacion', ['venta' => $id]);
        Mail::to($request->to)->send(new QuotationCreated($request->subject, $request->description, $url));
        return redirect()->back()->with('success', 'Operación exitosa');
    }

    public function sendCustomerMail(Request $request){
        Mail::to('contacto@gipssa.com')->send(new ContactForm('Este cliente quiere que lo contacten', $request));
        return redirect()->route('mailResponse');
        ;
    }
}
