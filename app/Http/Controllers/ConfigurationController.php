<?php

namespace App\Http\Controllers;

use App\Configuration;
use Exception;
use Illuminate\Http\Request;

class ConfigurationController extends Controller
{
    public function maintenance(Request $request){
        try{
          $data = $request->all();

          $configuration = Configuration::find(1);

          if($data['active'] == 'off'){
            $configuration->update(['active'=>'1']);
          }else{
            $configuration->update(['active'=>'0']);
          }
          
          return response(['success' => true, 'message' => 'Operacion Exitosa', 'data' => $configuration], 200);
        }catch(Exception $ex){
            return response(['invalid' => $ex->getMessage()]);
        }
      }
}
