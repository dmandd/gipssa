<?php

namespace App\Http\Controllers;

use App\Tag;
use Illuminate\Http\Request;

class TagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $info = [
            'title' => 'Etiquetas',
            'breadcrumb' => [
                [
                    'title' => 'Todos',
                    'route' => 'panel.tags.index',
                    'active' => true
                ]
            ],
            'buttons' => [
                [
                    'title' => 'Agregar Nuevo',
                    'route' => 'panel.tags.create'
                ]
            ]
        ];
        $info['data'] = Tag::all()->sortByDesc('id');
        return view('panel.tags.index', $info);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $info = [
            'title' => 'Etiquetas',
            'breadcrumb' => [
                [
                    'title' => 'Todos',
                    'route' => 'panel.tags.index',
                ],
                [
                    'title' => 'Nuevo',
                    'route' => 'panel.tags.create',
                    'active' => true
                ]
            ]
        ];
        return view('panel.tags.create', $info);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $tag = Tag::create($request->toArray());
        return redirect()->route('panel.tags.edit', ['etiqueta' => $tag->id])->with('success', 'Operación exitosa');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function show(Tag $tag)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function edit(Int $id)
    {
        $info = [
            'title' => 'Etiquetas',
            'breadcrumb' => [
                [
                    'title' => 'Todos',
                    'route' => 'panel.tags.index',
                ],
                [
                    'title' => 'Editar',
                    'route' => 'panel.tags.edit',
                    'params' => ['etiqueta' => $id],
                    'active' => true
                ]
            ]
        ];
        if($info['tag'] = Tag::find($id)){
            return view('panel.tags.edit', $info);
        }else{
            return redirect()->route('panel.tags.index');
        }
        

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Int $id)
    {
        $tag = Tag::find($id)->update($request->toArray());
        if(isset($arr['api'])){
            return response(['success' => true], 200);
        }else{
            return redirect()->route('panel.tags.edit', ['etiqueta' => $id])
                    ->with('success', 'Elemento actualizado');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function destroy(Int $id)
    {
        if(Tag::find($id)){
            Tag::destroy($id);
            return response(['success' => true], 200);
        }else{
            return response(['success' => false], 200);
        }
    }
}
