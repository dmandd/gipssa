<?php

namespace App\Http\Controllers;

use Exception;
use Carbon\Carbon;
use App\ComissionPayment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ComissionPaymentController extends Controller
{
  public function update(Request $request){
    try{
        $arr = $request->toArray();
        $data = $arr["detalle"];
       
        $comissionPayment = ComissionPayment::find($data["id"]);
        if($comissionPayment){
          $comissionPayment->payment_date = new Carbon(formatDate($data["payment_date"]));
          $comissionPayment->payment_amount = getAmmount($data["payment_amount"]);
          $comissionPayment->update();
        }

        return response(['success' => true], 200);
    }catch (Exception $ex) {
        return response(['invalid' => $ex->getMessage()], 403);
    }
  }

  public function storeFile(Request $request)
    {
      try{
        DB::beginTransaction();
        $comissionPayment = ComissionPayment::find($request->comission_payment_id);
        if($comissionPayment){
          if($request->file('file')){
            $file = $request->file('file');
            if($file->getSize() > 2000000){
              return redirect()->back()->withInputs($request->input())->withErrors(['invalid' => 'El tamaño máximo de los archivos es de 1.9MB']);
            }else{
              $path = 'public/comisiones'; // Ruta de la carpeta
              if (!is_dir($path)) {
                  // La carpeta no existe, intenta crearla
                  if (!mkdir($path, 0777, true)) {
                      die('No se pudo crear la carpeta');
                  }
              }
              $path_file = $file->store($path);
              $_exploded = explode('/', $path_file);
              $_exploded[0] = 'storage';
              $path_file = implode('/', $_exploded);
              $comissionPayment->update([
                  'file' => $path_file,
              ]);
            }
          }
        }

          DB::commit();
          return redirect()->back()->with('success', 'Operación exitosa');
        }catch(Exception $ex){
          DB::rollBack();
          return redirect()->back()->withErrors(['invalid' => $ex->getMessage()]);
        }
    }

  public function pay(Request $request)
    {
        try{
          $arr = $request->toArray();
          $payment_id = json_decode($arr['payment_id']);
                
          $comissionPayment = ComissionPayment::find($payment_id);
          if($comissionPayment){
              $comissionPayment->paid = 1;
              $comissionPayment->update();

              $comission = $comissionPayment->comission();
              $comission->paid_amount = $comission->paid_amount + $comissionPayment->payment_amount;
              $comission->update();
          }
          
          return response(['success' => true], 200);
        }catch (Exception $ex) {
            return response(['invalid' => $ex->getMessage()], 403);
        }
    }

    public function unpay(Request $request)
    {
        try{
          DB::beginTransaction();
          $arr = $request->toArray();
          $payment_id = json_decode($arr['payment_id']);
                
          $comissionPayment = ComissionPayment::find($payment_id);
          if($comissionPayment){
              $comissionPayment->paid = 0;
              $comissionPayment->update();

              $comission = $comissionPayment->comission();
              $comission->paid_amount = $comission->paid_amount - $comissionPayment->payment_amount;
              $comission->update();
          }
          DB::commit();
          return response(['success' => true], 200);
        }catch (Exception $ex) {
          DB::rollBack();
          return response(['invalid' => $ex->getMessage()], 403);
        }
    }
}
