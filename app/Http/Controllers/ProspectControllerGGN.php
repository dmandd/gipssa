<?php

namespace App\Http\Controllers;

use Exception;
use App\Task;
use App\Admin;
use App\Client;
use App\Prospect;
use App\TaskProspect;
use Illuminate\Http\Request;
use App\Imports\ProspectsImport;
use App\Providers\PermissionKey;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\ProspectsExport;

class ProspectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $info = [
            'title' => 'Leads',
            'breadcrumb' => [
                [
                    'title' => 'Todos',
                    'route' => 'panel.prospects.index',
                    'active' => true
                ]
            ],
        ];
        if($request->user()->can(PermissionKey::Prospect['permissions']['create']['name'])){
            $info['buttons'] = [
                [
                    'title' => 'Agregar Nuevo',
                    'route' => 'panel.prospects.create'
                ],
            ];
        }
        if(isset($request->usuario)){
            if($request->user()->can(PermissionKey::Prospect['permissions']['index']['name'])){
                $info['user'] = $request->usuario;
            }else{
                $info['user'] = $request->user()->id;
            }

            if(isset($request->vista)){
                return view('panel.prospects.indexTable', $info);
            }else{
                return view('panel.prospects.index', $info);
            }
              
        }else{
            if($request->user()->can(PermissionKey::Prospect['permissions']['index']['name'])){
                $info['users'] = Admin::all(); 
            }else{
                $info['users'] = Admin::where('id', $request->user()->id)->get();
            }
            return view('panel.prospects.usuarios', $info);   
        }
       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $info = [
            'title' => 'Leads',
            'breadcrumb' => [
                [
                    'title' => 'Todos',
                    'route' => 'panel.prospects.index',
                ],
                [
                    'title' => 'Nuevo',
                    'active' => true
                ]
            ],
        ];
        $info['admins'] = Admin::all();
        return view('panel.prospects.create', $info);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $inputs = $request->input();

        if(Prospect::where('email', $inputs['email'])->get()->count() > 0 || Client::where('email', $inputs['email'])->get()->count() > 0){
            return redirect()->back()->withInput($request->input())->withErrors(['invalid' => 'Lo sentimos el correo '.$inputs['email'].' ya está en uso intente con otro.']);
        } 

        if(!isset($inputs['admin_id'])){
            $inputs['admin_id'] = $request->user()->id;
        }
        
        $prospect = Prospect::create($inputs);

        return redirect()->route('panel.prospects.index', ['usuario' => $request->user()->id])->with('success', 'Operación exitosa');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Prospect  $prospect
     * @return \Illuminate\Http\Response
     */
    public function show(Prospect $prospect)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Prospect  $prospect
     * @return \Illuminate\Http\Response
     */
    public function edit(Int $prospect)
    {
        $info = [
            'title' => 'Leads',
            'breadcrumb' => [
                [
                    'title' => 'Todos',
                    'route' => 'panel.prospects.index',
                ],
                [
                    'title' => 'Editar',
                    'route' => 'panel.prospects.edit',
                    'params' => ['prospecto' => $prospect],
                    'active' => true
                ]
            ],
        ];
        $info['data'] = Prospect::find($prospect);
        $info['admins'] = Admin::all();
        Task::set_prospect_id($prospect);
        $info['tasks'] = Task::doesntHave('task_prospects')->where('default', 1)->orderBy('order','asc')->get()->toArray();
        $info['tasks_on_demand'] = Task::select('tasks.*', 'task_prospects.complete')->join('task_prospects', 'tasks.id', '=', 'task_prospects.task_id')->where('task_prospects.prospect_id', $prospect)->orderBy('order','asc')->get()->toArray();
        $info['tasks'] = array_merge($info['tasks'], $info['tasks_on_demand']);
        return view('panel.prospects.edit', $info);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Prospect  $prospect
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Int $id)
    {
        if(Prospect::where('id', '!=', $id)->where('email', $request->email)->get()->count() > 0 || Client::where('email', $request->email)->get()->count() > 0){
            return redirect()->back()->withInput($request->input())->withErrors(['invalid' => 'Lo sentimos el correo '.$request->email.' ya está en uso intente con otro.']);
        }   

        $prospect = Prospect::find($id);
        
        $prospect->update($request->toArray());
        
        if($request->avance == 'rojo'){
            $prospect->status = 4;
            $prospect->update();
        }elseif(($request->avance == 'verde' || $request->avance == 'amarillo') && $prospect->status != 2){
            $prospect->status = 2;
            $prospect->update();
        }
        // cambio para que no se redirija a otro lado
        return redirect()->route('panel.prospects.index', ['usuario' => $request->user()->id])->with('success', 'Operación exitosa');
       // return redirect()->route('panel.prospects.index', ['usuario' => $prospect->admin_id])->with('success', 'Operación exitosa');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Prospect  $prospect
     * @return \Illuminate\Http\Response
     */
    public function destroy(Int $prospect)
    {
        if(Prospect::find($prospect)){
            //Eliminamos las actividades
            $_prospect = Prospect::find($prospect);
            $_prospect->removeTasks();
            $_prospect->status = 4;
            $_prospect->avance = 'rojo';
            $_prospect->update();
            return response(['success' => true], 200);
        }else{
            return response(['success' => false], 200);
        }
    }

    /**
     * Recover the specified resource from storage.
     *
     * @param \App\Prospect  $prospect
     * @return \Illuminate\Http\Response
     */
    public function recover(Request $request)
    {
        if(Prospect::find($request->prospecto)){
            //Eliminamos las actividades
            $_prospect = Prospect::find($request->prospecto);
            $_prospect->status = 1;
            $_prospect->avance = 'verde';
            $_prospect->update();
            return response(['success' => true], 200);
        }else{
            return response(['success' => false], 200);
        }
    }

    /**
     * Recover the specified resource from storage.
     *
     * @param \App\Prospect  $prospect
     * @return \Illuminate\Http\Response
     */
    public function reserve(Request $request)
    {
        try{
            if(Prospect::find($request->prospect_id)){
                $prospect = Prospect::find($request->prospect_id);
                
                if(Client::where('email', $prospect->email)->get()->count() > 0)
                    return redirect()->back()->withInput($request->input())->withErrors(['invalid' => 'Lo sentimos el correo '.$prospect->email.' ya está en uso intente con otro.']);
            
                
                $admin_id = 0;
                
                if(Admin::find($prospect->admin_id)){
                    $admin_id = $prospect->admin_id;
                }else{
                    $admin_id = $request->user()->id;
                }
                
                DB::beginTransaction();
                $client = new Client();
                $client->admin_id = $admin_id;
                $client->name = $prospect->name;
                $client->surname = "";
                $client->phone = $prospect->phone;
                $client->email = $prospect->email;
                $client->nacionalidad = 'Mexicana';
                $client->extranjero = 0;
                $client->save();
                
                if($client){
                    $prospect->removeTasks();
                    $prospect->removeNotes();
                    $prospect->delete();
                    
                    DB::commit();
                    return redirect()->route('panel.ventas.create',['desarrollo'=>$request->desarrollo,'client_id'=>$client->id]);
                }
                throw new Exception("El cliente no se creo.");
            }else
                throw new Exception("Lo sentimos el prospecto no existe o ya es un cliente.");
        }catch(Exception $ex){
            DB::rollBack(); 
            return redirect()->back()->withInput($request->input())->withErrors(['invalid' => $ex->getMessage()]);
        }
    }

    /**
     * Recover the specified resource from storage.
     *
     * @param \App\Prospect  $prospect
     * @return \Illuminate\Http\Response
     */
    public function reserveClient(Request $request)
    {
        return redirect()->route('panel.ventas.create',['desarrollo'=>$request->desarrollo,'client_id'=>$request->client_id]);
          
    }

    /**
     * Recover the specified resource from storage.
     *
     * @param \App\Prospect  $prospect
     * @return \Illuminate\Http\Response
     */
    public function client(Request $request)
    {
        try{
            if(Prospect::find($request->prospecto)){
                $prospect = Prospect::find($request->prospecto);

                if(Client::where('email', $prospect->email)->get()->count() > 0){
                    return response(['invalid' => 'Lo sentimos el correo '.$prospect->email.' ya está en uso intente con otro.'], 403);
                } 

                $admin_id = 0;

                if(Admin::find($prospect->admin_id)){
                    $admin_id = $prospect->admin_id;
                }else{
                    $admin_id = $request->user()->id;
                }

                DB::beginTransaction();
                $client = new Client();
                $client->admin_id = $admin_id;
                $client->name = $prospect->name;
                $client->surname = "";
                $client->phone = $prospect->phone;
                $client->email = $prospect->email;
                $client->nacionalidad = 'Mexicana';
                $client->extranjero = 0;
                $client->save();
            
                if($client){
                    $prospect->removeTasks();
                    $prospect->removeNotes();
                    $prospect->delete();

                    DB::commit();
                    return response(['success' => true], 200);
                }
                throw new Exception("El cliente no se creo.");
            }else
                throw new Exception("Lo sentimos el prospecto no existe o ya es un cliente.");
        }catch (Exception $ex) {
            DB::rollBack();
            return response(['invalid' => $ex->getMessage()], 403);
        }
    }

    public function reassign(Request $request){
        try{
            $data = $request->all();
            $ids = json_decode($data['selected_ids']);
            foreach($ids as $id){
                $lead = Prospect::find($id);
                $lead->admin_id = $data["admin_id"];
                $lead->update();
            }

            return redirect()->back()->with('success', 'Operación exitosa');
        }catch(Exception $ex){
            return redirect()->back()->withInput($request->input())->withErrors(['invalid' => $ex->getMessage()]);
        }
    }

    public function import() 
    {
        Excel::import(new ProspectsImport, request()->file('import'));
        
        return redirect()->back()->with('success', 'Operación exitosa');
    }

    public function export(Request $request){
        try{
            $data = $request->all();
            return Excel::download(new ProspectsExport(intval($data['user'])), 'Leads.xlsx');
        }catch(Exception $ex){
            return redirect()->back()->withInput($request->input())->withErrors(['invalid' => $ex->getMessage()]);
        }
    }
}
