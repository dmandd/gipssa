<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $fillable = [
        'name',
        'type',
        'phone',
        'email',
        'address',
        'admin_id',
    ];

    public function type(){
        switch ($this->type) {
            case 'propietario':
                return 'Propietario';
            break;
            case 'abogado':
                return 'Abogado';
            break;
            case 'representante':
                return 'Representante';
            break;
            case 'otro':
                return 'Otro';
            break;
            
            default:
                return 'Otro';
            break;
        }
    }

    public function assigned(){
        return $this->belongsTo('App\Admin', 'admin_id')->first();
    }

    public function model(){
        return Contact::class;
    }
}
