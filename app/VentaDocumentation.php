<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VentaDocumentation extends Model
{
    protected $fillable = [
        'venta_id',
        'development_client_documentation_id',
        'file',
        'status',
        'message',
        'mostrar_condicion'
    ];

    public function document(){
        return $this->belongsTo('App\DevelopmentClientDocumentation')->first();
    }
    public function development_client_documentation(){
        return $this->belongsTo('App\DevelopmentClientDocumentation');
    }

    public function status(){
        switch ($this->status) {
            case 'created':
                return 'Esperando autorización';
            break;
            case 'authorized':  
                return 'Autorizado';
            break;
            case 'rejected':
                return 'Rechazado, favor de cambiar';
            break;
            default:
                return 'Sin registro';
            break;
        }
    }
}
