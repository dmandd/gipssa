<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VentaPagoEnganche extends Model
{
    protected $fillable = [
        'venta_id',
        'fecha',
        'monto'
    ];

    public function fecha(){
        return ($this->fecha) ? date('d/m/Y', strtotime($this->fecha)) : '';
    }
}
