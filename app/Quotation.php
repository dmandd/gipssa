<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quotation extends Model
{
    protected $fillable = [
        'admin_id',
        'lote_id',
        'detail'
    ];

    function admin(){
        return $this->belongsTo('App\Admin')->first();
    }

    function lote(){
        return $this->belongsTo('App\Lote')->first();
    }

    function vienta(){
        return $this->belongsTo('App\Venta')->first();
    }
}
