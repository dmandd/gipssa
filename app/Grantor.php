<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Grantor extends Model
{
    protected $fillable = [
        'development_id',
        'title',
        'name',
        'last_name',
        'birth_date',
        'celular',
        'email',
        'public_notary_description'
    ];

    public function desarrollo(){
        return $this->belongsTo('App\Development', 'development_id', 'id')->first();
    }
}
