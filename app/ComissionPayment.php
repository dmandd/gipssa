<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ComissionPayment extends Model
{
    protected $fillable = [
        'comission_id',
        'payment_date',
        'payment_amount',
        'paid',
        'file',
    ];

    public $timestamps = false;

    public function comission()
    {
        return $this->belongsTo(Comission::class, 'comission_id')->first();
    }
}
