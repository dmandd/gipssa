<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CadastralPlanking extends Model
{
    protected $fillable = [
        'name',
        'number',
        'name',
        'size',
        'location',
        'reference',
        'adjoining',
    ];

    public function developments(){
        return $this->hasMany('App\Development')->get();
    }
}
