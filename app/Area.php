<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\AreaEquipment;

class Area extends Model
{
    protected $fillable = [
        'name',
        'property_id',
        'status'
    ];

    public function syncEquipment(Array $arr){
        if((isset($arr)) && (count($arr) > 0)){
            foreach ($arr as $i => $e) {
                $arr[$i] = [
                    'area_id' => $this->id,
                    'equipment_id' => $e
                ];
            }
            //Eliminamos los registros
            DB::table('area_equipment')->where('area_id', $this->id)->delete();
            //Agregamos los nuevos
            DB::table('area_equipment')->insert($arr);
        }
    }

    public function hasEquipment(Int $id){
        return AreaEquipment::where('area_id', $this->id)->where('equipment_id', $id)->first();
    }

    public function equipment(){
        return $this->belongsToMany('App\Equipment', 'area_equipment')->where('area_id', $this->id)->get();
    }
}
