<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DevelopmentClientDocumentation extends Model
{
    protected $fillable = [
        'development_id',
        'name',
        'slug',
        'status',
        'mostrar_condicion',
        'show_contract',
        'fijo'
    ];
}
