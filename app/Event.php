<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $fillable = [
        'admin_id',
        'contact_id',
        'contact_type',
        'room_id',
        'title',
        'description',
        'color',
        'start_date',
        'end_date'
    ];
}
