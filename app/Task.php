<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $fillable = [
        'user_id',
        'description',
        'order',
        'required',
        'default',
        'status'
    ];

    public static $prospect_id;

    public function type(){

    }

    public function required(){
        switch ($this->required) {
            case 1:
                return 'Sí';
            break;
            case 0:
                return 'No';
            break;
            
        }
    }
    public function status(){
        switch ($this->status) {
            case "visible":
                return 'Visible';
            break;
            case "hidden":
                return 'Oculto';
            break;
            
        }
    }

    public function task_prospects(){
        return $this->hasMany('App\TaskProspect')->where('prospect_id', self::$prospect_id);
    }


    public function task_prospects_assigned(){
        return $this->hasMany('App\TaskProspect')->where('prospect_id', self::$prospect_id);
    }

    public static function set_prospect_id($prospect_id){
        self::$prospect_id = $prospect_id;
    }
}
