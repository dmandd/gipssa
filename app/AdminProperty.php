<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdminProperty extends Model
{
    protected $fillable = [
        'admin_id',
        'total'
    ];
}
