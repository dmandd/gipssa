<?php

namespace App\Exports;

use App\Admin;
use App\Comission;
use Carbon\Carbon;
use App\ComissionPayment;
use App\Providers\PermissionKey;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;

class ComissionsExport implements FromCollection, WithHeadings
{
    var $origin_params,$request;
    function __construct($origin_params,$request){
        $this->origin_params = $origin_params;
        $this->request = $request;
    }
    public function headings(): array
    {
        return [
            'Nombre',
            'No. de lote',
            'Desarrollo',
            'Etapa',
            'F. Apartado',
            'F. Primera mensualidad',
            'Comision neta',
            'F. Pago No.1',
            'F. Pago No.2',
            'F. Pago No.3',
            'F. Pago No.4',
            'F. Pago No.5',
            'F. Pago No.6',
            'F. Pago No.7',
            'F. Pago No.8',
            'F. Pago No.9',
            'F. Pago No.10',
            'Nota',
        ];
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $origin_params = $this->origin_params;
        $request = $this->request;
        $datos = Comission::where('comissions.status',1)->join('ventas', 'comissions.venta_id', '=', 'ventas.id')
        ->join('quotations', 'ventas.quotation_id', '=', 'quotations.id')
        ->join('admins', 'quotations.admin_id', '=', 'admins.id')
        ->join('developments', 'ventas.development_id', '=', 'developments.id')
        ->join('lotes', 'ventas.lote_id', '=', 'lotes.id')
        ->select('comissions.id','comissions.notes','comissions.admin_id',
        DB::raw('CONCAT(admins.name, " ", admins.last_name) as name_admin'),
        'lotes.name as lote','developments.name as desarrollo', 'developments.stage', 'ventas.fecha_bloqueo','ventas.dia_pago', 'comissions.pay_amount')
        ->orderBy('ventas.id', 'desc');

        if ($origin_params) {
          if (isset($origin_params['admin_id']) && $origin_params['admin_id'] != "") {
              $datos->where('quotations.admin_id', $origin_params['admin_id']);
          }
          if (isset($origin_params['desarrollo']) && $origin_params['desarrollo'] != "") {
            $datos->where('ventas.development_id', $origin_params['desarrollo']);
          }
        }
  
        if(request()->user()->can(PermissionKey::Comission['permissions']['show_sidebar']['name']) && request()->user()->can(PermissionKey::ComissionSupervision['permissions']['show_sidebar']['name'])){
          if ($origin_params) {
            if (isset($origin_params['type']) && $origin_params['type'] != "") {
              $datos->where('comissions.type', $origin_params['type']);
            }
          }
  
          if (!$request->user()->can(PermissionKey::ComissionSupervision['permissions']['index']['name']) && !$request->user()->can(PermissionKey::Comission['permissions']['index']['name'])) {
            $datos->where(function ($query) use ($request,$origin_params) {
              $query->where('comissions.admin_id', $request->user()->id)
                    ->orWhere('quotations.admin_id', $request->user()->id);
                    if ($origin_params) {
                      if (isset($origin_params['admin_id']) && $origin_params['admin_id'] != "") {
                          $query->where('quotations.admin_id', $origin_params['admin_id']);
                      }
                      if (isset($origin_params['desarrollo']) && $origin_params['desarrollo'] != "") {
                        $query->where('ventas.development_id', $origin_params['desarrollo']);
                      }
                    }
            }); 
          }else{
            if(!$request->user()->can(PermissionKey::Comission['permissions']['index']['name'])){
              $datos->where('comissions.type', Comission::TYPE['SUPERVISOR']);
              $datos->orWhere(function ($query) use ($request,$origin_params) {
                $query->where('quotations.admin_id', $request->user()->id);
                if ($origin_params) {
                  if (isset($origin_params['admin_id']) && $origin_params['admin_id'] != "") {
                      $query->where('quotations.admin_id', $origin_params['admin_id']);
                  }
                  if (isset($origin_params['desarrollo']) && $origin_params['desarrollo'] != "") {
                    $query->where('ventas.development_id', $origin_params['desarrollo']);
                  }
                }
              }); 
            }else if(!$request->user()->can(PermissionKey::ComissionSupervision['permissions']['index']['name'])){
              $datos->where('comissions.type', Comission::TYPE['ASESOR']);
              $datos->orWhere(function ($query) use ($request,$origin_params) {
                $query->where('comissions.admin_id', $request->user()->id);
                if ($origin_params) {
                  if (isset($origin_params['admin_id']) && $origin_params['admin_id'] != "") {
                      $query->where('quotations.admin_id', $origin_params['admin_id']);
                  }
                  if (isset($origin_params['desarrollo']) && $origin_params['desarrollo'] != "") {
                    $query->where('ventas.development_id', $origin_params['desarrollo']);
                  }
                }
              }); 
            }
          }
        }else if(request()->user()->can(PermissionKey::Comission['permissions']['show_sidebar']['name']))
        {
          $datos->where('comissions.type', Comission::TYPE['ASESOR']);
          if (!$request->user()->can(PermissionKey::Comission['permissions']['index']['name'])) {
            $datos->where('quotations.admin_id', $request->user()->id);
          }
        }else
        {
          $datos->where('comissions.type', Comission::TYPE['SUPERVISOR']);
          if (!$request->user()->can(PermissionKey::ComissionSupervision['permissions']['index']['name'])) {
            $datos->where('comissions.admin_id', $request->user()->id);
          }
        }

        if($origin_params){
          if ((isset($origin_params['fecha']) && isset($origin_params['corte'])) && ($origin_params['fecha'] != "" && $origin_params['corte'] != "")) {
            $fecha = "01/".$origin_params['fecha'];
  
            $fecha = Carbon::createFromFormat('d/m/Y', $fecha);
            
            if($origin_params['corte'] == 1){
              $fecha->endOfMonth();
              $end_date = $fecha->format("Y-m-d");
              $fecha->day(15);
              $start_date = $fecha->format("Y-m-d");
              
            }else if($origin_params['corte'] == 2){
              $fecha->addMonth()->startOfMonth();
              $start_date = $fecha->format("Y-m-d");
              $fecha->day(15);
              $end_date = $fecha->format("Y-m-d");
            }
           
            $datos->whereHas('comissionPayments', function ($query) use ($start_date,$end_date) {
              $query->where('payment_date', '>=', $start_date)  // Día mayor o igual a 1
                    ->where('payment_date', '<=', $end_date);
            });
          }else if (isset($origin_params['fecha']) && $origin_params['fecha'] != "") {
            $fecha = "01/".$origin_params['fecha'];
  
            $fecha = Carbon::createFromFormat('d/m/Y', $fecha);
            
            $fecha->endOfMonth();
            $end_date = $fecha->format("Y-m-d");
            $fecha->day(1);
            $start_date = $fecha->format("Y-m-d");
           
            $datos->whereHas('comissionPayments', function ($query) use ($start_date,$end_date) {
              $query->where('payment_date', '>=', $start_date)  // Día mayor o igual a 1
                    ->where('payment_date', '<=', $end_date);
            });
          }
        }

        $datos = $datos->get();

        $datos = $datos->map(function ($item) {
            $item['fecha_bloqueo'] = date('d/m/Y', strtotime($item->fecha_bloqueo));
            $item['dia_pago'] = date('d/m/Y', strtotime($item->dia_pago));

            $item['pay_amount'] = '$'.number_format($item->pay_amount,2);
            $admin = Admin::find($item->admin_id);
            if($admin){
              $item['name_admin'] = $item['name_admin']." | Supervisor: ".$admin->name." ".$admin->last_name;
            }
            $payments = ComissionPayment::where('comission_id',$item->id)->get();
            foreach($payments as $payment){
              $date = "--/--/--";
              if($payment->paid){
                $date = date('d/m/Y', strtotime($payment->payment_date));
              }
              $item['payment_date'.$payment->payment_date] = $date;
            }
            for($i = 0;$i<10-($payments->count());$i++){
                $item['payment_date'.$i] = "No aplica";
            }
            unset($item['id']);
            $item['nota'] = $item->notes;
            unset($item['notes']);
            unset($item['admin_id']);
            return $item;
        });
            
        return $datos;
    }
    
}
