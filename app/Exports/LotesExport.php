<?php

namespace App\Exports;

use App\Lote;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class LotesExport implements FromCollection, WithHeadings
{
    var $development_id;
    function __construct($development_id){
        $this->development_id = $development_id;
    }
    public function headings(): array
    {
        return [
            'id',
            'numero lote',
            'chepina',
            'nombre',
            'precio',
            'metros',
            'estatus',
            'norte','lindancia_norte',
            'nororiente', 'lindancia_nororiente', 
            'oriente', 'lindancia_oriente', 
            'suroriente', 'lindancia_suroriente',
            'sur', 'lindancia_sur',
            'surponiente', 'lindancia_surponiente',
            'poniente', 'lindancia_poniente',
            'norponiente', 'lindancia_norponiente'
        ];
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Lote::select(
        'id',
        'num_lote', 
        'chepina', 
        'name', 
        'price', 
        'area', 
        'status',
        'norte','lindancia_norte',
        'nororiente', 'lindancia_nororiente', 
        'oriente', 'lindancia_oriente', 
        'suroriente', 'lindancia_suroriente',
        'sur', 'lindancia_sur',
        'surponiente', 'lindancia_surponiente',
        'poniente', 'lindancia_poniente',
        'norponiente', 'lindancia_norponiente')->where('development_id', $this->development_id)->get();
    }
}
