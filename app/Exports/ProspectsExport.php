<?php

namespace App\Exports;

use App\Admin;
use App\Prospect;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;

class ProspectsExport implements FromCollection, WithHeadings
{
    var $admin_id;
    function __construct($admin_id){
        $this->admin_id = $admin_id;
    }
    public function headings(): array
    {
        return [
            'ID',
            'Usuario',
            'Nombre',
            'Correo',
            'Telefono',
            'Prioridad',
            'Procedencia',
            'Notas',
            'Avance',
            'Desarrollo'
        ];
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $data = DB::table("prospects as p")->leftJoin('admins as a', 'p.admin_id', '=', 'a.id');

        $data->where('status','!=','4');

        if($this->admin_id != 0)    
            $data->where('p.admin_id', $this->admin_id);
        else
            $data->whereNotNull('p.email');
        
        $data = $data->orderBy('p.status')->get(["p.id",
                                                "a.name as nombre",
                                                "p.name",
                                                "p.email",
                                                "p.phone",
                                                'p.priority',
                                                'p.procedence',
                                                'p.notes',
                                                'p.avance',
                                                'p.desarrollo']);
        
        return $data;
    }
}
