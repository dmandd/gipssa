<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Note extends Model
{
    protected $fillable = [
        'prospect_id',
        'admin_id',
        'title',
        'description'
    ];

    public function user(){
        return $this->belongsTo('App\Admin', 'admin_id')->first();
    }
}
