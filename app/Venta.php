<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use App\VentaRecibo;

class Venta extends Model
{
    protected $fillable = [
        'development_id',
        'lote_id',
        'client_id',
        'quotation_id',
        'total',
        'status',
        'observaciones',
        'fecha_bloqueo',
        'monto_bloqueo',
        'monto_contrato',
        'dia_pago_enganche',
        'dia_pago',
        'contrato',
        'fecha_firma_contrato',
        'ubicacion_firma_contrato',
        'fecha_siguiente_pago',
        'fecha_pago_saldo',
    ];

    public function development(){
        return $this->belongsTo('App\Development')->first();
    }

    public function developmentRelation(){
        return $this->belongsTo('App\Development');
    }

    public function getContrato(){
        // $this->default_contrato = json_decode(Storage::get('public/contrato/machote.json'), true)['machote'];
        // if($this->contrato){
        //     $this->contrato = str_replace('{$venta->cliente()}', $this->client()->name.' '.$this->client()->surname, $this->contrato);
        //     $this->contrato = str_replace('{$venta-&gt;lote()-&gt;area}', $this->lote()->area, $this->contrato);
        //     $this->contrato = str_replace('{descriptionNumber($venta-&gt;lote()-&gt;area)}', descriptionNumber($this->lote()->area), $this->contrato);
        //     $this->contrato = str_replace('{$venta-&gt;development()-&gt;name}', $this->development()->name, $this->contrato);
        //     $this->contrato = str_replace('{date("d", strtotime($venta-&gt;client()-&gt;fecha_nacimiento))}', date('d', strtotime($this->client()->fecha_nacimiento)), $this->contrato);
        //     $this->contrato = str_replace('{descriptionNumber(date("d", strtotime($venta-&gt;client()-&gt;fecha_nacimiento)))}', descriptionNumber(date('d', strtotime($this->client()->fecha_nacimiento))), $this->contrato);
        //     return $this->contrato;
        // }else{
        //     $this->default_contrato = str_replace('{dia}', num2letras(date('j'), false, false, false), $this->default_contrato);
        //     $this->default_contrato = str_replace('{mes}', date('F'), $this->default_contrato);
        //     $this->default_contrato = str_replace('{anio_letra}', num2letras(date('Y'), false, false, false), $this->default_contrato);
        //     $this->default_contrato = str_replace('{nombre_cliente}', $this->client()->name.' '.$this->client()->surname, $this->default_contrato);
        //     $this->default_contrato = str_replace('{nombre_lote}', $this->lote()->name, $this->default_contrato);
        //     // $this->default_contrato = str_replace('{numero_lote}', $this->lote()->name, $this->default_contrato);
        //     // $this->default_contrato = str_replace('{numero_lote}', $this->lote()->name, $this->default_contrato);
        //     $this->default_contrato = str_replace('{m2_lote}', $this->lote()->area, $this->default_contrato);
        //     $this->default_contrato = str_replace('{descripcion_m2_letra}', descriptionNumber($this->lote()->area), $this->default_contrato);
        //     $this->default_contrato = str_replace('{nombre_desarrollo_mayuscula}', strtoupper($this->development()->name), $this->default_contrato);
        //     $this->default_contrato = str_replace('{ubicacion_desarrollo_mayuscula}', strtoupper($this->development()->location), $this->default_contrato);
        //     $this->default_contrato = str_replace('{dia_nacimiento_cliente}', date('d', strtotime($this->client()->fecha_nacimiento)), $this->default_contrato);
        //     $this->default_contrato = str_replace('{descripcion_dia_nacimiento_cliente}', descriptionNumber(date('d', strtotime($this->client()->fecha_nacimiento))), $this->default_contrato);
        //     $this->default_contrato = str_replace('{mes_nacimiento_cliente}', date('F', strtotime($this->client()->fecha_nacimiento)), $this->default_contrato);
        //     $this->default_contrato = str_replace('{anio_nacimiento_cliente}', date('Y', strtotime($this->client()->fecha_nacimiento)), $this->default_contrato);
        //     $this->default_contrato = str_replace('{descripcion_anio_nacimiento_cliente}', descriptionNumber(date('Y', strtotime($this->client()->fecha_nacimiento))), $this->default_contrato);
        //     $this->default_contrato = str_replace('{ine_cliente}', $this->client()->identificacion, $this->default_contrato);
        //     $this->default_contrato = str_replace('{descripcion_ine_cliente}', descriptionNumber($this->client()->identificacion), $this->default_contrato);
        //     $this->default_contrato = str_replace('{monto_venta}', number_format(getAmmount($this->quotation()->detail->precio_final), 2), $this->default_contrato);
        //     $this->default_contrato = str_replace('{descripcion_monto_venta}', strtoupper(num2letras(getAmmount($this->quotation()->detail->precio_final), false, false)), $this->default_contrato);
        //     $this->default_contrato = str_replace('{monto_enganche}', number_format(getAmmount($this->quotation()->detail->monto_enganche), 2), $this->default_contrato);
        //     $this->default_contrato = str_replace('{descripcion_monto_enganche}', strtoupper(num2letras(getAmmount($this->quotation()->detail->monto_enganche))), $this->default_contrato);
        //     $this->default_contrato = str_replace('{plazo_financiamiento}', $this->quotation()->detail->mensualidad, $this->default_contrato);
        //     $this->default_contrato = str_replace('{plazo_financiamiento_letra}', num2letras($this->quotation()->detail->mensualidad, false, false, false), $this->default_contrato);
        //     return $this->default_contrato;
        // }
    }

    public function asesor(){
        return $this->belongsTo('App\Quotation', 'quotation_id')->first()->admin();
    }

    public function documentation($doc_id = null, Array $params = []){
        if($doc_id){
            return $this->hasOne('App\VentaDocumentation')->where('development_client_documentation_id', $doc_id)->where('venta_id', $this->id)->first();
        }else{
            $coll = $this->hasOne('App\VentaDocumentation')->with(['development_client_documentation']);
            if(count($params) > 0){
                foreach($params as $key => $value){
                    $coll = $coll->where($key, $value);
                }
            }
            return $coll->get();
        }
    }

    public function recibos($slug = '', $with_get = true){
        if($with_get){
            if($slug != ''){
                return $this->hasMany('App\VentaRecibo')->where('slug', $slug)->get();
            }else{
                return $this->hasMany('App\VentaRecibo')->get();
            }
        }else{
            return $this->hasMany('App\VentaRecibo')->where('slug', $slug);
        }
    }

    public function recibosComisiones(){
         return $this->hasMany('App\VentaRecibo')->whereIn('slug',["abono-enganche","mensualidad","saldo"])->where('status','paid')->get(); 
    }

    public function client(){
        return $this->belongsTo('App\Client')->first();
    }

    public function clientRelation(){
        return $this->belongsTo('App\Client');
    }

    public function quotation($complete = true){
        $quotation = $this->belongsTo('App\Quotation')->first();
        $quotation->detail = json_decode($quotation->detail);
        return $quotation;
    }

    public function withQuotation(){
        return $this->belongsTo('App\Quotation');
    }

    public function lote(){
        return $this->belongsTo('App\Lote')->first();
    }

    public function loteRelation(){
        return $this->belongsTo('App\Lote');
    }

    public function fechaBloqueo(){
        return ($this->fecha_bloqueo) ? date('d/m/Y', strtotime($this->fecha_bloqueo)) : '';
    }

    public function diaPago(){
        return ($this->dia_pago) ? date('d/m/Y', strtotime($this->dia_pago)) : '';
    }

    public function fechaPagoSaldo(){
        return ($this->fecha_pago_saldo) ? date('d/m/Y', strtotime($this->fecha_pago_saldo)) : '';
    }

    public function fechaFirmaContrato(){
        return ($this->fecha_firma_contrato) ? date('d/m/Y', strtotime($this->fecha_firma_contrato)) : '';
    }

    public function reciboBloqueo(){
        return $this->hasOne('App\VentaRecibo')->where('slug', 'comprobante-bloqueo')->first();
    }

    public function comission(){
        return $this->hasMany('App\Comission')->where("type",Comission::TYPE['ASESOR'])->first();
    }

    public function comissionSupervision(){
        return $this->hasMany('App\Comission')->where("type",Comission::TYPE['SUPERVISOR'])->first();
    }

    public function corrida($fecha = '',$cotizacion=false){
        $info = [];
        if($fecha !== ''){
            $info['fecha'] = $fecha;
        }else{
            $info['fecha'] = $this->dia_pago;
        }
        
        $info['dia'] = date('d', strtotime($info['fecha']));
        if($cotizacion){
            if(date('d', strtotime($info['fecha'])) >= 15){
                $_tmp = date('Y-m-15'); 
                $begin_date = date('Y-m-01', strtotime($_tmp)); 
                $info['fecha'] = date('Y-m-15', strtotime($begin_date)); 
                $info['dia'] = "15";
            }else{
                $_tmp = date('Y-m-15'); 
                $begin_date = date('Y-m-01', strtotime($_tmp)); 
                $info['fecha'] = date('Y-m-02', strtotime($begin_date)); 
                $info['dia'] = "2";
            }
        }
        
        if($this->quotation()->detail->financiamiento){
            $info['saldo'] = $this->quotation()->detail->financiamiento;
        }else{
            $info['saldo'] = 0;
        }
        if(VentaRecibo::where('venta_id', $this->id)->where('slug', 'mensualidad')->get()->count() > 0){
            //TODO: Restamos los abonos al saldo
        }
        $info['num_pagos'] = $this->quotation()->detail->mensualidad;
        return (Object) $info;
    // }else{
    //     return (Object) $info;
    // }
    }

    public function fechas_enganche(){
        return $this->hasMany('App\VentaPagoEnganche');
    }
}
