<?php

namespace App;

use App\Client;
use App\Contact;
use App\Providers\PermissionKey;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Admin extends Authenticatable
{
    use Notifiable;
    use HasRoles;
    protected $fillable = [
        'admin_id',
        'cadastral_planking_id',
        'avatar',
        'name',
        'last_name',
        'code',
        'email',
        'password',
        'birth_date',
        'comision',
        'celular',
        'direccion',
        'fecha_nacimiento',
        'lugar_nacimiento',
        'cp',
        'identification_type',
        'identificacion',
        'licencia_manejo',
        'poliza_seguro',
        'documento_permiso',
        'curp',
        'attorney',
        'rfc',
        'csd_cer',
        'csd_key',
        'csd_password',
        'numero_seguridad',
        'estado_salud',
        'enfermedad_cronica',
        'estatura',
        'peso',
        'tipo_sangre',
        'alergia',
        'enfermedades',
        'contacto',
        'ine1',
        'ine2',
        'hide'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function developments(){
        return $this->hasMany('App\Development')->get();
    }

    public function client($query = false){
        if($this->can(PermissionKey::Client['permissions']['all']['name'])){
            return Client::all();
        }else{
            if(!$query){
                return $this->hasMany('App\Client')->get();
            }else{
                return $this->hasMany('App\Client');
            }
        }
    }

    public function contacts($query = false){
        if($this->can(PermissionKey::Contact['permissions']['all']['name'])){
            if(!$query){
                return Contact::all();
            }
        }else{
            if(!$query){
                return $this->hasMany('App\Contact')->get();
            }else{
                return $this->hasMany('App\Contact');
            }
        }
    }

    public function prospects($get = true){
        if($get){
            return $this->hasMany('App\Prospect')->where('status','!=', '4')->get();
        }else{
            return $this->hasMany('App\Prospect');
        }
    }

    public function prospectos(): HasMany {
        return $this->hasMany(Prospect::class);
    }

    public function quotations(): HasMany {
        return $this->hasMany(Quotation::class);
    }

    public function currentRole(){
        if(isset($this->getRoleNames()->toArray()[0])){
            return $this->getRoleNames()->toArray()[0];
        }else{
            return 'Sin asignar';
        }
    }

    public function rendimiento(){
        $prospects_month = $this->prospects(false)->whereMonth('created_at', '>', date('m', strtotime("-1 month")))->where('avance', 'verde')->get()->count();
        $prospects = $this->prospects()->count();
        if($prospects > 0){
            return number_format(( $prospects_month / $prospects ) * 100, 2);
        }else{
            return number_format(0, 2);
        }
    }

    public function adminProperty(){
        return $this->hasOne('App\AdminProperty')->first();
    }

    public function Propertys(){
        return $this->hasMany('App\AdminProperty')->get();
    }
    
    public function comission(){
        return $this->hasMany('App\Comission');
    }
}
