<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VentaRecibo extends Model
{
    protected $fillable = [
        'venta_id',
        'file',
        'message',
        'title',
        'slug',
        'fecha_pago',
        'monto',
        'banco_id',
        'status',
        'factura_id',
        'serie',
        'folio',
        'payment_way',
        'payment_method',
    ];

    public function monto(){
        return number_format($this->monto, 2);
    }

    public function banco(){
        return $this->belongsTo('App\Banco')->first();
    }
}
