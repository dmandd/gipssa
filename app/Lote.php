<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lote extends Model
{
    protected $fillable = [
        'development_id',
        'chepina',
        'name',
        'price',
        'area',
        'medida',
        'num_lote',
        'status',
        'norte',
        'nororiente',
        'oriente',
        'suroriente',
        'sur',
        'surponiente',
        'poniente',
        'norponiente',
        'lindancia_norte',
        'lindancia_nororiente',
        'lindancia_oriente',
        'lindancia_suroriente',
        'lindancia_sur',
        'lindancia_surponiente',
        'lindancia_poniente',
        'lindancia_norponiente'
    ];

    public function development(){
        return $this->belongsTo('App\Development')->first();
    }

    public function ventas(){
        return $this->hasMany('App\Venta');
    }

}
