<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $fillable = [
        'fecha',
        'recibo',
        'beneficiario',
        'concepto',
        'movimiento',
        'monto',
        'saldo',
        'tipo',
    ];
}
