<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Client extends Model
{
   
    protected $fillable = [
        'admin_id',
        'name',
        'surname',
        'phone',
        'email',
        'lugar_nacimiento',
        'fecha_nacimiento',
        'ocupation',
        'address',
        'estado',
        'municipio',
        'cp',
        'identification_type',
        'identificacion',
        'curp',
        'rfc',
        'title_second',
        'name_second',
        'surname_second',
        'phone_second',
        'email_second',
        'nacionalidad',
        'extranjero'
    ];

    public function admin(){
        return $this->belongsTo('App\Admin')->first();
    }
    // las ventas que tiene asigando ese cliente   
    public function ventas(){        
        return $this->hasMany(Venta::class)->where("status",">","0")->get();
    }

    public function model(){
        return Client::class;
    }

    public function fecha_nacimiento(){
        if($this->fecha_nacimiento !== ''){
            return date('d/m/Y', strtotime($this->fecha_nacimiento));
        }else{
            return $this->fecha_nacimiento;
        }
    }
}
