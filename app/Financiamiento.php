<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Financiamiento extends Model
{
    protected $fillable = [
        'development_id',
        'nombre',
        'tipo',
        'tipo_descuento',
        'porcentaje_descuento',
        'porcentaje_interes',
        'tipo_enganche',
        'enganche',
        'enganche_diferido',
        'enganche_num_pagos',
        'tipo_mensualidad',
        'mensualidad',
        'mensualidad_num_pagos',
        'tipo_saldo',
        'saldo',
        'status',
    ];
}
