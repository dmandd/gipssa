<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class RevokeQuotation extends Notification
{
    use Queueable;
    var $venta, $motivo;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($venta, $motivo)
    {
        $this->venta = $venta;
        $this->motivo = $motivo;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject('Cotización rechazada')
                    ->markdown('emails.quotation.revoke', ['venta' => $this->venta, 'motivo' => $this->motivo]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'venta' => $this->venta,
            'motivo' => "Motivo: ".$this->motivo,
        ];
    }
}
