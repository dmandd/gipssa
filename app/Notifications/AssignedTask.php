<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class AssignedTask extends Notification
{
    use Queueable;
    var $task;
    var $prospect;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(\App\Task $task, Int $prospect)
    {
        $this->task = $task;
        $this->prospect = $prospect;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->from('noreply@gipssa.com', 'Gipssa')
                    ->subject('Tarea asignada')
                    ->markdown('emails.task.assigned', ['task' => $this->task, 'url' => route('panel.prospects.edit', ['prospecto' => $this->prospect])]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
