<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class RevokeDocument extends Notification
{
    use Queueable;
    var $venta, $document, $motivo;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($venta, $document, $motivo = 'No se especifica')
    {
        $this->venta = $venta;
        $this->document = $document;
        $this->motivo = $motivo;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject('Documento Rechazado')
                    ->markdown('emails.document.revoke', ['venta' => $this->venta, 'motivo' => $this->motivo]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
