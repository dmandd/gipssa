<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Development extends Model
{
    protected $fillable = [
        'admin_id',
        'cadastral_planking_id',
        'logo',
        'name',
        'location',
        'type',
        'status',
        'max_delivery_time',
        'plano',
        'stage',
        'svg',
        'svg_points',
        'serie',       
        'masterplan' // para guardar imagen del master plan

    ];

    public function cadastralPlanking(){
        return $this->belongsTo('App\CadastralPlanking')->first();
    }

    public function admin(){
        return $this->belongsTo('App\Admin')->first();
    }

    public function lotes(){
        return $this->hasMany('App\Lote')->get();
    }

    public function ventas(){
        return $this->hasMany('App\Venta');
    }

    public function clientDocuments($fijo = null){
        if(!is_null($fijo))
            return $this->hasMany('App\DevelopmentClientDocumentation')->where('fijo',1)->get();
        else
            return $this->hasMany('App\DevelopmentClientDocumentation')->get();
    }

    public function financiamientos(){
        return $this->hasMany('App\Financiamiento')->where('status', 'visible')->get();
    }

    public function bancos(){
        return $this->hasMany('App\Banco')->get();
    }

    public function grantor(){
        return $this->hasMany('App\Grantor')->get();
    }

    public function slug(){
        return Str::slug($this->name);
    }

    public function status(){
        switch ($this->status) {
            case 'visible':
                return "Visible";
            break;
            case 'hidden':
                return "Oculto";
            break;
            default:
                return "Sin asignar";
            break;
        }
    }
}
