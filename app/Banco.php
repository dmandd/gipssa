<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Banco extends Model
{
    protected $fillable = [
        'development_id',
        'name',
        'n_cuenta',
        'referencia',
        'status'
    ];

    public function desarrollo(){
        return $this->belongsTo('App\Development', 'development_id', 'id')->first();
    }
}
