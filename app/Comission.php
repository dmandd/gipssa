<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comission extends Model
{
    protected $fillable = [
        'venta_id',
        'admin_id',
        'payments',
        'percentage',
        'taxes',
        'sale_amount',
        'total_amount',
        'retentions',
        'pay_amount',
        'paid_amount',
        'notes',
        'type',
        'status',
    ];

    public $timestamps = false;

    public const TYPE = [
        "ASESOR"=>1,
        "SUPERVISOR"=>2,
    ];

    public function venta(){
        return $this->belongsTo('App\Venta')->first();
    }

    public function admin(){
        return $this->belongsTo('App\Admin','admin_id')->first();
    }

    public function payments(){
        return $this->hasMany('App\ComissionPayment')->get();
    }

    public function comissionPayments(){
        return $this->hasMany(ComissionPayment::class,'comission_id');
    }
}
