<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class QuotationCreated extends Mailable
{
    use Queueable, SerializesModels;
    var $description, $url;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($subject, $description, $url)
    {
        $this->subject = $subject;
        $this->description = $description;
        $this->url = $url;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject($this->subject)->markdown('emails.quotation.created');
    }
}
