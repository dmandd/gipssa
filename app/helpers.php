<?php
function time_passed($get_timestamp){
    $timestamp = strtotime($get_timestamp);
    $diff = time() - (int)$timestamp;

    if ($diff == 0) 
         return 'justo ahora';

    if ($diff > 604800)
        return date("d M Y",$timestamp);

    $intervals = array
    (
        //1                   => array('año',    31556926),
       // $diff < 31556926    => array('mes',   2628000),
       // $diff < 2629744     => array('semana',    604800),
        $diff < 604800      => array('día',     86400),
        $diff < 86400       => array('hora',    3600),
        $diff < 3600        => array('minuto',  60),
        $diff < 60          => array('segundo',  1)
    );

    $value = floor($diff/$intervals[1][1]);
    return 'hace '.$value.' '.$intervals[1][0].($value > 1 ? 's' : '');
}

function navLinkTab(Bool $bool, Array $arr, Bool $active){
    if($bool){
        return '<a class="nav-link mb-sm-3 mb-md-0 '.(($active) ? 'active' : '').'" id="'.$arr['slug'].'-tab" data-toggle="tab" href="#'.$arr['slug'].'" role="tab" aria-controls="'.$arr['slug'].'" aria-selected="false">'.$arr['name'].'</a>';
    }else{
        return '<a class="nav-link mb-sm-3 mb-md-0" disabled>'.$arr['name'].'</a>';
    }
}

function tofloat($num) {
    $dotPos = strrpos($num, '.');
    $commaPos = strrpos($num, ',');
    $sep = (($dotPos > $commaPos) && $dotPos) ? $dotPos :
        ((($commaPos > $dotPos) && $commaPos) ? $commaPos : false);
  
    if (!$sep) {
        return floatval(preg_replace("/[^0-9]/", "", $num));
    }

    return floatval(
        preg_replace("/[^0-9]/", "", substr($num, 0, $sep)) . '.' .
        preg_replace("/[^0-9]/", "", substr($num, $sep+1, strlen($num)))
    );
}

function getAmmount($money){
    $cleanString = preg_replace('/([^0-9\.,])/i', '', $money);
    $onlyNumbersString = preg_replace('/([^0-9])/i', '', $money);

    $separatorsCountToBeErased = strlen($cleanString) - strlen($onlyNumbersString) - 1;

    $stringWithCommaOrDot = preg_replace('/([,\.])/', '', $cleanString, $separatorsCountToBeErased);
    $removedThousandSeparator = preg_replace('/(\.|,)(?=[0-9]{3,}$)/', '',  $stringWithCommaOrDot);

    return (float) str_replace(',', '.', $removedThousandSeparator);
}

function showSVG($route){
    

    $svg = new DOMDocument();    
    //$svg->load(asset($route),LIBXML_NOWARNING);   
    $svg->load(base_path().'/public/'.$route,LIBXML_NOWARNING);
    // Add CSS class (you can omit this line)
    $svg->documentElement->setAttribute("class", "logo");
    // Echo XML without version element
    echo $svg->saveXML($svg->documentElement);
}

function formatDate($string){
    $date = str_replace('/', '-', $string);
    return date('Y-m-d', strtotime($date));
}
function monthTraductor($string){
    $mes = "";

    switch ($string) {
        case '01':
            $mes = "Enero";
        break;
        case '02':
            $mes = "Febrero";
        break;
        case '03':
            $mes = "Marzo";
        break;
        case '04':
            $mes = "Abril";
        break;
        case '05':
            $mes = "Mayo";
        break;
        case '06':
            $mes = "Junio";
        break;
        case '07':
            $mes = "Julio";
        break;
        case '08':
            $mes = "Agosto";
        break;
        case '09':
            $mes = "Septiembre";
        break;
        case '10':
            $mes = "Octubre";
        break;
        case '11':
            $mes = "Noviembre";
        break;
        case '12':
            $mes = "Diciembre";
        break;
        
        default:
            $mes = "Sin definir";
        break;
    }

    return $mes;
}

function descriptionNumber($string){
    $response = '';
    $string = str_split($string);
    foreach($string as $i => $s){
        if($i !== 0)
            $response .= ', ';

        switch ($s) {
            case '1':
                $response .= 'UNO';
            break;
            case '2':
                $response .= 'DOS';
            break;
            case '3':
                $response .= 'TRES';
            break;
            case '4':
                $response .= 'CUATRO';
            break;
            case '5':
                $response .= 'CINCO';
            break;
            case '6':
                $response .= 'SEIS';
            break;
            case '7':
                $response .= 'SIETE';
            break;
            case '8':
                $response .= 'OCHO';
            break;
            case '9':
                $response .= 'NUEVE';
            break;
            case '0':
                $response .= 'CERO';
            break;
            case '.':
                $response .= 'PUNTO';
            break;
            
            default:
                $response .= '';
            break;
        }
    }
    return $response;
}

function fraseNumToLetras($value)
{
    
    if($value > 0 || $value === '0'){
        return ($value." (".strtolower(num2letras($value,false,false,false)).")");
    }else{
        return $value;
    }
    
}

function documentToLetras($value)
{
    if($value > 0 || $value === '0'){
        return strtolower(descriptionNumber($value));
    }else{
        return $value;
    }
    
}

function identificationExtend($identification_type){
    switch($identification_type){
        case "1":
            return "el Instituto Nacional Electoral";
            break;
        case "2":
            return "el Instituto Federal Electoral";
            break;
        case "3":
            return "la Secretaría de Relaciones Exteriores";
            break;
        case "4":
            return "el Instituto Nacional de Migración";
            break;
        case "5":
            return "la Secretaría de Educación Pública";
            break;
        case "6":
            return "la Secretaría de Relaciones Exteriores";
            break;
        default:
            return "sin institución";
            break;
    }
}

function num2letras($num, $fem = false, $dec = true, $pesos = true) { 
    if($num==0)
      return "cero"; // El caso de valor 0 
    
    $matuni[2]  = "dos"; 
    $matuni[3]  = "tres"; 
    $matuni[4]  = "cuatro"; 
    $matuni[5]  = "cinco"; 
    $matuni[6]  = "seis"; 
    $matuni[7]  = "siete"; 
    $matuni[8]  = "ocho"; 
    $matuni[9]  = "nueve"; 
    $matuni[10] = "diez"; 
    $matuni[11] = "once"; 
    $matuni[12] = "doce"; 
    $matuni[13] = "trece"; 
    $matuni[14] = "catorce"; 
    $matuni[15] = "quince"; 
    $matuni[16] = "dieciseis"; 
    $matuni[17] = "diecisiete"; 
    $matuni[18] = "dieciocho"; 
    $matuni[19] = "diecinueve"; 
    $matuni[20] = "veinte"; 
    $matunisub[2] = "dos"; 
    $matunisub[3] = "tres"; 
    $matunisub[4] = "cuatro"; 
    $matunisub[5] = "quin"; 
    $matunisub[6] = "seis"; 
    $matunisub[7] = "sete"; 
    $matunisub[8] = "ocho"; 
    $matunisub[9] = "nove"; 
 
    $matdec[2] = "veint"; 
    $matdec[3] = "treinta"; 
    $matdec[4] = "cuarenta"; 
    $matdec[5] = "cincuenta"; 
    $matdec[6] = "sesenta"; 
    $matdec[7] = "setenta"; 
    $matdec[8] = "ochenta"; 
    $matdec[9] = "noventa"; 
    $matsub[3]  = 'mill'; 
    $matsub[5]  = 'bill'; 
    $matsub[7]  = 'mill'; 
    $matsub[9]  = 'trill'; 
    $matsub[11] = 'mill'; 
    $matsub[13] = 'bill'; 
    $matsub[15] = 'mill'; 
    $matmil[4]  = 'millones'; 
    $matmil[6]  = 'billones'; 
    $matmil[7]  = 'de billones'; 
    $matmil[8]  = 'millones de billones'; 
    $matmil[10] = 'trillones'; 
    $matmil[11] = 'de trillones'; 
    $matmil[12] = 'millones de trillones'; 
    $matmil[13] = 'de trillones'; 
    $matmil[14] = 'billones de trillones'; 
    $matmil[15] = 'de billones de trillones'; 
    $matmil[16] = 'millones de billones de trillones'; 
    
    //Zi hack
    $float=explode('.',$num);
    $num=$float[0];
 
    $num = trim((string)@$num); 
    if ($num[0] == '-') { 
       $neg = 'menos '; 
       $num = substr($num, 1); 
    }else 
       $neg = ''; 
    
    while ($num[0] == '0') $num = substr($num, 1); 
    if ($num[0] < '1' or $num[0] > 9) $num = '0' . $num; 
    $zeros = true; 
    $punt = false; 
    $ent = ''; 
    $fra = ''; 
    for ($c = 0; $c < strlen($num); $c++) { 
       $n = $num[$c]; 
       if (! (strpos(".,'''", $n) === false)) { 
          if ($punt) break; 
          else{ 
             $punt = true; 
             continue; 
          } 
 
       }elseif (! (strpos('0123456789', $n) === false)) { 
          if ($punt) { 
             if ($n != '0') $zeros = false; 
             $fra .= $n; 
          }else 
 
             $ent .= $n; 
       }else 
 
          break; 
 
    } 
    $ent = '     ' . $ent; 
    if ($dec and $fra and ! $zeros) { 
       $fin = ' coma'; 
       for ($n = 0; $n < strlen($fra); $n++) { 
          if (($s = $fra[$n]) == '0') 
             $fin .= ' cero'; 
          elseif ($s == '1') 
             $fin .= $fem ? ' una' : ' un'; 
          else 
             $fin .= ' ' . $matuni[$s]; 
       } 
    }else 
       $fin = ''; 
    if ((int)$ent === 0) return 'Cero ' . $fin; 
    $tex = ''; 
    $sub = 0; 
    $mils = 0; 
    $neutro = false; 
    while ( ($num = substr($ent, -3)) != '   ') { 
       $ent = substr($ent, 0, -3); 
       if (++$sub < 3 and $fem) { 
          $matuni[1] = 'una'; 
          $subcent = 'as'; 
       }else{ 
          $matuni[1] = $neutro ? 'un' : 'uno'; 
          $subcent = 'os'; 
       } 
       $t = ''; 
       $n2 = substr($num, 1); 
       if ($n2 == '00') { 
       }elseif ($n2 < 21) 
          $t = ' ' . $matuni[(int)$n2]; 
       elseif ($n2 < 30) { 
          $n3 = $num[2]; 
          if ($n3 != 0) $t = 'i' . $matuni[$n3]; 
          $n2 = $num[1]; 
          $t = ' ' . $matdec[$n2] . $t; 
       }else{ 
          $n3 = $num[2]; 
          if ($n3 != 0) $t = ' y ' . $matuni[$n3]; 
          $n2 = $num[1]; 
          $t = ' ' . $matdec[$n2] . $t; 
       } 
       $n = $num[0]; 
       if ($n == 1) { 
          $t = ' ciento' . $t; 
       }elseif ($n == 5){ 
          $t = ' ' . $matunisub[$n] . 'ient' . $subcent . $t; 
       }elseif ($n != 0){ 
          $t = ' ' . $matunisub[$n] . 'cient' . $subcent . $t; 
       } 
       if ($sub == 1) { 
       }elseif (! isset($matsub[$sub])) { 
          if ($num == 1) { 
             $t = ' mil'; 
          }elseif ($num > 1){ 
             $t .= ' mil'; 
          } 
       }elseif ($num == 1) { 
          $t .= ' ' . $matsub[$sub] . '?n'; 
       }elseif ($num > 1){ 
          $t .= ' ' . $matsub[$sub] . 'ones'; 
       }   
       if ($num == '000') $mils ++; 
       elseif ($mils != 0) { 
          if (isset($matmil[$sub])) $t .= ' ' . $matmil[$sub]; 
          $mils = 0; 
       } 
       $neutro = true; 
       $tex = $t . $tex; 
    } 
    $tex = $neg . substr($tex, 1) . $fin; 
    //Zi hack --> return ucfirst($tex);
    if($pesos){
        if(isset($float[1])){ // Dos digitos para los centavos
            $end_num=ucfirst($tex).' pesos '.strzero($float[1],2).'/100 MONEDA NACIONAL';
        }else{
            $end_num=ucfirst($tex).' pesos 00/100 MONEDA NACIONAL.';
        }
        return $end_num; 
    }else{
        return $end_num = ucfirst($tex); 
    }
} 

function upload_file($file){
    $path_file = $file->store('public/media');
    $_exploded = explode('/', $path_file);
    $_exploded[0] = 'storage';
    $path_file = implode('/', $_exploded);
    return $path_file;
}

function strzero($valor,$longitud){
    if(gettype($valor)!='string')
      $valor=strval($valor);
	$valor = trim($valor);
	$lenvalor = strlen($valor);
	$zero ="";
	if( ($longitud - $lenvalor) > 0){
		for ($i = 1 ; $i <=  ($longitud - $lenvalor); $i ++) {
			$zero = $zero."0";
		}
	}
	RETURN $zero.$valor;
}


function ordinalNumber($string){
    $response = '';
    $string = str_split($string);
    foreach($string as $i => $s){
        if($i !== 0)
            $response .= ', ';

        switch ($s) {
            case '1':
                $response .= 'primer';
            break;
            case '2':
                $response .= 'segundo';
            break;
            case '3':
                $response .= 'tercero';
            break;
            case '4':
                $response .= 'cuarto';
            break;
            case '5':
                $response .= 'quinto';
            break;
            case '6':
                $response .= 'sexto';
            break;
            case '7':
                $response .= 'septimo';
            break;
            case '8':
                $response .= 'octavo';
            break;
            case '9':
                $response .= 'noveno';
            break;
            case '10':
                $response .= 'decimo';
            break;
            case '.':
                $response .= '*REVISAR*';
            break;
            
            default:
                $response .= '';
            break;
        }
    }
    return $response;
}
