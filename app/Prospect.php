<?php

namespace App;

use App\TaskProspect;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Prospect extends Model
{
    protected $fillable = [
        'admin_id',
        'name',
        'email',
        'phone',
        'priority',
        'procedence',
        'notes',
        'avance',
        'desarrollo',
        'status'
    ];

    public function admin(): BelongsTo {
        return $this->belongsTo(Admin::class);
    }

    public function model(){
        return Prospect::class;
    }

    public function desarrollo(){
        /*if($this->desarrollo){
            if($this->desarrollo == 'inmobiliaria'){
                return "Inmobiliaria";
            }else{
                $desarrollo = Development::where('name', 'like', '%'.$this->desarrollo.'%')->first();
                
                return $desarrollo->name;
            }
        }else{
            return 'Sin asignar';
        }*/
        return $this->desarrollo;
    }

    public function removeTasks(){
        $tasks = TaskProspect::where('prospect_id', $this->id)->get();
        foreach($tasks as $task)
            $task->delete();
    }

    public function removeNotes(){
        $notes = Note::where('prospect_id', $this->id)->get();
        foreach($notes as $note)
            $note->delete();
    }

    public function notes(){
        return $this->hasMany('App\Note')->orderByDesc('created_at', 'desc')->get();
    }
}
