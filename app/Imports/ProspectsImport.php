<?php

namespace App\Imports;

use App\Client;
use App\Prospect;
use Exception;
use Maatwebsite\Excel\Concerns\ToModel;

class ProspectsImport implements ToModel
{
    var $array = [];
    var $admin_id;
    function __construct($admin_id){
        $this->admin_id = $admin_id;
    }

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        if($row[0] == 'ID'){
            foreach ($row as $key => $value) {
                $this->array[$value] = $key;
            }
        }

        if($row[0] !== 'ID'){
            if(!is_null($row[$this->array['ID']])){
                if(Prospect::where('id', '!=', $row[$this->array['ID']])->where('email', $row[$this->array['Correo']])->get()->count() > 0 || Client::where('email', $row[$this->array['Correo']])->get()->count() > 0){
                    throw new Exception('Lo sentimos el correo '.$row[$this->array['Correo']].' ya está en uso intente con otro.');
                }   
                $prospect = Prospect::where('id', $row[$this->array['ID']])->first();
                $prospect->update([
                    'admin_id' => $this->admin_id,
                    'name' => $row[$this->array['Nombre']],
                    'email' => $row[$this->array['Correo']],
                    'phone' => $row[$this->array['Telefono']],
                    'notes' => $row[$this->array['Notas']],
                ]);
                return $prospect;
            }else{
                if(Prospect::where('email',$row[$this->array['Correo']])->get()->count() > 0 || Client::where('email',$row[$this->array['Correo']])->get()->count() > 0){
                    throw new Exception('Lo sentimos el correo '.$row[$this->array['Correo']].' ya está en uso intente con otro.');
                } 
                $prospect = new Prospect([
                    'admin_id' => $this->admin_id,
                    'name' => $row[$this->array['Nombre']],
                    'email' => $row[$this->array['Correo']],
                    'phone' => $row[$this->array['Telefono']],
                    'notes' => $row[$this->array['Notas']],
                    'priority' => null,
                    'procedence' => null,
                    'avance' => 'girs',
                    'desarrollo' => null,
                    'status' => 1,
                ]);
                $prospect->save();
                
                return $prospect;
            }
        }
    }
}
