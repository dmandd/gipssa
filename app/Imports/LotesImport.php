<?php

namespace App\Imports;

use App\Lote;
use Maatwebsite\Excel\Concerns\ToModel;

class LotesImport implements ToModel
{
    var $array = [];
    var $development_id;
    function __construct($development_id){
        $this->development_id = $development_id;
    }

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        if($row[0] == 'id'){
            foreach ($row as $key => $value) {
                $this->array[$value] = $key;
            }
        }

        if($row[0] !== 'id'){
            if($lote = Lote::where('development_id', $this->development_id)->where('num_lote',$row[$this->array['numero lote']])->first()){
                $lote->update([
                    'num_lote' => $row[$this->array['numero lote']],
                    'name' => $row[$this->array['nombre']],
                    'price' => $row[$this->array['precio']],
                    'area' => $row[$this->array['metros']],
                    'status' => $row[$this->array['estatus']],
                    'norte' => $row[$this->array['norte']],
                    'nororiente' => $row[$this->array['nororiente']],
                    'oriente' => $row[$this->array['oriente']],
                    'suroriente' => $row[$this->array['suroriente']],
                    'sur' => $row[$this->array['sur']],
                    'surponiente' => $row[$this->array['surponiente']],
                    'poniente' => $row[$this->array['poniente']],
                    'norponiente' => $row[$this->array['norponiente']],
                    'lindancia_norte' => $row[$this->array['lindancia_norte']],
                    'lindancia_nororiente' => $row[$this->array['lindancia_nororiente']],
                    'lindancia_oriente' => $row[$this->array['lindancia_oriente']],
                    'lindancia_suroriente' => $row[$this->array['lindancia_suroriente']],
                    'lindancia_sur' => $row[$this->array['lindancia_sur']],
                    'lindancia_surponiente' => $row[$this->array['lindancia_surponiente']],
                    'lindancia_poniente' => $row[$this->array['lindancia_poniente']],
                    'lindancia_norponiente' => $row[$this->array['lindancia_norponiente']]
                ]);
                return $lote;
            }else{
                return new Lote([
                    'development_id' => $this->development_id,
                    'num_lote' => $row[$this->array['numero lote']],
                    'name' => $row[$this->array['nombre']],
                    'price' => $row[$this->array['precio']],
                    'area' => $row[$this->array['metros']],
                    'status' => $row[$this->array['estatus']],
                    'norte' => $row[$this->array['norte']],
                    'nororiente' => $row[$this->array['nororiente']],
                    'oriente' => $row[$this->array['oriente']],
                    'suroriente' => $row[$this->array['suroriente']],
                    'sur' => $row[$this->array['sur']],
                    'surponiente' => $row[$this->array['surponiente']],
                    'poniente' => $row[$this->array['poniente']],
                    'norponiente' => $row[$this->array['norponiente']],
                    'lindancia_norte' => $row[$this->array['lindancia_norte']],
                    'lindancia_nororiente' => $row[$this->array['lindancia_nororiente']],
                    'lindancia_oriente' => $row[$this->array['lindancia_oriente']],
                    'lindancia_suroriente' => $row[$this->array['lindancia_suroriente']],
                    'lindancia_sur' => $row[$this->array['lindancia_sur']],
                    'lindancia_surponiente' => $row[$this->array['lindancia_surponiente']],
                    'lindancia_poniente' => $row[$this->array['lindancia_poniente']],
                    'lindancia_norponiente' => $row[$this->array['lindancia_norponiente']]
                ]);
            }
        }
        
    }
}
