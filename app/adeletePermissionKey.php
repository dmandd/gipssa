<?php

namespace App\Providers;
use Illuminate\Support\ServiceProvider;

/**
 * ConstantExport Trait implementa el método getConstants() el cual nos permite
 * regresar las constantes de la clase como un array asociativo
 */
Trait ConstantExport 
{
    /**
     * @return [const_name => 'value', ...]
     */
    static function getConstants(){
        $refl = new \ReflectionClass(__CLASS__);
        return $refl->getConstants();
    }
}


class PermissionKey extends ServiceProvider
{
    use ConstantExport;

    const Admin = [
        'name' => 'Módulo usuarios',
        'permissions' => [
            'show_sidebar' => [
                'display_name' => 'Mostrar en menú',
                'name' => 'admins.menu'
            ],
            'index' => [
                'display_name' => 'Ver todos',
                'name' => 'admins.index'
            ],
            'create' => [
                'display_name' => 'Crear',
                'name' => 'admins.create'
            ],
            'edit' =>[
                'display_name' => 'Ver detalle',
                'name' => 'admins.edit'
            ],
            'update' => [
                'display_name' => 'Modificar',
                'name' => 'admins.update'
            ],
            'update_password' => [
                'display_name' => 'Modificar contraseña',
                'name' => 'admins.update.password'
            ],
            'destroy' =>[
                'display_name' => 'Eliminar',
                'name' => 'admins.destroy'
            ],
            'agent' =>[
                'display_name' => 'Asesor de ventas',
                'name' => 'admins.saleAgent'
            ],
            'manager' =>[
                'display_name' => 'Gerente de ventas',
                'name' => 'admins.saleManager'
            ]
        ]
    ];

    const Role = [
        'name' => 'Módulo roles',
        'permissions' => [
            'index' => [
                'display_name' => 'Ver todos',
                'name' => 'roles.index'
            ],
            'create' => [
                'display_name' => 'Crear',
                'name' => 'roles.create'
            ],
            'edit' => [
                'display_name' => 'Ver detalle',
                'name' => 'roles.edit'
            ],
            'update' => [
                'display_name' => 'Modificar',
                'name' => 'roles.update'
            ],
            'destroy' => [
                'display_name' => 'Eliminar',
                'name' => 'roles.destroy'
            ],
            'assign_role' => [
                'display_name' => 'Asignar Rol',
                'name' => 'roles.assign'
            ]
        ]
    ];

    const Image = [
        'name' => 'Módulo Material Subido',
        'permissions' => [
            'index' => [
                'display_name' => 'Ver todos',
                'name' => 'images.index'
            ],
            'destroy' => [
                'display_name' => 'Eliminar',
                'name' => 'images.destroy'
            ],
        ]
    ];

    const Zone = [
        'name' => 'Módulo Zonas',
        'permissions' => [
            'index' => [
                'display_name' => 'Ver todos',
                'name' => 'zones.index',
            ],
            'create' => [
                'display_name' => 'Crear',
                'name' => 'zones.create',
            ],
            'edit' => [
                'display_name' => 'Ver detalle',
                'name' => 'zones.edit',
            ],
            'update' => [
                'display_name' => 'Modificar',
                'name' => 'zones.update',
            ],
            'destroy' => [
                'display_name' => 'Eliminar',
                'name' => 'zones.destroy',
            ],
        ]
    ];

    const Tag = [
        'name' => 'Módulo etiquetas',
        'permissions' => [
            'index' => [
                'display_name' => 'Ver todos',
                'name' => 'tags.index'
            ],
            'create' => [
                'display_name' => 'Crear',
                'name' => 'tags.create',
            ],
            'edit' => [
                'display_name' => 'Ver detalle',
                'name' => 'tags.edit',
            ],
            'update' => [
                'display_name' => 'Modificar',
                'name' => 'tags.update',
            ],
            'destroy' => [
                'display_name' => 'Eliminar',
                'name' => 'tags.destroy',
            ],
        ]
    ];

    const PropertyType = [
        'name' => 'Módulo Tipo',
        'permissions' => [
            'index' => [
                'display_name' => 'Ver todos',
                'name' => 'types.index',
            ],
            'create' => [
                'display_name' => 'Crear',
                'name' => 'types.create',
            ],
            'edit' => [
                'display_name' => 'Ver detalle',
                'name' => 'types.edit',
            ],
            'update' => [
                'display_name' => 'Modificar',
                'name' => 'types.update',
            ],
            'destroy' => [
                'display_name' => 'Eliminar',
                'name' => 'types.destroy',
            ],
        ],
    ];

    const Operation = [
        'name' => 'Módulo Operaciones',
        'permissions' => [
            'index' => [
                'display_name' => 'Ver todos',
                'name' => 'operations.index',
            ],
            'create' => [
                'display_name' => 'Crear',
                'name' => 'operations.create',
            ],
            'edit' => [
                'display_name' => 'Ver detalle',
                'name' => 'operations.edit',
            ],
            'update' => [
                'display_name' => 'Modificar',
                'name' => 'operations.update',
            ],
            'destroy' => [
                'display_name' => 'Eliminar',
                'name' => 'operations.destroy',
            ],
        ],
    ];

    const Equipment = [
        'name' => 'Módulo equipamiento',
        'permissions' => [
            'index' => [
                'display_name' => 'Ver todos',
                'name' => 'equipment.index'
            ],
            'create' => [
                'display_name' => 'Crear',
                'name' => 'equipment.create',
            ],
            'edit' => [
                'display_name' => 'Ver detalle',
                'name' => 'equipment.edit',
            ],
            'update' => [
                'display_name' => 'Modificar',
                'name' => 'equipment.update',
            ],
            'destroy' => [
                'display_name' => 'Eliminar',
                'name' => 'equipment.destroy',
            ],
        ]
    ];

    const Property = [
        'name' => 'Módulo catálogo propiedades',
        'permissions' => [
            'show_sidebar' => [
                'display_name' => 'Mostrar en menú',
                'name' => 'properties.menu'
            ],
            'index' => [
                'display_name' => 'Ver todos',
                'name' => 'properties.index'
            ],
            'create' => [
                'display_name' => 'Crear',
                'name' => 'properties.create',
            ],
            'edit' => [
                'display_name' => 'Ver detalle',
                'name' => 'properties.edit',
            ],
            'update' => [
                'display_name' => 'Modificar',
                'name' => 'properties.update',
            ],
            'publish' => [
                'display_name' => 'Publicar',
                'name' => 'properties.publish'
            ],
            'destroy' => [
                'display_name' => 'Eliminar',
                'name' => 'properties.destroy',
            ],
            'update_code' => [
                'display_name' => 'Modificar Código',
                'name' => 'properties.update.code',
            ],
            'assign' => [
                'display_name' => 'Asignar asesor',
                'name' => 'properties.assign',
            ],
            'contact' => [
                'display_name' => 'Ver contacto',
                'name' => 'properties.contact',
            ]
        ]
    ];

    const Event = [
        'name' => 'Módulo eventos calendario',
        'permissions' => [
            'show_sidebar' => [
                'display_name' => 'Mostrar en menú',
                'name' => 'events.menu'
            ],
            'index' => [
                'display_name' => 'Ver todos los eventos (de todos los usuarios)',
                'name' => 'events.all'
            ],
            'create' => [
                'display_name' => 'Agregar y modificar',
                'name' => 'events.create',
            ],
            'destroy' => [
                'display_name' => 'Eliminar',
                'name' => 'events.destroy',
            ],
            'assign' => [
                'display_name' => 'Asignar evento',
                'name' => 'events.assign',
            ],
        ]
    ];

    const Prospect = [
        'name' => 'Módulo de Leads',
        'permissions' => [
            'show_sidebar' => [
                'display_name' => 'Mostrar en menú',
                'name' => 'prospects.menu'
            ],
            'index' => [
                'display_name' => 'Ver todos',
                'name' => 'prospects.all'
            ],
            'create' => [
                'display_name' => 'Agregar',
                'name' => 'prospects.create',
            ],
            'edit' => [
                'display_name' => 'Ver detalle',
                'name' => 'prospects.edit',
            ],
            'update' => [
                'display_name' => 'Modificar',
                'name' => 'prospects.update',
            ],
            'destroy' => [
                'display_name' => 'Eliminar',
                'name' => 'prospects.destroy',
            ],
            'assign' => [
                'display_name' => 'Asignar responsable',
                'name' => 'prospects.assign',
            ],
            'import' => [
                'display_name' => 'Importar',
                'name' => 'prospects.import'
            ],
            'create_client' => [
                'display_name' => 'Generar cliente desde prospecto',
                'name' => 'prospects.create.client'
            ]
        ]
    ];

    const Contact = [
        'name' => 'Módulo contactos propiedades',
        'permissions' => [
            'index' => [
                'display_name' => 'Ver módulo',
                'name' => 'contacts.index'
            ],
            'create' => [
                'display_name' => 'Crear',
                'name' => 'contacts.create',
            ],
            'edit' => [
                'display_name' => 'Ver detalle',
                'name' => 'contacts.edit',
            ],
            'update' => [
                'display_name' => 'Modificar',
                'name' => 'contacts.update',
            ],
            'destroy' => [
                'display_name' => 'Eliminar',
                'name' => 'contacts.destroy',
            ],
            'assign' => [
                'display_name' => 'Asignar a usuario',
                'name' => 'contacts.assign'
            ],
            'all' => [
                'display_name' => 'Ver todos',
                'name' => 'contacts.all'
            ],
        ]
    ];

    const CadastralPlanking = [
        'name' => 'Módulo Tablajes Catastrales',
        'permissions' => [
            'index' => [
                'display_name' => 'Ver menú',
                'name' => 'cadastralPlanking.index'
            ],
            'create' => [
                'display_name' => 'Crear',
                'name' => 'cadastralPlanking.create',
            ],
            'edit' => [
                'display_name' => 'Ver detalle',
                'name' => 'cadastralPlanking.edit',
            ],
            'update' => [
                'display_name' => 'Modificar',
                'name' => 'cadastralPlanking.update',
            ],
            'destroy' => [
                'display_name' => 'Eliminar',
                'name' => 'cadastralPlanking.destroy',
            ],
        ]
    ];

    const Development = [
        'name' => 'Módulo Desarrollos',
        'permissions' => [
            'index' => [
                'display_name' => 'Ver menú',
                'name' => 'development.index'
            ],
            'create' => [
                'display_name' => 'Crear',
                'name' => 'development.create',
            ],
            'edit' => [
                'display_name' => 'Ver detalle',
                'name' => 'development.edit',
            ],
            'update' => [
                'display_name' => 'Modificar',
                'name' => 'development.update',
            ],
            'destroy' => [
                'display_name' => 'Eliminar',
                'name' => 'development.destroy',
            ],
        ]
    ];

    const Client = [
        'name' => 'Módulo clientes desarrollos',
        'permissions' => [
            'index' => [
                'display_name' => 'Ver menú',
                'name' => 'clients.index'
            ],
            'create' => [
                'display_name' => 'Crear',
                'name' => 'clients.create',
            ],
            'edit' => [
                'display_name' => 'Ver detalle',
                'name' => 'clients.edit',
            ],
            'update' => [
                'display_name' => 'Modificar',
                'name' => 'clients.update',
            ],
            'destroy' => [
                'display_name' => 'Eliminar',
                'name' => 'clients.destroy',
            ],
            'assign' => [
                'display_name' => 'Asignar cliente',
                'name' => 'clients.assign'
            ],
            'all' => [
                'display_name' => 'Ver todos',
                'name' => 'clients.all'
            ],
        ]
    ];

    const Task = [
        'name' => 'Módulo Actividades',
        'permissions' => [
            'index' => [
                'display_name' => 'Ver todos',
                'name' => 'task.index'
            ],
            'create' => [
                'display_name' => 'Crear',
                'name' => 'task.create',
            ],
            'edit' => [
                'display_name' => 'Ver detalle',
                'name' => 'task.edit',
            ],
            'update' => [
                'display_name' => 'Modificar',
                'name' => 'task.update',
            ],
            'destroy' => [
                'display_name' => 'Eliminar',
                'name' => 'task.destroy',
            ],
            'assign' => [
                'display_name' => 'Asignar cliente',
                'name' => 'task.assign'
            ],
            'display_menu' => [
                'display_name' => 'Ver menú',
                'name' => 'task.menu'
            ],
        ]
    ];

    const Payment = [
        'name' => 'Módulo Caja Chica',
        'permissions' => [
            'index' => [
                'display_name' => 'Ver todos',
                'name' => 'payment.index'
            ],
            'create' => [
                'display_name' => 'Crear',
                'name' => 'payment.create',
            ],
            'edit' => [
                'display_name' => 'Ver detalle',
                'name' => 'payment.edit',
            ],
            'update' => [
                'display_name' => 'Modificar',
                'name' => 'payment.update',
            ],
            'destroy' => [
                'display_name' => 'Eliminar',
                'name' => 'payment.destroy',
            ],
            'display_menu' => [
                'display_name' => 'Ver menú',
                'name' => 'payment.menu'
            ],
        ]
    ];

    const Venta = [
        'name' => 'Módulo de ventas',
        'permissions' => [
            'show_sidebar' => [
                'display_name' => 'Mostrar en menú',
                'name' => 'ventas.show'
            ],
            'index' => [
                'display_name' => 'Ver todos',
                'name' => 'ventas.index',
            ],
            'create' => [
                'display_name' => 'Crear',
                'name' => 'ventas.create',
            ],
            'edit' => [
                'display_name' => 'Ver detalle',
                'name' => 'ventas.edit',
            ],
            'update' => [
                'display_name' => 'Modificar',
                'name' => 'ventas.update',
            ],
            'destroy' => [
                'display_name' => 'Eliminar',
                'name' => 'ventas.destroy'
            ],
            'update_status' => [
                'display_name' => 'Actualizar estado de venta',
                'name' => 'ventas.update.status'
            ],
            'authorize_quotation' => [
                'display_name' => 'Autorizar cotización cliente',
                'name' => 'ventas.authorize.client.quotation'
            ],
            'authorize_client_docs' => [
                'display_name' => 'Autorizar documentación del cliente',
                'name' => 'ventas.authorize.client.files'
            ],
            'parametros_bloqueo' => [
                'display_name' => 'Definir Parametros Bloqueo',
                'name' => 'ventas.bloqueo'
            ],
            'create_abono_enganche' => [
                'display_name' => 'Crear abono enganche',
                'name' => 'ventas.create.abono.enganche'
            ],
            'authorize_recibo_enganche' => [
                'display_name' => 'Autorizar abono enganche',
                'name' => 'ventas.authorize.abono.enganche'
            ],
            'destroy_recibo_enganche' => [
                'display_name' => 'Eliminar abono enganche',
                'name' => 'ventas.destroy.abono.enganche'
            ],
            'update_contrato' => [
                "display_name" => "Modificar Contrato",
                "name" => "ventas.update.contrato"
            ],
            'seguimiento_pagos' => [
                'display_name' => "Administración Pagos Financiamiento",
                'name' => 'ventas.pagos.financiamiento'
            ],
            'facturar_pagos' => [
                'display_name' => 'Facturación',
                'name' => 'ventas.pagos.facturacion'
            ],
            'comisiones' => [
                'display_name' => 'Comisiones',
                'name' => 'ventas.comisiones'
            ]
        ]
    ];
}