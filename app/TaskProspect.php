<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TaskProspect extends Model
{
    protected $fillable = [
        'prospect_id',
        'task_id',
        'admin_id',
        'complete'
    ];
}
