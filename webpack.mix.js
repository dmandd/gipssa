const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css');
// #Vendors del panel
mix.js('resources/js/panel/scripts/index.js', 'public/panel/js/main.js');
mix.js('resources/js/panel/scripts/svggotjs/index.js', 'public/panel/js/svgdot.js');
//No se necesitan estar copiando a cada rato
// mix.copy('node_modules/fullcalendar/dist/locale/es.js', 'public/panel/js/fullcalendar-es.js');
// mix.copy('resources/js/panel/scripts/pages/calendar/fullcalendar.js', 'public/panel/js/fullcalendar.js');
// mix.copy('resources/js/panel/scripts/pages/calendar/moment.js', 'public/panel/js/moment.js');
mix.js('resources/js/panel/scripts/pages/calendar/index.js', 'public/panel/js/calendar.js');
mix.js('resources/js/panel/scripts/autocomplete/index.js', 'public/panel/js/autocomplete.js');
mix.js('resources/js/panel/scripts/cotizacion/index.js', 'public/panel/js/cotizacion.js');
//No se necesita estar copiando a cada rato
// mix.copy('resources/vendor/nucleo', 'public/panel/vendor/nucleo');
// mix.copy('resources/vendor/svgdotjs', 'public/panel/vendor/svgdotjs');
// mix.copy('node_modules/@fortawesome/fontawesome-free', 'public/panel/vendor/@fortawesome/fontawesome-free');
// // #Corrige el problema con los íconos
// mix.copy('node_modules/trumbowyg/dist/ui/icons.svg', 'public/panel/vendor/trumbowyg/dist/ui/icons.svg');
mix.styles(['resources/vendor/panel.css'], 'public/panel/css/main.css');
