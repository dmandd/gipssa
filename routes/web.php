<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('/', 'PagesController@index')->name('home');
Route::get('/contacto', 'PagesController@contact')->name('contact');
Route::get('/response', 'PagesController@mailResponse')->name('mailResponse');
Route::get('/construccion', 'PagesController@building')->name('building');
Route::get('/nosotros', 'PagesController@aboutUs')->name('aboutUs');
Route::get('/desarrollo', 'PagesController@desarrollo')->name('desarrollo');
Route::get('/propiedades', 'PagesController@propiedades')->name('propiedades');
Route::get('/propiedades/{id}-{slug}', 'PagesController@detalle')->name('propiedades.detalle');
Route::get('/aviso-privacidad', 'PagesController@aviso')->name('aviso');
//Route::get('/desarrollo/detalle/{id}', 'PagesController@desarrollo')->name('desarrollo.detalle');
Route::post('/events/add/{id_propiedad}', 'EventController@agendar')->name('propiedades.agendar');
Route::get('/disponibilidad/{slug}', 'DevelopmentController@disponibilidad')->name('desarrollo.disponibilidad');

//Mails
// Route::post('/send/mail/property/{id}', 'MailController@sendProperty')->name('mail.send');
Route::post('/send/mail/quotation/{id}', 'MailController@sendQuotation')->name('mail.send.quotation');
Route::post('/send-customer-mail', 'MailController@sendCustomerMail')->name('sendcustomermail');

//PDF'S
Route::get('/documentos/pdf/condicion-compra/{venta}', 'VentaController@condicion')->name('ventas.pdf.condicion');
Route::get('/documentos/pdf/recibo-abono/{recibo}', 'VentaController@abono')->name('ventas.pdf.abono.enganche');
Route::get('/documentos/pdf/recibo-bloqueo/{venta}', 'VentaController@bloqueo')->name('ventas.pdf.bloqueo');
Route::get('/documentos/pdf/cotizacion/{venta}', 'VentaController@cotizacion')->name('ventas.pdf.cotizacion');
Route::get('/documentos/pdf/comission/history/{comission}', 'ComissionController@paymentHistory')->name('comission.pdf.history');
Route::post('/cotizar', 'VentaController@cotizar')->name('ventas.cotizar');
Route::get('/documentos/pdf/corrida-financiera/{venta}', 'VentaController@corrida')->name('ventas.pdf.corrida');
//Word'S
Route::get('/documents/word/contrato/{venta}', 'VentaController@contrato')->name('ventas.word.contrato');

Route::prefix('/admin')->group(function(){
    Route::get('/editor', 'EditorController@index')->name('panel.editor');
    Route::get('/', 'AdminController@unauthenticated')->name('panel.admins.unauthenticated');
    Route::get('/password/reset', 'Auth\ForgotPasswordController@showAdminLinkRequestForm')->name('panel.admins.password.reset');
    Route::post('/password/email', 'Auth\ForgotPasswordController@sendAdminResetLinkEmail')->name('panel.admins.password.email');
    Route::post('/login', 'AdminController@login')->name('panel.admins.login');
    Route::post('/logout', 'AdminController@logout')->name('panel.admins.logout');

    //Administradores
    Route::prefix('/cuentas')->middleware('auth:admin')->group(function(){
        Route::prefix('/usuarios')->group(function(){
            Route::get('/', 'AdminController@index')->name('panel.admins.index');          
            Route::get('/ejecutivos', 'AdminController@index_ejecutivos')->name('panel.admins.ejecutivos');              
            Route::get('/nuevo', 'AdminController@create')->name('panel.admins.create');
            Route::post('/store', 'AdminController@store')->name('panel.admins.store');
            Route::get('/editar/{id}', 'AdminController@edit')->name('panel.admins.edit');            
            Route::put('/update/{id}', 'AdminController@update')->name('panel.admins.update');
            Route::put('/reasign/{id}', 'AdminController@reasign')->name('panel.admins.reasign'); // reasignar clientes 21 Jun
            Route::get('/password/editar/{id}', 'AdminController@editPassword')->name('panel.admins.edit.password');
            Route::put('/password/update/{id}', 'AdminController@updatePassword')->name('panel.admins.update.password');
            Route::delete('/destroy/{id}', 'AdminController@destroy')->name('panel.admins.destroy');
            Route::get('/hide/{id}', 'AdminController@hide')->name('panel.admins.hide');
        });
        Route::prefix('/roles')->group(function(){
            Route::get('/', 'RoleController@index')->name('panel.roles.index');
            Route::get('/nuevo', 'RoleController@create')->name('panel.roles.create');
            Route::get('/editar/{id}', 'RoleController@edit')->name('panel.roles.edit');
            Route::post('/store', 'RoleController@store')->name('panel.roles.store');
            Route::put('/update/{id}', 'RoleController@update')->name('panel.roles.update');
            Route::delete('/destroy/{id}', 'RoleController@destroy')->name('panel.roles.destroy');
        });
    });



    //Materiales subidos
    Route::prefix('/materiales')->middleware('auth:admin')->group(function(){
        Route::get('/', 'ImageController@index')->name('panel.images.index');
        Route::delete('/destroy/{name}', 'ImageController@destroy')->name('panel.images.destroy');
    });


    //Configuración
    Route::prefix('/configuracion')->middleware('auth:admin')->group(function(){
        //Datos generales de la empresa
        Route::prefix('/general')->group(function(){
            Route::get('/', 'SettingController@index')->name('panel.settings.index');
            Route::get('/editar/{id}', 'SettingController@edit')->name('panel.settings.edit');
            Route::put('/update/{id}', 'SettingController@update')->name('panel.settings.update');
        });
    });

    //Direcciones
    Route::post('/store/image', 'ImageController@store')->name('images.store');
    Route::post('/store/image/multiple', 'ImageController@storeMultiple')->name('images.store.multiple');
    Route::get('/storage/list', 'ImageController@show')->name('images.show');
    Route::post('/propiedades/{id}/image/remove', 'PropiertyController@removeImage')->name('propierties.remove.image');

    Route::middleware('auth:admin')->group(function(){
        //Propiedades
        Route::resource('propiedades', 'PropiertyController')->names([
            'index' => 'panel.propierties.index',
            'create' => 'panel.propierties.create',
            'store' => 'panel.propierties.store',
            'edit' => 'panel.propierties.edit',
            'update' => 'panel.propierties.update',
            'destroy' => 'panel.propierties.destroy',
        ]);

        //Tipos de propiedades
        Route::resource('tipos', 'PropertyTypeController')->names([
            'index' => 'panel.types.index',
            'create' => 'panel.types.create',
            'store' => 'panel.types.store',
            'edit' => 'panel.types.edit',
            'update' => 'panel.types.update',
            'destroy' => 'panel.types.destroy',
        ]);
        
        //Operaciones de propiedades
        Route::resource('operaciones', 'OperationController')->names([
            'index' => 'panel.operations.index',
            'create' => 'panel.operations.create',
            'store' => 'panel.operations.store',
            'edit' => 'panel.operations.edit',
            'update' => 'panel.operations.update',
            'destroy' => 'panel.operations.destroy',
        ]);

        //Calendario
        Route::resource('eventos', 'EventController')->names([
            'index' => 'panel.events.index',
            'store' => 'panel.events.store',
            'update' => 'panel.events.update',
            'destroy' => 'panel.events.destroy',
        ]);
    
        //Contactos Propiedades
        Route::resource('contactos', 'ContactController')->names([
            'index' => 'panel.contacts.index',
            'create' => 'panel.contacts.create',
            'store' => 'panel.contacts.store',
            'edit' => 'panel.contacts.edit',
            'update' => 'panel.contacts.update',
            'destroy' => 'panel.contacts.destroy',
        ]);
    
        //Zonas propiedades
        Route::resource('zonas', 'ZoneController')->names([
            'index' => 'panel.zones.index',
            'create' => 'panel.zones.create',
            'store' => 'panel.zones.store',
            'edit' => 'panel.zones.edit',
            'update' => 'panel.zones.update',
            'destroy' => 'panel.zones.destroy',
        ]);
    
        //Etiquetas propiedades
        Route::resource('etiquetas', 'TagController')->names([
            'index' => 'panel.tags.index',
            'create' => 'panel.tags.create',
            'store' => 'panel.tags.store',
            'edit' => 'panel.tags.edit',
            'update' => 'panel.tags.update',
            'destroy' => 'panel.tags.destroy',
        ]);

        //Áreas equipamiento
        Route::resource('equipamiento', 'EquipmentController')->names([
            'index' => 'panel.equipment.index',
            'create' => 'panel.equipment.create',
            'store' => 'panel.equipment.store',
            'edit' => 'panel.equipment.edit',
            'update' => 'panel.equipment.update',
            'destroy' => 'panel.equipment.destroy',
        ]);

        //Áreas propiedades
        Route::resource('propiedades.areas', 'AreaController')->names([
            'index' => 'panel.areas.index',
            'create' => 'panel.areas.create',
            'store' => 'panel.areas.store',
            'edit' => 'panel.areas.edit',
            'update' => 'panel.areas.update',
            'destroy' => 'panel.areas.destroy',
        ]);

        //Clientes
        Route::resource('clientes', 'ClientController')->names([
            'index' => 'panel.clients.index',
            'create' => 'panel.clients.create',
            'store' => 'panel.clients.store',
            'edit' => 'panel.clients.edit',
            'update' => 'panel.clients.update',
            'destroy' => 'panel.clients.destroy',
        ]);

        //Tablajes Catastrales
        Route::resource('tablajesCatastrales', 'CadastralPlankingController')->names([
            'index' => 'panel.cadastralPlankings.index',
            'create' => 'panel.cadastralPlankings.create',
            'store' => 'panel.cadastralPlankings.store',
            'edit' => 'panel.cadastralPlankings.edit',
            'update' => 'panel.cadastralPlankings.update',
            'destroy' => 'panel.cadastralPlankings.destroy',
        ]);

        //Desarrollos
        Route::resource('desarrollos', 'DevelopmentController')->names([
            'index' => 'panel.developments.index',
            'create' => 'panel.developments.create',
            'store' => 'panel.developments.store',
            'edit' => 'panel.developments.edit',
            'update' => 'panel.developments.update',
            'destroy' => 'panel.developments.destroy',
        ]);

        Route::prefix('desarrollos/{desarrollo}')->group(function(){
            //Lotes
            Route::resource('lotes', 'LoteController')->names([
                'index' => 'panel.lotes.index',
                'create' => 'panel.lotes.create',
                'store' => 'panel.lotes.store',
                'edit' => 'panel.lotes.edit',
                'update' => 'panel.lotes.update',
                'destroy' => 'panel.lotes.destroy',
            ]);
            Route::post('/lotes/import', 'LoteController@import')->name('panel.lotes.import');
            Route::get('/export/lotes', 'LoteController@export')->name('panel.lotes.export');
            Route::get('/asignar', 'LoteController@assign')->name('panel.lotes.assign');
            Route::put('/asignar', 'DevelopmentController@assign')->name('panel.lotes.assign.update');
            // Route::get('/documentacion', 'DevelopmentController@documentation')->name('panel.developments.documentation');

            Route::resource('documentacion', 'DevelopmentClientDocumentationController')->names([
                'index' => 'panel.documentation.index',
                'create' => 'panel.documentation.create',
                'store' => 'panel.documentation.store',
                'edit' => 'panel.documentation.edit',
                'update' => 'panel.documentation.update',
                'destroy' => 'panel.documentation.destroy',
            ]);

            //Planes de financiamiento
            Route::resource('financiamiento', 'FinanciamientoController')->names([
                'index' => 'panel.financiamiento.index',
                'create' => 'panel.financiamiento.create',
                'store' => 'panel.financiamiento.store',
                'edit' => 'panel.financiamiento.edit',
                'update' => 'panel.financiamiento.update',
                'destroy' => 'panel.financiamiento.destroy',
            ]);

            //Bancos
            Route::resource('bancos', 'BancoController')->names([
                'index' => 'panel.bancos.index',
                'create' => 'panel.bancos.create',
                'store' => 'panel.bancos.store',
                'edit' => 'panel.bancos.edit',
                'update' => 'panel.bancos.update',
                'destroy' => 'panel.bancos.destroy',
            ]);

            //Poderdantes
            Route::resource('grantors', 'GrantorController')->names([
                'index' => 'panel.grantors.index',
                'create' => 'panel.grantors.create',
                'store' => 'panel.grantors.store',
                'edit' => 'panel.grantors.edit',
                'update' => 'panel.grantors.update',
                'destroy' => 'panel.grantors.destroy',
            ]);
        });

        //Ventas
        Route::resource('ventas', 'VentaController')->names([
            'index' => 'panel.ventas.index',
            'create' => 'panel.ventas.create',
            'store' => 'panel.ventas.store',
            'edit' => 'panel.ventas.edit',
            'update' => 'panel.ventas.update',
            'destroy' => 'panel.ventas.destroy',
        ]);

        //comisiones
        Route::resource('comisiones', 'ComissionController')->names([
            'index' => 'panel.comisiones.index',
            'destroy' => 'panel.comisiones.destroy',
        ]);
        Route::post('comisiones/get', 'ComissionController@getData')->name('panel.comissions.getData');
        Route::get('comisiones/lock/{comission}', 'ComissionController@locker')->name('panel.comissions.lock');
        Route::get('export/comissions', 'ComissionController@export')->name('panel.comissions.export');
        Route::post('comissions', 'ComissionController@store')->name('panel.comission.store');
        Route::get('comissions', 'ComissionController@update')->name('panel.comission.update');
        Route::post('comissions/payments/update', 'ComissionPaymentController@update')->name('panel.comission.payment.update');
        Route::post('comissions/payments/pay', 'ComissionPaymentController@pay')->name('panel.comission.payment.pay');
        Route::post('comissions/payments/unpay', 'ComissionPaymentController@unpay')->name('panel.comission.payment.unpay');
        Route::post('comissions/payments/file', 'ComissionPaymentController@storeFile')->name('panel.comission.payment.file');

        Route::get('/seguimiento-pagos', 'VentaController@pagos')->name('panel.pagos');
        //Ventas - Adicionales
        Route::prefix('ventas/{venta}')->group(function(){
            Route::post('revoke/step', 'VentaController@revoke')->name('panel.ventas.revoke');
            Route::post('review/step', 'VentaController@review')->name('panel.ventas.review');
            Route::resource('documentos', 'VentaDocumentationController')->names([
                'store' => 'panel.ventas.documentation.store',
                'update' => 'panel.ventas.documentation.update',
            ]);
            Route::resource('recibos', 'VentaReciboController')->names([
                'store' => 'panel.ventas.recibos.store',
                'update' => 'panel.ventas.recibos.update',
                'destroy' => 'panel.ventas.recibos.destroy'
            ]);
            Route::put('recibos/authorize/{recibo}', 'VentaReciboController@my_authorize')->name('panel.ventas.recibos.authorize');
        });

        Route::get('/facturar', 'FacturacionController@createCFDI')->name('panel.facturacion.store');
        Route::get('/descargar/factura/{id}', 'FacturacionController@downloadCFDI')->name('panel.facturacion.download');
        Route::get('/facturas', 'FacturacionController@getCFDIS')->name('panel.facturacion.get');
        Route::get('/factura/{id}', 'FacturacionController@findCFDI')->name('panel.facturacion.find');
        Route::get('/cancelar-factura', 'FacturacionController@cancelCFDI')->name('panel.facturacion.delete');
        Route::get('/enviar-factura', 'FacturacionController@sendCFDI')->name('panel.facturacion.send');

        Route::post('/configuration/maintenance', 'ConfigurationController@maintenance')->name('configuration.maintenance');

        Route::get('/get/csd/{rfc}', 'CsdController@getCSD')->name('panel.csd');
        Route::get('/get/csd', 'CsdController@getCSDS')->name('panel.csd');
        //Route::get('/get/csd/{rfc}', 'CsdController@deleteCSD')->name('panel.csd');
        //Route::get('/csd', 'CsdController@createCSD')->name('panel.csd');


        //Prospectos
        Route::resource('prospectos', 'ProspectController')->names([
            'index' => 'panel.prospects.index',
            'create' => 'panel.prospects.create',
            'store' => 'panel.prospects.store',
            'edit' => 'panel.prospects.edit',
            'update' => 'panel.prospects.update',
            'destroy' => 'panel.prospects.destroy'
        ]);
        Route::post('prospectos/reassign', 'ProspectController@reassign')->name('panel.prospects.reassign');
        Route::post('prospectos/client', 'ProspectController@client')->name('panel.prospects.client');
        Route::post('prospectos/recover', 'ProspectController@recover')->name('panel.prospects.recover');
        Route::post('prospectos/reserve', 'ProspectController@reserve')->name('panel.prospects.reserve');
        Route::post('prospectos/reserve/client', 'ProspectController@reserveClient')->name('panel.prospects.reserve.client');
        Route::post('prospectos/get', 'ProspectController@getData')->name('panel.prospects.getData');

         //Ejecutivos
         Route::resource('ejecutives', 'Ejecutive_Controller')->names([
            'index' => 'panel.ejecutives.index',
            'create' => 'panel.ejecutives.create',
            'store' => 'panel.ejecutives.store',
            'edit' => 'panel.ejecutives.edit',
            'update' => 'panel.ejecutives.update',
            'destroy' => 'panel.ejecutives.destroy'
        ]);
        Route::post('ejecutives/reassign', 'Ejecutive_Controller@reassign')->name('panel.ejecutives.reassign');
        Route::post('ejecutives/client', 'Ejecutive_Controller@client')->name('panel.ejecutives.client');
        Route::post('ejecutives/recover', 'Ejecutive_Controller@recover')->name('panel.ejecutives.recover');
        Route::post('ejecutives/reserve', 'Ejecutive_Controller@reserve')->name('panel.ejecutives.reserve');
        Route::post('ejecutives/reserve/client', 'Ejecutive_Controller@reserveClient')->name('panel.ejecutives.reserve.client');

        //Actividades
        Route::resource('actividades', 'TaskController')->names([
            'index' => 'panel.tasks.index',
            'create' => 'panel.tasks.create',
            'store' => 'panel.tasks.store',
            'edit' => 'panel.tasks.edit',
            'update' => 'panel.tasks.update',
            'destroy' => 'panel.tasks.destroy'
        ]);
        Route::post('actividades/prospect', 'TaskProspectController@store')->name('panel.tasks.create.from.prospect');
        Route::post('actividades/prospect/complete', 'TaskProspectController@update')->name('panel.tasks.create.from.prospect.update');
        Route::delete('/actividades/prospect/{id}', 'TaskProspectController@destroy')->name('panel.tasks.create.from.prospect.destroy');

        Route::post('prospectos/import', 'ProspectController@import')->name('panel.prospects.import');
        Route::get('export/prospectos', 'ProspectController@export')->name('panel.prospects.export');

        //Caja Chica
        Route::resource('pagos', 'PaymentController')->names([
            'index' => 'panel.payments.index',
            'create' => 'panel.payments.create',
            'store' => 'panel.payments.store',
            'edit' => 'panel.payments.edit',
            'update' => 'panel.payments.update',
            'destroy' => 'panel.payments.destroy'
        ]);

        //Notas prospectos
        Route::resource('notas', 'NoteController')->names([
            'index' => 'panel.notes.index',
            'create' => 'panel.notes.create',
            'store' => 'panel.notes.store',
            'edit' => 'panel.notes.edit',
            'update' => 'panel.notes.update',
            'destroy' => 'panel.notes.destroy'
        ]);
        // Route::put('ventas/')

        
    });
});

Auth::routes();